<?php
/**
 * Template part for displaying a message that posts cannot be found.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package camanabay
 */

?>

NO MATCHES
			</div>
		
<div class="cb_no_search_matches"><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'camanabay' ); ?></p>
			
			<?php //get_search_form(); ?>
			
			<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label' ) ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search �', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
	</label>
	<input type="submit" class="search-submit"  value="<?php echo esc_attr_x( '', 'submit button' ) ?>" />
</form>
			
			
			
			</div>