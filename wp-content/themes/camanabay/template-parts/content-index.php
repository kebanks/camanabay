<?php
/**
 * Template part for displaying posts from the Camana bay blog
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package camanabay
 */

?>


    
			
		<?php
	
			
			specialized_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'camanabay' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
			
			
		?>

		
		
	

