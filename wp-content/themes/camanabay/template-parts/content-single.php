<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package camanabay
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="cb-single-blog">
		<div class="entry-content">
            <div  class="customized-single-sidebar-title">
    			<div class="cb-blog-archive-day cb-blog-single-day">
    			             <?php echo the_time('l') ; ?>
    			</div>
    			<div class="cb-blog-archive-date cb-blog-single-date">
    		              <?php echo the_time('d F Y'); ?>
    			</div>
    		</div>
    		<div class="cb-single-blog-title">
    			<?php the_title( ) ?>
    		</div>

		<?php the_content(); ?>

		</div>
		<?php
		$orig_post = $post;
		$tags = wp_get_post_tags($post->ID);
		if ($tags) {

?>	<div  class="customized-sidebar-title">
		Related Stories

	</div>
 <?php

            $tag_ids = array();
        foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
            $args=array(
                'tag__in' => $tag_ids,
                'post__not_in' => array($post->ID),
                'posts_per_page'=>3 // Number of related posts to display.

            );

		$my_query = new WP_Query($args);
		if( $my_query->have_posts() ) {
		while ($my_query->have_posts()) : $my_query->the_post();
			specialized_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'camanabay' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		endwhile;
		}
		wp_reset_query();
	}

?>

	</div><!-- .entry-content -->


</article><!-- #post-## -->
