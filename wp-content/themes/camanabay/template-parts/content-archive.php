<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package camanabay
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	

	<!--<div class="entry-content">-->
	
		<?php
			//the_content( sprintf(
				/* translators: %s: Name of current post. */
			/*	wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'camanabay' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );*/
			echo do_shortcode('[pt_view id="96dc193d7a"]');
			
		?>

	<!--	<div class="cb-blog-archive-container">
		<div class="cb-blog-archive-date-time-br">
			<div class="cb-blog-archive-time">
				<?php echo the_time('g:i a') ; ?>
			
			</div>
			<div class="cb-blog-archive-day">
				<?php echo the_time('l') ; ?>
			
			</div>
			<div class="cb-blog-archive-date">
				<?php echo the_time('d F Y'); ?>
			
			</div>
		</div>
		</div> -->
<!--	</div><!-- .entry-content -->

	
</article><!-- #post-## -->
