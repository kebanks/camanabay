<?php
/**
 * Template part for displaying posts from the Camana bay blog
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package camanabay
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php camanabay_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->
	<?php 
 
	$slider_id = get_post_meta( get_the_ID(), "meta-box-dropdown", true);
	
    echo do_shortcode("[metaslider id=" . $slider_id . "]"); 
?>
    <div class='cb_blog_outside'>
	
	<div class="entry-content">
		<div class='cb_blog_left'>
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'camanabay' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'camanabay' ),
				'after'  => '</div>',
			) );
		?>
		</div>
		<div class="cb_blog_right">
		
		
		<div class="cb_blog_right-inner">
		<?php get_sidebar(); ?>
		</div>
		</div>
	</div><!-- .entry-content -->
	</div>
	
</article><!-- #post-## -->
