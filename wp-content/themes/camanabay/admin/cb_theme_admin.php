<?php

class cb_theme_admin{

	private $cb_admin_options = array();
	public function __construct()

    {
		
		add_action('admin_menu', array($this, 'ic_cb_add_menu'));
		add_action( 'admin_init', array( $this, 'ic_cb_basic_page_init' ) );

	}
	function ic_cb_add_menu() {
		add_menu_page( 'Camana Bay', 'Camana Bay Options', 'manage_options', 'ic_cb_options', array($this, 'ic_build_epanel'),"", 61);


	}
	function ic_cb_basic_page_init()
	{
		       register_setting(

            'ic_cb_admin_group1', // Option group

            'ic_cb_admin_settings', // Option name

            array( $this, 'sanitize' ) // Sanitize

        );
		
		
		
		 add_settings_section(

            'hardware_type', // ID

           __('<h2>Hardware Type</h2>','ic-cb-text-domain' ) , // Title

            array( $this, 'print_section_info' ), // Callback

            'ic_cb_settings_main' // Page

        );  
		
		
		 add_settings_section(

            'front_pages', // ID

           __('<h2>Front Pages</h2>','ic-cb-text-domain' ) , // Title

            array( $this, 'print_section_info' ), // Callback

            'ic_cb_settings_main' // Page

        );  
		
	}
	function ic_build_epanel()
	{	
	        $this->cb_admin_options = get_option( 'ic_cb_admin_settings' );
			if ($this->cb_admin_options == NULL)
			    $this->cb_admin_options['hardwareType'] = 'COMPUTER';


		?>
		    <div class="wrap">

            <?php screen_icon(); ?>

            <h2><?php _e('Camana Bay Theme Setup','camana-bay-theme' );?> </h2>         

            <form id="ic_cb_camanabay_admin" name="ic_cb_camanabay_admin" method="post" action="options.php">
			<?php 
				settings_fields( 'ic_cb_admin_group1' );
				do_settings_sections( 'ic_cb_settings_main' );
			submit_button(); 

			

			?>

			
			</form>
			
		<?php
	}
	    public function sanitize( $input )

    {
	
		$variableName=$_POST['camana_pages_local'];
		update_option( 'camana_pages_local', $variableName ,NULL); 
		
		$variableName=$_POST['camana_pages_visitor'];
		
		update_option( 'camana_pages_visitor', $variableName ,NULL); 
		createMenu();
			
	
	}
	function print_section_info()
	{
	
		add_settings_field(

            'ic_cb_hardware_type', // ID

            __('Hardware Type:','ic-cb-text-domain' ) , // Title 

            array( $this, 'hardware_type_cb' ), // Callback

            'ic_cb_settings_main', // Page

            'hardware_type' // Section           

        );  
		add_settings_field(

            'ic_cb_front_page_local', // ID

            __('Front Page Local:','ic-cb-text-domain' ) , // Title 

            array( $this, 'front_page_local_cb' ), // Callback

            'ic_cb_settings_main', // Page

            'front_pages' // Section           

        );  
		
		add_settings_field(

            'ic_cb_front_page_visitor', // ID

            __('Front Page Visitor:','ic-cb-text-domain' ) , // Title 

            array( $this, 'front_page_visitor_cb' ), // Callback

            'ic_cb_settings_main', // Page

            'front_pages' // Section           

        );  
	}
function front_page_visitor_cb()
	{

	$pages = get_pages(); 
	$link = get_option('camana_pages_visitor');
?> 
			<select name="camana_pages_visitor">
<?php
	
		foreach ( $pages as $page ) {
			if (get_page_link( $page->ID ) == $link)
				$option = '<option   selected="selected"  value="' . get_page_link( $page->ID ) . '">';
			else
				$option = '<option value="' . get_page_link( $page->ID ) . '">';
			$option .= $page->post_title;
			$option .= '</option>';
			echo $option;
		}
?>     </select>
			<br><br>


<?php
		
	
	}
	function front_page_local_cb()
	{	
		
	$pages = get_pages(); 
	$link = get_option('camana_pages_local');
?> 
			<select name="camana_pages_local">
<?php
	
		foreach ( $pages as $page ) {
			if (get_page_link( $page->ID ) == $link)
				$option = '<option   selected="selected"  value="' . get_page_link( $page->ID ) . '">';
			else
				$option = '<option value="' . get_page_link( $page->ID ) . '">';
			$option .= $page->post_title;
			$option .= '</option>';
			echo $option;
		}
?>     </select>
			<br><br>


<?php
		
	
	}
	function hardware_type_cb()
	{

			?>

			
		<input type="radio" value="COMPUTER" name="hardwareType" id="hardwareType" title="Hardware Type" <?php echo  ($this->cb_admin_options['hardwareType'] == 'COMPUTER'?'checked': ""); ?>><?php _e('Computer/Phone/iPad','ic-cb-text-domain' );?>
  
		<input type="radio" name="Kiosk" value="KIOSK" <?php echo  ($this->cb_admin_options['hardwareType'] == 'KIOSK'?'checked': ""); ?>><?php _e('Kiosk','ic-cb-text-domain' );?>
	<br />		
		
	
	<?php
	}

}


?>