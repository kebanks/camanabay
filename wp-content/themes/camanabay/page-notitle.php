<?php
/**
 * The template for displaying a regular page with no title.
 *
 * Template Name: Notitle Page
 * This is the template that displays the a regular page but no title
 * 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package camanabay
 */


get_header(); 


?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content-notitle', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
		<?php

 get_footer(); ?>
