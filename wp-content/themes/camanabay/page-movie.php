<?php
/**
 * The template for displaying all pages.
 *
 * Template Name: Movie  page
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package camanabay
 */

get_header(); 


?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'template-parts/content', 'movie' ); ?>

				

			<?php endwhile; // End of the loop. ?>

		</main><!-- #main -->
		<?php

 get_footer('movie'); ?>
