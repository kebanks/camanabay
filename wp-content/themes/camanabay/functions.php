<?php
/**
 * camanabay functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package camanabay
 */
global $wtnerd;
if ( ! function_exists( 'camanabay_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function camanabay_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on camanabay, use a find and replace
	 * to change 'camanabay' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'camanabay', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'camanabay' ),
		'local-visitor' => esc_html__( 'Local Visitor', 'camanabay' ),	
		'local-visitor-bottom' => esc_html__( 'Local Visitor Bottom', 'camanabay' ),
		'top-right' => esc_html__( 'Top Right', 'camanabay' ),
	) );

	
	
		
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'camanabay_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
	
	
	// Set up our admin menu 
	ic_cb_admin_menu_setup();
	
	
	
	
	

}
endif; // camanabay_setup
add_action( 'after_setup_theme', 'camanabay_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function camanabay_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'camanabay_content_width', 640 );
}
add_action( 'after_setup_theme', 'camanabay_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function camanabay_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'camanabay' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget  %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Search Page Sidebar', 'camanabay' ),
		'id'            => 'sidebar-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget  %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
/* footer area widgets */

	register_sidebar( array(
		'name' => __( 'Footer Area', 'camanabay' ) . ' #1',
		'id' => 'footer-sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget  %2$s">',
		'after_widget' => '</aside> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area', 'camanabay' ) . ' #2',
		'id' => 'footer-sidebar-2',
		'before_widget' => '<aside id="%1$s" class="widget  %2$s">',
		'after_widget' => '</aside> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area', 'camanabay' ) . ' #3',
		'id' => 'footer-sidebar-3',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Footer Area', 'camanabay' ) . ' #4',
		'id' => 'footer-sidebar-4',
		'before_widget' => '<aside id="%1$s" class="widget  %2$s">',
		'after_widget' => '</aside> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Area', 'camanabay' ) . ' #5',
		'id' => 'footer-sidebar-5',
		'before_widget' => '<aside id="%1$s" class="widget   %2$s">',
		'after_widget' => '</aside> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Area', 'camanabay' ) . ' #6',
		'id' => 'footer-sidebar-6',
		'before_widget' => '<aside id="%1$s" class="widget  %2$s">',
		'after_widget' => '</aside> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );
	
	
	


}
add_action( 'widgets_init', 'camanabay_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function camanabay_scripts() {
		wp_enqueue_style( 'camanabay-style', get_template_directory_uri() . '/style.min.css' );

	wp_enqueue_script( 'camanabay-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	wp_enqueue_script( 'jquery' );
	 
	 /* register the jquery script that sets and reads the cookie */
	 wp_register_script( 'set_cb_visitor_type', get_template_directory_uri() . '/js/js-cookie-master/src/js.cookie.js');
	 wp_enqueue_script( 'set_cb_visitor_type', get_template_directory_uri() .  '/js/js-cookie-master/src/js.cookie.js',array( 'jquery' ),"");
	

	
	 
	
	 
	 //  The cookie script - sets the cookie and handles the appears of the local/visitor menu and the top menus
	 
	 wp_register_script( 'ic-cb-cookie-js', get_template_directory_uri() . '/js/ic-cb-cookie.js');
	wp_enqueue_script( 'ic-cb-cookie-js', get_template_directory_uri() .  '/js/ic-cb-cookie.js',array( 'jquery' ),"");
	
	  
	/* scripts and jquery for the splash screen */
   
		wp_register_style(
			'splash-header', // handle name
			get_template_directory_uri() . '/css/splash.css', // the URL of the stylesheet
						"",
			null,null
		);
		wp_enqueue_style( 'splash-header' );
		
		wp_enqueue_script( 'cb_splash_js', get_template_directory_uri() . '/js/splash.js', array( 'jquery' ));
		wp_enqueue_script('cb_splash_js');
		$file = get_cb_splash_images();
		
	wp_localize_script( 'cb_splash_js', 'cb_splash_ajax',
				array( 'ajax_url' => admin_url( 'admin-ajax.php' ),'cb_splash_file' => $file));
		
		

	// Adds JavaScript for handling the navigation menu hide-and-show behavior.
	wp_enqueue_script( 'ic-cb-navigation', get_template_directory_uri() . '/js/navigation.js', array( 'jquery' ), '20140711', true );
	
	// jQuery library code
	wp_register_style('jquery-style',"http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css","","");
	wp_enqueue_style( 'jquery-style' );
	wp_register_script('jquery-style-code',"http://code.jquery.com/ui/1.11.4/jquery-ui.js");
	wp_enqueue_script ('jquery-style-code',"http://code.jquery.com/ui/1.11.4/jquery-ui.js",array( 'jquery' ));
	wp_register_script('jquery-style-customizer',get_template_directory_uri() . '/js/cb_customizer_gen.js');
	wp_enqueue_script ('jquery-style-customizer',get_template_directory_uri() . '/js/cb_customizer_gen.js',array( 'jquery' ));
	wp_register_style('cb-toggle', get_template_directory_uri() ."/css/cb_toggle.css","","");
	wp_enqueue_style( 'cb-toggle' );

 wp_register_script( 'cb-timeago-js', get_template_directory_uri() . '/js/jquery.timeago.js');
	wp_enqueue_script( 'cb-timeago-js', get_template_directory_uri() .  '/js/jquery.timeago.js',array( 'jquery' ),"");
	wp_register_script( 'cb-matchHeight-js', get_template_directory_uri() . '/js/matchHeight.js');
	wp_enqueue_script( 'cb-matchHeight-js', get_template_directory_uri() .  '/js/matchHeight.js',array( 'jquery' ),"");
	
	
	
	
}
add_action( 'wp_enqueue_scripts', 'camanabay_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


/* load the admin screen for the template */
require get_template_directory(). '/admin/cb_theme_admin.php';
function ic_cb_admin_menu_setup()
{
     new cb_theme_admin();

}
/*************************************************************************************
*	get_cb_visitor_type
*
*    returns whether a user is a visitor or resident
*
*
************************************************************************************/

function get_cb_visitor_type()
{
	if(isset($_COOKIE['visitor-type'])) {
			$visitor_type = $_COOKIE['visitor-type'];

	} else 
		$visitor_type = 'VISITOR';
	
	return $visitor_type;
}
// determine if a visitor type is set at all, if not, return 0
function splash_get_cb_visitor_type()
{
	
	if(isset($_COOKIE['visitor-type'])) {
			$visitor_type = $_COOKIE['visitor-type'];

	} else 
		$visitor_type = 'NOTSET';
	
	return $visitor_type;
}
function get_front_page_link()
{
	
	$visitor_url = get_option('camana_pages_visitor');
	$local_url = get_option ('camana_pages_local');
		
	$s = get_cb_visitor_type();
	if ($s == 'RESIDENT')
		$link = $local_url;
	else 
		$link = $visitor_url;
				 
	return $link;
				 
 }




/* determines if this is the camanabay front page (not the splash page) */
function is_cb_front_page()
{
	global $post;
	$permalink = get_permalink($post->ID);
	$rt = FALSE;
	$visitor_url = get_option('camana_pages_visitor');
	$local_url = get_option ('camana_pages_local');
	
	if (($visitor_url == $permalink) || $local_url == $permalink)
		$rt = TRUE;
	
	return $rt;
}



/**************************************************************************************
*	crunchify_social_sharing_buttons
*
*	manages the sharing buttons at the top right
*
*
****************************************************************************************/

function crunchify_social_sharing_buttons() {

		global $post;

		// Get current page URL 
		$crunchifyURL = get_permalink();
 
		// Get current page title
		$crunchifyTitle = str_replace( ' ', '%20', get_the_title());
		
		// Get Post Thumbnail for pinterest
		$crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
 
		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL;
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL;
		$googleURL = 'https://plus.google.com/share?url='.$crunchifyURL;
		$bufferURL = 'https://bufferapp.com/add?url='.$crunchifyURL.'&amp;text='.$crunchifyTitle;
		
		// Based on popular demand added Pinterest too
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$crunchifyTitle;
		
		$fb_icon_dir = get_template_directory_uri() . "/images/icon_fb-blue.png" ;
		$tw_icon_dir = get_template_directory_uri() . "/images/icon_twitter_blue.png";
		
		// Add sharing button at the end of page/page content
		$variable = '<div class="crunchify-social">';
		$variable .= '<a  href="'.$facebookURL.'" target="_blank" ><img class = "crunchify-facebook" src='.$fb_icon_dir .' /></a>';
		$variable .= '<a href="'. $twitterURL .'" target="_blank"><img class = "crunchify-twitter" src='.$tw_icon_dir . ' /></a>';
		
		$variable .= '</div>';

		return $variable;
	
};



function metaslider_nivo_js($javascript, $slider_id) {


    $javascript .= "var s = jQuery('.nivo-caption').text();
	  var y = s.length;
	  var w = y/3 +'em';
	  jQuery('.nivo-caption').width(w);

	 ";
 
    return $javascript;
 
}
add_filter('metaslider_nivo_slider_javascript', 'metaslider_nivo_js', 10, 2);


function createMenu()
{
	
	$visitor_url = get_option('camana_pages_visitor');
	$local_url = get_option ('camana_pages_local');
	
	
	$menu_name = 'local-visitor-bottom';
	$menu_exists = wp_get_nav_menu_object( $menu_name );
				
// If it doesn't exist, let's create it.
	if ($menu_exists)
			wp_delete_nav_menu($menu_name);	

		$menu_id = wp_create_nav_menu($menu_name);
	

	// Set up default menu items

			wp_update_nav_menu_item($menu_id, 0, array(
				'menu-item-title' =>  __('Visitor'),
				'menu-item-url' => $visitor_url, 
				'menu-item-classes' => 'visitor-only-bottom',
				'menu-item-status' => 'publish'));
				
				wp_update_nav_menu_item($menu_id, 0, array(
				'menu-item-title' =>  __('Local'),
				'menu-item-classes' => 'local-only-bottom',
				'menu-item-url' => $local_url, 
				'menu-item-status' => 'publish'));

		


		
		// Check if the menu exists
				$menu_name = 'local-visitor';
				$menu_exists = wp_get_nav_menu_object( $menu_name );
				if ($menu_exists)
					wp_delete_nav_menu($menu_name);		
// If it doesn't exist, let's create it.

		
				$menu_id = wp_create_nav_menu($menu_name);
		
	
	// Set up default menu items
			wp_update_nav_menu_item($menu_id, 0, array(
				'menu-item-title' =>  __('Local'),
				'menu-item-classes' => 'local-only-top',
				'menu-item-url' => $local_url, 
				'menu-item-status' => 'publish'));

			wp_update_nav_menu_item($menu_id, 0, array(
				'menu-item-title' =>  __('Visitor'),
				'menu-item-url' => $visitor_url, 
				'menu-item-classes' => 'visitor-only-top',
				'menu-item-status' => 'publish'));

		

	$active_widgets = get_option( 'sidebars_widgets' );
				
		if( !has_nav_menu( 'local-visitor' ) ){
			$locations = get_theme_mod('nav_menu_locations');
			$locations['local-visitor'] = $menu_id;
				set_theme_mod( 'nav_menu_locations', $locations );
			}

}
/*
 * Function creates post duplicate as a draft and redirects then to the edit post screen
 */
function rd_duplicate_post_as_draft(){
	global $wpdb;
	if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
		wp_die('No post to duplicate has been supplied!');
	}
 
	/*
	 * get the original post id
	 */
	$post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
	/*
	 * and all the original post data then
	 */
	$post = get_post( $post_id );
 
	/*
	 * if you don't want current user to be the new post author,
	 * then change next couple of lines to this: $new_post_author = $post->post_author;
	 */
	$current_user = wp_get_current_user();
	$new_post_author = $current_user->ID;
 
	/*
	 * if post data exists, create the post duplicate
	 */
	if (isset( $post ) && $post != null) {
 
		/*
		 * new post data array
		 */
		$args = array(
			'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $new_post_author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => 'draft',
			'post_title'     => $post->post_title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
		);
 
		/*
		 * insert the post by wp_insert_post() function
		 */
		$new_post_id = wp_insert_post( $args );
 
		/*
		 * get all current post terms ad set them to the new post draft
		 */
		$taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
		foreach ($taxonomies as $taxonomy) {
			$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
			wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
		}
 
		/*
		 * duplicate all post meta just in two SQL queries
		 */
		$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
		if (count($post_meta_infos)!=0) {
			$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
			foreach ($post_meta_infos as $meta_info) {
				$meta_key = $meta_info->meta_key;
				$meta_value = addslashes($meta_info->meta_value);
				$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
			}
			$sql_query.= implode(" UNION ALL ", $sql_query_sel);
			$wpdb->query($sql_query);
		}
 
 
		/*
		 * finally, redirect to the edit post screen for the new draft
		 */
		wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
		exit;
	} else {
		wp_die('Post creation failed, could not find original post: ' . $post_id);
	}
}
add_action( 'admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft' );
 
/*
 * Add the duplicate link to action list for post_row_actions
 */
function rd_duplicate_post_link( $actions, $post ) {
	if (current_user_can('edit_posts')) {
		$actions['duplicate'] = '<a href="admin.php?action=rd_duplicate_post_as_draft&amp;post=' . $post->ID . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
	}
	return $actions;
}
 

add_filter('page_row_actions', 'rd_duplicate_post_link', 10, 2);



function specialized_content($first,$search_page = FALSE)
{

	
	 $post = get_post();

?><div class="cb-blog-item">
			<div class="cb-blog-item-inside">
<?php
	 if ( $search_page == FALSE) 
	{
?>
	
			
				<div class="cb-blog-archive-date-time-br">
					
					<div class="cb-blog-archive-day">
						<?php echo the_time('l') ; ?>
			
					</div>
					<div class="cb-blog-archive-date">
						<?php echo the_time('d F Y'); ?>
			
					</div>
				</div>
			
			
			
	
		
		
	
<?php
		}
?>
	<div class="cb-blog-image-item">
<?php
	$link = get_post_permalink( $post->ID);
		
		$first_img = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		$img = '<a href=" ' . $link . '" ><img src="'.$first_img.'" /></a>';
		
		
         echo  $img;

?>
	</div>
	
	

		<div class="cb-blog-archive-title">
		<a href="<?php echo $link ?>"> <?php echo $post->post_title ?></a>	
	
		</div>
	
	</div>

	</div>
<?php
		

}
add_filter('get_the_archive_title', function ($title) {


    return preg_replace('/^\w+: /', '', $title);
});

/************************************************************************************
	Gets the file names for the splash images in the splash directory



*********************************************************************************/
function get_cb_splash_images()
{
	$folder =  dirname(__FILE__) . "/images/rotate-images/" ;
	$s =   get_template_directory_uri() . "/images/rotate-images/" ;


	

// Space seperated list of extensions, you probably won't have to change this.
	$exts = 'jpg jpeg png gif';

	$files = array(); $i = -1; // Initialize some variables
	if ('' == $folder) $folder =  dirname(__FILE__) . "/images/rotate-images/";

	$handle = @opendir($folder) or die("Cannot open the file $folder");
	$exts = explode(' ', $exts);
	while (false !== ($file = readdir($handle))) {
		foreach($exts as $ext) { // for each extension check the extension
			if (preg_match('/\.'.$ext.'$/i', $file, $test)) { // faster than ereg, case insensitive
				$files[] = $file; // it's good
				++$i;
			}
		}		
	}

	closedir($handle); // We're not using it anymore
	mt_srand((double)microtime()*1000000); // seed for PHP < 4.2
	$rand = mt_rand(0, $i); // $i was incremented as we went along
	
	return $s.$files[$rand];
}
function cb_custom_meta_box_markup($object)
{
    wp_nonce_field(basename(__FILE__), "meta-box-nonce");
	$option_values = cb_all_meta_sliders();

    ?>
        <div>
            
            <label for="meta-box-dropdown">Choose a Slider</label>
            <select name="meta-box-dropdown">
                <?php 
                   

                    foreach($option_values as $key => $value) 
                    {
                        if($value['id'] == get_post_meta($object->ID, "meta-box-dropdown", true))
                        {
                            ?>
                                <option value="<?php echo $value['id']; ?>"	selected="selected"><?php echo $value['title']; ?></option>
                            <?php    
                        }
                        else
                        {
                            ?>
                                <option value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
                            <?php
                        }
                    }
                ?>
            </select>

           
       
        </div>
    <?php  
}

function cb_add_custom_meta_box()
{
	global $post;

	$template_file = get_post_meta( $post->ID, '_wp_page_template', true );

     
		add_meta_box("cb-blog-meta-box", "Select Slider", "cb_custom_meta_box_markup", "page", "side", "high", null);
 
}

 function cb_all_meta_sliders( $sort_key = 'date' ) {

        $sliders = array();

        // list the tabs
        $args = array(
            'post_type' => 'ml-slider',
            'post_status' => 'publish',
            'orderby' => $sort_key,
            'suppress_filters' => 1, // wpml, ignore language filter
            'order' => 'ASC',
            'posts_per_page' => -1
        );

        $args = apply_filters( 'metaslider_all_meta_sliders_args', $args );

        // WP_Query causes issues with other plugins using admin_footer to insert scripts
        // use get_posts instead
        $all_sliders = get_posts( $args );

        foreach( $all_sliders as $slideshow ) {

         //   $active = $this->slider && ( $this->slider->id == $slideshow->ID ) ? true : false;

            $sliders[] = array(
                'active' => TRUE,
                'title' => $slideshow->post_title,
                'id' => $slideshow->ID
            );

        }

        return $sliders;

    }



add_action("add_meta_boxes", "cb_add_custom_meta_box",10, 0);




add_filter( 'hidden_meta_boxes', function( $hidden, $screen, $use_defaults )
{
    global $wp_meta_boxes;
    $cpt = 'page'; // Modify this to your needs!

    if( $cpt === $screen->id && isset( $wp_meta_boxes[$cpt] ) )
    {
        $tmp = array();
        foreach( (array) $wp_meta_boxes[$cpt] as $context_key => $context_item )
        {
            foreach( $context_item as $priority_key => $priority_item )
            {
                foreach( $priority_item as $metabox_key => $metabox_item )
                    $tmp[] = 'cb-blog-meta-box';
            }
        }
        $hidden = $tmp;  // Override the current user option here.
    }
    return $hidden;
}, 10, 3 );















function cb_save_custom_meta_box($post_id, $post, $update)
{
    if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
        return $post_id;

    if(!current_user_can("edit_post", $post_id))
        return $post_id;

    if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;

    $slug = "page";
    if($slug != $post->post_type)
        return $post_id;

   
    $meta_box_dropdown_value = "";
   

    

    if(isset($_POST["meta-box-dropdown"]))
    {
        $meta_box_dropdown_value = $_POST["meta-box-dropdown"];
    }   

    update_post_meta($post_id, "meta-box-dropdown", $meta_box_dropdown_value);

    
}

add_action("save_post", "cb_save_custom_meta_box", 10, 3);



function load_cb_custom_wp_admin_style() {
        wp_register_script( 'cb_admin_blog_style', get_template_directory_uri() . '/js/cb_template_change.js');
	 wp_enqueue_script( 'cb_admin_blog_style', get_template_directory_uri() .  '/js/cb_template_change.js',array( 'jquery' ),"");
}
add_action( 'admin_enqueue_scripts', 'load_cb_custom_wp_admin_style' );
/*
function cb_dynamic_menu_items( $menu_items ) {


	global $post;

		// Get current page URL 
		$crunchifyURL = get_permalink();
 
		// Get current page title
		$crunchifyTitle = str_replace( ' ', '%20', get_the_title());
		
		// Get Post Thumbnail for pinterest
		$crunchifyThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
 
		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$crunchifyTitle.'&amp;url='.$crunchifyURL;
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$crunchifyURL ;
		$googleURL = 'https://plus.google.com/share?url='.$crunchifyURL;
		$bufferURL = 'https://bufferapp.com/add?url='.$crunchifyURL.'&amp;text='.$crunchifyTitle;
		
		// Based on popular demand added Pinterest too
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$crunchifyURL.'&amp;media='.$crunchifyThumbnail[0].'&amp;description='.$crunchifyTitle;
	


    foreach ( $menu_items as $menu_item ) {

		switch($menu_item->url){
		case'#fb_ss#':

			$menu_item->url = $facebookURL;
		   break;
		   case'#tw_ss#':
				$menu_item->url = $twitterURL;
		   break;
		 
		}
       
    }

    return $menu_items;
}
add_filter( 'wp_nav_menu_objects', 'cb_dynamic_menu_items' );
*/

function nav_replace_wpse_189788($item_output, $item) {
 
  if ('Search' == $item->title) {
    
	$classes = "";
	foreach($item->classes as $i)
	{
		$classes .= $i . " ";
		
	
	}
	
	 ob_start();
	 

get_search_form();

	 
       // get_search_form();
        $searchform = ob_get_contents();
        ob_end_clean();
		
		
		$display_string = '<li class="' . $classes .'">' . $searchform . '</li>';
      return $display_string;
 
  }
  return $item_output;
}
add_filter('walker_nav_menu_start_el','nav_replace_wpse_189788',10,2);
function photonic_default_lightbox_text($text){


error_log('photonic text is ' . $text);
if ($text === "View")
	$text = "   ";

return $text;
}
add_filter('photonic_default_lightbox_text', 'photonic_default_lightbox_text', 10, 2);
?>