<?php
/**
 * The template for displaying splash pages.
 *
 * Template Name: Splash page
 * This is the template that displays the splash page
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package camanabay
 */

$cookie = splash_get_cb_visitor_type();
$z =  get_template_directory_uri() . "/images/top-blue-button.png" ;
	$w =  get_template_directory_uri() . "/images/logo_white.png" ;
	$xx=  get_template_directory_uri() . "/images/go.png";
	$visitor_url = get_option('camana_pages_visitor');
	$local_url = get_option ('camana_pages_local');


if ($cookie != 'NOTSET')
{

	if ($cookie == 'RESIDENT'){
		//header($local_url);
		$s = 'Location: ' .$local_url;
		
	}
	else 
	   if ($cookie == 'VISITOR'){
	   $s = 'Location: ' .$visitor_url;
	    
		}
	 header($s);  	   
	die();
}

get_header('splash'); ?>



<!--<img class="splash" src="<?php echo $s.$files[$rand]; ?>" /> -->
<img class="splash"  />
<div id='splash-content'>
	<div id='innersplash'>

	



	<div id="splash-hdr-img">
		<img src="wp-content/themes/camanabay/images/logo_white.png" alt="logo" />

	</div>
	<div class="top_blue_button">
	  <p class="splash_blue_button_text">
		Welcome to Camana Bay, a lively waterfront town on Grand Cayman.  It all happens here, from retail therapy to culinary adventures to amazingly good, good times.
		</p>
	</div>
	<span id="splash-locations">

	<div class="splash_button_area">

		<h3 class="splash_button_header">Camana Bay <br /> Local</h3>
		<div><p class="splash_button_text">I know my way <br  /> around. Let&#39;s go!</p>
		   <a id='splash-i-live-here' href='<?php echo $local_url ?>'><img class="splash_go_button" src='<?php echo $xx ?>'></a></div>
	</div>

<div class="splash_button_area">

		<h3 class="splash_button_header">Camana Bay <br /> Visitor</h3>
		 <div><p class="splash_button_text">I'm new and ready <br  /> to explore!</p>
	    <a id='splash-i-am-visiting' href='<?php echo $visitor_url ?>'><img class="splash_go_button" src='<?php echo $xx ?>'></a></div>
	</div>

		</span>
	</div> <!--innersplash -->
	</div> <!--splash content -->
		</div><!-- #content -->


</div><!-- #page -->
