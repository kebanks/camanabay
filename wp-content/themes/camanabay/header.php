<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package camanabay
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


<?php

wp_head(); ?>
</head>
<div id="container">
<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'camanabay' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<div class="site-branding">


		<nav id="site-navigation" class="main-navigation" role="navigation">

		<?php


		wp_nav_menu( array( 'theme_location' => 'primary', 'menu' => 'Resident' ) );
		$front_page_link = get_front_page_link();

?>
		</nav><!-- #site-navigation -->

		<div id='ic-cb-logo-container'>
		   <div id='ic-cb-logo-row'>
				<div id='ic-cb-logo-cell' class='ic-cb-logo-cell'>
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"></button>


<?php



?>
<span class='cb-breadcrumbs'><b>MENU</b></span>

<?php




?>
				</div>
			<!-- Show the logo -->
				<div class='ic-cb-logo-cell'>
					<a href='<?php echo $front_page_link ?>'><img id='cb-header-img' src="<?php echo( get_header_image() ); ?>" alt="<?php echo( get_bloginfo( 'title' ) ); ?>" />
					</a>
				</div>
				<div class='ic-cb-logo-cell'>

					<div id="top-right-popup-container">
                        <div class="containerHeaderSearch">
                            <form role="search" method="get" class="search-form1" action="http://camanabay.com/">
                            	<label>
                            		<span class="screen-reader-text"> Search for:</span>
                            		 <span class="fa fa-search"></span>
                            		<input type="search" class="search-field" placeholder="" value="" name="s" title="Search for:">
                            	</label>
                            </form>
                        </div>

                        <ul class="listSocialMedia">
                            <li><a href="https://www.facebook.com/CamanaBay" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/camanabay" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.instagram.com/camana_bay/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        </ul>
					</div>


				</div>
			</div>

		</div>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
