<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package camanabay
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
<div class='cb_blog_outside'>
<h1 class="entry-title">Search Results </h1>

	<div class="entry-content">
		<div class='cb_blog_left'>
		<div  class="customized-sidebar-title">

		<?php if ( have_posts() ) : ?>
		
		
	<!--	<div class='cb_blog_outside'>
	
	<div class="entry-content">
		<div class='cb_blog_left'>
		<div  class="customized-sidebar-title"> -->

		<?php printf( esc_html__( 'Search Results for: %s', 'camanabay' ), '<span>' . get_search_query() . '</span>' ); ?>
</div>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );
				//get_template_part( 'template-parts/content', 'index' );
				?>

			<?php endwhile; ?>
	        <div class="nav-previous">
			<?php 
			
			previous_posts_link('Previous');
			
			?> </div>
			<div class="nav-next">
			<?php
			 next_posts_link( 'Next' ); 
			 
			
			
		?>
        </div>
		<?php else : ?>

			<?php // get_template_part( 'template-parts/content', 'none' ); ?>
			<?php  get_template_part( 'template-parts/search', 'none' ); ?>
			
			
		<?php endif; ?>

		</div>
		<div class="cb_blog_right">
		
		<div class="cb_blog_right-inner">

		<?php get_sidebar('sidebar-2'); ?>
		</div>
		</div>
	</div><!-- .entry-content -->
	</div>
		</main><!-- #main -->
		
	</div><!-- #primary -->


<?php get_footer(); ?>