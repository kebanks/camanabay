<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package camanabay
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div id="footer-sidebar" class="secondary">
		<div class="site-footer">
		<div class="mycontainer ">
<?php

			 echo do_shortcode('[showCB_BottomMenu]');

			//bottom menu bar goes here

	?>
		</div>
		</div>
			<div class="first-row site-footer">
						<div class="mycontainer">
<?php

		$footer_sidebars = array( 'footer-sidebar-1', 'footer-sidebar-2', 'footer-sidebar-3' );


		foreach ( $footer_sidebars as $key => $footer_sidebar ) :
			if ( is_active_sidebar( $footer_sidebar ) ) :
			$i = $key + 1;
			echo '<div class="footer-widget footer-widget-' . $i . '">';

				dynamic_sidebar( $footer_sidebar );
				echo '</div> <!-- end .footer-widget -->';
			endif;
		endforeach;

?>


	</div>
		</div>

		<div class='second-row site-footer'>
			<div class="mycontainer">
<?php





		$footer_sidebars = array( 'footer-sidebar-4', 'footer-sidebar-5', 'footer-sidebar-6' );

		foreach ( $footer_sidebars as $key => $footer_sidebar ) :
			if ( is_active_sidebar( $footer_sidebar ) ) :
				$i = $key + 1;

				echo '<div class="footer-widget footer-widget-' . $i . ' ">';
				dynamic_sidebar( $footer_sidebar );
				echo '</div> <!-- end .footer-widget -->';
			endif;
		endforeach;

?>
</div>
</div>
</div>

	</footer><!-- #colophon -->
</div><!-- #page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 983217317;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/983217317/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<?php wp_footer(); ?>
<script id="eztixKioskLinkId" type="text/javascript">
(function()
{ var ezLoad = document.createElement('script'); ezLoad.type = 'text/javascript'; ezLoad.src = 'https://kiosk.eztix.co/js/ver' + parseInt(Math.random() * 2147483647) + '/kioskIntegrated/kioskIntegratedExtLoader.js'; var s = document.getElementById('eztixKioskLinkId'); s.parentNode.insertBefore(ezLoad, s.nextSibling); }

)();
</script>
</div> <!---container -->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1746432248956843');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1746432248956843&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->




</body>
</html>
