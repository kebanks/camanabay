<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package camanabay
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<div id="footer-sidebar" class="secondary">
		<div class="site-footer entry-content-movie">
		<div class="mycontainer movie-container">
<?php		

			 echo do_shortcode('[showCB_BottomMenu]');

			//bottom menu bar goes here
				
	?>	
		</div>
		</div>
			<div class="first-row site-footer">
						<div class="mycontainer">
<?php
	
		$footer_sidebars = array( 'footer-sidebar-1', 'footer-sidebar-2', 'footer-sidebar-3' );
	
	
		foreach ( $footer_sidebars as $key => $footer_sidebar ) :
			if ( is_active_sidebar( $footer_sidebar ) ) :
			$i = $key + 1;
			echo '<div class="footer-widget footer-widget-' . $i . '">';
			
				dynamic_sidebar( $footer_sidebar );
				echo '</div> <!-- end .footer-widget -->';
			endif;
		endforeach;
		
?>

			
	</div>
		</div>

		<div class='second-row site-footer'>
			<div class="mycontainer">
<?php

			
			

	
		$footer_sidebars = array( 'footer-sidebar-4', 'footer-sidebar-5', 'footer-sidebar-6' );

		foreach ( $footer_sidebars as $key => $footer_sidebar ) :
			if ( is_active_sidebar( $footer_sidebar ) ) :
				$i = $key + 1;
			
				echo '<div class="footer-widget footer-widget-' . $i . ' ">';
				dynamic_sidebar( $footer_sidebar );
				echo '</div> <!-- end .footer-widget -->';
			endif;
		endforeach;
	
?>
</div>
</div>
</div>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
</div> <!---container -->
</body>
</html>
