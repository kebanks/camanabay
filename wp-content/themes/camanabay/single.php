<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package camanabay
 */ 

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		 <div class='cb_blog_outside cb_blog_outside_single'>
		  <div class='cb_blog_left'>
			
		<?php while ( have_posts() ) : the_post(); ?>
		
		

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

			<?php //the_post_navigation(); ?>

			<?php
				// If comments are open or we have at least one comment, load up the comment template.
			/*	if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;*/
			?>

		<?php endwhile; // End of the loop. ?>
		
		</div>
		<div class="cb_blog_right">
		
		<div class="cb_blog_right-inner">
		<?php get_sidebar('index'); ?>
		</div>
		</div>
		</div><!-- blog outside -->
		</main><!-- #main -->
	


</div> <!-- primary -->
<?php get_footer(); ?>
