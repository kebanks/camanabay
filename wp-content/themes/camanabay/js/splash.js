/**********************************************

   splash.js 
   
   Handles the rotating images for the splash screen


***************************************************/


jQuery(document).ready(function ($) {
		var output_string = cb_splash_ajax.cb_splash_file;	

	jQuery('.splash').css('background-image','url("' + output_string + '")');
	jQuery('.splash').css('background-position','50% 100%');
	jQuery('.splash').css('background-repeat','no-repeat');
	jQuery('.splash').css('background-size','cover');


});