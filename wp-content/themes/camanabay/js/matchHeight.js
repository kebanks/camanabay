// home pg local
// make movie container same height as event, featured, and work containers
function homeMatchHeight() {

	// find movie containers
	var cardMovie = document.querySelectorAll(".cb-movie-bottom-widget-bk");
	// find other activity containers
	var cardActivity = document.querySelectorAll(".cb_headline-area");

	// create loop to add new height on every movie container
	for (var i = 0; i < cardMovie.length; i++) {

		// other activity containers same height
		// just grab first one
		var cardActivityHeight = cardActivity[0].offsetHeight;

		// add new height on movie containers
		cardMovie[i].style.height = cardActivityHeight + "px";
	}
}

homeMatchHeight();

window.addEventListener("resize", homeMatchHeight, false);
