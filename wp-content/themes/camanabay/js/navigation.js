/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens and enables tab
 * support for dropdown menus.  
 */
( function() {

	
	button_container =document.getElementById( 'ic-cb-logo-cell' );
	button = button_container.getElementsByTagName( 'button' )[0];
	if ( 'undefined' === typeof button ) {
		return;
	}
	
	
		button.onclick = function() {
	
		jQuery(".main-navigation").slideToggle(1000);
	
	};

	
} )();
