
document.addEventListener('DOMContentLoaded', function(){

    function menuStyles() {

        var i = 0;
        var items = document.querySelectorAll(".menuItems .panel-first-child");
        var itemsHeight = [];

        for (i = 0; i < items.length; i++) {

            itemsHeight.push(items[i].offsetHeight);
        }

        var maxHeight = Math.max(...itemsHeight);

        for (i = 0; i < items.length; i++) {

            items[i].style.height = maxHeight + "px";
        }
    }

    menuStyles();

    window.onresize = function() {

        menuStyles();
    }

}, false);
