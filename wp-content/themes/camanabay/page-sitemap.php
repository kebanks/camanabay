<?php
/**
  * The template for displaying sitemap pages
 *
 * Template Name: Sitemap page
 * This is the template that displays the sitemap page
 * 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package camanabay
 */

get_header(); 


?>

	<div id="primary" class="content-area ">
	
		<main id="main" class="site-main" role="main">
<header class="entry-header">
			<div id="site-map-header" class="customized-sidebar-title">
			Sitemap
			</div>
</header>
				<?php get_template_part( 'template-parts/sitemap', 'page' ); ?>

				

			

		</main><!-- #main -->
		</div>
		<?php

 get_footer(); ?>
