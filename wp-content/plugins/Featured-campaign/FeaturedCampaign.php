<?php
/*
Plugin Name: Camana Bay Featured Events Plugin
Plugin URI: http://www.camanabay.com/
Description: Camana Bay Featured Events Plugin
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php
/*******************************************************************************************************************
*
*		Featured Events Plugin 
*
*		Plugin that handles the creation, listing and display of Featured Events elements
*
*
*
*
*
**********************************************************************************************************************/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access
}
require_once 'includes/featuredEvDisplay.php';

if ( ! class_exists( 'FeaturedEventsPlugin' ) ) :


class FeaturedEventsPlugin {

 /**
     * @var string
     */
    public $version = '1.0';
	private $edit_id ;
	public static function init() {

        $FeaturedEvents = new self();

    }
	public function __construct() {

       
        $this->setup_actions();
        

    }
	 /**
     * Hook camanabay featured events into WordPress
     */
    private function setup_actions() {
	
	
	/*   we want a separate menu for our plugin 
	 */
	 
		 add_action( 'admin_head', array( $this,'cb_show_cb_featured_events_enqueue_styles' ));
		add_action( 'init', array($this, 'cb_featured_events_plugin_init' ));
	
		
		add_action( 'admin_menu', array($this,  'register_admin_menu' ));
		add_action( 'save_post_cb-fe-ev-page-type', array( $this, 'create_featured_events' ) );
		
		add_filter( 'manage_edit-cb-fe-ev-page-type_columns', array($this, 'edit_featured_events_cols' ) );
          add_action( 'manage_cb-fe-ev-page-type_posts_custom_column', array($this, 'manage_featured_events_cols'),10,2);
      
		 
	
	
	}
	
	
	
function cb_show_cb_featured_events_enqueue_styles()
{


	wp_register_style( 'cb_featured_events_css', plugins_url( 'Featured-campaign/css/cb_featured_events_admin.css' ) );
	wp_enqueue_style( 'cb_featured_events_css' );

}
function manage_featured_events_cols( $column, $post_id ) {

	switch( $column ) {
		
		case 'shortcode':
			
			$s = '[showDB_featuredEv id="' . $post_id . '"]';
				echo  $s;
			break;
	     case "image_l":
		    $s = get_post_meta($post_id,'cb-featured-events_settings');
			$image = $s[0]['cb-featured-events-pic-url1'];
			$output = '<img class="cb_featured_events_img_display" src = ' . $image . ' /> ';
			echo $output;
		 
		    break;
			
		 case "image_m":
		    $s = get_post_meta($post_id,'cb-featured-events_settings');
			$image = $s[0]['cb-featured-events-pic-url2'];
			$output = '<img class="cb_featured_events_img_display" src = ' . $image . ' /> ';
			echo $output;
		 
		    break;
			
			 case "image_r":
		    $s = get_post_meta($post_id,'cb-featured-events_settings');
			$image = $s[0]['cb-featured-events-pic-url3'];
			$output = '<img class="cb_featured_events_img_display" src = ' . $image . ' /> ';
			echo $output;
		 
		    break;
		default:
                return print_r($column,true); //Show the whole array for troubleshooting purposes
       
    }
	
	

}

function edit_featured_events_cols( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'shortcode' => __( 'Short Code'),
	    'image_l' => __('Image Left'),
	    'image_m' => __('Image Middle'),
	    'image_r' => __('Image Right'),
		'date' => __( 'Date' )
	);

	return $columns;
}
	
	
	function add_featured_events_metaboxes()
	{
	
		add_meta_box('cb-featured-events-metabox', "<h1> Featured Events</h1>", array(&$this,'cb_featured_events_plugin_metabox_page'),'cb-fe-ev-page-type' , 'normal','high');
		
	
	}
	
	
	/******************************************************
	*
	*		create_featured_events
	*
	*		Function to create a New Featured Events Entry
	*
	******************************************************/
	public function create_featured_events( $post_id, $post, $update )
	{
		
	
		
		$post_data = array();
		$post_data['cb-featured-events-pic-url1'] = $_POST['cb-featured-events-pic-url1'];
		$post_data['cb-featured-events-pic-url2'] = $_POST['cb-featured-events-pic-url2'];
		$post_data['cb-featured-events-pic-url3'] = $_POST['cb-featured-events-pic-url3'];
		$post_data['cb-featured-events-bottom-text'] = $_POST['cb-featured-events-bottom-text'];
		$post_data['cb-featured-events-arrow-link'] = $_POST['cb-featured-events-arrow-link'];
		
		
		
		
		update_post_meta( $post_id, 'cb-featured-events_settings', $post_data );
		
	}
	
	/********************************************************************************************
	*
	*   create_featured_events_custom_post_type
	*
	*	Inserts the initial post for create featured events element element
	*
	*
	*
	****************************************************************************************/
	
	
	
	
	
	
	public function register_admin_menu() {

		add_submenu_page( 'cb-featured-events-plugin-menu', 'Featured Events List', 'Featured Events List ', 'manage_options', 'cb-featured-events-listing',array($this,'cb_featured_events_plugin_listing'))	;	
		add_meta_box('cb-featured-events-metabox', "<h1> Featured Events</h1>", array(&$this, 'cb_featured_events_plugin_metabox_page'),'cb-fe-ev-page-type' , 'normal','high');
		
		
	}
	public function cb_featured_events_plugin_listing($arg)
	{
	$wp_list_table = new _cb_featured_events_list_table();
	$wp_list_table->prepare_items();
	echo '<div class="wrap"><h2>' . esc_html( __( 'Featured Events', 'cb-featured-events' ) ) ;
	echo ' <a href="' . esc_url( menu_page_url( 'cb-featured-events-plugin-menu', false ) ) . '" class="add-new-h2">' . esc_html( __( 'Add New', 'cb-featured-events' ) ) . '</a></h2>';

?> <form id="cb-featured-events-filter"" method="get"> 
	<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
  
<?php
	
	$wp_list_table->display();
?> </form> <?php
	echo '</div>';
	
	
	
	}
	/********************************************************
	 *
	 *		cb_featured_events_plugin_options_page
	 *
	 *		Options page and form for Featured events Plugin
	 *
	 *
	 *
	 **************************************************/
	public function cb_featured_events_plugin_options_page()
	{

	
		
		
		
	   ?>
    <div class="wrap">

        <form accept-charset="UTF-8" action="<?php echo admin_url( 'admin-post.php'); ?>" method="post">
		<input type="hidden" name="action" value="<?php echo $this->action ?>"  >
		<input type="hidden" name="edit_id" value="<?php echo $id ?>"  >

		
	
          
		 
            <?php 
				
				
			
			
			
	
	
         submit_button();



			 ?>
		 
        </form>
    </div>

    <?php
	
	}

	/*****************************************************************
	 *
	 *		cb_featured_events_plugin_metabox_page
	 *
	 *		put the fields in a metabox
	 *
	 *
	 ****************************************************************/
	public function cb_featured_events_plugin_metabox_page($post, $metabox)
	{
	
		
		$id =  $post->ID;
		
		
		if (isset($id))
		{
			
			$post_meta = get_post_meta($id, 'cb-featured-events_settings');
			$cb_featured_events_title_text = get_the_title( $id );
			$cb_pic_url1 = $post_meta[0]['cb-featured-events-pic-url1'];
			$cb_pic_url2 = $post_meta[0]['cb-featured-events-pic-url2'];
			$cb_pic_url3 = $post_meta[0]['cb-featured-events-pic-url3'];
			$cb_featured_events_bottom_text = $post_meta[0]['cb-featured-events-bottom-text'];
			$cb_featured_events_arrow_link = $post_meta[0]['cb-featured-events-arrow-link'];
		
		
		}else 
		{
			$cb_featured_events_title_text = "";
			$cb_pic_url1 = "";
			$cb_pic_url2 = "";
			$cb_pic_url3 = "";
			$cb_featured_events_bottom_text = "";
			$cb_featured_events_arrow_link = "";
		
		
		}
		
		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__FILE__) . '../js/upload-media.js', array('jquery'));
      

        wp_enqueue_style('thickbox');
		
	
			
?>	
		<p>
			
            <label for="cb-featured-events-pic-url1"><?php _e( 'Display Image (best size is 460px by 460px):' ); ?></label>
            <input name="cb-featured-events-pic-url1" id="cb-featured-events-pic-url1" type="text" size="50"  value="<?php echo esc_url( $cb_pic_url1 ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
		
		<p>
			
            <label for="cb-featured-events-pic-url2"><?php _e( 'Display Image (best size is 460px by 460px):' ); ?></label>
            <input name="cb-featured-events-pic-url2" id="cb-featured-events-pic-url2" type="text" size="50"  value="<?php echo esc_url( $cb_pic_url2 ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
		
		<p>
			
            <label for="cb-featured-events-pic-url3"><?php _e( 'Display Image (best size is 460px by 460px):' ); ?></label>
            <input name="cb-featured-events-pic-url3" id="cb-featured-events-pic-url3" type="text" size="50"  value="<?php echo esc_url( $cb_pic_url3 ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
<?php
			printf(
            '<p><label for "cb-featured-events-bottom-text">Bottom Link Text: </label><input type="text" size="50"  name="cb-featured-events-bottom-text" value="%s" /></p>',
             isset( $cb_featured_events_bottom_text ) ? esc_attr( $cb_featured_events_bottom_text) : ''
        );
		
		printf(
            '<label for "cb-featured-events-arrow-link">Arrow Link: </label><input type="text" size="50"  name="cb-featured-events-arrow-link" value="%s" />',
             isset( $cb_featured_events_arrow_link ) ? esc_attr( $cb_featured_events_arrow_link) : ''
        );
	}
	
	/*****************************************************************
	 *
	 *		cb_featured_events_plugin_init
	 *
	 *		register the featured events page type
	 *
	 *
	 ****************************************************************/
	
	public function cb_featured_events_plugin_init() {
  

		register_post_type( 'cb-fe-ev-page-type',
        array(
            'labels' => array(
                'name' => 'Featured Events',
                'singular_name' => 'Featured Events',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Featured Events Entry',
                'edit' => 'Edit',
                'edit_item' => 'Edit Featured Events Entry',
                'new_item' => 'New Featured Events Entry',
                'view' => 'View',
                'view_item' => 'View Featured Events Entry',
				 'all_items' => __( 'All Featured Events', 'cb-featured-events' ),	
                'not_found' => 'No Featured Events found',
                'not_found_in_trash' => 'No Featured Events found in Trash',
                'parent' => 'Parent Featured Events Entry',
				 'menu_name'=>  'Featured Events'
				
            ),
 
            
            'menu_position' => 17,
            'supports' => array( 'title' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => false,
			'exclude_from_search' => true,
                'publicly_queryable' => false,
			'query_var' => false,
                'rewrite' => false,
				'public' => true,
				'show_ui' => true,
				'show_in_admin_bar' => true,
				'show_in_menu' =>true,
						'show_in_nav_menus' => true,
              
                
			)
		);
		
		
	
		
	}

}

endif;

add_action( 'plugins_loaded', array( 'FeaturedEventsPlugin', 'init' ), 10 );
?>