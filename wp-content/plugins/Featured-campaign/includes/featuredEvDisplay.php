<?php
/******************************************************************************
*
*		featuredEvDisplay.php
*
*	    Displays the Camana Bay Featured Events via the short code showDB_featuredEv with an id
*
*****************************************************************************/
class _Featured_Ev_Display{

	private $options;
	public function __construct()

    {

		add_shortcode( 'showDB_featuredEv', array( $this, 'cb_show_Featured_Ev_Display_func' ) );

		add_action( 'wp_enqueue_scripts', array( $this,'cb_show_Featured_Ev_Display_enqueue_styles' ));
		//add_action('wp_ajax_nopriv_cb-dining-action',  array($this,'cb_dining_action_function'));
		//add_action('wp_ajax_cb-dining-action',  array($this,'cb_dining_action_function'));
	}
	
/*	function cb_dining_action_function()
	{
	
		$id = $_POST['field'];
		error_log('id is ' . $id);
	    $s =  $this->get_layout($id);
$mystring = json_encode($s,JSON_UNESCAPED_SLASHES);
		error_log('json is ' . $mystring);
 echo json_encode( $s ,JSON_UNESCAPED_SLASHES);
		die();
	}*/
	
	/******************************************************************
	*
	*	cb_show_Featured_Ev_Display_func
	*
	*	returns the string that shows the slider for display and sets up the localized jQuery script
	*
	******************************************************************/
	function cb_show_Featured_Ev_Display_func($atts, $content="" )

	{
		
		
		$atts = shortcode_atts(
		array(
			'id' => 0,
			
		), $atts, 'showDB_featuredEv' );


		
		$post_id = $atts['id'];
		// this is the cb_stories type post 
		
		 $post_meta = get_post_meta($post_id,'cb-featured-events_settings');
		$image1 = $post_meta[0]['cb-featured-events-pic-url1'];
		$image2 = $post_meta[0]['cb-featured-events-pic-url2'];
		$image3 = $post_meta[0]['cb-featured-events-pic-url3'];
		$bottom_text_link = $post_meta[0]['cb-featured-events-bottom-text'];
		$events_arrow_link = $post_meta[0]['cb-featured-events-arrow-link'];
		
	
		
		$string = '<div id="main-cb-ev-container">';
		$string .= '<div class="cb_ev_title_bk cb_ev_title_text_top"> '. 
								get_the_title( $post_id ) . 
						'</div>  
			
			<div class="cb_ev_middle_bk">
			  <div class="cb_ev_image_outer">
				<div class="cb_ev_image ">
					<img id="cb_ev_image1"   />
				</div>
				<div class="cb_ev_image">
				 <img  id="cb_ev_image2" />
				</div>
				<div class="cb_ev_image">
				<img  id="cb_ev_image3"  />
				</div>
			  </div>
			  </div>
			  
			<div class="cb_ev_title_bk cb_ev_title_text_bottom" > '. $bottom_text_link . '
			
			<a href="#" class="cb_ev_right_arrow" >  </a>
			</div>
			  </div>';
		                    
	
	    wp_register_script( 'cb_fet_events_js', plugins_url( '/Featured-campaign/js/cb_fet_events_js.js' ), array('jquery') );
		wp_enqueue_script('cb_fet_events_js');
		
		
		
		wp_localize_script( 'cb_fet_events_js', 'cb_fe_ajax',
				array( 'ajax_url' => admin_url( 'admin-ajax.php' ),'cb_image1' => $image1,'cb_image2' => $image2,'cb_image3' => $image3, 'cb_ev_link' => $events_arrow_link));

		
					
		
	

		
		return $string;
	//	

	}
	
	function cb_show_Featured_Ev_Display_enqueue_styles()
	{

		
			wp_register_style( 'cb_Featured_Ev_Display_css', plugins_url( 'Featured-campaign/css/CamanaFeaturedEv.css' ) );
			wp_enqueue_style( 'cb_Featured_Ev_Display_css' );
	
	}
	
	
	
	public static function instance() {
		
		 new _Featured_Ev_Display;

			

	}
}

	
	
	function cb_init_Featured_Ev_Display(){


		return _Featured_Ev_Display::instance();

	}

add_action( 'plugins_loaded',  'cb_init_Featured_Ev_Display'  );

?>