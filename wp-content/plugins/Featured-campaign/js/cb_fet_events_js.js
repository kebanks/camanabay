/* **************************************************************************************
*  b_fet_events_js.js 
*
*
*   JQuery code that manages the featured events display for the featured events plugin
*
*
*
*******************************************************************************************/


jQuery(document).ready(function ($) {

	var image1 = cb_fe_ajax.cb_image1;	
	var image2 = cb_fe_ajax.cb_image2;	
	var image3 = cb_fe_ajax.cb_image3;	

	jQuery('#cb_ev_image1').css('background-image','url("' + image1 + '")');
	jQuery('#cb_ev_image1').css('background-repeat','no-repeat');
	jQuery('#cb_ev_image1').css('background-size','cover');

	jQuery('#cb_ev_image2').css('background-image','url("' + image2 + '")');
	
	jQuery('#cb_ev_image2').css('background-repeat','no-repeat');
	jQuery('#cb_ev_image2').css('background-size','cover');
	
	jQuery('#cb_ev_image3').css('background-image','url("' + image3 + '")');

	jQuery('#cb_ev_image3').css('background-repeat','no-repeat');
	jQuery('#cb_ev_image3').css('background-size','cover');



	 jQuery('.cb_ev_right_arrow').click(function(e) {
			
		
			var link = cb_fe_ajax.cb_ev_link;
			jQuery(".cb_ev_right_arrow").attr("href", link);
			
			
		
	});
	
	

	
	
	
});    
