<?php

/*
Plugin Name:CB Color Picker for Plugins
Plugin URI: http://www.camanabay.com/
Description: Color Picker parent for plugins
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access
}
if ( ! class_exists( 'CB_Color_Picker_Plugin' ) ) :
class CB_Color_Picker_Plugin
{
public function __construct() {

       
         add_action('admin_enqueue_scripts', array($this, 'cb_cp_admin_styles'));
        

    }
public static function init() {

         new self();

    }
public function cb_cp_admin_styles(){

	wp_enqueue_style( 'wp-color-picker' );        
        wp_enqueue_script( 'wp-color-picker' );      
		 wp_register_style( 'cb_cp_css', plugins_url( '/css/colorpicker.css',__FILE__ ) );
		wp_enqueue_style( 'cb_cp_css' );



}
public function add_color_picker($cb_button_color)
	{
		?>

<script type='text/javascript'>
      jQuery(document).ready(function($) {
         $('.cb-action-cp').wpColorPicker();
		jQuery('.cb_cp_select').click(function(e) {
					e.preventDefault();
					var id = '#'+jQuery (this).attr('id');
					var hexText =  jQuery(id).text();
					var x = '.cb-action-cp';
				
				jQuery(x).val(hexText);
					
				jQuery('.wp-color-result').css({backgroundColor: hexText});
				jQuery(x).wpColorPicker('color', hexText);
		 	});
       });
        </script>


	
<?php 
	 $color_array = array( "#a87eb1" ,"#cb003c", "#FFB718 ", "#faa519", "#77bc1f","#64cbc8","#00b6dd","#008aab","#d7d1c4","#c4e76a");
		echo "<h2>Colors</h2>";
		echo "<p>";
		foreach($color_array as $key => $color)
		{	
			$class = "color_" . $key;
		
				echo "<a href='#' id='" . $class . "' class='cb_cp_select' >". $color ."</a>" ;
				if ($key == 4)
				echo "</p></p>";
	
		}
		echo "</p>";
		
?>

<p><label for="cb-cp-color"></label><?php _e('Text Color:', 'cb-color-picker'); ?></label>
  <p> <input class="cb-action-cp" type="text" id="cb-cp-color" name="cb-cp-color" value="<?php echo $cb_button_color; ?>" />                            
	</p></p>



<?php
	}
	
	
}
endif;

add_action( 'plugins_loaded', array( 'CB_Color_Picker_Plugin', 'init' ), 10 );