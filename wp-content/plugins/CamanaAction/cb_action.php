<?php
/*
Plugin Name: Camana Action Plugin
Plugin URI: http://www.camanabay.com/
Description: Camana Action Plugin
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php
/*******************************************************************************************************************
*
*		Camana Action plugin 
*
*		Plugin that handles the creation, listing and display of Camana Action elements
*
*
*
*
*
**********************************************************************************************************************/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access
}
require_once 'includes/CB_EA_Display.php';

if ( ! class_exists( 'CamanaEventActionPlugin' ) ) :


class CamanaEventActionPlugin extends CB_Color_Picker_Plugin {

 /**
     * @var string
     */
    public $version = '1.0';
	private $edit_id ;
	public static function init() {

        $CamanaDirectoryDetail = new self();

    }
	public function __construct() {

       
        $this->setup_actions();
        

    }
	 /**
     * Hook camanaCamana Action into WordPress
     */
    private function setup_actions() {
	
	
	/*   we want a separate menu for our plugin 
	 */
	 
		 add_action( 'admin_head', array( $this,'cb_camana_event_action_enqueue_styles' ));
		add_action( 'init', array($this, 'cb_camana_event_action_plugin_init' ));
	
		
		add_action( 'admin_menu', array($this,  'register_admin_menu' ));
		add_action( 'save_post_cb-ev-act-pg-type', array( $this, 'create_camana_event_action' ) );
		
		add_filter( 'manage_edit-cb-ev-act-pg-type_columns', array($this, 'edit_camana_event_action_columns' ) );
        add_action( 'manage_cb-ev-act-pg-type_posts_custom_column', array($this, 'manage_camana_event_action_columns'),10,2);
      
		 
	
	
	}
	
	
	
function cb_camana_event_action_enqueue_styles()
{


	wp_register_style( 'cb_camana_event_action_css', plugins_url( 'CamanaAction/css/cb_Event_Action_admin.css' ) );
	wp_enqueue_style( 'cb_camana_event_action_css' );
	
	wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script('upload_media_widget', plugin_dir_url(__DIR__) . 'js/upload-media.js', array('jquery'));
      

        wp_enqueue_style('thickbox');
	
}


function manage_camana_event_action_columns( $column, $post_id ) {

	switch( $column ) {
		
		case "shortcode":
		
		
			$s = '[CB_EventAction id="' . $post_id . '"]';
			echo $s;
			break;
		 case "title":
		 
		 
			break;
	     case "image":
		    $s = get_post_meta($post_id,'cb-action_detail_settings');
			$image = $s[0]['cb_ea_pic_url'];
			$output = '<img class="cb_camana_event_action_img_display" src = ' . $image . ' /> ';
			echo $output;
		 
		    break;
			
		default:
                return print_r($column,true); //Show the whole array for troubleshooting purposes
       
    }
	
	

}

function edit_camana_event_action_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'shortcode' =>__( 'Short Code' ),
	    'image' => __('Image'),
		'date' => __( 'Date' )
	);

	return $columns;
}
	
	
	
	
	
	
	
	/********************************************************************************************
	*
	*   create_camana_event_action_custom_post_type
	*
	*	Inserts the initial post for create camana action element
	*
	*
	*
	****************************************************************************************/
	
	
	
	
	
	
	public function register_admin_menu() {

		add_submenu_page( 'cb-camana_event_action-plugin-menu', 'Camana Event Action List', 'Camana Bay Action List ', 'manage_options', 'cb-camana_event_action-listing',array($this,'cb_camana_event_action_plugin_listing'))	;	
		add_meta_box('cb-camana_event_action-metabox', "<h1> Camana Bay Camana Action </h1>", array(&$this, 'cb_camana_event_action_plugin_metabox_page'),'cb-ev-act-pg-type' , 'normal','high');
		
		
	}
	public function cb_camana_event_action_plugin_listing($arg)
	{
	$wp_list_table = new _cb_camana_event_action_list_table();
	$wp_list_table->prepare_items();
	echo '<div class="wrap"><h2>' . esc_html( __( 'Camana Bay Action ', 'cb-camana_event_action' ) ) ;
	echo ' <a href="' . esc_url( menu_page_url( 'cb-camana_event_action-plugin-menu', false ) ) . '" class="add-new-h2">' . esc_html( __( 'Add New', 'cb-camana_event_action' ) ) . '</a></h2>';

?> <form id="cb-camana_event_action-filter"" method="get"> 
	<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
  
<?php
	
	$wp_list_table->display();
?> </form> <?php
	echo '</div>';
	
	
	
	}
	/********************************************************
	 *
	 *		cb_camana_event_action_plugin_options_page
	 *
	 *		Options page and form for Camana Action  Plugin
	 *
	 *
	 *
	 **************************************************/
	public function cb_camana_event_action_plugin_options_page()
	{

	
		
		
		
	   ?>
    <div class="wrap">

        <form accept-charset="UTF-8" action="<?php echo admin_url( 'admin-post.php'); ?>" method="post">
		<input type="hidden" name="action" value="<?php echo $this->action ?>"  >
		<input type="hidden" name="edit_id" value="<?php echo $id ?>"  >

		
	
          
		 
            <?php 
				
				
			
			
			
	
	
         submit_button();



			 ?>
		 
        </form>
    </div>

    <?php
	
	}
	/******************************************************
	*
	*		create_camana_event_action
	*
	*		Function to create a New Camana Action  Entry
	*
	******************************************************/
	public function create_camana_event_action( $post_id, $post, $update )
	{
		
	
		
		$post_data = array();
		
		

		$post_data['cb_ea_pic_url'] = $_POST['cb_ea_pic_url'];
		$post_data['cb-ea-destination-link'] = $_POST['cb-ea-destination-link'];

		$post_data['cb_ea_text_add_link'] = $_POST['cb_ea_text_add_link'];
		$post_data['cb-ea-destination-link-text'] = $_POST['cb-ea-destination-link-text'];
	
		$post_data['cb-ea-block-text'] = wpautop($_POST['blockText']);
		$post_data['cb-ea-color'] = $_POST['cb-cp-color'];
		update_post_meta( $post_id, 'cb-action_detail_settings', $post_data );
	
		
	}
	/*****************************************************************
	 *
	 *		cb_camana_event_action_plugin_metabox_page
	 *
	 *		put the fields in a metabox
	 *
	 *
	 ****************************************************************/
	public function cb_camana_event_action_plugin_metabox_page($post, $metabox)
	{
	
		
		$id =  $post->ID;
		
		
		if (isset($id))
		{
			
			$post_meta = get_post_meta($id, 'cb-action_detail_settings');
			$post_title = get_the_title( $id );
			$cb_pic_url = $post_meta[0]['cb_ea_pic_url'];
			$cb_dest_link = $post_meta[0]['cb-ea-destination-link'];

			$cb_ea_text_add_link = $post_meta[0]['cb_ea_text_add_link'];
		    $cb_ea_color =$post_meta[0]['cb-ea-color'];
			$cb_ea_destination_link_text = $post_meta[0]['cb-ea-destination-link-text'];
			$cb_ea_block_text = $post_meta[0]['cb-ea-block-text'];
			

		}
		
	
	
			
?>	
		<p>
			<h3>Best size for image is 320px by 320px</h3>
            <label for="cb_ea_pic_url"><?php _e( 'Image (best size 382px by 295) :' ); ?></label>
            <input name="cb_ea_pic_url" id="cb_ea_pic_url" type="text" size="50"  value="<?php echo esc_url( $cb_pic_url ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
		
		<p><label for="cb-ea-destination-link"><?php _e('Destination Url:'); ?></label>
	<input class="widefat" id="cb-ea-destination-link" name="cb-ea-destination-link" type="text" value="<?php echo $cb_dest_link; ?>" />
	</p>
		
		<p><label for="cb_ea_text_add_link">Add the link? </label>
	
	<?php  if ($cb_ea_text_add_link == "YES")
					$checked="checked";
					else 
					$checked = "unchecked";
		
				?>

 <input type="checkbox" id="cb_ea_text_add_link"   name="cb_ea_text_add_link" value="YES"<?php echo $checked; ?>>
 <p><label for="cb-ea-destination-link-text'"><?php _e('Action Text:', 'cb_event_action_widget')?></label>
	<input class="widefat" id="cb-ea-destination-link-text'" name="cb-ea-destination-link-text" type="text" value="<?php echo $cb_ea_destination_link_text; ?>" />
	</p>
	
	
	
	
 </p>
		
		
		<p><label for="blockText"><?php _e('Text:'); ?></label>
			
<?php
		
			$settings = array( 'tinymce' => true, 'textarea_rows' => '30', 'quicktags' => true, 'media_buttons' => false);
			wp_editor($cb_ea_block_text,'blockText', $settings);
			
			
			$this->add_color_picker($cb_ea_color);
	}
	
	/*****************************************************************
	 *
	 *		cb_camana_event_action_plugin_init
	 *
	 *		register the Camana Action  page type
	 *
	 *
	 ****************************************************************/
	
	public function cb_camana_event_action_plugin_init() {
  

		register_post_type( 'cb-ev-act-pg-type',
        array(
            'labels' => array(
                'name' => 'Camana Bay Camana Action ',
                'singular_name' => 'Camana Bay Camana Action ',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Camana Bay Camana Action  Entry',
                'edit' => 'Edit',
                'edit_item' => 'Edit Camana Bay Camana Action  Entry',
                'new_item' => 'New Camana Bay Camana Action  Entry',
                'view' => 'View',
                'view_item' => 'View Camana Bay Camana Action  Entry',
				 'all_items' => __( 'All Camana Bay Camana Action ', 'cb-camana_event_action' ),	
                'not_found' => 'No Camana Bay Camana Action  found',
                'not_found_in_trash' => 'No Camana Bay Camana Action  found in Trash',
                'parent' => 'Parent Camana Bay Camana Action  Entry',
				 'menu_name'=>  'Camana Bay Camana Action '
				
            ),
 
            
            'menu_position' => 15.5,
            'supports' => array( 'title' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => false,
			'exclude_from_search' => true,
                'publicly_queryable' => false,
                'show_in_nav_menus' => false,
				'hierarchical' => true,
			'query_var' => false,
                'rewrite' => false,
				'public' => true,
				'show_ui' => true,
				'show_in_admin_bar' => true,
				'show_in_menu' =>true,
						'show_in_nav_menus' => true,
              
                
			)
		);
		
		
	
		
	}

}

endif;

add_action( 'plugins_loaded', array( 'CamanaEventActionPlugin', 'init' ), 10 );
function child_plugin_activate(){

    // Require parent plugin
    if ( ! is_plugin_active( 'CB_Color_Picker/cb_color_picker.php' ) and current_user_can( 'activate_plugins' ) ) {
        // Stop activation redirect and show error
        wp_die('Sorry, but this plugin requires the CB Color Plugin to be installed and active. <br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Return to Plugins</a>');
    }
}

  
  register_activation_hook( __FILE__, 'child_plugin_activate' );

  
  
  
  
  
  
  
