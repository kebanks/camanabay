<?php
/******************************************************************************
*
*		CB_EA_Display.php
*
*	    Displays the Camana Bay Event Action via the short code CB_EventAction with an id
*
*****************************************************************************/
class _Event_Action_Display{


	private $type;
	public function __construct()

    {

		add_shortcode( 'CB_EventAction', array( $this, 'cb_show_Event_Action_Display_func' ) );

		add_action( 'wp_enqueue_scripts', array( $this,'cb_show_Event_Action_Display_enqueue_styles' ));
		
	}
	
	
	

	/******************************************************************
	*
	*	cb_show_Event_Action_Display_func
	*
	*	returns the string that shows the slider for display and sets up the localized jQuery script
	*
	******************************************************************/
	function cb_show_Event_Action_Display_func($atts, $content="" )

	{
	
		
		$atts = shortcode_atts(
		array(
			'id' => "",
			
		), $atts, 'CB_EventAction' );

	
		
		$id = $atts['id'];
		 $s = get_post_meta($id,'cb-action_detail_settings');
		 $cb_ea_pic_url = $s[0]['cb_ea_pic_url'];
		 $cb_destination_link  = $s[0]['cb-ea-destination-link'];
		 $cb_ev_action_make_link = $s[0]['cb_ea_text_add_link'];
		 $cb_destination_link_text = $s[0]['cb-ea-destination-link-text'];
		 $cb_widget_text = $s[0]['cb-ea-block-text'];
		 $cb_widget_text_title = get_the_title($id);
		 $cb_button_color = $s[0]['cb-ea-color'];

		 
		 
		 
		$string = "<h1>  the id is " . $id . "</h1>";
		$string= '<div class="cb_event_action_plugin_outside">
			<div class="event_action_plugin_left">';
		$string .='<img src="' . $cb_ea_pic_url . '" />';
		if ($cb_ev_action_make_link == "YES"){
			$string .= '<div class="event_action_plugin_bottom">
			 <a  href="'. $cb_destination_link . '" target="_blank"> <span id="' . $id . '" class="event_action_text" > ' . $cb_destination_link_text . '</span></a></div>' ;
		      
		}
		$string .= '</div> <div class="event_action_plugin_right">';
		if (!empty($cb_widget_text)){
		
		     
		 $string .='<h1>' . $cb_widget_text_title . '</h1>';
		 $string .= '<div class="event_action_body_text"> ' .$cb_widget_text . '</div></div></div>';
		
		}
?>
<script>

	 jQuery(document).ready(function($) {

		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		jQuery($id).css('color',$buttonColor);
		var borderInfo = "1px solid " + $buttonColor;
		jQuery('.event_action_plugin_bottom').css('border',borderInfo);


	 });

</script>
<?php		
		
		return $string;
	//	

	}
	
	function cb_show_Event_Action_Display_enqueue_styles()
	{

		
			wp_register_style( 'cb_Event_Action_Display_css', plugins_url( 'CamanaAction/css/cb_ca_display.min.css' ) );
			wp_enqueue_style( 'cb_Event_Action_Display_css' );
	
	}
	
	
	
	public static function instance() {
		
		 new _Event_Action_Display;

			

	}
}

	
	
	function cb_init_Event_Action_Display(){


		return _Event_Action_Display::instance();

	}

add_action( 'plugins_loaded',  'cb_init_Event_Action_Display'  );

?>