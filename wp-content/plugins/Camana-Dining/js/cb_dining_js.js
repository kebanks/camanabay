/* **************************************************************************************
*  cb_dining_js.js 
*
*
*   JQuery code that manages the dining display for the dining plugin
*
*
*
*******************************************************************************************/


jQuery(document).ready(function ($) {

	var output_string = cb_dining_ajax.cb_content;	
	console.log('output string ' + output_string);

	
	jQuery('#dining_content').html(output_string);


function do_ajax(mydata){

	jQuery.ajax({                
			url: cb_dining_ajax.ajax_url,
			type: "POST",
			data: mydata,     
			cache: false,
			success: function(data, status) {
	
				 var posts = JSON.parse(data);

			 jQuery('#dining_content').html(posts);
      }
		});		


}

	// jQuery('.filter_inner_right_item').click(function(e) {
	jQuery( document ).on( 'click', '.filter_inner_left_item', function(e) {
			e.preventDefault();
			var mydata = {
			action: 'cb-dining-action',
			field: ""
		};
		do_ajax(mydata);
	
		
	});
	
	

	jQuery( document ).on( 'click', '.filter_inner_right_item', function(e) {

		e.preventDefault();
		var a_el = jQuery(this).find('a');
		
		var id = jQuery(a_el).attr('id');
		var mydata = {
			action: 'cb-dining-action',
			field: id
		};
	 
		
		do_ajax(mydata);
		
	
		
	});
	
	
});    
