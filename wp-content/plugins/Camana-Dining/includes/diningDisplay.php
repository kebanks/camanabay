<?php
/******************************************************************************
*
*		diningDisplay.php
*
*	    Displays the Camana Bay Dining via the short code showCB_diningDisplay with an id
*
*****************************************************************************/
class _Dining_Display{

	private $options;
	public function __construct()

    {

		add_shortcode( 'showCB_diningDisplay', array( $this, 'cb_show_Dining_Display_func' ) );

		add_action( 'wp_enqueue_scripts', array( $this,'cb_show_Dining_Display_enqueue_styles' ));
		add_action('wp_ajax_nopriv_cb-dining-action',  array($this,'cb_dining_action_function'));
		add_action('wp_ajax_cb-dining-action',  array($this,'cb_dining_action_function'));
	}

	function cb_dining_action_function()
	{

		$id = $_POST['field'];

	    $s =  $this->get_layout($id);
$mystring = json_encode($s,JSON_UNESCAPED_SLASHES);

 echo json_encode( $s ,JSON_UNESCAPED_SLASHES);
		die();
	}
	function get_layout($sort_terms){

		$taxonomy_terms = get_terms( 'DiningCategory' );

		$string = '<div id="filter_outer">
		  <div id="filter_inner_left">
		  <div class="filter_inner_left_item">
		  <a id="" href="#">
				Show All
		  </a>
		  </div>
		   </div>
		   <div id="filter_inner_right">';

			/* get the top buttons */
		foreach( $taxonomy_terms as $term ) {
			$string.= "<div class='filter_inner_right_item'>
			<a id=" . $term->slug . " href='#'>" .
			$term->name .
			"</a>
			</div>";




		}
		$string.= "</div>
		</div>";



		$string .= "<div class='cb-dining-filter-outside'>";
	$args = array(
        'slug' => $sort_terms

    );
		$taxonomy_terms = get_terms('DiningCategory', $args);
		$do_not_duplicate = array();
		foreach( $taxonomy_terms as $term ) {

			//error_log('dining terms ' . print_r($term, TRUE));

	$args = array(
        'post_type' => 'cb-dining-page-type',
        'DiningCategory' => $term->slug,
		'order_by' => 'title',
		'post_status'	 => 'publish'
    );

		$query = new WP_Query( $args );

		while ( $query->have_posts() ) : $query->the_post();
				$id= get_the_ID();

			// check for duplicates
			if (in_array($id, $do_not_duplicate)) {
				 continue;

			}
			array_push($do_not_duplicate, $id);
			 $post_meta= get_post_meta($id,'cb-dining_settings');
			 $image = $post_meta[0]['cb-dining-pic-url'];
			 $link = $post_meta[0]['cb-dining-link-text'];

			$output = '<a href="' . $link . '"><img  src = ' . $image . ' /> </a>';


		   $string .= '<div class="cb-dining-filter-item">
							<div class="cb-dining-filter-item-inside">
								<div class="cb-dining-filter-image-item"> ' . $output .
								'</div>
								</div>
								</div>';
?>



        <?php endwhile;

		wp_reset_postdata();

		}
	$string .= "</div>";
		return $string;



	}
	/******************************************************************
	*
	*	cb_show_Dining_Display_func
	*
	*	returns the string that shows the slider for display and sets up the localized jQuery script
	*
	******************************************************************/
	function cb_show_Dining_Display_func($atts, $content="" )

	{


		$atts = shortcode_atts(
		array(
			'id' => 0,

		), $atts, 'showCB_diningDisplay' );



		$post_id = $atts['id'];
		// this is the cb_stories type post




		$string = '<div id="dining_content">';
	//

	    $string .= "</div>";



		 wp_register_script( 'cb_dining_js', plugins_url( '/Camana-Dining/js/cb_dining_js.js' ), array('jquery') );
		wp_enqueue_script('cb_dining_js');




		$string1 =  $this->get_layout("");

		wp_localize_script( 'cb_dining_js', 'cb_dining_ajax',
				array( 'ajax_url' => admin_url( 'admin-ajax.php' ),'cb_content' => $string1));



		return $string;
	//

	}

	function cb_show_Dining_Display_enqueue_styles()
	{


			wp_register_style( 'cb_dining_display_css', plugins_url( 'Camana-Dining/css/CamanaBayDining.css' ) );
			wp_enqueue_style( 'cb_dining_display_css' );


	}



	public static function instance() {

		 new _Dining_Display;



	}
}



	function cb_init_Dining_Display(){


		return _Dining_Display::instance();

	}

add_action( 'plugins_loaded',  'cb_init_Dining_Display'  );

?>
