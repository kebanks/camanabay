<?php
/*
Plugin Name: Camana Bay Dining Plugin
Plugin URI: http://www.camanabay.com/
Description: Camana Bay Dining Plugin
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php
/*******************************************************************************************************************
*
*		Camana Bay Dining Plugin 
*
*		Plugin that handles the creation, listing and display of Camana Bay Dining elements
*
*
*
*
*
**********************************************************************************************************************/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access
}
require_once 'includes/diningDisplay.php';

if ( ! class_exists( 'CamanaDiningPlugin' ) ) :


class CamanaDiningPlugin {

 /**
     * @var string
     */
    public $version = '1.0';
	private $edit_id ;
	public static function init() {

        $CamanaDining = new self();

    }
	public function __construct() {

       
        $this->setup_actions();
        

    }
	 /**
     * Hook camanabay dining into WordPress
     */
    private function setup_actions() {
	
	
	/*   we want a separate menu for our plugin 
	 */
	 
		 add_action( 'admin_head', array( $this,'cb_show_cb_dining_enqueue_styles' ));
		add_action( 'init', array($this, 'cb_dining_plugin_init' ));
		add_action( 'init', array($this, 'cb_dining_taxonomy_init' ));
		
		add_action( 'admin_menu', array($this,  'register_admin_menu' ));
		add_action( 'save_post_cb-dining-page-type', array( $this, 'create_dining' ) );
		
		add_filter( 'manage_edit-cb-dining-page-type_columns', array($this, 'edit_dining_columns' ) );
          add_action( 'manage_cb-dining-page-type_posts_custom_column', array($this, 'manage_dining_columns'),10,2);
      
		 
	
	
	}
	
	
	
function cb_show_cb_dining_enqueue_styles()
{


	wp_register_style( 'cb_dining_css', plugins_url( 'Camana-Dining/css/cb_dining_admin.css' ) );
	wp_enqueue_style( 'cb_dining_css' );

}
function manage_dining_columns( $column, $post_id ) {

	switch( $column ) {
		
		case "diningtype":
		
		
			$s =  wp_get_post_terms( $post_id, 'DiningCategory' );
	
			echo ($s[0]->name);
			break;
	     case "image":
		    $s = get_post_meta($post_id,'cb-dining_settings');
			$image = $s[0]['cb-dining-pic-url'];
			$output = '<img class="cb_dining_img_display" src = ' . $image . ' /> ';
			echo $output;
		 
		    break;
		default:
                return print_r($column,true); //Show the whole array for troubleshooting purposes
       
    }
	
	

}

function edit_dining_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
	    'image' => __('Image'),
		'diningtype' => 'Dining Type',
		'date' => __( 'Date' )
	);

	return $columns;
}
	
	
	function add_dining_metaboxes()
	{
	
		add_meta_box('cb-dining-metabox', "<h1> Camana Bay Dining</h1>", array(&$this,'cb_dining_plugin_metabox_page'),'cb-dining-page-type' , 'normal','high');
		
	
	}
	
	
	/******************************************************
	*
	*		create_dining
	*
	*		Function to create a New Dining Entry
	*
	******************************************************/
	public function create_dining( $post_id, $post, $update )
	{
		
	
		
		$post_data = array();
		$post_data['cb-dining-pic-url'] = $_POST['cb-dining-pic-url'];
		$post_data['cb-dining-link-text'] = $_POST['cb-dining-link-text'];
		update_post_meta( $post_id, 'cb-dining_settings', $post_data );
		
	}
	
	/********************************************************************************************
	*
	*   create_dining_custom_post_type
	*
	*	Inserts the initial post for create dining element
	*
	*
	*
	****************************************************************************************/
	
	
	
	
	
	
	public function register_admin_menu() {

		add_submenu_page( 'cb-dining-plugin-menu', 'Camana Bay Dining List', 'Camana Bay Dining List ', 'manage_options', 'cb-dining-listing',array($this,'cb_dining_plugin_listing'))	;	
		add_meta_box('cb-dining-metabox', "<h1> Camana Bay Dining</h1>", array(&$this, 'cb_dining_plugin_metabox_page'),'cb-dining-page-type' , 'normal','high');
		
		
	}
	public function cb_dining_plugin_listing($arg)
	{
	$wp_list_table = new _cb_dining_list_table();
	$wp_list_table->prepare_items();
	echo '<div class="wrap"><h2>' . esc_html( __( 'Camana Bay Dining', 'cb-dining' ) ) ;
	echo ' <a href="' . esc_url( menu_page_url( 'cb-dining-plugin-menu', false ) ) . '" class="add-new-h2">' . esc_html( __( 'Add New', 'cb-dining' ) ) . '</a></h2>';

?> <form id="cb-dining-filter"" method="get"> 
	<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
  
<?php
	
	$wp_list_table->display();
?> </form> <?php
	echo '</div>';
	
	
	
	}
	/********************************************************
	 *
	 *		cb_dining_plugin_options_page
	 *
	 *		Options page and form for Dining Plugin
	 *
	 *
	 *
	 **************************************************/
	public function cb_dining_plugin_options_page()
	{

	
		
		
		
	   ?>
    <div class="wrap">

        <form accept-charset="UTF-8" action="<?php echo admin_url( 'admin-post.php'); ?>" method="post">
		<input type="hidden" name="action" value="<?php echo $this->action ?>"  >
		<input type="hidden" name="edit_id" value="<?php echo $id ?>"  >

		
	
          
		 
            <?php 
				
				
			
			
			
	
	
         submit_button();



			 ?>
		 
        </form>
    </div>

    <?php
	
	}
public function cb_dining_taxonomy_init()
{
	
	$labels = array(
        'name'              => 'Restaurant Types',
        'singular_name'     => 'Restaurant Type',
        'search_items'      => 'Search Dining Categories',
        'all_items'         => 'All Image Catories',
        'parent_item'       => 'Parent Dining Category',
        'parent_item_colon' => 'Parent Dining Category:',
        'edit_item'         => 'Edit Dining Category',
        'update_item'       => 'Update Dining Category',
        'add_new_item'      => 'Add New Dining Category',
        'new_item_name'     => 'New Dining Category Name',
        'menu_name'         => 'Dining Category',
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'query_var' => true,
        'rewrite' => true,
        'show_admin_column' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           =>  'dining-images' ,
    );
 
    register_taxonomy( 'DiningCategory', 'cb-dining-page-type' ,$args);
	
}	
	/*****************************************************************
	 *
	 *		cb_dining_plugin_metabox_page
	 *
	 *		put the fields in a metabox
	 *
	 *
	 ****************************************************************/
	public function cb_dining_plugin_metabox_page($post, $metabox)
	{
	
		
		$id =  $post->ID;
		
		
		if (isset($id))
		{
			
			$post_meta = get_post_meta($id, 'cb-dining_settings');
			$post_title = get_the_title( $id );
			$cb_pic_url = $post_meta[0]['cb-dining-pic-url'];
			$cb_dining_link_text = $post_meta[0]['cb-dining-link-text'];
			
		
		}
		
		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__FILE__) . '../js/upload-media.js', array('jquery'));
      

        wp_enqueue_style('thickbox');
		
	
			
?>	
		<p>
			<h3>Best size for image is 320px by 320px</h3>
            <label for="cb-dining-pic-url"><?php _e( 'Display Image:' ); ?></label>
            <input name="cb-dining-pic-url" id="cb-dining-pic-url" type="text" size="50"  value="<?php echo esc_url( $cb_pic_url ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
<?php
			printf(
            '<label for "cb-dining-link-text">Link: </label><input type="text" size="50"  name="cb-dining-link-text" value="%s" />',
             isset( $cb_dining_link_text ) ? esc_attr( $cb_dining_link_text) : ''
        );
	}
	
	/*****************************************************************
	 *
	 *		cb_dining_plugin_init
	 *
	 *		register the Dining page type
	 *
	 *
	 ****************************************************************/
	
	public function cb_dining_plugin_init() {
  

		register_post_type( 'cb-dining-page-type',
        array(
            'labels' => array(
                'name' => 'Camana Bay Dining',
                'singular_name' => 'Camana Bay Dining',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Camana Bay Dining Entry',
                'edit' => 'Edit',
                'edit_item' => 'Edit Camana Bay Dining Entry',
                'new_item' => 'New Camana Bay Dining Entry',
                'view' => 'View',
                'view_item' => 'View Camana Bay Dining Entry',
				 'all_items' => __( 'All Camana Bay Dining', 'cb-dining' ),	
                'not_found' => 'No Camana Bay Dining found',
                'not_found_in_trash' => 'No Camana Bay Dining found in Trash',
                'parent' => 'Parent Camana Bay Dining Entry',
				 'menu_name'=>  'Camana Bay Dining'
				
            ),
 
            
            'menu_position' => 15.5,
            'supports' => array( 'title' ),
            'taxonomies' => array( 'DiningCategory' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => false,
			'exclude_from_search' => true,
                'publicly_queryable' => false,
                'show_in_nav_menus' => false,
			'query_var' => false,
                'rewrite' => false,
				'public' => true,
				'show_ui' => true,
				'show_in_admin_bar' => true,
				'show_in_menu' =>true,
						'show_in_nav_menus' => true,
              
                
			)
		);
		
		
	
		
	}

}

endif;

add_action( 'plugins_loaded', array( 'CamanaDiningPlugin', 'init' ), 10 );


  
  

  
  
  
  
  
  
  
