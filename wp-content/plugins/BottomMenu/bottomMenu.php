<?php
/*
Plugin Name: Camana Bay Bottom Menu Plugin
Plugin URI: http://www.camanabay.com/
Description: Bottom Menu Plugin
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php
/******************************************************************************
*
*	bottomMenu.php 
*
*	Main file for the bottom menu plugin. 
*
*	Bottom menu is not the footer menu.  It goes on the bottom of every page.  
*
*
*
*
*********************************************************************************/

require_once 'includes/menuDisplay.php';



class __CamanaBay_bottom_menu_plugin{
	private $options = array();
public function __construct()
    {
		
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'init', array( $this, 'page_init' ) );
		add_action ('admin_menu', array($this,'admin_page_setup'));
		
		
		$this->counter=0;
	}
	
	public function add_plugin_page()
	{
        // This page will be under "Settings"
        
		add_options_page( 'bottom_menu_util',
							'Bottom Menu',
							'manage_options',
							'bottom_menu_utility',
							array( $this, 'create_admin_page' ) );
		
		add_action( 'admin_enqueue_scripts', array( $this,'cb_bottom_menu_admin_styles' ));		
				
	}	
	function cb_bottom_menu_admin_styles()
	{
	
		 wp_register_style( 'CB_Bottom_Menu_StyleSheets', plugins_url('css/style.css', __FILE__) );
		wp_enqueue_style('CB_Bottom_Menu_StyleSheets');
		wp_register_script( 'CB_Bottom_TabbedScript', plugins_url( 'js/tabbed.js', __FILE__ ) );
		wp_enqueue_script( 'CB_Bottom_TabbedScript', plugins_url( 'js/tabbed.js', __FILE__ ) , array(), '1.0.0', true );
		
		wp_enqueue_media();
		wp_enqueue_script('thickbox');
		wp_enqueue_script('media-upload');
        wp_register_script('cb-bmw-admin-js', WP_PLUGIN_URL.'/BottomMenu/js/my-admin.js', array('jquery'));
        wp_enqueue_script('cb-bmw-admin-js');
		wp_enqueue_style('thickbox');
	}
	public function create_admin_page()
    {
	
		$this->options = get_option('bottom_menu_manager_settings');
		
	/* lay out the 5 panels  */
	?>
		
		<div class="wrap">

            <?php screen_icon(); ?>

            <h2><?php _e('Bottom Menu Block','cb-bottom-menu-text-domain' );?> </h2>         

            <form id="cb_bottom_menu_admin" name="submit_button(); " method="post" action="options.php">

			<div class="tabbedPanels">

			<ul class="tabs">

				<li><a href="#panel1" tabindex="1"><?php _e('First Menu Item','cb-bottom-menu-text-domain' );?></a></li>

				<li><a href="#panel2" tabindex="2"><?php _e('Second Menu Item','cb-bottom-menu-text-domain' );?></a></li>
				<li><a href="#panel3" tabindex="3"><?php _e('Third Menu Item','cb-bottom-menu-text-domain' );?></a></li>
				<li><a href="#panel4" tabindex="4"><?php _e('Fourth Menu Item','cb-bottom-menu-text-domain' );?></a></li>
				<li><a href="#panel5" tabindex="5"><?php _e('Fifth Menu Item','cb-bottom-menu-text-domain' );?></a></li>

			</ul>

			<div class="panelContainer">
<?php
				// This prints out all hidden setting fields

					settings_fields( 'manage_bottom_menu' ); 

			?> <div id="panel1" class="panel"> 

			<?php

					do_settings_sections( 'bottom-menu-setting-admin1' );

				   

            ?>
				</div>
			<div id="panel2" class="panel"> 
			<?php

					do_settings_sections( 'bottom-menu-setting-admin2' );

				   

            ?>
			</div>
			<div id="panel3" class="panel"> 
			<?php

					do_settings_sections( 'bottom-menu-setting-admin3' );

				   

            ?>
			</div>
			<div id="panel4" class="panel"> 
			<?php

					do_settings_sections( 'bottom-menu-setting-admin4' );

				   

            ?>
			</div>
			<div id="panel5" class="panel"> 
			<?php

					do_settings_sections( 'bottom-menu-setting-admin5' );

				   

            ?>
			
			</div>
			</div>
		</div>
	<?php	submit_button(); ?>
		</form>
	
	<?php
	
	}
	public function print_section_info($arg)
	{
	
		$my_args=array(
			'id' =>$arg['id'],
			'i' => 0,
			'field_id' =>''
		
		);
		for ($i=1;$i<=5;$i++)
		{
		$my_args['i'] = $i;
		$my_args['field_id']= 'menu_text_'. $i;
		add_settings_field(

            $arg['id'] . '_text', // ID

            __('Menu Text:','cb-bottom-menu-text-domain' ) , // Title 

            array( $this, 'menu_text_cb' ), // Callback

            'bottom-menu-setting-admin' . $i, // Page

            $arg['id'], // Section           
			$my_args
        );
		
		$my_args['field_id']= 'centerTitle_'. $i;
		add_settings_field(

            $arg['id'] . '_centerTitle', // ID

            __('Center Title:','cb-bottom-menu-text-domain' ) , // Title 

            array( $this, 'menu_text_cb' ), // Callback

           'bottom-menu-setting-admin' . $i, // Page

            $arg['id'], // Section          
			$my_args
        );
	


	$my_args['field_id']= 'centerText_'. $i;
		add_settings_field(

           $arg['id'] . '_centerText', // ID

            __('Text:','cb-bottom-menu-text-domain' ) , // Title 

            array( $this, 'text_cb' ), // Callback

            'bottom-menu-setting-admin' . $i, // Page

            $arg['id'], // Section        
			$my_args
        );  

		
		$my_args['field_id']= 'centerbuttonText_'. $i;
		add_settings_field(

           $arg['id'] . '_centerbuttonText_', // ID

            __('Center Button Text:','cb-bottom-menu-text-domain' ) , // Title 

            array( $this, 'menu_text_cb' ), // Callback

            'bottom-menu-setting-admin' . $i, // Page

            $arg['id'], // Section        
			$my_args
        );    
		$my_args['field_id']= 'centerbuttonLink_'. $i;
		add_settings_field(

           $arg['id'] . '_centerbuttonLink_', // ID

            __('Center Button Link:','cb-bottom-menu-text-domain' ) , // Title 

            array( $this, 'menu_text_cb' ), // Callback

            'bottom-menu-setting-admin' . $i, // Page

            $arg['id'], // Section        
			$my_args
        );    
		$my_args['field_id']= 'linktoImage_'. $i;
		add_settings_field(

           $arg['id'] . '_linktoImage_', // ID

            __('Link To Image:','cb-bottom-menu-text-domain' ) , // Title 

            array( $this, 'menu_text_cb' ), // Callback

            'bottom-menu-setting-admin' . $i, // Page

            $arg['id'], // Section        
			$my_args
        );    
		
		
		
		$my_args['field_id']= $i;
		add_settings_field(

           $arg['id'] . '_linktoButton_', // ID

            __('','cb-bottom-menu-text-domain' ) , // Title 

            array( $this, 'link_button_cb' ), // Callback

            'bottom-menu-setting-admin' . $i, // Page

            $arg['id'], // Section        
			$my_args
        );    
		
	
		}
	}
	public function link_button_cb($arg)
	{
		$i = $arg['i'];
		$field_id = $arg['field_id'];

	
		
		 printf(
		
			    '<input type="button" id="'.$field_id.'"   name="bottom_menu_manager_settings['.$field_id.']" value="Select Image"  class="imageButton"/>',
            isset( $this->options[$field_id] ) ? esc_attr( $this->options[$field_id]) : ''
		
		);
		
	}
	
	
	public function text_cb($arg)
	{
		$i = $arg['i'];
		$field_id = $arg['field_id'];
		
		

	$text_area_html = sprintf(
			
			'<textarea class="widefat" id="%s" name="bottom_menu_manager_settings[%s]"  >%s</textarea></p>',
				$field_id,
				$field_id,
			     isset( $this->options[$field_id] ) ? esc_attr( $this->options[$field_id]) : '' );	

		

      
		echo $text_area_html;
	
	}
	public function menu_text_cb($arg)
	{

	$i = $arg['i'];
	$field_id = $arg['field_id'];
		 printf(

            '<input type="text" id="'.$field_id.'" size="35"  name="bottom_menu_manager_settings['.$field_id.']" value="%s" />',

            isset( $this->options[$field_id] ) ? esc_attr( $this->options[$field_id]) : ''

		

			

        );
	
	
	}
	public function sanitize( $input )

    {

	 $new_input = $input;
	 return $new_input;
	
	}
	public function admin_page_setup()
	{
		register_setting(

            'manage_bottom_menu', // Option group

            'bottom_menu_manager_settings', // Option name

            array( $this, 'sanitize' ) // Sanitize

        );

        for ($i=1;$i<=5;$i++)
		{
			$menu_string='menu'.$i;
			$title_string='<h2>Menu #'.$i.'</h2>';
			$call_back= 'print_section_info';
			$page = 'bottom-menu-setting-admin'.$i;
			
			add_settings_section(
				$menu_string,
				__($title_string, 'cb-bottom-menu-text-domain'),
				array($this, $call_back),
				$page
			);
				
		
		
		
		}

      
	}
	public function page_init()
    { 

		add_action( 'wp_ajax_menu_item_func', array($this,'cb_menu_item_func') );
		add_action( 'wp_enqueue_scripts', array($this,'my_enqueue' ));
		
		
	
	}
	function my_enqueue()
	{
	
	
		
		wp_localize_script( 'cb_bmw_menu_change', 'ajax_object',
				array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ));
	}
}
 new __CamanaBay_bottom_menu_plugin();
?>