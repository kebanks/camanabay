/************************************************************************************
*
*   Jquery file for Bottom menu Widget
*
*	Controls the changes for the bottom menu widget
*
*
*
*
************************************************************************************/


jQuery(document).ready(function($) {
		
		
		var options=myAjax.options;
		var indicator=myAjax.indicator;
		console.log('indicator is ' + indicator);

		
		 /*  put in the selection indicator */
		jQuery('.cb_bmw_indicator_1').attr('src',indicator);
		jQuery(".cb_bmw_middle_title").append(options['centerTitle_1']);
		jQuery(".cb_bmw_middle_text").append(options['centerText_1']);
		var link = '<img  src="' +options['linktoImage_1'] + '"/>';

		jQuery(".cb_bmw_image").append(link);
		link = '<a href=\"'+ options['centerbuttonLink_1'] + '\">' + options['centerbuttonText_1']  + '</a>';


		jQuery("#cb_bmw_middle_bottom").append(link);
		
		
	/* clear out the indicators before the new one is put in */	
	function remove_old(notselected)
	{
		var className;
		for (i=1;i<=5;i++)
		{
			className = '.cb_bmw_indicator_'+i;
			jQuery(className).attr('src',notselected);
		
		
		}
	
	
	}
	 jQuery('#cb_bmw_middle_container').click(function(e) {
	 
		var  x = jQuery('#cb_bmw_middle_bottom a').attr("href");
	 
	    
		window.location.href = x;
			
	 
	 
	 });
	 jQuery( "#cb_bmw_middle_container" ).hover(
  function() {
		jQuery(this).css('cursor','pointer');
  
  }
);
	 
	 
// click function 
	 jQuery('.cb_menu_item').click(function(e) {
	  
	   
		var id = jQuery (this).attr('id');
		var options=myAjax.options;
		e.preventDefault();
		
		var indicator=myAjax.indicator;
		var notselected=myAjax.notselected;
		var className = '.cb_bmw_indicator_'+id;
		
		remove_old(notselected);
		jQuery(".cb_bmw_middle_title").text(options['centerTitle_'+id]);

		jQuery(".cb_bmw_middle_text").text(options['centerText_'+id]);
			
			jQuery("#cb_bmw_middle_bottom a").text(options['centerbuttonText_'+id]);
			jQuery("#cb_bmw_middle_bottom a").attr("href", options['centerbuttonLink_'+id]);
			 jQuery('img',jQuery('.cb_bmw_image')).attr('src',options['linktoImage_'+id]);
			 
			/* put in the new indicator */ 
			jQuery(className).attr('src',indicator);
			

	});



});

