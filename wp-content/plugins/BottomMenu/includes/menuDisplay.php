<?php
/********************************************************************************************************
*
*
*	Menudisplay.php
*
*	Handles the display for the bottom menu
*	Bottom menu has 5 selections and each time the user selects a different one, the picture on the right changes, the middle text and title change
*
***************************************************************************************************/

class _Menu_Display{

	private $options;
	public function __construct()

    {

		$this->options = get_option('bottom_menu_manager_settings');

		add_shortcode( 'showCB_BottomMenu', array( $this, 'cb_show_Bottom_Menu_func' ) );

		add_action( 'wp_enqueue_scripts', array( $this,'cb_show_Bottom_Menu_enqueue_styles' ));
	}
	function cb_show_Bottom_Menu_func($atts, $content="" )

	{
		$url = plugins_url('images/cbw_notselected.png',__DIR__);
		$string1='<div id="cb_bmw_outer_container">
			<div id="cb_bmw_left_menu" class="cb_bmw_inner_container  ">
				<div><ul id="cb_bmw_right_menu">
				';
		$menu_display = '';
				for ($i=1;$i<=5;$i++)
				{	$index = 'menu_text_' . $i;
					$menu_display.= "<li id=". $i . " class='cb_menu_item'><a href='#'>" . $this->options[$index] ."<img class='cb_bmw_indicator_".$i ."' src=" . $url . " /></a></li>";  
					$test = "<li id=". $i . " class='cb_menu_item'><a href='#'>" . $this->options[$index] ."</a></li>";
		
					
				}
		$string2= '	
				</ul>
				</div>
		
			</div>
			<div id="cb_bmw_middle_container" class="cb_bmw_inner_container">
				<h2 class="cb_bmw_middle_title"></h2>
			
				<p  class="cb_bmw_middle_text"></p>
			
				<div id="cb_bmw_middle_bottom">
					
				</div>
			
			
			</div>
			<div class="cb_bmw_inner_container">
				<div class="cb_bmw_image"></div>
			</div>
		</div>';
		return ($string1.$menu_display.$string2);
	//	

	
	}
	function cb_show_Bottom_Menu_enqueue_styles()
	{
	
		wp_register_style( 'my-plugin4', plugins_url( 'BottomMenu/css/menuwidget.min.css' ) );
		wp_enqueue_style( 'my-plugin4' );
			
		 wp_register_script( 'bottom-menu-script1', plugins_url( '/BottomMenu/js/cb_bmw_menu_change_plugin.js' ), array('jquery') );
		wp_enqueue_script('bottom-menu-script1');
		$params = array(
  'options' => $this->options,
  'indicator' => plugins_url('BottomMenu/images/cbw_indicator.png'),
  'notselected' =>plugins_url('BottomMenu/images/cbw_notselected.png'),

);
		// in javascript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
			wp_localize_script( 'bottom-menu-script1', 'myAjax',$params);
				

	
	}
	
	
	
	public static function instance() {

		 new _Menu_Display;

			

	}
}

	
	
	function cb_init_Bottom_Menu_Display(){


		return _Menu_Display::instance();

	}

add_action( 'plugins_loaded',  'cb_init_Bottom_Menu_Display'  );

?>