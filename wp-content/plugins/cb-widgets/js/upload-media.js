/*jQuery(document).ready(function($) {
    $(document).on("click", ".upload_image_button", function() {

        jQuery.data(document.body, 'prevElement', $(this).prev());

        window.send_to_editor = function(html) {
            var imgurl = jQuery('img',html).attr('src');
            var inputText = jQuery.data(document.body, 'prevElement');

            if(inputText != undefined && inputText != '')
            {
                inputText.val(imgurl);
            }

            tb_remove();
        };

        tb_show('', 'media-upload.php?type=image&TB_iframe=true');
        return false;
    });
});

*/
jQuery(document).ready(function($){
	var _custom_media = true,
	_orig_send_attachment = wp.media.editor.send.attachment;

	//$('.upload_image_button').click(function(e) {
	$(document).on("click", ".upload_image_button", function(e) {
	console.log('button clicked');
		var send_attachment_bkp = wp.media.editor.send.attachment;
		
		
		var prevElement = jQuery.data(document.body, 'prevElement', $(this).prev());
		_custom_media = true;
	wp.media.editor.send.attachment = function(props, attachment){
		if ( _custom_media ) {
				
				jQuery(prevElement).val(attachment.url);
				 wp.media.editor.send.attachment = send_attachment_bkp;
			} else {
				return _orig_send_attachment.apply( this, [props, attachment] );
			};
		}

		//wp.media.editor.open(button);
		wp.media.editor.open();
		return false;
	});

	
});