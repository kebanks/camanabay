<?php
/********************************************************************************
*  
*   Blog article widget
*
*
*	
*	 
*
*	
*
*
*
*
*********************************************************************************/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	

class __CamanaBay_blog_article_widget extends WP_Widget{
	
	// constructor
	
	
		public function __construct($id = 'cb_widget_blog_article', $description = 'Blog Article Widget', $opts = array()) {

	
		$opts['description'] = 'Displays a blog article, pic and title';
		parent::__construct($id,$description,$opts);
		
      
		
		
	}
	
private function get_all_our_posts(){
	global $posts;
	
	$args = array(
	
			'post_status'      => 'publish',
			'suppress_filters' => true,
			'post_type' => 'post',
			'numberposts' => -1
		);
	$posts = get_posts($args);

	return $posts;

}	
	
	// widget form creation
	function form($instance) {	
		
	
	// Check values
if( $instance) {

     
   	$cb_blog_post_id=esc_attr($instance['cb-blog-article-setting']); 
	
} else {

	
     
	 
	 $cb_blog_post_id='';
	
}

	

	$posts = $this->get_all_our_posts();


	$post_lists = array();
		
		foreach ($posts as $key => $p) {
	
			$post_lists[$key]['id'] = $p->ID;
			$post_lists[$key]['title'] = $p->post_title;
			
		}
?>
		<p><label for="<?php echo $this->get_field_id( 'cb-blog-article-setting' ); ?>"></label><?php _e('Select your article:', 'cb_widget_blog_article_'); ?></label>
		<select id="<?php echo $this->get_field_id( 'cb-blog-article-setting' ); ?>" style="min-width:150px; min-height:50px; height:auto;" size="3" name="<?php echo $this->get_field_name( 'cb-blog-article-setting' ); ?>">

<?php 

		foreach($post_lists as $key => $p)
				{ 
				
				if ($p['id'] ==  $cb_blog_post_id){
?>
				
					<option id="<?php echo 'list_' .$key ?>" value="<?php echo $p['id'] ?>"  selected><?php echo $p['title'] ?></option>
<?php 	
				}else{

?>
			<option id="<?php echo 'list_' .$key ?>" value="<?php echo $p['id'] ?>"><?php echo $p['title'] ?></option>

<?php			}
			}
?>


		</select>

<?php

		
	}
   
	// widget update
	function update($new_instance, $old_instance) {
	 $instance = $old_instance;
		
		$value = $new_instance['cb-blog-article-setting'];
		
		
      // Fields

	  
     return $instance;
	}
	function catch_that_image_oustide_loop($some_id) {
		$fetch_content = get_post($some_id);
		$content_to_search_through = $fetch_content->post_content;
		$first_img = "";
		ob_start();
		ob_end_clean();
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content_to_search_through, $matches);
		
		$first_img = $matches[1][0];

		if(empty($first_img)) {
		$first_img = "";
	}
	return $first_img;
}// 
	// widget display
	function widget($args, $instance) {

		global $post;
		$post_id = $instance['cb-blog-article-setting'];
		$post = get_post($post_id);
	
		
		wp_register_style( 'cb-blog-article-css', plugins_url( 'cb-widgets/css/cb-blog.min.css' ) );
		wp_enqueue_style( 'cb-blog-article-css' );
?>
		<div class="cb-blog-featured-date-time-br">

			
			<div class="cb-blog-day">
				<?php echo get_the_time('l',$post_id) ; ?>
			
			</div>
			<div class="cb-blog-date">
				<?php echo get_the_time('d F Y',$post_id); ?>
			
			</div>
		</div>
		
<?php

		$first_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
		
		$link = get_post_permalink( $post_id);
?>
		<a href="<?php echo $link ?>"><img src="<?php echo $first_image ?>" /></a>
		<div class="cb-blog-list-title">
		<a href="<?php echo $link ?>"> <?php echo $post->post_title ?></a>
		
		</div>
<?php 
	}
}