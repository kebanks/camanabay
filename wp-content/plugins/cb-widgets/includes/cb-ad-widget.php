<?php
/********************************************************************************
*  
*   Ad Widget
*
*	 
* 	 Picture for promotion in background and link
*	
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	

class __CamanaBay_ad_widget extends WP_Widget{
	var $textdomain;
	// constructor
	
	  function __construct($id = 'cb_ad_widget', $description = 'Camana Bay Ad Widget', $opts = array()) {
	
		 add_action( 'admin_enqueue_scripts', array(&$this, 'my_custom_load') );

		$opts['description'] = "Picture in background for the ad and link ";
		parent::__construct($id,$description,$opts);
		
		
	}
	
	function my_custom_load() {    
    //  this is for the picture links
		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__DIR__) . 'js/upload-media.js', array('jquery'));
      

        wp_enqueue_style('thickbox');
		 wp_enqueue_style( 'wp-color-picker' );        
        wp_enqueue_script( 'wp-color-picker' );    
		
    }
	
	// widget form creation
	function form($instance) {	

	
	// Check values
	if( $instance) {
		$cb_ad_text = esc_attr($instance['cb-ad']);
		$cb_ad_text_destination = esc_attr($instance['cb-ad-destination']);
		$cb_ad_pic_url = esc_attr($instance['cb-ad-pic-url']);
		$cb_ad_pic_height = esc_attr($instance['cb-ad-height']);
			$cb_ad_text_color=esc_attr($instance['cb-ad-text-color']); 
	
		} else {
	 
		$cb_ad_text = "";
		$cb_ad_text_destination="";
		$cb_ad_pic_url='';
		$cb_ad_pic_height = '';
		$cb_ad_text_color = '';
	}
?>
	<p>
            <label for="<?php echo $this->get_field_name( 'cb-ad-pic-url' ); ?>"><?php _e( 'Display Image :' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'cb-ad-pic-url' ); ?>" id="<?php echo $this->get_field_id( 'cb-ad-pic-url' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $cb_ad_pic_url ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
	
	<p><label for="<?php echo $this->get_field_id('cb-ad-height'); ?>"><?php _e('Ad height (width should be 1500px):', 'cb_ad_widget'); ?></label>
	<input  size="10" required id="<?php echo $this->get_field_id('cb-ad-height'); ?>" name="<?php echo $this->get_field_name('cb-ad-height'); ?>" type="text" value="<?php echo $cb_ad_pic_height; ?>" />
	</p>
	
	<p><label for="<?php echo $this->get_field_id('cb-ad'); ?>"><?php _e('Ad Link Text (without the word "here" ):', 'cb_ad_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-ad'); ?>" name="<?php echo $this->get_field_name('cb-ad'); ?>" type="text" value="<?php echo $cb_ad_text; ?>" />
	</p>
	<p><label for="<?php echo $this->get_field_id('cb-ad-destination'); ?>"><?php _e('Ad  Link:', 'cb_ad_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-ad-destination'); ?>" name="<?php echo $this->get_field_name('cb-ad-destination'); ?>" type="text" value="<?php echo $cb_ad_text_destination; ?>" />
	</p>
	<p><label for="<?php echo $this->get_field_id( 'cb-ad-text-color' ); ?>"></label><?php _e('Text Color:', 'cb_ad_color_picker'); ?></label>
	<input class="my-color-picker" type="text" id="<?php echo $this->get_field_id( 'cb-ad-text-color' ); ?>" name="<?php echo $this->get_field_name( 'cb-ad-text-color' ); ?>" value="<?php echo $cb_ad_text_color; ?>" />                            
	</p>
<script type='text/javascript'>
      jQuery(document).ready(function($) {
         $('.my-color-picker').wpColorPicker();
			
            });
        </script>	
<?php	
		
}
   
	// widget update
	function update($new_instance, $old_instance) {
	 
		 $instance = $old_instance;
      // Fields

	  $instance['cb-ad'] = $new_instance['cb-ad'];
	  $instance['cb-ad-destination'] = $new_instance['cb-ad-destination'];
	  $instance['cb-ad-pic-url'] = $new_instance['cb-ad-pic-url'];
	  $instance['cb-ad-height'] = $new_instance['cb-ad-height'];
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		wp_register_style( 'cb-ad-css', plugins_url( 'cb-widgets/css/cb-ad.css' ) );
		wp_enqueue_style( 'cb-ad-css' );
		$cb_ad_text = '';
		$cb_ad_pic_url = '';
		$cb_ad_text_destination='';
		$cb_ad_height = '';
		$cb_ad_text_color='';
		
		if (isset($instance['cb-ad']))
			$cb_ad_text= esc_attr($instance['cb-ad']);
		
		if (isset($instance['cb-ad-destination']))
			$cb_ad_text_destination= esc_attr($instance['cb-ad-destination']);
		if (isset($instance['cb-ad-pic-url']))
			$cb_ad_pic_url = esc_attr($instance['cb-ad-pic-url']);
		if (isset( $instance['cb-ad-height']))
			$cb_ad_height =  $instance['cb-ad-height'];
		if (isset ($instance['cb-ad-text-color']))
			$cb_ad_text_color=$instance['cb-ad-text-color']; 
			$id = rand();
			$id1 = rand();

?>		
<script>

	 jQuery(document).ready(function($) {
	 
		var image1 = "<?php echo $cb_ad_pic_url ?>";
		var link = "<?php echo $cb_ad_text_destination ?>";
		var height_background = "<?php echo $cb_ad_height ?>";
		var id = <?php echo $id ?>;
		var id1 = <?php echo $id1 ?>;
		jQuery('#'+id).css('background-image','url("' + image1 + '")');
		jQuery('#'+id).css('height',height_background);
		jQuery('#'+id1).text("<?php echo $cb_ad_text ?>");
		
		jQuery('#'+id1).append(' here');
		var bottom_text = jQuery('#'+id1);
		bottom_text.html(bottom_text.html().replace(/here/ig, '<a href="' + link + '">here</a>')); 
		var textColor= '<?php echo $cb_ad_text_color;?>';
		jQuery('#'+id1).css('color',textColor);
		jQuery('#'+id1 + ' a').css('color',textColor);
	 });

</script>

		<div id="<?php echo $id ?>" class="cb_ad_container">
			<span id="<?php echo $id1 ?>" class="cb_ad_text"> </span>
		</div>
<?php		
	}
}
?>