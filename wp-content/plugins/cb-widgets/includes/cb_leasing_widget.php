<?php
/********************************************************************************
*  
*    Leasing  Widget 
*
*	 Widget that has an image and title, input for leasing and no go button
*
*	 This widget 
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	

class __CamanaBay_leasing_widget_plugin extends __CamanaBay_directory_widget_plugin{
	var $textdomain;
	// constructor
	private $cb_leasing_widget_id;
	  function __construct($id = 'cb_leasing_widget_leasing', $description = 'Camana Bay Leasing', $opts = array()) {
	
		 add_action( 'admin_enqueue_scripts', array(&$this, 'my_custom_load') );
		$this->cb_leasing_widget_id = $id;
		$opts['description'] = 'pic and title text in custom color';
		parent::__construct($id,$description,$opts);
		  
      
		
		
	}
	function my_custom_load() {    

		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__DIR__) . 'js/upload-media.js', array('jquery'));
         wp_register_style( 'cb-leasing-plugin', plugins_url( 'cb-widgets/css/imageBlock.min.css' ) );
		wp_enqueue_style( 'cb-leasing-plugin' );

        wp_enqueue_style('thickbox');
		
    }
	
	
	// widget form creation
	function form($instance) {	
		
	
		
	// Check values
if( $instance) {

     
     $cb_leasing_widget_address = esc_textarea($instance['cb-leasing-widget-address']);
	 $cb_leasing_widget_tel = esc_attr($instance['cb-leasing-widget-tel']);
	$cb_leasing_widget_email=esc_attr($instance['cb-leasing-widget-email']);

} else {

	
     
	 $cb_leasing_widget_tel = '';
	  $cb_leasing_widget_email='';
	 $cb_leasing_widget_address ='';

	 $cb_button_color='';
	
}

		parent::form($instance);

?>


	
	
	<p><label for="<?php echo $this->get_field_id('cb-leasing-widget-address'); ?>"><?php _e('Address:', 'cb_leasing_widget_leasing'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-leasing-widget-address'); ?>" name="<?php echo $this->get_field_name('cb-leasing-widget-address'); ?>" type="text" value="<?php echo $cb_leasing_widget_address; ?>" />
	</p>
	
	<p><label for="<?php echo $this->get_field_id('cb-leasing-widget-tel'); ?>"><?php _e('Telephone:', 'cb_leasing_widget_leasing'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-leasing-widget-tel'); ?>" name="<?php echo $this->get_field_name('cb-leasing-widget-tel'); ?>" type="text" value="<?php echo $cb_leasing_widget_tel; ?>" />
	</p>

	<p><label for="<?php echo $this->get_field_id('cb-leasing-widget-email'); ?>"><?php _e('Email:', 'cb_leasing_widget_leasing'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-leasing-widget-email'); ?>" name="<?php echo $this->get_field_name('cb-leasing-widget-email'); ?>" type="text" value="<?php echo $cb_leasing_widget_email; ?>" />
	</p>
	
	

	
	





<?php
	
		
	}
   
	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $old_instance;
      // Fields

  
      $instance['cb-leasing-widget-address'] = strip_tags($new_instance['cb-leasing-widget-address']);
	  $instance['cb-leasing-widget-tel'] = strip_tags($new_instance['cb-leasing-widget-tel']);
	  $instance['cb-leasing-widget-web'] = strip_tags($new_instance['cb-leasing-widget-web']);
	  $instance['cb-leasing-widget-email'] = strip_tags($new_instance['cb-leasing-widget-email']);
	  
	

     return $instance;
	}


	// widget display
	function widget($args, $instance) {
	
	
		extract( $args );
   // these are the widget options
	//parent::widget($args, $instance);
	$cb_leasing_widget_address ='';
	$cb_leasing_widget_tel ='';
	$cb_leasing_widget_email='';

	$this->my_custom_load();
	$cb_widget_title = esc_attr($instance['cb-widget-title']);
    
	if (isset($instance['cb-leasing-widget-address']))
		$cb_leasing_widget_address = esc_attr($instance['cb-leasing-widget-address']);
	if (isset($instance['cb-leasing-widget-tel']))
		$cb_leasing_widget_tel = esc_attr($instance['cb-leasing-widget-tel']);
	if (isset ($instance['cb-leasing-widget-email']))
		$cb_leasing_widget_email =  esc_attr($instance['cb-leasing-widget-email']);
	$cb_button_color=$instance['cb-button-color'];
	
	
	if (isset($instance['cb-widget-title']))
		$cb_widget_title = esc_attr($instance['cb-widget-title']);
	if (isset($instance['cb-widget-pic-url']))
		$cb_pic_url = esc_attr($instance['cb-widget-pic-url']);
	if (isset ($instance['cb-go-destination-link']))
		$cb_go_destination_link =  esc_attr($instance['cb-go-destination-link']);

	
	 
   echo $before_widget;
   // Display the widget
   echo '<div class="widget-text cb_leasing_widget_leasing_box">';
	
	
	$id= rand();
	$id1 = rand();
	$id2 = rand();
	$id_address = rand();
	$id_tel = rand();
	  
?>
<script>

	 jQuery(document).ready(function($) {
	 
		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		var $id1='#<?php echo $id1 ?>';
		var $id2='#<?php echo $id2 ?>';
		var $id_address='#<?php echo $id_address ?>';
		var $id_tel='#<?php echo $id_tel ?>';
		var id_title = '#'+jQuery('.cb_dir_block_title').attr('id') ;
		
		
		console.log('id1 is ' + $id1);

		
		jQuery($id).css('color',$buttonColor);
		jQuery($id1).css('color',$buttonColor);
		jQuery($id_address).css('color',$buttonColor);
		jQuery($id_tel).css('color',$buttonColor);
		
		
		jQuery($id2).css('color',$buttonColor);
		
		
	 });

</script>
	<div class="cb_out_block_image">
			<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>' target='_blank'>
			<img class="cb_db_image" src=<?php if (isset ($cb_pic_url)) echo "$cb_pic_url" ?> /></a>
		</div>
	<div class="cb_db_image_title_outer">
			<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>' target='_blank'>
			<div id='<?php echo $id ?>' class="cb_leasing_title cb_image_block_text"><?php if (isset ($cb_widget_title)) echo $cb_widget_title?></div>
			</a>
		</div>
		<div class="cb_leasing_info">
			<div  id='<?php echo $id_address ?>'class="cb_leasing_info_address">
	<?php 		echo $cb_leasing_widget_address  ?>
			</div>
			<div class="cb_leasing_detail_info">
			<?php 		echo "<div id='" . $id_tel ."'> TEL: " . $cb_leasing_widget_tel  . "</div>"; ?>
			<?php 		echo "<a class='cb_leasing_web_text' id='" . $id1 ."' href='mailto:" . $cb_leasing_widget_email . "' target='_blank' >click to email</a><br/>" ; ?>
			<?php 		echo "<a class='cb_leasing_web_text'  id='" . $id2."' href=" . $cb_go_destination_link . " target='_blank' >click to visit website</a>" ; ?>
			
			
			</div>
		
		</div>
		
		
		
		
<?php

   
   echo '</div>';
  
   echo $after_widget;
  
	}
}
?>