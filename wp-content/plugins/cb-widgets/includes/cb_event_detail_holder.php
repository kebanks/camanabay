<?php
/********************************************************************************
*
*   Event Detail Place Holder
*
*
*	Parent widget to most of the custom widgets in this section.  Manages the color picker
*
*
*
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');





class __CamanaBay_event_detail_widget extends __CamanaBay_color_picker_widget{
	  static $variables=array();
	// constructor

	  function __construct($id = 'cb_event_detail_placeholder', $description = 'Event Detail Place Holder', $opts = array()) {
				$opts['description'] = "Select the place you want the three column events description";


		parent::__construct($id,$description,$opts);




	}





	// widget form creation
	function form($instance) {



	 global $wpdb;

?>

			<p><label for="<?php echo $this->get_field_id( 'cb-event-holder-event' ); ?>"></label><?php _e('Select your event:', 'cb-event-holder-event'); ?></label>
		 <select id="events" style="min-width:150px; " name="<?php echo $this->get_field_name( 'cb-event-holder-event' ); ?>" >
			 <option value="0">Select Event</option>

		<?php  {
			$query = "SELECT * FROM " .$wpdb->prefix   . "spidercalendar_event   WHERE calendar=1 and published=1";
		    $rows=  $wpdb->get_results($query);

			if (!empty($instance['cb-event-holder-event'] ))
				$event_selected = $instance['cb-event-holder-event'] ;
			else 
				$event_selected = '';

			foreach($rows as $i => $row){


?>
			 <option value="<?php echo $row->id; ?>" <?php if ($event_selected == $row->id) echo  'selected="selected"'; ?>><?php echo $row->title ?></option>
 <?php





			}

		}
?>
		 </select>



<?php
parent::form($instance);
	}
	// widget update
	function update($new_instance, $old_instance) {

	$parent_instance = parent::update($new_instance, $old_instance);


	 $instance = $new_instance;


	 return $instance;

	}

	// widget display
	function widget($args, $instance) {
		global $wpdb;
// check if empty
		if (isset($instance['cb-event-holder-event'])){

		$query = "SELECT * FROM " .$wpdb->prefix   . "spidercalendar_event   WHERE calendar=1  and published=1 and id=" . $instance['cb-event-holder-event'];
		  $rows=  $wpdb->get_results($query);
		  $x = $rows[0];


			$temp = preg_replace("/<img[^>]+\>/i", "", $x->text_for_date);

			$description = preg_replace('/<a href="(.*?)">(.*?)<\/a>/', "\\2", $temp);
		$times = explode("-", $x->time);

			$time_in_24_hour_format  = date("H:i", strtotime($times[0]));
        $time_start = $x->date . ' ' . 	$time_in_24_hour_format;


		$time_in_24_hour_format  = date("H:i", strtotime($times[1]));
		$time_end = $x->date_end . ' ' . 	$time_in_24_hour_format;
		
		
		
		wp_register_style( 'cb_event_detail_calendar_css', plugins_url( '/cb-widgets/css/atc-style-blue.min.css' ) );
		wp_enqueue_style( 'cb_event_detail_calendar_css' );
		
		
		wp_register_style( 'cb_event_detail_placeholder_css', plugins_url( '/cb-widgets/css/cb_event_detail.min.css' ) );
		wp_enqueue_style( 'cb_event_detail_placeholder_css' );


		


		 wp_register_script( 'cb_event_detail_calendar_js', plugins_url() . '/cb-widgets/js/atc.js');
	 wp_enqueue_script( 'cb_event_detail_calendar_js', plugins_url() .  '/cb-widgets/js/atc.js',array( 'jquery' ),"");


		$cb_button_color=$instance['cb-button-color'];
		$id=rand();
		$id1=rand();
		$id2=rand();
		$id_atcb_link=rand();
		$id_color_id = rand();


?>


	<div id="cb_event_detail_column_container">
		<div id="cb_event_detail_inside_container">
			<div class="cb_event_detail_column_item">
			    <div id="<?php echo $id ?>" class="cb_event_detail_column_title">WHEN
				</div>
				<div class="cb_event_detail_line cb_event_detail_left">
					<?php
					if (!$x->repeat){
						echo date("F d", strtotime($x->date)) . " <br /> " . $x->time;
					}else{
						switch($x->repeat_method)
						{

							case 'daily':
								if ($x->repeat == 1)
								{
									echo "Daily";
								}else
								{
									echo "Every " . $x->repeat . " days";
								}
								echo "<br>" . $x->time;


								break;
							case 'weekly':
								$weeks = explode(",", $x->week);
								echo "Every ";
								$output = '';
								foreach($weeks as $w){

								   if (strlen($w) > 0)
									$output .= $w . ", " ;


								}
								if (strlen($output) > 0)
								{


									echo substr($output, 0, -2);

								}

							echo "<br>" .  $x->time;
								break;
							case 'monthly':


								break;
							default:

								break;





						}



					}

				?>  </div>

					<hr class="horizontal-line" />
			</div>

			<div class="cb_vertical-line"> </div>

			<div class="cb_event_detail_column_item">
				<div id="<?php echo $id1 ?>" class="cb_event_detail_column_title">WHERE

				</div>
				<div class="cb_event_detail_line">
						<?php echo $x->location ?>
					<br />

				</div>

				<hr class="horizontal-line" />
			</div>
			<div class="cb_vertical-line"> </div>
			<div class="cb_event_detail_column_item">

				<div  id="<?php echo $id2 ?>"class="cb_event_detail_column_title">HOW

				</div>
		
				<div id="<?php echo $id_color_id ?>"  class="cb_event_detail_line  cb_event_calendar"   data-variable-one="<?php echo $cb_button_color; ?>">
					<span class="addtocalendar atc-style-blue">
        <a id="<?php echo $id_atcb_link ?>"  class="atcb-link">Add to Calendar</a> <!-- You can change button title by adding this line -->
        <var class="atc_event">
            <var class="atc_date_start"><?php echo $time_start ?></var>
            <var class="atc_date_end"><?php echo $time_end?></var>
            <var class="atc_timezone">America/Jamaica</var>
            <var class="atc_title"><?php echo $x->title?></var>
            <var class="atc_description"><?php echo $description ?></var>
            <var class="atc_location"><?php echo $x->location?></var>

        </var>
    </span>

				</div>







			</div>
		</div>
	</div>
<script>
jQuery(document).ready(function($) {

		var buttonColor= '<?php echo $cb_button_color;?>';
		var id= '#<?php echo $id;?>';
		var id1= '#<?php echo $id1;?>';
		var id2= '#<?php echo $id2;?>';
		var id_atcb_link = '#<?php echo $id_atcb_link;?>';
		var border_info = '2px solid ' + buttonColor;
		var id3 = '<?php echo $id_color_id ?>';


		jQuery(id).css('color',buttonColor);
		jQuery(id1).css('color',buttonColor);
		jQuery(id2).css('color',buttonColor);
		jQuery(id_atcb_link).css('color',buttonColor);
		jQuery('.atcb-item-link').css('color',buttonColor);
		jQuery(id_atcb_link).css('border',border_info);
		
		jQuery('.atc-style-blue .atcb-list').css('border',border_info + " !important");
	
			
		
		 
		 jQuery(id_atcb_link).click(function(){
			jQuery('.cb_event_detail_line .addtocalendar .atcb-list').css('border-left',border_info);
			jQuery('.cb_event_detail_line .addtocalendar .atcb-list').css('border-right',border_info);
			jQuery('.cb_event_detail_line .addtocalendar .atcb-list').css('border-bottom',border_info);
		
			jQuery('.atcb-item-link').css('color',buttonColor);
			
		 });
	
	  
	
	     

});
</script>
<?php




		}


	}
}
?>
