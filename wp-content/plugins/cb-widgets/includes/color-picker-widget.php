<?php
/********************************************************************************
*  
*   Color Picker Widget
*
*
*	Parent widget to most of the custom widgets in this section.  Manages the color picker
*	 
*
*	
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	

class __CamanaBay_color_picker_widget extends WP_Widget{
	var $textdomain;
	// constructor
	
	  function __construct($id = 'cb_widget_color_picker', $description = 'Color Picker Widget', $opts = array()) {
	
		 add_action( 'admin_enqueue_scripts', array(&$this, 'my_custom_load') );


		parent::__construct($id,$description,$opts);
	
      
		
		
	}
	function my_custom_load() {   
	
        wp_enqueue_style( 'wp-color-picker' );        
        wp_enqueue_script( 'wp-color-picker' );    
		 wp_register_style( 'cb_widget_color_picker_css', plugins_url( 'cb-widgets/css/colorpicker.min.css' ) );
		wp_enqueue_style( 'cb_widget_color_picker_css' );
		
    }
	
	
	// widget form creation
	function form($instance) {	
		
	
	// Check values
if( $instance) {

     
   	$cb_button_color=esc_attr($instance['cb-button-color']); 
	
} else {

	
     
	 
	 $cb_button_color='';
	
}

	
        // Merge the user-selected arguments with the defaults
      //  $instance = wp_parse_args( (array) $instance, $defaults ); 
$id1=rand();
?>

<script type='text/javascript'>
      jQuery(document).ready(function($) {
         $('.my-color-picker').wpColorPicker();
			jQuery('.cb_color_select').click(function(e) {
					e.preventDefault();
					var id = '#'+jQuery (this).attr('id');
					var hexText =  jQuery(id).text();

				var x = '#<?php echo $this->get_field_id( 'cb-button-color' ); ?> ';
				var id1="#<?php echo $id1 ?>";
		
				
				var cr= jQuery(id1).find(".wp-color-result");
		jQuery(x).val(hexText);
		//jQuery('.wp-color-result').css({backgroundColor: hexText});
		jQuery(cr).css({backgroundColor: hexText});
		jQuery(x).wpColorPicker('color', hexText);
	 
	 });
            });
        </script>


	
<?php $color_array = array( "#a87eb1" ,"#cb003c", "#FFB718 ", "#faa519", "#77bc1f","#64cbc8","#00b6dd","#008aab","#d7d1c4","#c4e76a");
	echo "<h2>Colors</h2>";
	echo "<p>"; 
	
	foreach($color_array as $key => $color)
	{	
		$class = "color_" . $key;
		
		echo "<a href='#' id='" . $class . "' class='cb_color_select' >". $color ."</a>" ;

		
	}
	echo "</p>";

?>
	
	<p><label for="<?php echo $this->get_field_id( 'cb-button-color' ); ?>"><?php _e('Title:', 'cb_widget_color_picker') ?>;</label>
	<div id="<?php echo $id1; ?>" class="cb_color_picker_container">
  <p> <input class="my-color-picker" type="text" id="<?php echo $this->get_field_id( 'cb-button-color' ); ?>" name="<?php echo $this->get_field_name( 'cb-button-color' ); ?>" value="<?php echo $cb_button_color; ?>" />                            
	</p></div></p>
	





<?php

		
	}
    function get_id($input_string)
	{
		$id = str_replace(array(',', '\\', '/', '*', ' ',"&#039;",'$','?','&amp;'), '', $input_string);
		$id = rand();
		return $id;
	
	}
	// widget update
	function update($new_instance, $old_instance) {
		 $instance = $old_instance;
      // Fields

	  $instance['cb-button-color'] = $new_instance['cb-button-color'];
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		
	}
}
?>