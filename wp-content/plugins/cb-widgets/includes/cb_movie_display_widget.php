<?php
	
// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	

class __CamanaBay_movie_display_widget_plugin extends __CamanaBay_color_picker_widget{

	// constructor

	function __construct($id = 'cb_movie_display_block', $description = 'Camana Bay Movie Display Block', $opts = array()) {
	  
		$opts['description'] = 'Widget for Displaying Movie on the Front Page -  Image, title, custom color for text and button,button text';
	
				parent::__construct($id,$description,$opts);
				
		
	}
	
	function get_movie_times($TMSid,$schedules_file_name)
	{
	
		

		$xml = simplexml_load_file($schedules_file_name );
		$today = date("Y-m-d");
		$times= '';
		foreach($xml->schedules->schedule->event as $e)
				{
					$s = (string) $e->attributes()->TMSId[0];
					
			/*	echo "<p> attributes date is " . $e->attributes()->date . "</p>";*/
			
			//	echo "<p> in loop tmsid is " . $e->attributes()->TMSId . "</p>";
				if ( (strcmp($s,$TMSid) == 0 )  && ($e->attributes()->date == $today))
				{
					
					
					foreach($e->times->time as $t)
					{
					
						// echo "<p> t is " . $t . "</p>";
						$time_in_12_hour_format  = date("g:i A", strtotime($t));
						
					   $times.=$time_in_12_hour_format;
					   $times .=", ";
					  
										
					}
					
	
	
				}
				
			}
				$times = rtrim($times,', ');
				return $times;
				
	}
	
	// widget form creation
	function form($instance) {	
		parent::form($instance);

		if( $instance) {
			$cb_front_movie = esc_attr($instance['cb-movie-display-widget-text']);
			$cb_go_destination_link=esc_attr($instance['cb-go-destination-link']);
			 	$cb_button_color=esc_attr($instance['cb-button-color']); 
			
		$cb_widget_title = esc_attr($instance['cb-widget-title']);
		} else
		{
			$cb_front_movie = '';
			$cb_go_destination_link='';
			 	$cb_button_color='';
				$cb_widget_title='';
		}
	
		?>	
		
		<p><label for="<?php echo $this->get_field_id('cb-movie-display-widget-text'); ?>"><?php _e('Button Text:', 'cb_movie_display_block'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-movie-display-widget-text'); ?>" name="<?php echo $this->get_field_name('cb-movie-display-widget-text'); ?>" type="text" value="<?php echo $cb_front_movie; ?>" />
	</p>
		<p><label for="<?php echo $this->get_field_id('cb-go-destination-link'); ?>"><?php _e('Destination Link', 'cb_movie_display_block'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-go-destination-link'); ?>" name="<?php echo $this->get_field_name('cb-go-destination-link'); ?>" type="text" value="<?php echo $cb_go_destination_link; ?>" />
	</p>

	
	

<?php

	}
   function _get_local_xml_file_name($date, $name)
	{
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/CamanaMovies/';
		$file_name = $date . ".xml";
		$local_name = $dir  . $name . $file_name;
		return $local_name;
	
	}
	// widget update
	function update($new_instance, $old_instance) {
		 $instance = $old_instance;
      // Fields
		parent::update($new_instance, $old_instance);
		$instance = $old_instance;
		$s = $new_instance['cb-movie-display-widget-text'];
			$s =  str_replace(' ', '<br>', $s);
		  $instance['cb-movie-display-widget-text'] = strip_tags($s,'<br>');
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
   // these are the widget options
	
	
    $display_ptr = new _CB_Movie_Funcs();

	
	if (isset ($instance['cb-go-destination-link']))
		$cb_go_destination_link =  esc_attr($instance['cb-go-destination-link']);
	if (isset ($instance['cb-button-color']))
		$cb_button_color=$instance['cb-button-color'];
	if (isset(  $instance['cb-movie-display-widget-text']))
		$cb_movie_display_button_text =   $instance['cb-movie-display-widget-text'];
	
	
  

   // Register style sheet.
	
	
	wp_register_style( 'cb_movie_display_style_widget', plugins_url( 'cb-widgets/css/movie-display.min.css' ) );
	wp_enqueue_style( 'cb_movie_display_style_widget' );
	
	
	$id = rand();
?>

<script>

	 jQuery(document).ready(function($) {
	 
		var $buttonColor= '<?php if (isset ($instance['cb-button-color']))  echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		var $button_id='#<?php echo $id ?>' + '_button';
		jQuery($id).css('color',$buttonColor);
		jQuery($button_id).css('background-color',$buttonColor);
		var border_css = "1px solid " + $buttonColor;
		jQuery($button_id).css('border',border_css);
		
		movieTitle();
		
		function movieTitle() {

    var windowWidth = $(window).width();
    var title = jQuery(".cb-movie-title-widget");
	console.log('html is ' + jQuery(".cb-movie-title-widget").text());
	var str = jQuery(".cb-movie-title-widget").text();
	var strLength =  str.length;
   // var strLength = jQuery(".cb-movie-title-widget").html().length();

    if (windowWidth > 767 && windowWidth < 1400) {

        if (strLength > 12) {

            title.text(title.text().substr(0,15)).append("...");
        }
    }
}



	jQuery(window).resize(function() {

		movieTitle();
	});
	
		
 });
	 
	 
	 

</script>
		
		
<?php
	  $date =  date("Ymd");

	$ret = $display_ptr->get_correct_xml_files($program_file_name ,$schedules_file_name ,$source_file_name);

	
//	if (    (file_exists($program_file_name))  && (file_exists($schedules_file_name)) && (file_exists($source_file_name))) {
		if (    ($ret == TRUE)  || ($ret === -1)){
				$xml = simplexml_load_file($program_file_name);
				$p = $xml->programs->program;
				$TMSId = $p->attributes()->TMSId;
					$title =  $p->titles->title[0] ;
					
				   // print_r($p);
					$genres = $p->genres->genre;
					$genre_output = '';
					
					foreach ($genres as $g)
					{
						$genre_output.= $g;
						$genre_output.= ', ';
						
					
					}
					$genre_output = rtrim($genre_output,', ');
					$rt = $p->runTime;
					
					$t = explode("H",$rt);
			
					$h = explode("PT",$t[0]);
			
					$hours = $h[1];
					if ($hours[0] == '0')
					{
						$hours=$hours[1];
					}
					$m = explode("M",$t[1]);
					$minutes = $m[0];
					if ($minutes[0] == '0')
						$minutes=$minutes[1];
				
					$rating = "Not Rated";
				    foreach($p->ratings->rating as $r)
					{
						if ($r->attributes()->ratingsBody == "Motion Picture Association of America")
							$rating =$r->attributes()->code;
					
					}
				
		
					$movie_times = $display_ptr->get_movie_times($TMSId,$schedules_file_name);
					if (empty($movie_times) || ($ret === -1))
					   $movie_times = "<a href='http://www.regmovies.com/theatres/theatre-folder/regal-camana-bay-stadium-6-9504'  target='_blank'> Click here for movie times </a>";
				
				
				
					$altFilmId = $p->attributes()->altFilmId;
				
				foreach (glob($_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/CamanaMovies/images/*.jpg") as $filename) {
	
					// the pattern is the alternate film id 
					$pattern = '/' . preg_quote($altFilmId, '/') . '/';
			
					$ret = preg_match($pattern, $filename, $matches);

					
					if ($ret === 1){

				
						$local_file = $filename;
						break;
					}
	
				}

					$d = "";
					if (isset($local_file))
						$d = $local_file;	
					// check if file exists
					// if it doesn't exist, it probably is the 3D version and we have to find it's filename
					if (!file_exists($d)){
			
			$count = 0;
						foreach($p->images as $i)
						{
							foreach($i->image as $ii){
					
								if ($ii->attributes()->category[0] == "Poster Art"){
						
									$filename =   $ii->URI[0];
						  
									$name = basename($filename);
						// we don't want thumb nails
									if (stristr($name, '_t') == TRUE)
										continue;
							
									$image = content_url() .'/uploads/CamanaMovies/images/' . $name ;
									// same file name, but this is the way we have to check it - not with the URL name
									$d = $_SERVER['DOCUMENT_ROOT']. '/wp-content/uploads/CamanaMovies/images/' . $name;
									// if the file does not exist, we use the placeholder
									if (!file_exists($d)){
										$image = content_url() .'/uploads/2016/05/movie_fallback.jpg';
										break;
									}
								}
							}
						   
						}		   
						   
					// first file exists -  build the path
					}	else {
							$name = basename($local_file);
							$image =  content_url() .'/uploads/CamanaMovies/images/' . $name;
												

					}
				if (!isset($image))
					$image = content_url() .'/uploads/2016/05/movie_fallback.jpg';
				echo $before_widget;
				$cb_widget_title= "MOVIES";
   // Display the widget
   
  
			?>
			<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>'>
		<h3 id='<?php echo $id ?>' class="cb_dir_block_title cb_image_block_text"><?php if (isset ($cb_widget_title)) echo $cb_widget_title?></h3>
	</a><?php
				$string = '<div class="cb_movie-widget-box"><div class="cb-movie-item-widget"> <div class="cb-movie-item-inside">
						<div class="cb-image-outside">

<a href="' . $cb_go_destination_link . '" >	<img class="cb-movie-image-widget" src="' . $image . '" ></a></div>';
					$string .= '<div class="cb-movie-bottom-widget-bk"><div class="cb-movie-title-widget">' . $title . '</div>';
					$string .= '<div class="cb-movie-info-widget"> RATED: ' . $rating . ', Duration: ' . $hours . 'hr ' . $minutes . 'min
					<br /> GENRE: ' . $genre_output . ' </div>';
					$string .= '<div class="cb-movie-times-widget">'. $movie_times . ' </div>';
				     
					$string .= '</div></div>';

					
					
					
					
					
					
					
					
					
					$string .= '</div>';
echo $string;
		

?>

		

				<a id='<?php echo $id ?>_button' class="cb_outline_button" href='<?php if (isset ($cb_go_destination_link)) echo $cb_go_destination_link?>'>
	
			<div class='cb_button_text'><?php echo $cb_movie_display_button_text ?></div>
				
				</a>

		
		
		
<?php

   }else{
	echo $before_widget;
	 $image = content_url() .'/uploads/2016/05/movie_fallback.jpg';
     $string = '<div class="cb_movie-widget-box">';
	 $string .= '<h3  class="cb_dir_block_title cb_image_block_text">MOVIES</h3>';
	 $string .= '<div class="cb_movie-widget-box">
						<div class="cb-movie-item-widget"> 
								<div class="cb-movie-item-inside">
										<div class="cb-image-outside">	
												<img class="cb-movie-image-widget" src="' . $image . '"  />
												
										</div>
										<div class="cb-movie-bottom-widget-bk"> <center><h2><a href="http://www.regmovies.com/theatres/theatre-folder/regal-camana-bay-stadium-6-9504"  target="_blank"> Click here for movie times </a>
		
												</center></h2></div>
								</div>
						</div>
					</div>';
	 echo $string;
	}
 echo '</div>';
   echo $after_widget;
	}
}