<?php
/********************************************************************************
*
*   Blog article widget
*
*
*
*
*
*
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');






class __CamanaBay_blog_latest_widget extends __CamanaBay_blog_article_widget{

	// constructor


		public function __construct($id = 'cb_widget_blog_latest', $description = 'Latest Three Blog Articles Widget', $opts = array()) {
         add_action('wp_enqueue_scripts', array($this, 'blog_latest_styles'));

		$opts['description'] = 'displays latest 3 blog articles';
		parent::__construct($id,$description,$opts);




	}
    function blog_latest_styles()
	{
		wp_register_style( 'cb-blog-latest-css', plugins_url( 'cb-widgets/css/cb-blog.min.css' ) );
		wp_enqueue_style( 'cb-blog-latest-css' );
		wp_register_style( 'button-on-right-widget-latest', plugins_url( 'cb-widgets/css/buttonOnRightWidget.css' ) );
		wp_enqueue_style( 'button-on-right-widget-latest' );
	}


	// widget form creation
	function form($instance) {

	}

	// widget update
	function update($new_instance, $old_instance) {

	}


	function get_the_latest_post()
	{
			$args = array( 'numberposts' => 3 );
			$lastposts = get_posts( $args );

			return $lastposts;

	}
	// widget display
	function widget($args, $instance) {

	$posts = $this->get_the_latest_post();


	  foreach ($posts as $post)
	  {
	  $link = get_post_permalink( $post->ID);
?>
	   <div class="cb-blog-featured-container">
		<div class="cb-blog-featured-date-time-br">

			<div class="cb-blog-day">
				<?php echo get_the_date('l', $post->ID) ; ?>

			</div>
			<div class="cb-blog-date">
				<?php echo get_the_date('d F Y', $post->ID); ?>

			</div>
		</div>
        <a class="cb_arrow_widget" href="<?php echo $link ?>">
		          <div class="cb-blog-featured-title">
		                    <?php echo $post->post_title ?>
                  </div>
        </a>
		</div>

<?php
		$first_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );






?>


<?php




	//	$content = get_extended( $post->post_content );
		//$cb_trimmed_text = $content['main'];
		$cb_trimmed_text = wp_trim_words($post->post_content);







		$arrow_image_url = plugin_dir_url(__FILE__) . '../images/arrows_blog.png';

?>
 <script>

	 jQuery(document).ready(function($) {





		jQuery('.cb_arrow_on_right_widget_outside').css('background-color','#efece7');
		jQuery('.cb_arrow_widget').css('vertical-align', 'middle');
		jQuery('.cb_blog_arrow_button').css('vertical-align','unset');
		jQuery('.cb_arrow_on_right_widget_h2').css('color','#008aab');
		jQuery('.cb_arrow_on_right_widget_h2').css('color','#00b6dd');
		jQuery('.cb_arrow_on_right_widget_h2').css('font-family','nexabold');
		jQuery('.cb_blog_arrow_button').css('background-color', 'transparent');
		jQuery('.cb-text-link').css('padding', '0.6em 0');
		jQuery('.cb_arrow_on_right_widget_outside').css('margin-top', '2em');

	 });

</script>

	<div id="cb-blog-info-container">
		<div class="cb-blog-featured-image">
			<img  src="<?php echo $first_image ?>" />
		</div>
		<div class="cb-blog-featured-body-text">
			<?php echo $cb_trimmed_text ?>

			<div  class='cb_arrow_on_right_widget_outside'>
				<a class="cb_arrow_widget" href="<?php echo $link ?>">
					<div class="cb_button_on_right_detail">
						 <h2 class='cb_arrow_on_right_widget_h2'> READ FULL STORY</h2>
						 <img class='cb_blog_arrow_button'    src=" <?php echo $arrow_image_url ?>" alt="arrow" />
					</div>
				</a>
			</div>
		</div>
	</div>











<?php


	}


	}

}
