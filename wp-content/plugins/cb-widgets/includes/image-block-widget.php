<?php
/********************************************************************************
*  
*    Image Widget Plugin 
*
*	 Plugin that has an image, title, caption
*
*
*
*
*********************************************************************************/

// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	
class __CamanaBay_image_widget_plugin extends __CamanaBay_directory_widget_plugin {
	var $textdomain;
	// constructor
		function __construct($id = 'cb_image_block', $description = 'Camana Bay Image Block', $opts = array()) {
				$opts['description'] = 'Image, title, description, Go Button';

				parent::__construct($id,$description,$opts);
				
	}
	
	
	// widget form creation
	function form($instance) {	
		parent::form($instance);
		if( $instance) {

			$cb_widget_text = esc_textarea($instance['cb-widget-text']);
			$cb_go_destination_link=esc_attr($instance['cb-go-destination-link']);
		}
		else {

			$cb_widget_text = '';
			$cb_go_destination_link='';
		}
?>	<p><label for="<?php echo $this->get_field_id('cb-widget-text'); ?>"><?php _e('Text:', 'cb_widget_image_block'); ?></label>
	<textarea class="widefat" id="<?php echo $this->get_field_id('cb-widget-text'); ?>" name="<?php echo $this->get_field_name('cb-widget-text'); ?>"><?php echo $cb_widget_text; ?></textarea>
	</p>
<?php	
	}
	

	// widget update
function update($new_instance, $old_instance) {
		 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $old_instance;
      // Fields

      $instance['cb-widget-text'] = strip_tags($new_instance['cb-widget-text']);
	   $instance['cb-go-destination-link'] = strip_tags($new_instance['cb-go-destination-link']);
	 
   
     return $instance;
	}
// widget display
	function widget($args, $instance) {

		extract( $args );
   // these are the widget options
	$cb_widget_text = '';
	$cb_widget_title = '';
	$cb_pic_url = '';
	$cb_go_destination_link = '';
	
     if (isset($instance['cb-widget-text']))
		$cb_widget_text = esc_textarea($instance['cb-widget-text']);
	if (isset($instance['cb-widget-title']))
		$cb_widget_title = esc_attr($instance['cb-widget-title']);
	if (isset($instance['cb-widget-pic-url']))
		$cb_pic_url = esc_attr($instance['cb-widget-pic-url']);
	if (isset($instance['cb-go-destination-link']))
		$cb_go_destination_link=esc_attr($instance['cb-go-destination-link']);
	$cb_button_color=$instance['cb-button-color'];
	
	 
   echo $before_widget;
   // Display the widget
  

  
 
	
	$id = rand();
	$id1 = rand();
	
	
	 echo '<div id="' . $id  . '"class="widget-text cb_widget_image_block_box">';
?>

<script>

	 jQuery(document).ready(function($) {
	 
		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		var $id1='#<?php echo $id1 ?>';
		var $button_id='#<?php echo $id ?>' + '_button';
		var s = $id + ' .cb_image_block_title';
		var s1 = $id + ' .cb_image_block_text';
		
		jQuery(s).css('color',$buttonColor);
		jQuery(s1).css('color',$buttonColor);
		
		
		
	 });

</script>
		<div class="cb_out_block_image">
<?php if (!empty($cb_go_destination_link)){
?>
	<a href="<?php if (isset ($cb_go_destination_link)) echo $cb_go_destination_link ?>">			<img src=<?php if (isset ($cb_pic_url)) echo "$cb_pic_url" ?> ></a>
		</div>
		<div class="cb_image_outside_of_title">
	<a href="<?php if (isset ($cb_go_destination_link)) echo $cb_go_destination_link ?>">		<div  class="cb_image_block_title"><?php if (isset ($cb_widget_title)) echo $cb_widget_title?></div></a>
		</div>
   

		<?php 
		
		if (!empty($cb_widget_text)) { ?>
	<a href="<?php if (isset ($cb_go_destination_link)) echo $cb_go_destination_link ?>"><div  class="cb_image_block_text  cb_image_block_text_area"><?php if (isset ($cb_widget_text)) echo $cb_widget_text?></div> </a>
		<?php }
}else{
?>		
		<img src=<?php if (isset ($cb_pic_url)) echo "$cb_pic_url" ?> >
		</div>
		<div class="cb_image_outside_of_title">
			<div  class="cb_image_block_title"><?php if (isset ($cb_widget_title)) echo $cb_widget_title?></div>
		</div>
<?php		
		
		if (!empty($cb_widget_text)) { ?>
<div  class="cb_image_block_text  cb_image_block_text_area"><?php if (isset ($cb_widget_text)) echo $cb_widget_text?></div> 
		<?php }
}		?>
	
<?php

   
   echo '</div>';
   echo $after_widget;
	}
}
?>