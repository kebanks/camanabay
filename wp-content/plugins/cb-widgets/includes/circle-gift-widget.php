<?php
/********************************************************************************
*  
*   Circle gift Widget
*
*	 
* 	 Picture, Circle with text underneath the picture with link
*	
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	

class __CamanaBay_circle_gift_widget extends __CamanaBay_color_picker_widget{
	var $textdomain;
	// constructor
	
	  function __construct($id = 'cb_circle_gift_widget', $description = 'Camana Bay Circle Gift Widget', $opts = array()) {
	
		 add_action( 'admin_enqueue_scripts', array(&$this, 'my_custom_load') );
		$opts['description'] = $description;
		$opts['description'] = "Picture, Circle with text underneath with link ";
		parent::__construct($id,$description,$opts);
		
		
	}
	
	function my_custom_load() {    
    //  this is for the picture links
		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__DIR__) . 'js/upload-media.js', array('jquery'));
      

        wp_enqueue_style('thickbox');
		
    }
	
	// widget form creation
	function form($instance) {	

	
	// Check values
	if( $instance) {
		$cb_circle_gift_text = esc_attr($instance['cb-circle-gift']);
		$cb_circle_gift_text_destination = esc_attr($instance['cb-circle-gift-destination']);
		$cb_gift_pic_url = esc_attr($instance['cb-circle-gift-pic-url']);
	
		} else {
	 
		$cb_circle_gift_text = "";
		$cb_circle_gift_text_destination="";
		$cb_gift_pic_url='';
	}
?>
	<p>
            <label for="<?php echo $this->get_field_name( 'cb-circle-gift-pic-url' ); ?>"><?php _e( 'Display Image (size 276px by 278px):' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'cb-circle-gift-pic-url' ); ?>" id="<?php echo $this->get_field_id( 'cb-circle-gift-pic-url' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $cb_gift_pic_url ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
	
	<p><label for="<?php echo $this->get_field_id('cb-circle-gift'); ?>"><?php _e('Circle Button Text:', 'cb_circle_gift_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-circle-gift'); ?>" name="<?php echo $this->get_field_name('cb-circle-gift'); ?>" type="text" value="<?php echo $cb_circle_gift_text; ?>" />
	</p>
	<p><label for="<?php echo $this->get_field_id('cb-circle-gift-destination'); ?>"><?php _e('Destination Link:', 'cb_circle_gift_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-circle-gift-destination'); ?>" name="<?php echo $this->get_field_name('cb-circle-gift-destination'); ?>" type="text" value="<?php echo $cb_circle_gift_text_destination; ?>" />
	</p>
<?php	
		parent::form($instance);
}
   
	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $old_instance;
      // Fields

	  $instance['cb-circle-gift'] = $new_instance['cb-circle-gift'];
	  $instance['cb-circle-gift-destination'] = $new_instance['cb-circle-gift-destination'];
	  $instance['cb-circle-gift-pic-url'] = $new_instance['cb-circle-gift-pic-url'];
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		wp_register_style( 'cb-circle-gift-css', plugins_url( 'cb-widgets/css/cb-circle-gift.css' ) );
		wp_enqueue_style( 'cb-circle-gift-css' );
		$cb_circle_gift_text = '';
		$cb_circle_pic_url = '';
		$cb_circle_gift_text_destination='';
		
		
		if (isset($instance['cb-circle-gift']))
			$cb_circle_gift_text= esc_attr($instance['cb-circle-gift']);
		
		if (isset($instance['cb-circle-gift-destination']))
			$cb_circle_gift_text_destination= esc_attr($instance['cb-circle-gift-destination']);
		if (isset($instance['cb-circle-gift-pic-url']))
			$cb_circle_pic_url = esc_attr($instance['cb-circle-gift-pic-url']);
		$cb_button_color=$instance['cb-button-color'];
		$id = $this->get_id($cb_circle_gift_text);
?>		


<script>

	 jQuery(document).ready(function($) {
	 
		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		var $button_id='#<?php echo $id ?>' + '_button';
		jQuery($id).css('color','white');
	
		jQuery($id).css('backgroundColor',$buttonColor);
	
		
	 });

</script>
		<div>
			<img class="cb_circle_gift_image" src=<?php if (isset ($cb_circle_pic_url)) echo "$cb_circle_pic_url" ?> /></a>
			<div id='<?php echo $id ?>' class='cb_circle_gift_text'>
				<a  class="cb_circle_gift_text_a" href="<?php echo $cb_circle_gift_text_destination ?>">  <?php echo $cb_circle_gift_text ?></a>
			</div>
		</div>
<?php		
	}
}
?>