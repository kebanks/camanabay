<?php
/********************************************************************************
*  
*    Amenity widget
*
*	 
*    Title/Text/Details and right arrow button
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	


class __CamanaBay_amenity_widget extends __CamanaBay_color_picker_widget{
	var $textdomain;
	// constructor
	
	  function __construct($id = 'cb_amenity_widget', $description = 'Camana Amenity Widget', $opts = array()) {
	

		$opts['description'] = "Title/Text/Details and right arrow button";
		parent::__construct($id,$description,$opts);
		
  }
	
	
	function my_custom_load()
	{
	
		wp_register_style( 'amenity-widget', plugins_url( 'cb-widgets/css/amenityWidget.css' ) );
		wp_enqueue_style( 'amenity-widget' );
	
	}
	
	// widget form creation
	function form($instance) {	
	

	// Check values
if( $instance) {

     
     
	
	
	$cb_widget_text = esc_textarea($instance['cb-widget-text']);
	$cb_widget_text_title = esc_attr($instance['cb-widget-text-title']);
	 $cb_bottom_link = esc_attr($instance['cb-destination-link']);
	
	
	

} else {
	
	$cb_widget_text='';
	 $cb_bottom_link = "";

	$cb_widget_text_title='';
	
}

?>

	<p><label for="<?php echo $this->get_field_id('cb-destination-link'); ?>"><?php _e('Destination Url:', 'cb_amenity_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-destination-link'); ?>" name="<?php echo $this->get_field_name('cb-destination-link'); ?>" type="text" value="<?php echo $cb_bottom_link; ?>" />
	</p>
	
	<p><label for="<?php echo $this->get_field_id('cb-widget-text-title'); ?>"><?php _e('Title:', 'cb_amenity_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-widget-text-title'); ?>" name="<?php echo $this->get_field_name('cb-widget-text-title'); ?>" type="text" value="<?php echo $cb_widget_text_title; ?>" />
	</p>
	
	<p><label for="<?php echo $this->get_field_id('cb-widget-text'); ?>"><?php _e('Text:', 'cb_widget_image_block'); ?></label>
	<textarea class="widefat" id="<?php echo $this->get_field_id('cb-widget-text'); ?>" name="<?php echo $this->get_field_name('cb-widget-text'); ?>"><?php echo $cb_widget_text; ?></textarea>
	</p>


<?php
	parent::form($instance);
			
	}
   
	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $old_instance;
      // Fields


	
      $instance['cb-destination-link'] = strip_tags($new_instance['cb-destination-link']);
     

	 $instance['cb-widget-text'] = strip_tags($new_instance['cb-widget-text']);
	  $instance['cb-widget-text-title'] = strip_tags($new_instance['cb-widget-text-title']);
	  
	
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
   
	
    
	if (isset($instance['cb-destination-link']))
		$cb_destination_link= esc_attr($instance['cb-destination-link']);	
	
	if (isset($instance['cb-widget-text']))
		$cb_widget_text = esc_textarea($instance['cb-widget-text']);
		if (isset($instance['cb-widget-text-title']))
				$cb_widget_text_title = esc_attr($instance['cb-widget-text-title']);

	
	$cb_button_color=$instance['cb-button-color'];
	
	 
   echo $before_widget;
   // Display the widget
   echo '<div class="widget-text cb_amenity_widget_box">';


	$id = $this->get_id($cb_widget_text_title);
	$this->my_custom_load();
	$arrow_image_url = plugin_dir_url(__FILE__) . '../images/white_arrow_small_right.png';
	
?>
		<div class='cb_amenity_widget_outside'>
			<div class="cb_amenity_widget_top_part">
				<h2 class='cb_amenity_widget_h2'> <?php if (isset ($cb_widget_text_title)) echo $cb_widget_text_title?></h2>
				<?php	if (!empty($cb_widget_text)) { ?>
		
				<p>
					<?php if (isset ($cb_widget_text)) echo $cb_widget_text?>
				</p>
				<?php } ?>
			</div>
<?php if (!empty($cb_destination_link)){ ?>		
			<div class="cb_amenity_detail">
				Details
	
				<a  href="<?php echo $cb_destination_link ?>"> <span  > <img  id='<?php echo $id ?>' class="cb_amenity_widget_img"  src=" <?php echo $arrow_image_url ?>"  /></span></a>
			</div>
		</div>
	
<?php } ?>	


	 
	 <script>

	 jQuery(document).ready(function($) {
	 
		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';

		
		jQuery($id).css('background-color',$buttonColor);
		
	 });

</script>
		
	
		
<?php

   
   echo '</div>';
   echo $after_widget;
	}
}
?>