<?php
/********************************************************************************
*  
*    Image Widget with a class Widget 
*
*	 Widget that has an image and a class
*
*	 This widget 
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	


class __CamanaBay_image_widget extends WP_Widget{
	var $textdomain;
	// constructor
	private $cb_widget_id;
	  function __construct($id = 'cb_image_widget', $description = 'Camana Bay Image Widget with a class', $opts = array()) {
	
		 add_action( 'admin_enqueue_scripts', array(&$this, 'my_custom_load') );\
		 add_action('wp_enqueue_scripts', array(&$this,'image_block_load_scripts'));
		$this->cb_widget_id = $id;
		
		if ($id == 'cb_image_widget')
			$opts['description'] = 'a custom widget where you can add a class to the image';
		parent::__construct($id,$description,$opts);
		  
      
		
		
	}
	function image_block_load_scripts()
	{

		wp_register_style( 'im_widget_css', plugins_url( 'cb-widgets/css/cb-image-widget.css' ) );
		wp_enqueue_style( 'im_widget_css' );
	}
	function my_custom_load() {    

		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
		
        wp_enqueue_script('upload_media_widget',plugin_dir_url(__DIR__) . 'js/upload-media.js', array('jquery'));
      

        wp_enqueue_style('thickbox');
		
    }
	
	
	// widget form creation
	function form($instance) {	
		
		 wp_register_style( 'image-block-css', plugins_url( 'cb-widgets/css/imageBlock.min.css' ) );
		wp_enqueue_style( 'image-block-css' );
		$this->my_custom_load();
	// Check values
if( $instance) {

     
     $cb_widget_caption = esc_textarea($instance['cb-widget-caption']);
	 $cb_pic_url = esc_attr($instance['cb-widget-pic-url']);
	$cb_go_destination_link=esc_attr($instance['cb-iw-dest-link']);
	$cb_widget_class=esc_attr($instance['cb-iw-widget-class']);
	
	
	 if (isset($instance['cb-im-open-new-win']))
		$cb_open_new_win = $instance['cb-im-open-new-win'];
	else
		$cb_open_new_win = "NO";

} else {

	
     
	 $cb_pic_url = '';
	  $cb_go_destination_link='';
	 $cb_widget_caption ='';
$cb_open_new_win = "NO";
	$cb_widget_class ='';
}
       
	

?>


	<p>
        
            <input name="<?php echo $this->get_field_name( 'cb-widget-pic-url' ); ?>" id="<?php echo $this->get_field_id( 'cb-widget-pic-url' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $cb_pic_url ); ?>" />
            <input id="my-upload-button" class="upload_image_button" type="button" value="Upload Image" />
        </p>
	
	<p><label for="<?php echo $this->get_field_id('cb-iw-widget-class'); ?>"><?php _e('Class:', 'cb_image_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-iw-widget-class'); ?>" name="<?php echo $this->get_field_name('cb-iw-widget-class'); ?>" type="text" value="<?php echo $cb_widget_class; ?>" />
	</p>

	<p><label for="<?php echo $this->get_field_id('cb-iw-dest-link'); ?>"><?php _e('Destination Url:', 'cb_image_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-iw-dest-link'); ?>" name="<?php echo $this->get_field_name('cb-iw-dest-link'); ?>" type="text" value="<?php echo $cb_go_destination_link; ?>" />
	</p>
	
	
	<p><label for="<?php echo $this->get_field_name('cb-im-open-new-win'); ?>">Open Link in New Window? </label>

	<?php  if ($cb_open_new_win == "YES")
					$checked="checked";
					else
					$checked = "unchecked";

				?>

 <input type="checkbox" id="<?php echo $this->get_field_name('cb-im-open-new-win'); ?>"   name="<?php echo $this->get_field_name('cb-im-open-new-win'); ?>" value="YES"<?php echo $checked; ?>>

	
	
	
	
	
	
	
	

	<p><label for="<?php echo $this->get_field_id('cb-widget-caption'); ?>"><?php _e('Caption:', 'cb_image_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-widget-caption'); ?>" name="<?php echo $this->get_field_name('cb-widget-caption'); ?>" type="text" value="<?php echo $cb_widget_caption; ?>" />
	</p>
	





	
<?php


		
	}
   
	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $old_instance;
      // Fields

  
      $instance['cb-widget-caption'] = strip_tags($new_instance['cb-widget-caption']);
	  $instance['cb-widget-pic-url'] = strip_tags($new_instance['cb-widget-pic-url']);
	  $instance['cb-iw-widget-class'] = strip_tags($new_instance['cb-iw-widget-class']);
	  
	

     return $instance;
	}


	// widget display
	function widget($args, $instance) {
	
		extract( $args );
   // these are the widget options
	
	$cb_widget_caption ='';
	$cb_pic_url ='';
	$cb_go_destination_link='';
	$cb_image_class = '';
    
	if (isset($instance['cb-widget-caption']))
		$cb_widget_caption = esc_attr($instance['cb-widget-caption']);
	if (isset($instance['cb-widget-pic-url']))
		$cb_pic_url = esc_attr($instance['cb-widget-pic-url']);
	if (isset ($instance['cb-iw-dest-link']))
		$cb_go_destination_link =  esc_attr($instance['cb-iw-dest-link']);
	if (isset ($instance['cb-iw-widget-class']))
		$cb_image_class =  esc_attr($instance['cb-iw-widget-class']);
	//target="_blank"
	$cb_target='';
	if (isset($instance['cb-im-open-new-win']))
	{
	  if ($instance['cb-im-open-new-win'] === "YES")
		$cb_target = ' target="_blank" ';
	
	}
	if (isset($instance['cb-ev-action-add-link']))
		$cb_ev_action_make_link = $instance['cb-ev-action-add-link'];
	else
		$cb_ev_action_make_link = "NO";
	 
   echo $before_widget;
   // Display the widget
   echo '<div class="widget-text cb_image_widget_box">';

   // Register style sheet.


	
	
	//$id = $this->get_id($cb_widget_caption);
	
?>

		<div class="cb_out_block_image">
			<a href='<?php if (isset ($cb_go_destination_link)) echo  "$cb_go_destination_link" ?>' <?php echo $cb_target ?>>
			<img class="<?php echo $cb_image_class ?>" src=<?php if (isset ($cb_pic_url)) echo "$cb_pic_url" ?> /></a>
		</div>
		<div class="cb_iw_title_outer">
			<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>'  <?php echo $cb_target ?>>
			<div ><?php if (isset ($cb_widget_caption)) echo $cb_widget_caption?></div>
			</a>
		</div>
		


		
		
		
		
		
<?php

   
   echo '</div>';
   echo $after_widget;
	}
}
?>