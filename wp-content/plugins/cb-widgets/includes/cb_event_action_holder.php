<?php
/********************************************************************************
*
*   Event Action Place Holder
*
*
*	Parent widget to most of the custom widgets in this section.  Manages the color picker
*
*
*
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');





class __CamanaBay_event_action_holder_widget extends WP_Widget{
	  static $variables=array();
	// constructor

	  function __construct($id = 'cb_event_action_holder_placeholder', $description = 'Event Action Place Holder', $opts = array()) {
				$opts['description'] = "Select the place you want the Event Action plugin";


		parent::__construct($id,$description,$opts);




	}


 


	// widget form creation
	function form($instance) {

	//	error_log('instance is ' . print_r($instance, TRUE));
	
	$id = $instance['cb-ea-page-dropdown'];
	$args = array(
	'sort_order' => 'asc',
	'sort_column' => 'post_title',
	'hierarchical' => 1,
	'exclude' => '',
	'include' => '',
	'meta_key' => '',
	'meta_value' => '',
	'authors' => '',
	'child_of' => 0,
	'parent' => -1,
	'exclude_tree' => '',
	'number' => '',
	'offset' => 0,

		'post_type' => 'cb-ev-act-pg-type',
	'post_status' => 'publish'
); 
$pages = get_pages($args); 
?>
<select name="<?php echo $this->get_field_name( 'cb-ea-page-dropdown' ); ?>">
<?php
foreach ($pages as $pagg) {


		if ($pagg->ID == $id){
		   $option = '<option value="'.$pagg->ID.'" selected >';
		 
		 }
		else
            $option = '<option value="'.$pagg->ID.'">';
            $option .= $pagg->post_title;
            $option .= '</option>';
            echo $option;
        }

?>
</select>	
<?php		



	}
	// widget update
	function update($new_instance, $old_instance) {

	


	 $instance = $new_instance;


	 return $instance;

	}

	// widget display
	function widget($args, $instance) {
	
		$id = $instance['cb-ea-page-dropdown'];
		echo do_shortcode('[CB_EventAction id="'. $id .'"]');
		
	}
}
?>
