<?php
/********************************************************************************
*
*    Horizontal text button
*
*     Text Link with Gray background
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');





class __CamanaBay_Horizontal_Text_Link extends __CamanaBay_color_picker_widget{
	var $textdomain;
	// constructor

	  function __construct($id = 'cb_horiz_text_link', $description = 'Camana Bay Horizontal Text Link', $opts = array()) {

		$opts['description'] = 'Text Link with Gray background';
		parent::__construct($id,$description,$opts);




	}



	// widget form creation
	function form($instance) {


	// Check values
if( $instance) {



	 $cb_text_link_text= esc_attr($instance['cb-widget-link-text']);
	$cb_text_link_destination=esc_attr($instance['cb-text-destination-link']);
	$cb_text_font_size=$instance['cb-text-font-size'];
	$cb_text_font_family=$instance['cb-text-font-family'];
	if (isset($instance['cb-text-make-link']))
		$cb_text_make_link = $instance['cb-text-make-link'];
	else
		$cb_text_make_link = "NO";


} else {



	 $cb_text_link_text= '';
	  $cb_text_link_destination='';
	$cb_text_font_size = 2;
	$cb_text_font_family = 'NexaLight sans-serif';
	$cb_text_make_link = "NO";

}


?>


	<p>
            <label for="<?php echo $this->get_field_name( 'cb-widget-link-text' ); ?>"><?php _e( 'Text:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'cb-widget-link-text' ); ?>" id="<?php echo $this->get_field_id( 'cb-widget-link-text' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_attr( $cb_text_link_text); ?>" />

        </p>

	<p><label for="cb-text-make-link">Make text a link? </label>

	<?php  if ($cb_text_make_link == "YES")
					$checked="checked";
					else
					$checked = "unchecked";

				?>

 <input type="checkbox" id="<?php echo $this->get_field_name('cb-text-make-link'); ?>"   name="<?php echo $this->get_field_name('cb-text-make-link'); ?>" value="YES"<?php echo $checked; ?>>


 </p>

	<p><label for="<?php echo $this->get_field_id('cb-text-destination-link'); ?>"><?php _e('Destination Url:', 'cb_horiz_text_link'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-text-destination-link'); ?>" name="<?php echo $this->get_field_name('cb-text-destination-link'); ?>" type="text" value="<?php echo $cb_text_link_destination; ?>" />
	</p>

	<p><label for="<?php echo $this->get_field_id('cb-text-font-size'); ?>"><?php _e('Font size in pixels:', 'cb_horiz_text_link'); ?></label>






	<select name="<?php echo $this->get_field_name( 'cb-text-font-size' ); ?>">
<?php for ($i=2;$i <=40; $i+=2){


if ($cb_text_font_size == $i)
{
 ?>
<option value="<?php echo $i ?>" selected ><?php echo $i . " px" ?></option>
<?php } else{
?>
  <option value="<?php echo $i ?>" ><?php echo $i . " px" ?></option>
<?php }
} ?>
</select>
</p>

<p><label for="<?php echo $this->get_field_id('cb-text-font-family'); ?>"><?php _e('Font family:', 'cb_horiz_text_link'); ?></label>

	<select name="<?php echo $this->get_field_name( 'cb-text-font-family' ); ?>">

  <option value="NexaLight" <?php if ($cb_text_font_family == 'NexaLight') echo 'selected' ?>>Nexa Light</option>
  <option value="NexaBold" <?php if ($cb_text_font_family == 'NexaBold') echo 'selected' ?>>Nexa Bold</option>

</select>
</p>









<?php
	parent::form($instance);

	}

	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $new_instance;
      // Fields


      $instance['cb-widget-title'] = strip_tags($new_instance['cb-widget-title']);
	  $instance['cb-widget-link-text'] = strip_tags($new_instance['cb-widget-link-text']);


     return $instance;
	}


	// widget display
	function widget($args, $instance) {
	// parent::widget($args, $instance);
		extract( $args );
   // these are the widget options
	$cb_text_link_text = '';
	$cb_text_link_destination='';

	if (isset($instance['cb-widget-link-text']))
		$cb_text_link_text= esc_attr($instance['cb-widget-link-text']);
	if (isset ($instance['cb-text-destination-link']))
		$cb_text_link_destination =  esc_attr($instance['cb-text-destination-link']);
	$cb_button_color=$instance['cb-button-color'];
	$cb_button_font_size=$instance['cb-text-font-size'];
	$cb_button_font_family=$instance['cb-text-font-family'];
	if (isset($instance['cb-text-make-link']))
		$cb_text_make_link = $instance['cb-text-make-link'];
	else
		$cb_text_make_link = "NO";

   echo $before_widget;
   // Display the widget
   echo '<div class="widget-text cb_horiz_text_link_box">';


   // Register style sheet.
	wp_register_style( 'cb-horz-link-text', plugins_url( 'cb-widgets/css/horiz-text-link.css' ) );
	wp_enqueue_style( 'cb-horz-link-text' );



	$id = rand();

?>
<script>

	 jQuery(document).ready(function($) {


		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		var font_family='<?php echo $cb_button_font_family ?>';

		jQuery($id).css('color',$buttonColor);

		var font_size="<?php echo $cb_button_font_size;?>"+'px';

		jQuery($id).css("font-size",font_size  );
		jQuery($id).css("font-family",font_family  );


	 });

</script>

<?php if ($cb_text_make_link == "YES"){ ?>
	<div   class='cb-text-link'>
		<a  id='<?php echo $id ?>' href='<?php if (isset ($cb_text_link_destination)) echo "$cb_text_link_destination"?> '">
	<?php if (isset ($cb_text_link_text)) echo "$cb_text_link_text"?>	</a>
	</div>
<?php }else{
?> <div id='<?php echo $id ?>'  class='cb-text-link'>
<?php
	if (isset ($cb_text_link_text))
		echo "$cb_text_link_text";



?> </div>

<?php  }






   echo '</div>';
   echo $after_widget;
	}
}
?>
