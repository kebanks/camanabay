<?php
/********************************************************************************
*  
*    Directory Widget 
*
*	 Widget that has an image and title and go button
*
*	 This widget 
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	

//class __CamanaBay_directory_widget_plugin extends __CamanaBay_color_picker_widget{
class __CamanaBay_directory_widget_plugin extends __CamanaBay_color_picker_widget{
	var $textdomain;
	// constructor
	private $cb_widget_id;
	  function __construct($id = 'cb_widget_directory_block', $description = 'Camana Bay Directory Block', $opts = array()) {
	
		 add_action( 'admin_enqueue_scripts', array(&$this, 'my_custom_load') );\
		 add_action('wp_enqueue_scripts', array(&$this,'the_scripts'));
		$this->cb_widget_id = $id;
		error_log('id is ' . $id);
		error_log('opts is ' . print_r($opts, TRUE));
		if ($id == 'cb_widget_directory_block')
			$opts['description'] = 'pic and title text in custom color';
		parent::__construct($id,$description,$opts);
		  
      
		
		
	}
	function the_scripts()
	{
		wp_register_style( 'dir_plugin_css', plugins_url( 'cb-widgets/css/imageBlock.min.css' ) );
		wp_enqueue_style( 'dir_plugin_css' );
	}
	function my_custom_load() {    

		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
		
        wp_enqueue_script('upload_media_widget',plugin_dir_url(__DIR__) . 'js/upload-media.js', array('jquery'));
      

        wp_enqueue_style('thickbox');
		
    }
	
	
	// widget form creation
	function form($instance) {	
		
		 wp_register_style( 'image-block-css', plugins_url( 'cb-widgets/css/imageBlock.min.css' ) );
		wp_enqueue_style( 'image-block-css' );
		$this->my_custom_load();
	// Check values
if( $instance) {

     
     $cb_widget_title = esc_textarea($instance['cb-widget-title']);
	 $cb_pic_url = esc_attr($instance['cb-widget-pic-url']);
	$cb_go_destination_link=esc_attr($instance['cb-go-destination-link']);
} else {

	
     
	 $cb_pic_url = '';
	  $cb_go_destination_link='';
	 $cb_widget_title ='';
	 $cb_button_color='';
	
}
       
		$cb_image_width = 'Display Image - recommended image size is ';
		 $label1 = 'Text and Button Color:';
       switch ($this->cb_widget_id )
	   {
		case 'cb_widget_directory_block':
			$cb_image_width .= '453px x 462';
			break;
		case 'cb_front_page_block':
			$cb_image_width .= '460px x 940px';
			break;
	   
	   case 'cb_image_block':
			$cb_image_width .= '460px x 460px';
			break;
		case 'cb_leasing_widget_leasing':
			$cb_image_width .= '92px x 92px';
			break;
		case 'cb_home_page_component':
		$label1 = 'Background Color:';
			$cb_image_width .= '288px x 432px';
			break;
	   
	   }

?>


	<p>
            <label for="<?php echo $this->get_field_name( 'cb-widget-pic-url' ); ?>"><?php _e( $cb_image_width ); ?></label>
            <input name="<?php echo $this->get_field_name( 'cb-widget-pic-url' ); ?>" id="<?php echo $this->get_field_id( 'cb-widget-pic-url' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $cb_pic_url ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
	


	<p><label for="<?php echo $this->get_field_id('cb-go-destination-link'); ?>"><?php _e('Destination Url:', 'cb_widget_directory_block'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-go-destination-link'); ?>" name="<?php echo $this->get_field_name('cb-go-destination-link'); ?>" type="text" value="<?php echo $cb_go_destination_link; ?>" />
	</p>

	<p><label for="<?php echo $this->get_field_id('cb-widget-title'); ?>"><?php _e('Title:', 'cb_widget_directory_block'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-widget-title'); ?>" name="<?php echo $this->get_field_name('cb-widget-title'); ?>" type="text" value="<?php echo $cb_widget_title; ?>" />
	</p>
	




<p><label for="<?php echo $this->get_field_id( 'cb-button-color' ); ?>"><?php echo $label1 ?></label>
	
<?php

	parent::form($instance);
		
	}
   
	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $old_instance;
      // Fields

  
      $instance['cb-widget-title'] = strip_tags($new_instance['cb-widget-title']);
	  $instance['cb-widget-pic-url'] = strip_tags($new_instance['cb-widget-pic-url']);
	  
	//  $instance['cb-button-color'] = $new_instance['cb-button-color'];

     return $instance;
	}


	// widget display
	function widget($args, $instance) {
	// parent::widget($args, $instance);
		extract( $args );
   // these are the widget options
	
	$cb_widget_title ='';
	$cb_pic_url ='';
	$cb_go_destination_link='';
    
	if (isset($instance['cb-widget-title']))
		$cb_widget_title = esc_attr($instance['cb-widget-title']);
	if (isset($instance['cb-widget-pic-url']))
		$cb_pic_url = esc_attr($instance['cb-widget-pic-url']);
	if (isset ($instance['cb-go-destination-link']))
		$cb_go_destination_link =  esc_attr($instance['cb-go-destination-link']);
	$cb_button_color=$instance['cb-button-color'];
	
	 
   echo $before_widget;
   // Display the widget
   echo '<div class="widget-text cb_widget_directory_block_box">';

   // Register style sheet.

  
	
	
	$id = $this->get_id($cb_widget_title);
	
?>
<script>

	 jQuery(document).ready(function($) {
	 
		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		var $button_id='#<?php echo $id ?>' + '_button';
		jQuery($id).css('color',$buttonColor);
		jQuery($button_id).css('background-color',$buttonColor);
		
	 });

</script>
		<div class="cb_out_block_image">
			<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>'>
			<img class="cb_db_image" src=<?php if (isset ($cb_pic_url)) echo "$cb_pic_url" ?> /></a>
		</div>
		<div class="cb_db_image_title_outer">
			<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>'>
			<h3 id='<?php echo $id ?>' class="cb_dir_block_title cb_image_block_text"><?php if (isset ($cb_widget_title)) echo $cb_widget_title?></h3>
			</a>
		</div>
		


		
		
		
		
		
<?php

   
   echo '</div>';
   echo $after_widget;
	}
}
?>