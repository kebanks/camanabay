<?php
/********************************************************************************
*  
*    Front Page widget
*
*	 Widget that has an image and title and go button
*
*	 This widget 
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	

class __CamanaBay_front_page_widget_plugin extends __CamanaBay_directory_widget_plugin{

	// constructor

	function __construct($id = 'cb_front_page_block', $description = 'Camana Bay Front Page Image Block', $opts = array()) {
	  
		if ($id == 'cb_front_page_block')
		$opts['description'] = 'Widget for Front Page -  Image, title, custom color for text and button,button text';

				parent::__construct($id,$description,$opts);
				
		
	}
	
	
	
	// widget form creation
	function form($instance) {	
	
		parent::form($instance);

		if( $instance) {
			$cb_front = esc_attr($instance['cb-front-page-widget-text']);
		} else
		{
			$cb_front = '';
		}
	
		?>	
		
		<p><label for="<?php echo $this->get_field_id('cb-front-page-widget-text'); ?>"><?php _e('Button Text:', 'cb_front_page_block'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-front-page-widget-text'); ?>" name="<?php echo $this->get_field_name('cb-front-page-widget-text'); ?>" type="text" value="<?php echo $cb_front; ?>" />
	</p>
		
		
<?php	
	}
   
	// widget update
	function update($new_instance, $old_instance) {
		 $instance = $old_instance;
      // Fields
		parent::update($new_instance, $old_instance);
		$instance = $old_instance;
		$s = $new_instance['cb-front-page-widget-text'];
			$s =  str_replace(' ', '<br>', $s);
		  $instance['cb-front-page-widget-text'] = strip_tags($s,'<br>');
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
   // these are the widget options
	
	
    
	if (isset($instance['cb-widget-title']))
		$cb_widget_title = esc_attr($instance['cb-widget-title']);
	if (isset($instance['cb-widget-pic-url']))
		$cb_pic_url = esc_attr($instance['cb-widget-pic-url']);
	if (isset ($instance['cb-go-destination-link']))
		$cb_go_destination_link =  esc_attr($instance['cb-go-destination-link']);
	$cb_button_color=$instance['cb-button-color'];
	if (isset(  $instance['cb-front-page-widget-text']))
		$cb_front_page_button_text =   $instance['cb-front-page-widget-text'];
	
	
  
	
  
	
	
	$id = rand();
	echo $before_widget;
   // Display the widget
   echo '<div id="'.$id.'" class="widget-text cb_front_page_block_box">';
?>

<script>

	 jQuery(document).ready(function($) {
	 
		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		var $button_id='#<?php echo $id ?>' + '_button';
	
		jQuery($id + ' .cb_dir_block_title').css('color',$buttonColor);
		jQuery($id + ' .cb_outline_button').css('background-color',$buttonColor);
		var border_css = "1px solid " + $buttonColor;
		jQuery($button_id).css('border',border_css);
		
		
	 });

</script>
		<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>'>
			<h3  class="cb_dir_block_title cb_image_block_text"><?php if (isset ($cb_widget_title)) echo $cb_widget_title?></h3>
		</a>
		<div class="cb_out_block_image">
		<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>'>	<img  class="cb_db_image" src=<?php if (isset ($cb_pic_url)) echo "$cb_pic_url" ?> /></a>
			</div>
				<a  class="cb_outline_button" href='<?php if (isset ($cb_go_destination_link)) echo $cb_go_destination_link?>'>
	
			<div class='cb_button_text'><?php echo $cb_front_page_button_text ?></div>
				
		</a>
		

		
		
		
		
		
<?php

   
   echo '</div>';
   echo $after_widget;
	}
}
?>