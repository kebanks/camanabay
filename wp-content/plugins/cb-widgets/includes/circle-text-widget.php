<?php
/********************************************************************************
*  
*   Circle Text Widget
*
*	  Circle with border, text, transparent background   -  
*
*	
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	

class __CamanaBay_circle_text_widget extends WP_Widget{
	var $textdomain;
	// constructor
	
	  function __construct($id = 'cb_circle_text_widget', $description = 'Camana Bay Circle Text Widget', $opts = array()) {
	
	

		$opts['description'] = "Circle with border, text, transparent background";
		parent::__construct($id,$description,$opts);
		  
		
	}
	
	
	
	// widget form creation
	function form($instance) {	

	
	// Check values
	if( $instance) {
		$cb_circle_text = esc_attr($instance['cb-circle-text']);
		$cb_circle_text_destination = esc_attr($instance['cb-circle-text-destination']);

		} else {
	 
		$cb_circle_text = "";
		$cb_circle_text_destination="";
	}
?>
	<p><label for="<?php echo $this->get_field_id('cb-circle-text'); ?>"><?php _e('Circle Button Text:', 'cb_circle_text_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-circle-text'); ?>" name="<?php echo $this->get_field_name('cb-circle-text'); ?>" type="text" value="<?php echo $cb_circle_text; ?>" />
	</p>
	<p><label for="<?php echo $this->get_field_id('cb-circle-text-destination'); ?>"><?php _e('Destination Link:', 'cb_circle_text_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-circle-text-destination'); ?>" name="<?php echo $this->get_field_name('cb-circle-text-destination'); ?>" type="text" value="<?php echo $cb_circle_text_destination; ?>" />
	</p>
<?php		
}
   
	// widget update
	function update($new_instance, $old_instance) {
		 $instance = $old_instance;
      // Fields

	  $instance['cb-circle-text'] = $new_instance['cb-circle-text'];
	  $instance['cb-circle-text-destination'] = $new_instance['cb-circle-text-destination'];
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		wp_register_style( 'cb-circle-text-css', plugins_url( 'cb-widgets/css/cb-circle.css' ) );
		wp_enqueue_style( 'cb-circle-text-css' );
		$cb_circle_text='';
		$cb_circle_text_destination='';
		
		if (isset($instance['cb-circle-text']))
			$cb_circle_text= esc_attr($instance['cb-circle-text']);
		
			
		if (isset($instance['cb-circle-text-destination']))
			$cb_circle_text_destination= esc_attr($instance['cb-circle-text-destination']);
?>		
		<div class='cb_circle_text'>
			<a class="cb_circle_text_a" href="<?php echo $cb_circle_text_destination ?>">  <?php echo $cb_circle_text ?></a>
			
		
		</div>
<?php		
	}
}
?>