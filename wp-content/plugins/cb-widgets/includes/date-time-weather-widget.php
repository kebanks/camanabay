<?php
/********************************************************************************
*
*    Date Time Weather Widget Plugin
*
*
*
*
*
*
*********************************************************************************/
class __CamanaBay_date_time_weather_widget extends WP_Widget {
	var $textdomain;
	// constructor
	function __CamanaBay_date_time_weather_widget() {


		$widget_ops = array('classname' => 'cb_date_time_weather_block', 'description' => __( 'Camana Bay Date Time Weather.') );
		parent::__construct('cb_date_time_weather', __('Camana Bay Date Time Weather'), $widget_ops);

	}

	// widget form creation
function form($instance) {



	}

	// widget update
function update($new_instance, $old_instance) {
		 $instance = $old_instance;
      // Fields


     return $instance;
	}

	// widget display
function widget($args, $instance) {
		extract( $args );
   // these are the widget options

	date_default_timezone_set('America/Jamaica');
     wp_register_style( 'cb-weather-widget-css', plugins_url( 'cb-widgets/css/date-time-weather.min.css' ) );
	wp_enqueue_style( 'cb-weather-widget-css' );
	$now_ping = 'http://api.openweathermap.org/data/2.5/weather?id=3580733&lang=en&units=imperial&APPID=f975925b9f442652ddd8d0b00df3900c';
	$now_ping_get = wp_remote_get( $now_ping );
	$city_data = json_decode( $now_ping_get['body'] );
	$weather_data['now'] = $city_data;
		$today 			= $weather_data['now'];
	$today_temp 	= round($today->main->temp);
	$units_display_symbol = apply_filters('awesome_weather_units_display', "&deg;" );
 echo $before_widget;
   // Display the widget



?>

	<div class="cb_date_time_weather">
	    <div class="cb_date_time_weather_container">
			<div class="cb_date_time">
				<div class="date_inner">
					<?php echo date("g:iA"); ?>
				</div>
				<div class="cb_date_time_vertical-line">
				</div>
			</div>
			<div class="cb_date_day">
			<div class="date_inner">
		<?php   echo date("l"); ?>
				</div>
				<div class="cb_date_time_vertical-line">
				</div>
			</div>
			<div class="cb_date_date">
		<div class="date_inner">
		<?php  echo date("F j"); ?>
		</div>
			</div>

			<div class="cb_date_temp">
				<div class="date_inner">
			<?php echo $today_temp . $units_display_symbol . "F"; ?>
				</div>
			</div>

            <div class="cb_weather_img">
				<?php  $img_link = plugins_url('cb-widgets/images/weather_icon.png');?>
				<img class="weatherImg" src = "<?php echo $img_link ?>" />
			</div>
		</div>


	</div>
<?php

   echo $after_widget;
	}
}
?>
