<?php
/********************************************************************************
*
*    Home Page Component
*
*	 Widget that has an image and title and go button
*
*	 This widget
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');






class __CamanaBay_home_page_component_widget extends __CamanaBay_front_page_widget_plugin{

	// constructor
	private $cpp;
	function __construct($id = 'cb_home_page_component', $description = 'Camana Bay Home Page Component', $opts = array()) {

		$opts['description'] = 'Widget for Home Page -  Image, title, custom color for background description - user inputs description';
        $this->cpp = new CB_Color_Picker_Plugin();
				parent::__construct($id,$description,$opts);

	}



	// widget form creation
	function form($instance) {

		parent::form($instance);

		if( $instance) {
			$cb_headline = esc_attr($instance['cb-home-page-headline']);
			$cb_sub_headline = esc_attr($instance['cb-home-page-subheadline']);
			$cb_background_color = esc_attr($instance['cb-background-color']);
			$cb_button_color1 = esc_attr($instance['cb-button-color1']);
		} else
		{
			$cb_headline = '';
			$cb_sub_headline = '';
			$cb_background_color='';
			$cb_button_color1='#00b6dd';
		}

		$id2 = rand();

		?>
		<p><label for="<?php echo $this->get_field_id('cb-home-page-headline'); ?>"><?php _e('Headline:', 'cb_home_page_component'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-home-page-headline'); ?>" name="<?php echo $this->get_field_name('cb-home-page-headline'); ?>" type="text" value="<?php echo $cb_headline ; ?>" />
	</p>

	<p><label for="<?php echo $this->get_field_id('cb-home-page-subheadline'); ?>"><?php _e('Sub Headline:', 'cb_home_page_component'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-home-page-subheadline'); ?>" name="<?php echo $this->get_field_name('cb-home-page-subheadline'); ?>" type="text" value="<?php echo $cb_sub_headline; ?>" />
	</p>



<?php
  $color_array = array( "#a87eb1" ,"#cb003c", "#FFB718 ", "#faa519", "#77bc1f","#64cbc8","#00b6dd","#008aab","#d7d1c4","#c4e76a");
	echo "<h2>Colors</h2>";
	echo "<p id='second_array'>";

	foreach($color_array as $key => $color)
	{
		$class = "color_" . $key;

		echo "<a href='#' id='" . $class . "' class='cb_color_select1' >". $color ."</a>" ;


	}
	echo "</p>";

?>



		<p><label for="<?php echo $this->get_field_id( 'cb-button-color1' ); ?>"><?php _e('Title and Button Color:', 'cb_widget_color_picker') ?>;</label>
	<div id="my-color-picker1" class="cb_color_picker_container">
  <p> <input  class="my-color-picker1" type="text" id="<?php echo $this->get_field_id( 'cb-button-color1' ); ?>" name="<?php echo $this->get_field_name( 'cb-button-color1' ); ?>" value="<?php echo $cb_button_color1; ?>" />
	</p></div></p>

<script type='text/javascript'>
      jQuery(document).ready(function($) {
         $('.my-color-picker1').wpColorPicker();
			jQuery('.cb_color_select1').click(function(e) {
					e.preventDefault();
					var id = '#'+jQuery (this).attr('id');
					var hexText =  jQuery(id).text();

				var x = '#<?php echo $this->get_field_id( 'cb-button-color' ); ?> ';
				var id2="#<?php echo $id2 ?>";


				var cr= jQuery("#my-color-picker1").find(".wp-color-result");
		//jQuery(x).val(hexText);

		jQuery(cr).css({backgroundColor: hexText});

		jQuery('.my-color-picker1').wpColorPicker('color', hexText);

	 });
            });
        </script>


<?php



	}

	// widget update
	function update($new_instance, $old_instance) {
	    error_log('new instance ' . print_r($new_instance, TRUE));
		 $instance = $new_instance;
      // Fields
		parent::update($new_instance, $old_instance);


     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
   // these are the widget options



	if (isset($instance['cb-widget-title']))
		$cb_widget_title = esc_attr($instance['cb-widget-title']);
	if (isset($instance['cb-widget-pic-url']))
		$cb_pic_url = esc_attr($instance['cb-widget-pic-url']);
	if (isset ($instance['cb-go-destination-link']))
		$cb_go_destination_link =  esc_attr($instance['cb-go-destination-link']);
	$cb_button_color=$instance['cb-button-color'];
	if (isset(  $instance['cb-front-page-widget-text']))
		$cb_home_page_button_text =  $instance['cb-front-page-widget-text'];
	if (isset(  $instance['cb-home-page-headline']))
		$cb_headline = esc_attr($instance['cb-home-page-headline']);
	if (isset(  $instance['cb-home-page-subheadline']))
		$cb_sub_headline = esc_attr($instance['cb-home-page-subheadline']);
  $cb_button_color1=$instance['cb-button-color1'];




	$id = rand();
	$id1= rand();
	echo $before_widget;
   // Display the widget
   echo '<div id="'.$id.'" class="widget-text cb_home_page_component_box">';
?>

<script>

	 jQuery(document).ready(function($) {

		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $buttonColor1= '<?php echo $cb_button_color1;?>';
		var $id='#<?php echo $id ?>';
		var id1='#<?php echo $id1 ?>';
		var $button_id='#<?php echo $id ?>' + '_button';

		jQuery($id + ' .cb_dir_block_title').css('color',$buttonColor1);
		jQuery($id + ' .cb_outline_button').css('background-color',$buttonColor1);
		var border_css = "1px solid " + $buttonColor;
		jQuery($button_id).css('border',border_css);
		jQuery(id1).css('background-color',$buttonColor);


	 });

</script>
		<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>'>
			<h3  class="cb_dir_block_title cb_image_block_text"><?php if (isset ($cb_widget_title)) echo $cb_widget_title?></h3>
		</a>
		<div class="cb_out_block_image">
		<a href='<?php if (isset ($cb_go_destination_link)) echo "$cb_go_destination_link"?>'>	<img  class="cb_home_image" src=<?php if (isset ($cb_pic_url)) echo "$cb_pic_url" ?> /></a>
			</div>
		<div id="<?php echo $id1 ?>" class="cb_headline-area">
		 <div class="cb_home_headline">
		    <?php echo $cb_headline ?>

		 </div>
		<div class="cb_home_subheadline">
			 <?php echo $cb_sub_headline ?>

		 </div>

		</div>
				<a  class="cb_outline_button" href='<?php if (isset ($cb_go_destination_link)) echo $cb_go_destination_link?>'>

			<div class='cb_button_text'><?php echo $cb_home_page_button_text ?></div>

		</a>







<?php


   echo '</div>';
   echo $after_widget;
	}
}
?>
