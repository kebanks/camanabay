<?php
/********************************************************************************
*  
*    Button on Right Widget
*
*	 
*   Text and arrow on right side with background color.  And link
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');
	
	
	
	


class __CamanaBay_button_on_right_widget extends __CamanaBay_color_picker_widget{
	var $textdomain;
	// constructor
	
	  function __construct($id = 'cb_arrow_on_right_widget', $description = 'Camana Arrow On Right Widget', $opts = array()) {
	

		$opts['description'] = "Text/Link/arrow on right with background color";
		parent::__construct($id,$description,$opts);
		
  }
	
	
	function my_custom_load()
	{
	
		wp_register_style( 'button-on-right-widget', plugins_url( 'cb-widgets/css/buttonOnRightWidget.css' ) );
		wp_enqueue_style( 'button-on-right-widget' );
	
	}
	
	// widget form creation
	function form($instance) {	
	

	// Check values
if( $instance) {

     
     
	
	

	$cb_widget_text_title = esc_attr($instance['cb-button-right-text']);
	 $cb_bottom_link = esc_attr($instance['cb-button-right-dest-link']);
	
	
	

} else {
	

	 $cb_bottom_link = "";

	$cb_widget_text_title='';
	
}

?>

	<p><label for="<?php echo $this->get_field_id('cb-button-right-dest-link'); ?>"><?php _e('Destination Url:', 'cb_arrow_on_right_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-button-right-dest-link'); ?>" name="<?php echo $this->get_field_name('cb-button-right-dest-link'); ?>" type="text" value="<?php echo $cb_bottom_link; ?>" />
	</p>
	
	<p><label for="<?php echo $this->get_field_id('cb-button-right-text'); ?>"><?php _e('Title:', 'cb_arrow_on_right_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-button-right-text'); ?>" name="<?php echo $this->get_field_name('cb-button-right-text'); ?>" type="text" value="<?php echo $cb_widget_text_title; ?>" />
	</p>
	
	

<?php
	parent::form($instance);
			
	}
   
	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $old_instance;
      // Fields


	
      $instance['cb-button-right-dest-link'] = strip_tags($new_instance['cb-button-right-dest-link']);
     


	  $instance['cb-button-right-text'] = strip_tags($new_instance['cb-button-right-text']);
	  
	
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
   
	
    
	if (isset($instance['cb-button-right-dest-link']))
		$cb_destination_link= esc_attr($instance['cb-button-right-dest-link']);	
	
	
		if (isset($instance['cb-button-right-text']))
				$cb_widget_text_title = esc_attr($instance['cb-button-right-text']);

	
	$cb_button_color=$instance['cb-button-color'];
	
	 
   echo $before_widget;
   // Display the widget
   echo '<div class="widget-text cb_arrow_on_right_widget_box">';


	$id = rand();
	$this->my_custom_load();
	$arrow_image_url = plugin_dir_url(__FILE__) . '../images/white_arrow_small_right.png';
	
?>
		<div  id='<?php echo $id ?>' class='cb_arrow_on_right_widget_outside'>
			
			
		
			<div class="cb_button_on_right_detail">
			<h2 class='cb_arrow_on_right_widget_h2'> <?php if (isset ($cb_widget_text_title)) echo $cb_widget_text_title?></h2>
			<a class="cb_arrow_widget" href="<?php echo $cb_destination_link ?>"> <span  > <img id='cb_arrow_button'    src=" <?php echo $arrow_image_url ?>"  /></span></a>
	
			
			</div>
		</div>
	
	


	 
	 <script>

	 jQuery(document).ready(function($) {
	 
		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
	    
		
		jQuery($id).css('background-color',$buttonColor);
		
	 });

</script>
		
	
		
<?php

   
   echo '</div>';
   echo $after_widget;
	}
}
?>