<?php
/********************************************************************************
*
*    Event Action Widget
*
*
*    Has Picture/Description plus action button
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');






class __CamanaBay_event_action_widget extends __CamanaBay_color_picker_widget{
	var $textdomain;
	// constructor

	  function __construct($id = 'cb_event_action_widget', $description = 'Camana Bay Event Action Widget', $opts = array()) {

		add_action( 'admin_enqueue_scripts', array(&$this, 'admin_styles') );
		$opts['description'] = "Picture/Description plus action button";
		parent::__construct($id,$description,$opts);




	}
	function admin_styles()
	{
		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__DIR__) . 'js/upload-media.js', array('jquery'));


        wp_enqueue_style('thickbox');

	}

	function my_custom_load()
	{

		wp_register_style( 'event-action', plugins_url( 'cb-widgets/css/eventAction.css' ) );
		wp_enqueue_style( 'event-action' );

	}

	// widget form creation
	function form($instance) {


	// Check values
if( $instance) {



	 $cb_pic_url1 = esc_attr($instance['cb-widget-pic-url1']);

	$cb_widget_text = esc_textarea($instance['cb-widget-text']);
	$cb_widget_text_title = esc_attr($instance['cb-widget-text-title']);
	 $cb_bottom_link = esc_attr($instance['cb-destination-link']);
	 $cb_bottom_link_text = esc_attr($instance['cb-destination-link-text']);
	 if (isset($instance['cb-ev-action-add-link']))
		$cb_text_make_link = $instance['cb-ev-action-add-link'];
	else
		$cb_text_make_link = "NO";



} else {
	$cb_pic_url1 = "";
	$cb_widget_text='';
	 $cb_bottom_link = "";
	 $cb_bottom_link_text = "";
	 $cb_widget_text_title = '';
	$cb_text_make_link = "NO";
}




?>




	<p><label for="<?php echo $this->get_field_id('cb-widget-pic-url1'); ?>"><?php _e('Image (best size 382px by 295) :', 'cb_event_action_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-widget-pic-url1'); ?>" name="<?php echo $this->get_field_name('cb-widget-pic-url1'); ?>" type="text" value="<?php echo $cb_pic_url1; ?>" />
<input class="upload_image_button" type="button" value="Upload Image" />
	</p>




	<p><label for="<?php echo $this->get_field_id('cb-destination-link'); ?>"><?php _e('Destination Url:', 'cb_event_action_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-destination-link'); ?>" name="<?php echo $this->get_field_name('cb-destination-link'); ?>" type="text" value="<?php echo $cb_bottom_link; ?>" />
	</p>


	<p><label for="<?php echo $this->get_field_name('cb-ev-action-add-link'); ?>">Add the link? </label>

	<?php  if ($cb_text_make_link == "YES")
					$checked="checked";
					else
					$checked = "unchecked";

				?>

 <input type="checkbox" id="<?php echo $this->get_field_name('cb-ev-action-add-link'); ?>"   name="<?php echo $this->get_field_name('cb-ev-action-add-link'); ?>" value="YES"<?php echo $checked; ?>>


 </p>



	<p><label for="<?php echo $this->get_field_id('cb-destination-link-text'); ?>"><?php _e('Action Text:', 'cb_event_action_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-destination-link-text'); ?>" name="<?php echo $this->get_field_name('cb-destination-link-text'); ?>" type="text" value="<?php echo $cb_bottom_link_text; ?>" />
	</p>

	<p><label for="<?php echo $this->get_field_id('cb-widget-text-title'); ?>"><?php _e('Title:', 'cb_event_action_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-widget-text-titlet'); ?>" name="<?php echo $this->get_field_name('cb-widget-text-title'); ?>" type="text" value="<?php echo $cb_widget_text_title; ?>" />
	</p>

	<p><label for="<?php echo $this->get_field_id('cb-widget-text'); ?>"><?php _e('Text:', 'cb_widget_image_block'); ?></label>
	<textarea class="widefat" id="<?php echo $this->get_field_id('cb-widget-text'); ?>" name="<?php echo $this->get_field_name('cb-widget-text'); ?>"><?php echo $cb_widget_text; ?></textarea>
	</p>


<?php
	parent::form($instance);

	}

	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $new_instance;
      // Fields



      $instance['cb-destination-link'] = strip_tags($new_instance['cb-destination-link']);
      $instance['cb-destination-link-text'] = strip_tags($new_instance['cb-destination-link-text']);
	  $instance['cb-widget-pic-url1'] = strip_tags($new_instance['cb-widget-pic-url1']);
	 $instance['cb-widget-text'] = strip_tags($new_instance['cb-widget-text']);
	  $instance['cb-widget-text-title'] = strip_tags($new_instance['cb-widget-text-title']);


     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
   // these are the widget options



	if (isset($instance['cb-destination-link']))
		$cb_destination_link= esc_attr($instance['cb-destination-link']);

	if (isset($instance['cb-destination-link-text']))
		$cb_destination_link_text = esc_attr($instance['cb-destination-link-text']);


	if (isset($instance['cb-widget-pic-url1']))
		$cb_pic_url1 = esc_attr($instance['cb-widget-pic-url1']);
	if (isset($instance['cb-widget-text']))
		$cb_widget_text = esc_textarea($instance['cb-widget-text']);
		if (isset($instance['cb-widget-text-title']))
	  $cb_widget_text_title = esc_attr($instance['cb-widget-text-title']);
	  if (isset($instance['cb-ev-action-add-link']))
		$cb_ev_action_make_link = $instance['cb-ev-action-add-link'];
	else
		$cb_ev_action_make_link = "NO";




	$id = $this->get_id($cb_destination_link_text);
	$cb_button_color=$instance['cb-button-color'];


   echo $before_widget;
   // Display the widget
   echo '<div class="widget-text cb_event_action_widget_box">';

   // Register style sheet.
	$id = $this->get_id($cb_destination_link_text);
	$this->my_custom_load();


?>
	<div class="cb_event_widget_outside">
	<div class="event_action_left">
        <img src="<?php echo $cb_pic_url1 ?>"/>
<?php if ($cb_ev_action_make_link == "YES") { ?>
		<div class="event_action_bottom">
		<a  href="<?php echo $cb_destination_link ?>"  target="_blank"> <span id='<?php echo $id ?>' class="event_text" > <?php echo $cb_destination_link_text ?></span></a>
		</div>
<?php } ?>
	</div>
	<div class="event_action_right">
<?php	if (!empty($cb_widget_text)) { ?>
		<h1><?php if (isset ($cb_widget_text_title)) echo $cb_widget_text_title?></h1>
		<div class="event_body_text"><?php if (isset ($cb_widget_text)) echo $cb_widget_text?></div>
	<?php } ?>
	</div>
	</div>

	 <script>

	 jQuery(document).ready(function($) {

		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		jQuery($id).css('color',$buttonColor);
		var borderInfo = "1px solid " + $buttonColor;
		jQuery('.event_action_bottom').css('border',borderInfo);

/*		var l_height = jQuery ('.event_action_right').height();

		var l_height1= jQuery ('.event_action_left').css('min-height');

		var l_height2 = parseInt(l_height1, 10);

		if (l_height < l_height2)

			l_height = l_height2;

	jQuery('.event_action_left').height(l_height);


		x=jQuery('.event_action_left').offset().left;
		width=jQuery('.event_action_left').width();

		var outer_width = jQuery('.event_action_bottom').outerWidth();

		bottom_left= parseInt((width - outer_width)/2);

		w = width/2;
		ww = x+w;


		/*y=jQuery('.event_action_left').offset().top;
		zz = jQuery('.event_action_bottom').outerHeight();
		z_padding = jQuery('event_action_bottom').css('padding-top');
		z = y+ l_height-zz ;


		jQuery(".event_action_bottom").offset({ top: z});

		jQuery(".event_action_bottom").offset({ left: bottom_left});
		*/
	 });

</script>



<?php


   echo '</div>';
   echo $after_widget;
	}
}
?>
