<?php
/********************************************************************************
*
*   Full Action Widget
*
*
* 	 Logo on Left, Title, subtitle , arrows
*
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');





class __CamanaBay_full_action_widget extends __CamanaBay_color_picker_widget{
	var $textdomain;
	// constructor

	  function __construct($id = 'cb_full_action_widget', $description = 'Camana Bay Full Action Widget', $opts = array()) {

		 add_action( 'admin_enqueue_scripts', array(&$this, 'my_custom_load') );
		$opts['description'] = $description;
		$opts['description'] = "Logo on Left, Title, subtitle , arrows ";
		parent::__construct($id,$description,$opts);


	}

	function my_custom_load() {
    //  this is for the picture links
		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget',plugin_dir_url(__DIR__) . 'js/upload-media.js', array('jquery'));


        wp_enqueue_style('thickbox');

    }

	// widget form creation
	function form($instance) {


	// Check values
	if( $instance) {
		$cb_full_action_title = esc_attr($instance['cb-full-action-title']);
		$cb_full_action_sub_title = esc_attr($instance['cb-full-action-sub-title']);
		$cb_full_action_title_destination = esc_attr($instance['cb-full-action-destination']);
		$cb_full_action_logo = esc_attr($instance['cb-full-action-logo-url']);

		} else {

		$cb_full_action_title = "";
		$cb_full_action_sub_title = "";
		$cb_full_action_title_destination="";
		$cb_full_action_logo='';
	}
?>
	<p>
            <label for="<?php echo $this->get_field_name( 'cb-full-action-logo-url' ); ?>"><?php _e( 'Logo:' ); ?></label>
            <input name="<?php echo $this->get_field_name( 'cb-full-action-logo-url' ); ?>" id="<?php echo $this->get_field_id( 'cb-full-action-logo-url' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $cb_full_action_logo ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>

	<p><label for="<?php echo $this->get_field_id('cb-full-action-title'); ?>"><?php _e('Main Text:', 'cb_full_action_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-full-action-title'); ?>" name="<?php echo $this->get_field_name('cb-full-action-title'); ?>" type="text" value="<?php echo $cb_full_action_title; ?>" />
	</p>

	<p><label for="<?php echo $this->get_field_id('cb-full-action-sub-title'); ?>"><?php _e('Sub Text:', 'cb_full_action_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-full-action-sub-title'); ?>" name="<?php echo $this->get_field_name('cb-full-action-sub-title'); ?>" type="text" value="<?php echo $cb_full_action_sub_title; ?>" />
	</p>
	<p><label for="<?php echo $this->get_field_id('cb-full-action-destination'); ?>"><?php _e('Destination Link:', 'cb_full_action_widget'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-full-action-destination'); ?>" name="<?php echo $this->get_field_name('cb-full-action-destination'); ?>" type="text" value="<?php echo $cb_full_action_title_destination; ?>" />
	</p>
<?php
		parent::form($instance);
}

	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $old_instance;
      // Fields

	  $instance['cb-full-action-title'] = $new_instance['cb-full-action-title'];
	  $instance['cb-full-action-sub-title'] = $new_instance['cb-full-action-sub-title'];
	  $instance['cb-full-action-destination'] = $new_instance['cb-full-action-destination'];
	  $instance['cb-full-action-logo-url'] = $new_instance['cb-full-action-logo-url'];
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		wp_register_style( 'cb-full-action-css', plugins_url( 'cb-widgets/css/cb-full-action.min.css' ) );
		wp_enqueue_style( 'cb-full-action-css' );
		$cb_full_action_title = '';
		$cb_full_action_sub_title = '';
		$cb_action_logo_url = '';
		$cb_full_action_title_destination='';


		if (isset($instance['cb-full-action-title']))
			$cb_full_action_title= esc_attr($instance['cb-full-action-title']);
		if (isset($instance['cb-full-action-sub-title']))
			$cb_full_action_sub_title= esc_attr($instance['cb-full-action-sub-title']);
		if (isset($instance['cb-full-action-destination']))
			$cb_full_action_title_destination= esc_attr($instance['cb-full-action-destination']);
		if (isset($instance['cb-full-action-logo-url']))
			$cb_action_logo_url = esc_attr($instance['cb-full-action-logo-url']);
		$cb_button_color=$instance['cb-button-color'];
		$cb_full_action_chevron = plugins_url(). '/cb-widgets/images/white-chevrons.png';
		$id = $this->get_id($cb_full_action_title);
?>


<script>

	 jQuery(document).ready(function($) {

		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		var $button_id='#<?php echo $id ?>' + '_button';
		jQuery($id).css('color','white');

		jQuery($id).css('backgroundColor',$buttonColor);


	 });

</script>
		<div id="<?php echo $id ?>"  class='full-action-container'>
			<div class="cb_full_action_img">
				<img  src=<?php if (isset ($cb_action_logo_url)) echo "$cb_action_logo_url" ?> /></a>
			</div>
			<div class="cb_full_action_right_side">
				<div  class='cb_full_action_title'>
					<p>
					<?php echo $cb_full_action_title ?>
					</p>


				</div>
				<div class='cb_full_action_sub_title'>
					<?php echo $cb_full_action_sub_title ?>
				</div>
			</div>
			<div class='cb_full_action_dbl_arrows'>
				<img  src='<?php echo $cb_full_action_chevron ?>'/>
			</div>

			<a class="linkRetail"  href='<?php echo $cb_full_action_title_destination ?>'>  </a>
		</div>
<?php
	}
}
?>
