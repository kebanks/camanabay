<?php
/********************************************************************************
*  
*    Double Image Widget 
*
*	 Widget with 2 images side by side and link at bottom
*
*	
*
*
*
*
*********************************************************************************/


// Block direct requests
if ( !defined('ABSPATH') )
	die('-1');

class __CamanaBay_double_image_widget extends __CamanaBay_color_picker_widget{
	var $textdomain;
	// constructor
	
	  function __construct($id = 'cb_widget_double_image', $description = 'Camana Bay Double Image', $opts = array()) {
	
		add_action( 'admin_enqueue_scripts', array(&$this, 'admin_styles') );
		$opts['description'] = 'image on left and image on right with action button at bottom ';
		parent::__construct($id,$description,$opts);
        
		
		
	}
	function admin_styles()
	{
		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__DIR__) . 'js/upload-media.js', array('jquery'));
      

        wp_enqueue_style('thickbox');
	
	}
	
	function my_custom_load()
	{
	
	wp_register_style( 'cb_double_image_css', plugins_url( 'cb-widgets/css/dblImage.css' ) );
	wp_enqueue_style( 'cb_double_image_css' );
	
	}
	
	// widget form creation
	function form($instance) {	
	

	// Check values
if( $instance) {

     
     
	 $cb_pic_url1 = esc_attr($instance['cb-widget-pic-url1']);
	
	 $cb_pic_url2 = esc_attr($instance['cb-widget-pic-url2']);
	 $cb_bottom_link = esc_attr($instance['cb-destination-link']);
	 $cb_bottom_link_text = esc_attr($instance['cb-destination-link-text']);
	 $cb_pic_url3 = esc_attr($instance['cb-widget-pic-url3']);
	 $cb_image_loc = esc_attr($instance['cb-widget-image-loc']);

	 $cb_image_loc = esc_attr($instance['cb-widget-image-loc']);	

} else {
	$cb_pic_url1 = "";
	 $cb_pic_url2 ="";
	 $cb_pic_url3 ="";
	 $cb_bottom_link = "";
	 $cb_bottom_link_text = "";
	$cb_image_loc ='left';
	
}

	
        

?>




	<p><label for="<?php echo $this->get_field_id('cb-widget-pic-url1'); ?>"><?php _e('Left Image (best size 445px by 425px):', 'cb_widget_double_image'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-widget-pic-url1'); ?>" name="<?php echo $this->get_field_name('cb-widget-pic-url1'); ?>" type="text" value="<?php echo $cb_pic_url1; ?>" />
<input class="upload_image_button" type="button" value="Upload Image" />
	</p>
	
	<p><label for="<?php echo $this->get_field_id('cb-widget-pic-url2'); ?>"><?php _e('Right Image (best size 445px by 425px):', 'cb_widget_double_image'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-widget-pic-url2'); ?>" name="<?php echo $this->get_field_name('cb-widget-pic-url2'); ?>" type="text" value="<?php echo $cb_pic_url2; ?>" />
	<input class="upload_image_button" type="button" value="Upload Image" />
	</p>


	<p><label for="<?php echo $this->get_field_id('cb-destination-link'); ?>"><?php _e('Destination Url:', 'cb_widget_double_image'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-destination-link'); ?>" name="<?php echo $this->get_field_name('cb-destination-link'); ?>" type="text" value="<?php echo $cb_bottom_link; ?>" />
	</p>
	
	
	<p><label for="<?php echo $this->get_field_id('cb-destination-link-text'); ?>"><?php _e('Bottom Text:', 'cb_widget_double_image'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-destination-link-text'); ?>" name="<?php echo $this->get_field_name('cb-destination-link-text'); ?>" type="text" value="<?php echo $cb_bottom_link_text; ?>" />
	</p>

<p><label for="<?php echo $this->get_field_id('cb-widget-pic-url3'); ?>"><?php _e('Single Image (best size 460px by 460px):', 'cb_widget_double_image'); ?></label>
	<input class="widefat" id="<?php echo $this->get_field_id('cb-widget-pic-url3'); ?>" name="<?php echo $this->get_field_name('cb-widget-pic-url3'); ?>" type="text" value="<?php echo $cb_pic_url3; ?>" />
<input class="upload_image_button" type="button" value="Upload Image" />
	</p>
<p><label for="<?php echo $this->get_field_id('cb-widget-image-loc'); ?>"><?php _e('Single Image Placement:', 'cb_widget_double_image'); ?></label>
	 <input type="radio" name="<?php echo $this->get_field_name('cb-widget-image-loc'); ?>" <?php if (isset($cb_image_loc) && $cb_image_loc=="left") echo "checked";?>  value="left">left
   <input type="radio" name="<?php echo $this->get_field_name('cb-widget-image-loc'); ?>" <?php if (isset($cb_image_loc) && $cb_image_loc=="right") echo "checked";?>  value="right">right
 </p>
<?php
	parent::form($instance);
			
	}
   
	// widget update
	function update($new_instance, $old_instance) {
	 $parent_instance = parent::update($new_instance, $old_instance);
		 $instance = $old_instance;
      // Fields


	
      $instance['cb-destination-link'] = strip_tags($new_instance['cb-destination-link']);
      $instance['cb-destination-link-text'] = strip_tags($new_instance['cb-destination-link-text']);
	  $instance['cb-widget-pic-url1'] = strip_tags($new_instance['cb-widget-pic-url1']);
	  $instance['cb-widget-pic-url2'] = strip_tags($new_instance['cb-widget-pic-url2']);
	  $instance['cb-widget-pic-url3'] = strip_tags($new_instance['cb-widget-pic-url3']);
	  
	
     return $instance;
	}

	// widget display
	function widget($args, $instance) {
		extract( $args );
   // these are the widget options

	$cb_destination_link = '';
	$cb_destination_link_text = '';
	$cb_pic_url1 = '';
	$cb_pic_url2 = '';
	$cb_pic_url3 = '';
	$cb_image_loc ='left';
    
	if (isset($instance['cb-destination-link']))
		$cb_destination_link= esc_attr($instance['cb-destination-link']);
	if (isset($instance['cb-destination-link-text']))
		$cb_destination_link_text = esc_attr($instance['cb-destination-link-text']);
	if (isset($instance['cb-widget-pic-url1']))
		$cb_pic_url1 = esc_attr($instance['cb-widget-pic-url1']);
	if (isset($instance['cb-widget-pic-url2']))
		$cb_pic_url2 = esc_attr($instance['cb-widget-pic-url2']);
	if (isset($instance['cb-widget-pic-url3']))
			$cb_pic_url3 = esc_attr($instance['cb-widget-pic-url3']);
	if (isset($instance['cb-widget-image-loc']))
		$cb_image_loc = esc_attr($instance['cb-widget-image-loc']);


	$id = $this->get_id($cb_destination_link_text);
	$cb_button_color=$instance['cb-button-color'];
	
	 
   echo $before_widget;
   // Display the widget
   echo '<div class="widget-text cb_widget_double_image_box">';

   // Register style sheet.

	$this->my_custom_load();
	if ($cb_image_loc == 'left')
	{
		$dbl_img_class='dbl_go_right';
		$single_img_class	='dbl_single_go_left';
		
	}else {
		$dbl_img_class='dbl_go_left';
		$single_img_class ='dbl_single_go_right';
	}
?>

<script>

	 jQuery(document).ready(function($) {
	 
		var $buttonColor= '<?php echo $cb_button_color;?>';
		var $id='#<?php echo $id ?>';
		jQuery($id).css('color',$buttonColor);
		jQuery($id).css('background-color',$buttonColor);
		jQuery('.dbl_image_bottom').css('background-color',$buttonColor);
	
		
	 });

</script>

		<div id="dbl_single_image_container" class="<?php echo $single_img_class ?>">
			
		 <img src="<?php echo $cb_pic_url3 ?>"/>

	
		</div>

		<div id="dbl_img_container" class="<?php echo $dbl_img_class ?>" >
				<div id="dbl_image_left">
					 <img src="<?php echo $cb_pic_url1 ?>"/>
				</div>
				<div id="dbl_image_right">
					 <img src="<?php echo $cb_pic_url2 ?>"/>
				 <div class='dbl_image_bottom'><a  href="<?php echo $cb_destination_link ?>"> <div id='<?php echo $id ?>' > <?php echo $cb_destination_link_text ?></div></a>
		    </div>
				</div>
			<!--	<a  href="<?php echo $cb_destination_link ?>"> <span id='<?php echo $id ?>' class="dbl_image_bottom"> <?php echo $cb_destination_link_text ?></span></a>-->
			
		</div>
	

	
		
<?php

   
   echo '</div>';
   echo $after_widget;
	}
}
?>