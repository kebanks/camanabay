<?php
/*
Plugin Name: Widgets for Camana Bay Plugin
Plugin URI: http://www.camanabay.com/
Description: Widgets for Camanabay's custom template
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php

require_once(dirname(__FILE__) . '/includes/color-picker-widget.php');
require_once(dirname(__FILE__) . '/includes/directory-block-widget.php');
require_once(dirname(__FILE__) . '/includes/front-page-widget.php');



require_once(dirname(__FILE__) . '/includes/date-time-weather-widget.php');

require_once(dirname(__FILE__) . '/includes/image-block-widget.php');
require_once(dirname(__FILE__) . '/includes/double-image-widget.php');
require_once(dirname(__FILE__) . '/includes/circle-text-widget.php');
require_once(dirname(__FILE__) . '/includes/event-action-widget.php');
require_once(dirname(__FILE__) . '/includes/horiz-text-button.php');
require_once(dirname(__FILE__) . '/includes/circle-gift-widget.php');
require_once(dirname(__FILE__) . '/includes/full-action-widget.php');
require_once(dirname(__FILE__) . '/includes/amenity-widget.php');
require_once(dirname(__FILE__) . '/includes/button-on-right-widget.php');
require_once(dirname(__FILE__) . '/includes/blog-article-widget.php');
require_once(dirname(__FILE__) . '/includes/blog-latest-widget.php');
require_once(dirname(__FILE__) . '/includes/cb-ad-widget.php');
require_once(dirname(__FILE__) . '/includes/cb_event_detail_holder.php');
require_once(dirname(__FILE__) . '/includes/cb_leasing_widget.php');
require_once(dirname(__FILE__) . '/includes/cb_movie_display_widget.php');
require_once(dirname(__FILE__) . '/includes/cb_event_action_holder.php');
require_once(dirname(__FILE__) . '/includes/home-page-widget.php');
require_once(dirname(__FILE__) . '/includes/cb-image-widget.php');



// register widget

add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_image_widget_plugin");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_directory_widget_plugin");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_front_page_widget_plugin");'));

add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_date_time_weather_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_double_image_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_color_picker_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_circle_text_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_event_action_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_Horizontal_Text_Link");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_circle_gift_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_full_action_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_amenity_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_button_on_right_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_blog_article_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_blog_latest_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_ad_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_event_detail_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_leasing_widget_plugin");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_movie_display_widget_plugin");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_event_action_holder_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_home_page_component_widget");'));
add_action('widgets_init', create_function('', 'return register_widget("__CamanaBay_image_widget");'));

 



