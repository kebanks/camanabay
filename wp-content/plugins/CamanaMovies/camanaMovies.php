<?php
/*
Plugin Name: Camana Movie Plugin
Plugin URI: http://www.camanabay.com/
Description: Camana Movie Plugin
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php
/*******************************************************************************************************************
*
*		Camana Movie Plugin 
*
*		Plugin that handles the creation, listing and display of Camana Movie elements
*
*
*
*
*
**********************************************************************************************************************/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access
}
require_once 'includes/cbMovieDisplay.php';


if ( ! class_exists( 'CamanayMoviePlugin' ) ) :


class CamanayMoviePlugin {

 /**
     * @var string
     */
    public $version = '1.0';
	private $edit_id ;
	public static function init() {

        $CamanaMovie = new self();

    }
	public function __construct() {

      
	   add_action( 'my_daily_event',  array($this, 'update_db_daily' ));
	  
        $this->setup_actions();
        

    }
	 /**
     * Hook camanaMovie into WordPress
     */
    private function setup_actions() {
	
	
	/*   we want a separate menu for our plugin 
	 */
	 
		
		//add_action( 'init', array($this, 'cb_movie_plugin_init' ));

		
		add_action( 'admin_menu', array($this,  'register_admin_menu' ));
		
		
		
      
		 
	
	
	}
	
	
	public function register_admin_menu() {

	
			add_menu_page ( 'Camana Movie Files', 'CB Regenerate Movie Files ', 'manage_options', 'cb-movie-listing',array($this,'cb_movie_plugin_options_page'))	;

			
		
	}
	function ftp_func($conn_id, $local_file,$server_file)
	
	{
		$ret = FALSE;
		
		if (ftp_get($conn_id, $local_file, $server_file, FTP_BINARY)) {
			$ret = TRUE;
			echo "<p><b>" . $local_file . "</b> successfully created </p>";
			error_log( "Successfully written to" . $local_file);
		} else {
			echo "<p>Could not create file <b>" . $local_file . ".</b> Check if server file <b> " . $server_file . " </b>exists.</p>";
			error_log( "There was a problem with file " . $server_file);
		}
		
		return $ret;

	
	}
	
	function _get_server_file_name($suffix, $name)
	{
		
	
		$server_name = $name . $suffix;
	
		return $server_name;
	}
	function _get_server_name()
	{
		$name = '';
	  if ( isset($_SERVER['DOCUMENT_ROOT']))
	     $name = $_SERVER['DOCUMENT_ROOT'];
	else 
		$name = $_SERVER['PWD'];
		
		return $name;
	
	
	}

	function _get_local_file_name($file_name, $name)
	{
	
	//$_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/CamanaMovies/

		$dir = $this->_get_server_name() . '/wp-content/uploads/CamanaMovies/';
	
		$local_name = $dir  . $name . $file_name;
	
		return $local_name;
	
	}
	function download_the_files($conn_id, $date)
	{
		$ret = TRUE;
		$dir = $this->_get_server_name() . '/wp-content/uploads/CamanaMovies/';
		$file_name = $date . ".xml.gz";
		$local_file =  $this->_get_local_file_name( $file_name, 'on_caym_mov_programs_' );
		$server_file = $this->_get_server_file_name($file_name, '/On2/caym/on_caym_mov_programs_' );
		$ret =  $this->ftp_func($conn_id, $local_file,$server_file);
		
	
		$local_file =  $this->_get_local_file_name($file_name, 'on_caym_mov_schedules_' );
		$server_file = $this->_get_server_file_name($file_name,'/On2/caym/on_caym_mov_schedules_');
		$ret &= $this->ftp_func($conn_id, $local_file,$server_file);
		
		
		$local_file =  $this->_get_local_file_name($file_name, 'on_common_' );
		$server_file = $this->_get_server_file_name($file_name,'/On2/on_common_');
		$ret &= $this->ftp_func($conn_id, $local_file,$server_file);
		
		$local_file =  $this->_get_local_file_name($file_name, 'on_caym_mov_sources_' );
		$server_file = $this->_get_server_file_name($file_name,'/On2/caym/on_caym_mov_sources_');
		$ret &= $this->ftp_func($conn_id, $local_file,$server_file);
		
		return $ret;
		
		
	
	}
	
	function unzip_the_files($date)
	{
		$file_name = $date . ".xml.gz";
		$ret = TRUE;
		
		$ret &= $this->unzip_gz_file($file_name, 'on_caym_mov_programs_');
			
		
		$ret &= $this->unzip_gz_file($file_name, 'on_caym_mov_schedules_');
		
	
		$ret &= $this->unzip_gz_file($file_name, 'on_common_');
		
		
		$ret &= $this->unzip_gz_file($file_name, 'on_caym_mov_sources_');
		
		return $ret;
	}
	function get_the_images($conn_id,$date)
	{		
	
	


			$file_name = $date . ".xml";
			$local_file =  $this->_get_local_file_name( $file_name, 'on_caym_mov_programs_' );
			if (file_exists($local_file)) {
				$xml = simplexml_load_file($local_file);
			
			
				foreach($xml->programs->program as $p)
				{
			
			
				foreach($p->images as $i)
				{
					foreach($i->image as $ii){
					
					if ($ii->attributes()->category[0] == "Poster Art"){
						
							
						  
						  $server_file =  '/photos/movies/'. $ii->URI[0];
						  
						$name = basename($server_file);
						
						if (stristr($name, '_t') == TRUE)
							continue;
						
					
					$dir = $this->_get_server_name() . '/wp-content/uploads/CamanaMovies/';
	
			
				
					$local_movie_file = $dir . 'images/' . $name ;
					
					
			        
					$ret = ftp_nb_get($conn_id, $local_movie_file, $server_file, FTP_BINARY);
					if ($ret ==  FTP_FAILED)
						error_log('Ret is ' . "FTP_FAILED");
						
						
					while ($ret == FTP_MOREDATA) {
   
   

				// Continue downloading...
					$ret = ftp_nb_continue($conn_id);
					if ($ret ==  FTP_FAILED)
					error_log('Ret in more data loop  is ' . "FTP_FAILED");
					}
					if ($ret != FTP_FINISHED) {
					
					error_log( "There was a problem with image file " . $server_file);
					
					}
					else 
					
					error_log( "Image Successfully written to" . $local_movie_file);
					/*
					if (ftp_nb_get($conn_id, $local_movie_file, $server_file, FTP_BINARY)) {
			error_log( "Image Successfully written to" . $local_movie_file);
		} else {
			error_log( "There was a problem with image file " . $server_file);
		}*/
					}
					}
					}
				}
			}else error_log($local_file . " does not exist ");
				
				
	
	
	}
	function unzip_gz_file($suffix, $file_name)
	{
	
	$local_file =  $this->_get_local_file_name($suffix, $file_name );
	
	
	$ret = FALSE;	
	
	if (file_exists ( $local_file )){
			
		
	
	
// Raising this value may increase performance
		$buffer_size = 4096; // read 4kb at a time
		$out_file_name = str_replace('.gz', '', $local_file);

// Open our files (in binary mode)
		$file = gzopen($local_file, 'rb');
		$out_file = fopen($out_file_name, 'wb');

// Keep repeating until the end of the input file
		while(!gzeof($file)) {
    // Read buffer-size bytes
    // Both fwrite and gzread and binary-safe
			fwrite($out_file, gzread($file, $buffer_size));
		}

// Files are done, close files
		fclose($out_file);
		gzclose($file);
		unlink($local_file);
		$ret = TRUE;
	}
	return $ret;
}
	function do_the_ftp()
	{
	date_default_timezone_set('America/Jamaica');

		$ftp_server = 'on.tmstv.com';
		$conn_id = ftp_connect($ftp_server);
		
		$ftp_user_name = "cmnshdon";
		$ftp_user_pass = "919is722";
		

		$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
		ftp_pasv($conn_id, TRUE);
		$date =  date("Ymd");
		//error_log('the date is ' . date("F j, Y, g:i a"));
	    $ret =  $this->download_the_files($conn_id, $date);
		//error_log('ret after download is ' . $ret);
		if ($ret){
			$this->unzip_the_files($date);
			$this->get_the_images($conn_id, $date);
		}
		ftp_close($conn_id);
		
		
		return $ret;

	}
  
 
	public static function cb_create_daily_backup_schedule() {
		
		//error_log('event scheduled');
		//wp_schedule_event( time(), 'daily', 'my_daily_event' );
		wp_schedule_event( strtotime('10:00:00'), 'daily', 'my_daily_event' );
		
	} // end activate
	public function update_db_daily() {
	
		$ret = $this->do_the_ftp();
		if ($ret)
			error_log( "Successfully done!");
		else 
			error_log("Download and unzip not completed.  Check source files ");
	
	
	}
	
	public static function run_on_deactivate() {
		wp_clear_scheduled_hook('my_daily_event');

	} // end activate
	
	

	
	/********************************************************
	 *
	 *		cb_movie_plugin_options_page
	 *
	 *		Options page and form for Movie  Plugin
	 *
	 *
	 *
	 **************************************************/
	public function cb_movie_plugin_options_page()
	{

	
		
		
		
	   ?>
    <div class="wrap">
 <h1> Remake the movie files </h1>
        <form accept-charset="UTF-8" action="" method="post">
		
		
		
	
          
		 
            <?php 
				
				
			
			
			
	
	
         submit_button("Process New Movie Files");



			 ?>
		 
        </form>
    </div>

    <?php
	if(isset($_POST['submit'])) {
		echo "post submitted";
		
		$ret = $this->do_the_ftp();
		if ($ret)
			echo "<h3>Successfully done! </h3>";
		else 
			echo "<h3>Download and unzip not completed.  Check source files </h3>";
	} else {
		echo "not submitted yet";
	}
 }

	
}	
	
endif;

add_action( 'plugins_loaded', array( 'CamanayMoviePlugin', 'init' ), 10 );
 

  
  register_activation_hook( __FILE__, array( 'CamanayMoviePlugin', 'cb_create_daily_backup_schedule' ) );

  
  register_deactivation_hook( __FILE__, array('CamanayMoviePlugin', 'run_on_deactivate') );
  
  
  
  
  
