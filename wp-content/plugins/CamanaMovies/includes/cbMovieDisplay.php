<?php
/******************************************************************************
*
*		cbMovieDisplay.php
*
*	    Displays the Camana Bay Movies via the short code showCB_Movies 
*
*****************************************************************************/


require_once 'cbMovieFuncs.php';
class _CB_Movie_Display extends _CB_Movie_Funcs{


	private $type;
	public function __construct()

    {

		add_shortcode( 'showCB_Movies', array( $this, 'cb_show_CB_Movie_Display_func' ) );

		add_action( 'wp_enqueue_scripts', array( $this,'cb_show_CB_Movie_Display_enqueue_styles' ));
	
	}
	
	
	
	
	/******************************************************************
	*
	*	cb_show_CB_Movie_Display_func
	*
	*	returns the string that shows the slider for display and sets up the localized jQuery script
	*
	******************************************************************/
	function cb_show_CB_Movie_Display_func($atts, $content="" )

	{
	
		
		$atts = shortcode_atts(
		array(
			'type' => "",
			
		), $atts, 'showCB_Movies' );

	    
		
		$ret = $this->get_correct_xml_files($program_file_name ,$schedules_file_name ,$source_file_name);
		
			$string = '<div id="show_movies_content">';


		if (    ($ret == TRUE)  || ($ret == -1)){
				$xml = simplexml_load_file($program_file_name);
		
				foreach($xml->programs->program as $p)
				{
					$TMSId = $p->attributes()->TMSId;
					$title =  $p->titles->title[0] ;
					
				   // print_r($p);
					$genres = $p->genres->genre;
					$genre_output = '';
					foreach ($genres as $g)
					{
						$genre_output.= $g;
						$genre_output.= ', ';
						
					
					}
					$genre_output = rtrim($genre_output,', ');
					$rt = $p->runTime;
					
					$t = explode("H",$rt);
			
					$h = explode("PT",$t[0]);
			
					$hours = $h[1];
					if ($hours[0] == '0')
					{
						$hours=$hours[1];
					}
					$m = explode("M",$t[1]);
					$minutes = $m[0];
					if ($minutes[0] == '0')
						$minutes=$minutes[1];
				
					$rating = "Not Rated";
				    foreach($p->ratings->rating as $r)
					{
						if ($r->attributes()->ratingsBody == "Motion Picture Association of America")
							$rating =$r->attributes()->code;
					
					}
				
		
					$movie_times = $this->get_movie_times($TMSId,$schedules_file_name);

					if (  (strlen($movie_times)== 0) || ($ret === -1)){
					 
						$movie_times = "<a href='http://www.regmovies.com/theatres/theatre-folder/regal-camana-bay-stadium-6-9504'  target='_blank'> Click here for movie times </a>";
				
					}
			
					$altFilmId = $p->attributes()->altFilmId;
			
		
			
				$local_file = '';
				/* Look at each file that was downloaded from the server */
				foreach (glob($_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/CamanaMovies/images/*.jpg") as $filename) {
	
					// the pattern is the alternate film id 
					$pattern = '/' . preg_quote($altFilmId, '/') . '/';
			
					$ret = preg_match($pattern, $filename, $matches);

					
					if ($ret === 1){

				
						$local_file = $filename;
						break;
					}
	
				}

					error_log('title is ' . $title);
					error_log('local file is ' . $local_file);
					
					$d = $local_file;	
					// check if file exists
					// if it doesn't exist, it probably is the 3D version and we have to find it's filename
					if (!file_exists($d)){
			error_log("#1 "  . $title);
						$count = 0;
						foreach($p->images as $i)
						{
							
							foreach($i->image as $ii){
						error_log("#2 "  . $title);
								if ($ii->attributes()->category[0] == "Poster Art"){
						
									$filename =   $ii->URI[0];
								error_log('title #2 is ' . $title);
						  error_log('file name is  ' . $filename);
									$name = basename($filename);
								error_log('base name is  ' . $name);
						// we don't want thumb nails
									if (stristr($name, '_t') == TRUE)
										continue;
						
								$image = content_url() .'/uploads/CamanaMovies/images/' . $name ;
								// same file name, but this is the way we have to check it - not with the URL name
									$d = $_SERVER['DOCUMENT_ROOT']. '/wp-content/uploads/CamanaMovies/images/' . $name;
								// if the file does not exist, we use the placeholder
													
								error_log('seraching for ' . $d);
								if (!file_exists($d)){
								error_log('in if statement');
									$image = content_url() .'/uploads/2016/05/movie_fallback.jpg';
								}else 
								$count++;
							}
						}
						   
					}		   
						if ($count == 0)
                                $image = content_url() .'/uploads/2016/05/movie_fallback.jpg';						
					// first file exists -  build the path
					}	else {
							$name = basename($local_file);
							$image =  content_url() .'/uploads/CamanaMovies/images/' . $name;
												

					}
					
					
					// display
					$string .= '<div class="cb-movie-item"> <div class="cb-movie-item-inside">
						<div class="cb-image-outside"><a href="http://www.regmovies.com/Theatres/Theatre-Folder/Regal-Camana-Bay-Stadium-6-9504" target="_blank"> <img class="cb-movie-image" src="' . $image . '" ></a></div>';
					$string .= '<div class="cb-movie-bottom-bk"><div class="cb-movie-title"><a href="http://www.regmovies.com/Theatres/Theatre-Folder/Regal-Camana-Bay-Stadium-6-9504" target="_blank">' . $title . '</a></div>';
					$string .= '<div class="cb-movie-info"> Rated: ' . $rating . ', Duration: ' . $hours . 'hr ' . $minutes . 'min
					<br /> Genre: ' . $genre_output . ' </div>';
					$string .= '<div class="cb-movie-times">'. $movie_times . ' </div>';
				     
					$string .= '</div></div></div>';
				}
		
		}else echo "<center><h2><a href='http://www.regmovies.com/theatres/theatre-folder/regal-camana-bay-stadium-6-9504'  target='_blank'> Click here for movie times </a></h2></center>";
		
		
		
	 
	
	    $string .= "</div>";

		
					
		
		
		return $string;
	//	

	}
	
	function cb_show_CB_Movie_Display_enqueue_styles()
	{

		
			wp_register_style( 'cb_CB_Movie_Display_css', plugins_url( 'CamanaMovies/css/cb_movie_display.min.css' ) );
			wp_enqueue_style( 'cb_CB_Movie_Display_css' );
	
	}
	
	
	
	public static function instance() {
		
		 new _CB_Movie_Display;

			

	}
}

	
	
	function cb_init_CB_Movie_Display(){


		return _CB_Movie_Display::instance();

	}

add_action( 'plugins_loaded',  'cb_init_CB_Movie_Display'  );
// Block direct requests

	
?>