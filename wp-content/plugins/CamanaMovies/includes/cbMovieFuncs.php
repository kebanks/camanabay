<?php
class  _CB_Movie_Funcs{

	function get_movie_times($TMSid,$schedules_file_name)
	{
	
		

		$xml = simplexml_load_file($schedules_file_name );
		$today = date("Y-m-d");
		$times= '';
		foreach($xml->schedules->schedule->event as $e)
				{
					$s = (string) $e->attributes()->TMSId[0];
					
			/*	echo "<p> attributes date is " . $e->attributes()->date . "</p>";*/
			
			//	echo "<p> in loop tmsid is " . $e->attributes()->TMSId . "</p>";
				if ( (strcmp($s,$TMSid) == 0 )  && ($e->attributes()->date == $today))
				{
					
					
					foreach($e->times->time as $t)
					{
					
						// echo "<p> t is " . $t . "</p>";
						$time_in_12_hour_format  = date("g:i A", strtotime($t));
						
					   $times.=$time_in_12_hour_format;
					   $times .=", ";
					  
										
					}
					
	
	
				}
				
			}
				$times = rtrim($times,', ');
				return $times;
				
	}
	function get_correct_file_date(&$date)
	{
	
		$ret = FALSE;
		// the day before 
	    $date =  date('Ymd',strtotime("-1 days"));
		$program_file_name = $this->_get_local_xml_file_name($date, 'on_caym_mov_programs_');
		if (file_exists($program_file_name))
				$ret = -1;
		else{
			 $date =  date('Ymd',strtotime("-2 days"));
			$program_file_name = $this->_get_local_xml_file_name($date, 'on_caym_mov_programs_');
			if (file_exists($program_file_name))
				$ret = -1;
		}
		
		return $ret;
	
	
	
	}
	function get_correct_xml_files(&$program_file_name ,&$schedules_file_name ,&$source_file_name)
	{
		$ret = FALSE;
		$date =  date("Ymd");
		
		
		$program_file_name = $this->_get_local_xml_file_name($date, 'on_caym_mov_programs_');
		
		if (!(file_exists($program_file_name))){
	       $ret =  $this->get_correct_file_date($date);	
		}else $ret = TRUE;
		
		if ( ($ret == TRUE) || ($ret == -1))
		{
			$program_file_name = $this->_get_local_xml_file_name($date, 'on_caym_mov_programs_');
			$schedules_file_name = $this->_get_local_xml_file_name($date, 'on_caym_mov_schedules_');
			$source_file_name = $this->_get_local_xml_file_name($date, 'on_caym_mov_sources_');
		
		
		
		}
			
		
		
		
		
		return ($ret);
	}
	function _get_local_xml_file_name($date, $name)
	{
		$dir = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/CamanaMovies/';
		$file_name = $date . ".xml";
		$local_name = $dir  . $name . $file_name;
		return $local_name;
	
	}	


}

?>