<?php
 /*Template Name: Single 
 */
 
$thumbId = get_post_thumbnail_id(get_the_ID());
 
$heroCaption = get_post($thumbId)->post_excerpt;
$image_data = wp_get_attachment_image_src( $thumbId, "full" );

$term = false;
$terms = get_the_terms($post_id, 'news_article_category');

$id = 0;
if(count($terms) > 0) {
	$term = get_term_by('id', $terms[0]->term_id, 'news_article_category');
}

$post = get_post();
$authorId = $post->post_author;

get_header(); ?>

<div class="times-wrapper" role="main">
<?php include( TIMES_PLUGIN_DIR . '/includes/header.php'); ?>
<div class="row">
    <div class="col col-9">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="times-article-indent desktop">
                <p>&nbsp;</p>
            </div>
            <div class="times-article">
                <header class="entry-header">
                    <h1 class="times-article-headline">
                        <?php the_title(); ?>
                        <span> <?php echo get_post_meta( get_the_ID(), 'cbt_sub_headline', true );  ?></span>
                    </h1>

                    </header>
                    <div class="times-article-hero-wrapper">
                    	<img class="times-article-hero" src="<?php echo $image_data[0]; ?>" width="<?php echo $image_data[1]; ?>" height="<?php echo $image_data[2]; ?>" />
                    	<?php if($heroCaption): ?>
                        <p><?php echo $heroCaption; ?></p>
                        <?php endif; ?>
                    
                    </div>
            </div>
            <div class="times-article-indent desktop" id="meta">
            	<div id="times-article-meta">
                <p><strong><?php echo $term->name; ?></strong></p>
                <?php 
				$datePosted = get_post_meta( get_the_ID(), 'cbt_month_posted', true ); 
                $t = explode('-', $datePosted);	
				$dateObj   = DateTime::createFromFormat('!m', $t[1]);

				?>
                <p><?php echo $dateObj->format('F'); ?> <?php echo $t[0]; ?><br />
                <?php if($authorId != 34) : ?>
                By <em><?php echo get_the_author();?>
                <?php endif; ?>
                </em></p>
                
                <div class="times-article-social">
                	<p>
                    <span>
                    <a href="javascript:popup('http://www.facebook.com/sharer.php?s=100&u=<?php echo "http://{$_SERVER[HTTP_HOST]}{$_SERVER[REQUEST_URI]}";?>');"><img src="<?php echo TIMES_DIR_URL . '/img/fb.svg'; ?>" width="24" height="24" /></a>
                    <a href="javascript:popup('https://plus.google.com/share?url=<?php echo "http://{$_SERVER[HTTP_HOST]}{$_SERVER[REQUEST_URI]}";?>');"><img src="<?php echo TIMES_DIR_URL . '/img/plus.svg'; ?>" width="42" height="24" /></a>
                    <a href="javascript:popup('https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title(). " http://{$_SERVER[HTTP_HOST]}{$_SERVER[REQUEST_URI]}");?>');"><img src="<?php echo TIMES_DIR_URL . '/img/tw.svg'; ?>" width="30" height="24" /></a>
                   	</span>
                    </p>
                </div>
                </div>
            </div>
            
                <div class="times-article-indent tablet mobile">
                <?php if($term->term_id != 205): ?>
                    <p><strong><?php echo $term->name; ?></strong></p>
                    <p><?php echo $dateObj->format('F'); ?> <?php echo $t[0]; ?> 
                    
                    <?php if($authorId != 34) : ?>
                    -  <em><?php echo get_the_author();?></em>
                    <?php endif; ?>
                    
                    
                    
                    </p>
                    <?php endif; ?>
                </div>
            
            <div class="times-article" id="article-copy">
                <div class="times-article-body">
                    <?php the_content(); ?>
                </div>
            </div>
        </article>
    </div>
    <div class="col col-3">
        <h2 class="editors-picks">Top Stories</h2>
        
        <?php 
		$posts = get_posts(array(
			'numberposts'	=> 2,
			'post_type'		=> 'cb_news_article',
			'meta_query'	=> array(
				'relation'		=> 'AND',
				array(
					'key'	 	=> 'cbt_featured',
					'value'	  	=> 'checked',
					'compare' 	=> '=',
				),
			),
		));
		
		foreach ( $posts as $post ) {
			$image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" );
			?>
			<a class="times-home-featured single" href="<?php echo get_permalink($post); ?>">
				<div class="responsive-image"><img src="<?php echo $image_data[0]; ?>" width="<?php echo $image_data[1]; ?>" height="<?php echo $image_data[2]; ?>" /></div>
				<p><?php echo $post->post_title; ?></p>
			</a>
		<?php }?>

        <?php include( TIMES_PLUGIN_DIR . '/includes/past.php'); ?>
        <?php include( TIMES_PLUGIN_DIR . '/includes/categories.php'); ?>
    </div>
</div>

<div class="tablet mobile times-article-social">
    <p> &nbsp; 
        <a href="javascript:popup('http://www.facebook.com/sharer.php?s=100&u=<?php echo "http://{$_SERVER[HTTP_HOST]}{$_SERVER[REQUEST_URI]}";?>');"><img src="<?php echo TIMES_DIR_URL . '/img/fb.svg'; ?>" width="24" height="24" /></a>
        <a href="javascript:popup('https://plus.google.com/share?url=<?php echo "http://{$_SERVER[HTTP_HOST]}{$_SERVER[REQUEST_URI]}";?>');"><img src="<?php echo TIMES_DIR_URL . '/img/plus.svg'; ?>" width="42" height="24" /></a>
        <a href="javascript:popup('https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title(). " http://{$_SERVER[HTTP_HOST]}{$_SERVER[REQUEST_URI]}");?>');"><img src="<?php echo TIMES_DIR_URL . '/img/tw.svg'; ?>" width="30" height="24" /></a>
        </p>
</div>
<script type="text/javascript">

jQuery(document).ready(function(){

    jQuery(window).scroll(function(){
		
		var metaStart = jQuery("#article-copy").offset().top -10;
		var metaEnd = metaStart + jQuery("#article-copy").outerHeight();
		
		var scrollPos = jQuery(document).scrollTop();
		var footerTop = jQuery("#cb_bmw_left_menu").offset().top;
		
		jQuery("#meta").height(jQuery("#article-copy").height());

		var maxScroll = metaEnd - jQuery("#times-article-meta").outerHeight();
		if (scrollPos > maxScroll){
			jQuery("#times-article-meta").removeClass("fixed");
			jQuery("#times-article-meta").addClass("bottom");
		}else if(scrollPos > metaStart) {
			
			jQuery("#times-article-meta").attr('style', '');
			jQuery("#times-article-meta").removeClass("bottom");
			jQuery("#times-article-meta").addClass("fixed");
		}else{
			jQuery("#times-article-meta").attr('style', '');
			jQuery("#times-article-meta").removeClass("fixed");
			jQuery("#times-article-meta").removeClass("bottom");
		}
		
		jQuery("#times-article-meta").width(jQuery("#meta").width());
    });
	
	 jQuery(window).scroll();
	 
	 jQuery("blockquote p cite").each(function(index, element) {
		jQuery(this).parent().parent().addClass("with-quotes");
	});
});
function popup(url) {
	var winWidth = 400;
	var winHeight = 500;
	var winTop = (screen.height / 2) - (winHeight / 2);
	var winLeft = (screen.width / 2) - (winWidth / 2);
	window.open(url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width='+winWidth+',height='+winHeight);
}
</script>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>
