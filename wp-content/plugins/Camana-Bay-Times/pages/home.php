<?php
 /*Template Name: Single 
 */
 

$sortedPosts = array();
$sortedPosts["headline"] = false;
$sortedPosts["magic"] = false;
$sortedPosts["featured"] = array();
$sortedPosts["picks"] = array();
$sortedPosts["byCategory"] = array();

$categories = array(202, 200, 203, 201, 204, 206);


foreach ( $posts as $post ) {
	
	$terms = wp_get_post_terms ($post->ID, 'news_article_category');

	if(count($terms>0)) {
		
		$category = $terms[0]->term_id;
		
		$headline = get_post_meta( $post->ID, 'cbt_headline', true );
		$featured = get_post_meta( $post->ID, 'cbt_featured', true );
		$pick = get_post_meta( $post->ID, 'cbt_pick', true );
		$top = get_post_meta( $post->ID, 'cbt_top', true );
		
		if($headline && $sortedPosts["headline"] == false){
			$sortedPosts["headline"] = $post;
		} else if ($featured && count($sortedPosts["featured"]) < 2){
			$sortedPosts["featured"][] = $post;
		} else if ($pick && count($sortedPosts["picks"]) < 3){
			$sortedPosts["picks"][] = $post;
		} else if ($category==205){
			$sortedPosts["magic"] = $post;
		} 
		
		if(!isset($sortedPosts["byCategory"][$category])){
			$sortedPosts["byCategory"][$category] = array();
		}
		
		if($top) {
			array_unshift($sortedPosts["byCategory"][$category], $post);
		}else{
			$sortedPosts["byCategory"][$category][] = $post;
		}
	}
	
}

get_header(); ?>
<?php 
$post = $sortedPosts["headline"];
$image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" );

?>

<div class="times-wrapper" role="main">
    <?php include( TIMES_PLUGIN_DIR . '/includes/header.php'); ?>
    <div class="row">
        <div class="col col-9"> <a class="times-headline" href="<?php echo get_permalink($post); ?>">
            <div class="times-headline-img" style="background-image: url(<?php echo $image_data[0]; ?>);"></div>
            <div class="times-headline-text">
                <h1><?php echo $post->post_title; ?> <span><?php echo get_post_meta( $post->ID, 'cbt_sub_headline', true );  ?></span> </h1>
            </div>
            </a> </div>
        <div class="col col-3 desktop">
            <?php foreach($sortedPosts["featured"] as $post): ?>
            <?php $image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" ); ?>
            <a class="times-home-featured" href="<?php echo get_permalink($post); ?>">
            <div class="responsive-image"><img src="<?php echo $image_data[0]; ?>" width="<?php echo $image_data[1]; ?>" height="<?php echo $image_data[2]; ?>" /></div>
            <p><?php echo $post->post_title; ?></p>
            </a>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="row mobile tablet">
        <?php foreach($sortedPosts["featured"] as $post): ?>
        <?php $image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" ); ?>
        <div class="col col-6"> <a class="times-home-featured" href="<?php echo get_permalink($post); ?>">
            <div class="responsive-image"><img src="<?php echo $image_data[0]; ?>" width="<?php echo $image_data[1]; ?>" height="<?php echo $image_data[2]; ?>" /></div>
            <p><?php echo $post->post_title; ?></p>
            </a> </div>
        <?php endforeach; ?>
    </div>
    <h2 class="editors-picks">Editor's Picks</h2>
    <?php $count = 0 ; ?>
    <div class="row editor-picks">
        <?php foreach($sortedPosts["picks"] as $post): ?>
        <?php $count++; ?>
        <?php $class = $count == 3 ? 'end' : ''; ?>
        <?php $image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" ); ?>
        <div class="col col-4"> <a class="editor-pick <?php echo $class; ?>" href="<?php echo get_permalink($post); ?>">
            <div class="responsive-image"><img src="<?php echo $image_data[0]; ?>" width="<?php echo $image_data[1]; ?>" height="<?php echo $image_data[2]; ?>" /></div>
            <div class="editor-pick-text">
                <p><?php echo $post->post_title; ?></p>
            </div>
            </a> </div>
        <?php endforeach; ?>
    </div>
    <?php $post = $sortedPosts["magic"]; ?>
    <?php $thumbId = get_post_thumbnail_id( $post->ID ); ?>
    <?php $magicCaption = get_post($thumbId)->post_excerpt; ?>
    <?php $image_data = wp_get_attachment_image_src( $thumbId, "full" ); ?>
    <div class="row">
        <div class="col col-12"> <a class="magic-moments" href="<?php echo get_permalink($post); ?>">
            <div class="magic-moments-img" style="background-image: url(<?php echo $image_data[0]; ?>);"></div>
            <div class="magic-moments-title">
                <h2><?php echo $post->post_title; ?></h2>
                <p><?php echo get_post_meta( $post->ID, 'cbt_sub_headline', true );  ?></p>
            </div>
            <?php if($magicCaption): ?>
            <p class="magic-moments-caption"><?php echo $magicCaption; ?> <span>VIEW GALLERY</span> </p>
            <?php endif; ?>
            </a> </div>
    </div>
    <div class="row">
        <div class="col col-9">
            <?php $count = 1; ?>
            <?php foreach($categories as $category) : ?>
            <?php if($count == 1 || ($count-1) % 3 == 0): ?>
            <div class="row">
                <?php endif; ?>
                <?php $term = get_term_by('id', $category, 'news_article_category'); ?>
                <div class="col col-4">
                    <div class="times-category resize">
                        <div class="times-category-content"> <a class="times-category-title" href="<?php echo get_site_url() . '/times-categories/' . $term->slug;?>">
                            <p><?php echo  $term->name ?></p>
                            </a>
                            <?php $posts = $sortedPosts["byCategory"][$category]; ?>
                            <?php $posts = array_slice($posts, 0, 4) ?>
                            <?php $first = $posts[0]; ?>
                            <?php $firstImage = wp_get_attachment_image_src( get_post_thumbnail_id( $first->ID ), "full" ); ?>
                            <?php $ignoreFirst = false; ?>
                            <?php if(is_array($firstImage)): ?>
                            <a class="times-category-hero" href="<?php echo get_permalink($first); ?>">
                            <div class="responsive-image"><img src="<?php echo $firstImage[0]; ?>" width="<?php echo $firstImage[1]; ?>" height="<?php echo $firstImage[2]; ?>" /></div>
                            </a>
                            <?php $ignoreFirst = true; ?>
                            <?php endif; ?>
                            <div class="times-category-links">
                                <?php foreach($posts as $post): ?>
                                <?php $image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" ); ?>
                                <a href="<?php echo get_permalink($post); ?>">
                                <?php if(!$ignoreFirst && is_array($image_data)): ?>
                                <div class="times-category-links-img">
                                    <div class="responsive-image"><img src="<?php echo $image_data[0]; ?>" width="<?php echo $image_data[1]; ?>" height="<?php echo $image_data[2]; ?>" /></div>
                                </div>
                                <?php endif; ?>
                                <?php $ignoreFirst = false; ?>
                                <p><?php echo $post->post_title; ?></p>
                                </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $count++ ?>
                <?php if($count-1 != 0 && ($count-1) % 3 == 0): ?>
            </div>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php if(($count-1) % 3 != 0): ?>
            <?php echo '</div>'; ?>
            <?php endif; ?>
        </div>
        <div class="col col-3">
            <?php include( TIMES_PLUGIN_DIR . '/includes/past.php'); ?>
            <?php include( TIMES_PLUGIN_DIR . '/includes/categories.php'); ?>
        </div>
    </div>
</div>
<script type="text/javascript">

	jQuery( window ).resize(function() {
		
		jQuery("div.times-category.resize").attr("style", "");
		
		if(jQuery( window ).width()>640) {
			jQuery("div.times-category.resize").each(function(index, element) {
				
				var margin = parseInt(jQuery(this).css('margin-bottom'));
				jQuery(this).innerHeight(jQuery(this).parent().parent().innerHeight()-margin);
				
				var dif = jQuery(this).height() - jQuery(this).find(">:first-child").height();
				
				if(dif > 142)
					jQuery(this).addClass("water");
				else
					jQuery(this).removeClass("water");
				
			});
		} else {
			jQuery(this).removeClass("water");
		}
	});
	jQuery(document).ready(function(e) {
		jQuery( window ).resize();
		
		
    });
</script>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>
