<?php
 /*Template Name: Single 
 */

global $timesCategory;
$sortedPosts = array();

foreach ( $posts as $post ) {
	
	$edition = get_post_meta( $post->ID, 'cbt_month_posted', true );
	$t = explode('-', $ediition);
	$year = $t[0];
	$month = $t[1];
	
	$top = get_post_meta( $post->ID, 'cbt_top', true );
	$post->image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" );
	$post->edition = $edition;

	if(!isset($sortedPosts[$edition])){
		$sortedPosts[$edition] = array();
	}
	
	if($top) {
		array_unshift($sortedPosts[$edition], $post);
	}else{
		$sortedPosts[$edition][]  = $post;
	}

}

krsort($sortedPosts);
$count = 1;



get_header(); ?>
<div class="times-wrapper" role="main">
    <?php include( TIMES_PLUGIN_DIR . '/includes/header.php'); ?>

    <h2 class="editors-picks"><?php single_term_title(); ?></h2>
    <div class="row">
        <div class="col col-9">
        	<?php foreach($sortedPosts as $monthPosts): ?>
            
            <?php if($count == 1 || ($count-1) % 3 == 0): ?>
            	<div class="row">
            <?php endif; ?>
            	<?php $first = $monthPosts[0]; ?>
                <?php $firstImage = wp_get_attachment_image_src( get_post_thumbnail_id( $first->ID ), "full" ); ?>
                <div class="col col-4">
                    <div class="times-category resize">
                    	<div class="times-category-content">
                        <div class="times-category-title date">
                        	<?php $dateObj   = DateTime::createFromFormat('!Y-m', $first->edition);
							
							$monthName = $dateObj->format('F'); ?>
                            <p class="times-category-date"><strong><?php echo $monthName; ?></strong>2017</p>
                        </div>
                        
                        <?php $ignoreFirst = false; ?>
                        <?php if(is_array($firstImage)): ?>
                        
                            <a class="times-category-hero"  href="<?php echo get_permalink($first); ?>">
                                <div class="responsive-image"><img src="<?php echo $firstImage[0]; ?>" width="<?php echo $firstImage[1]; ?>" height="<?php echo $firstImage[2]; ?>" /></div>
                            </a>
                            <?php $ignoreFirst = true; ?>
                        <?php endif; ?>
                        <div class="times-category-links">
                        	<?php foreach($monthPosts as $post): ?>
								<?php $image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" ); ?>
                                <a href="<?php echo get_permalink($post); ?>">
                                	<?php if(!$ignoreFirst && is_array($image_data)): ?>
                                        <div class="times-category-links-img">
                                            <div class="responsive-image"><img src="<?php echo $image_data[0]; ?>" width="<?php echo $image_data[1]; ?>" height="<?php echo $image_data[2]; ?>" /></div>
                                        </div>
                                        
                                    <?php endif; ?>
                                    <?php $ignoreFirst = false; ?>
                                    <p><?php echo $post->post_title; ?></p>
                                </a> 
                            <?php endforeach; ?>
                    	</div>
                    </div>
                    </div>
                </div>
                
            <?php $count++ ?>
            <?php if($count-1 != 0 && ($count-1) % 3 == 0): ?>
            	</div>
            <?php endif; ?>
            <?php endforeach; ?>
            
            <?php if(($count-1) % 3 != 0): ?>
            	<?php echo '</div>'; ?>
            <?php endif; ?>
            
        </div>
        <div class="col col-3">
        <?php include( TIMES_PLUGIN_DIR . '/includes/categories.php'); ?>
        <?php include( TIMES_PLUGIN_DIR . '/includes/past.php'); ?>
        
        </div>
    </div>
</div>
<?php foreach ( $sortedPosts["byCategory"][201] as $post ):?>
<?php endforeach; ?>
<script type="text/javascript">

	jQuery( window ).resize(function() {
		
		jQuery("div.times-category.resize").attr("style", "");
		
		if(jQuery( window ).width()>640) {
			jQuery("div.times-category.resize").each(function(index, element) {
				
				var margin = parseInt(jQuery(this).css('margin-bottom'));
				jQuery(this).innerHeight(jQuery(this).parent().parent().innerHeight()-margin);
				
				var dif = jQuery(this).height() - jQuery(this).find(">:first-child").height();
				
				console.log(dif);
				
				if(dif > 142)
					jQuery(this).addClass("water");
				else
					jQuery(this).removeClass("water");
				
			});
		} else {
			jQuery(this).removeClass("water");
		}
	});
	jQuery(document).ready(function(e) {
		jQuery( window ).resize();
    });
</script>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>
