<?php

/*

	Plugin Name: Camana Bay Times
	Plugin URI: http://bbandp.com/
	Description: Camana Bay Times section.
	Version: 1.0
	Author: Simon Cruise
	Author URI: http://bbandp.com/
	License: GPLv2
	
*/


ini_set('date.timezone', 'America/Cayman');

define('TIMES_PLUGIN_DIR', dirname(__FILE__));
define('TIMES_DIR_URL', plugin_dir_url( __FILE__ ));
define('TIMES_URL', site_url() . '/times/');

add_action( 'init', 'cb_create_news_article' );

function cb_create_news_article() {
	
    register_post_type( 'cb_news_article',
        array(
            'labels' => array (
                'name' => 'Camana Bay Times',
                'singular_name' => 'News Article',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New News Article',
                'edit' => 'Edit',
                'edit_item' => 'Edit News Article',
                'new_item' => 'New News Article',
				'featured_image' => 'News Article featured image',
                'view' => 'View',
                'view_item' => 'View News Article',
                'search_items' => 'Search News Articles',
                'not_found' => 'No News Articles found',
                'not_found_in_trash' => 'No News Articles found in Trash',
				'menu_name'=>  'Camana Bay Times',
				'all_items' => 'All Articles'
            ),
			'has_archive' => true,
            'public' => true,
            'menu_position' => 18,
            'supports' => array( 'title', 'editor', 'thumbnail', 'author' ),
            'taxonomies' => array('news_article_category'),
            'menu_icon' => plugins_url( 'icon.png', __FILE__ ),
            'has_archive' => true,
			'with_front' => true,
			'rewrite'     => array('slug' => 'times'),
			'exclude_from_search' => false
        )
    );
	
	register_taxonomy("news_article_category", array("cb_news_article"), array(	
		'has_archive' => false,
		'with_front' => true, 
		"hierarchical" => true, 
		"label" => "News category", 
		"singular_label" => "News category", 
		"rewrite" => true,
		'rewrite' => array('slug' => 'times-categories'),
		'meta_box_cb' => 'drop_cat',
		'public' => true,
        'hierarchical' => true,
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'query_var' => true,
        'publicly_queryable' => true,
		'exclude_from_search' => false,
        'capability_type' => 'post'));
		
	flush_rewrite_rules();
}


function drop_cat($post, $box) {
	
	$terms = get_the_terms($post_id, 'news_article_category');
	
	$id = 0;
	
	if(count($terms) > 0) {
		$id = $terms[0]->term_id;
	}
	
	$defaults = array('taxonomy' => 'category');
	if ( !isset($box['args']) || !is_array($box['args']) )
		$args = array();
	else
		$args = $box['args'];
	extract( wp_parse_args($args, $defaults), EXTR_SKIP );
	$tax = get_taxonomy($taxonomy);
	
	wp_dropdown_categories( array( 'taxonomy' => $taxonomy, 'hide_empty' => 0, 'name' => "cbt_category", 'selected' => $id, 'orderby' => 'name', 'hierarchical' => 0 ) ); 
	
}

add_action("admin_init", "admin_init");

add_action('edit_form_after_title', function() {
    global $post, $wp_meta_boxes;
    do_meta_boxes(get_current_screen(), 'after_title', $post);
    unset($wp_meta_boxes[get_post_type($post)]['cbt_sub_headline']);
});

function admin_init(){
  add_meta_box("cbt_month_posted", "Month", "cbt_month_posted", "cb_news_article", "side", "low");
  add_meta_box("cbt_featured", "Featured", "cbt_featured", "cb_news_article", "side", "low");
  add_meta_box("cbt_sub_headline", "Sub headline", "cbt_sub_headline", "cb_news_article", "after_title", "high");
}

function cbt_featured($post) {

	$custom = get_post_custom($post->ID);
	$cbt_featured = $custom["cbt_featured"][0];
	$checked = $cbt_featured == "checked" ? 'checked="checked"' : '';
	?>
    <br />
    <label style="width: 80px; float:left;">Featured</label>
    <input type="checkbox" name="cbt_featured" value="checked" <?php echo $checked; ?> />
	<?php
	
	$cbt_pick = $custom["cbt_pick"][0];
	$checked = $cbt_pick == "checked" ? 'checked="checked"' : '';
	?>
    <br />
    <label style="width: 80px; float:left;">Editor pick</label>
    <input type="checkbox" name="cbt_pick" value="checked" <?php echo $checked; ?> />
	<?php
	
	$cbt_headline = $custom["cbt_headline"][0];
	$checked = $cbt_headline == "checked" ? 'checked="checked"' : '';
	?>
    <br />
    <label style="width: 80px; float:left;">Headline</label>
    <input type="checkbox" name="cbt_headline" value="checked" <?php echo $checked; ?> />
	<?php
	
	$cbt_top = $custom["cbt_top"][0];
	$checked = $cbt_top == "checked" ? 'checked="checked"' : '';
	?>
    <br />
    <label style="width: 80px; float:left;">Top</label>
    <input type="checkbox" name="cbt_top" value="checked" <?php echo $checked; ?> />
	<?php
} 
 
function cbt_sub_headline($post){

	$custom = get_post_custom($post->ID);
	$cbt_sub_headline = $custom["cbt_sub_headline"][0];
	?>
	<input type="text" name="cbt_sub_headline" value="<?php echo $cbt_sub_headline; ?>" style="padding: 3px 8px; font-size: 1.7em; display:block; width:100%;" />
	<?php
} 


function cbt_month_posted($post){
	
  $custom = get_post_custom($post->ID);
  $cbt_month_posted = $custom["cbt_month_posted"][0];
  
	if(!$cbt_month_posted) {
		$cbt_month_posted = '2017-' . date('n');
	}
  
  ?>
  <label>Month:</label>
  <select name="cbt_month_posted">
  	<?php for($month = 1; $month <= 12; $month++): ?>
    <?php
	
	$value = '2017-' . $month;
	$dateObj   = DateTime::createFromFormat('!m', $month);
	$monthName = $dateObj->format('F');
	$selected = $value == $cbt_month_posted ? 'selected="selected"' : '';
	?>
  	<option <?php echo $selected; ?> value="<?php echo $value; ?>"><?php echo $monthName; ?> 2017</option>
    <?php endfor; ?>
  </select>
  <?php
}

add_action('save_post', 'save_details');

function save_details($post_id){

	if(get_post_type($post_id) == 'cb_news_article' && isset($_POST["cbt_month_posted"])){
		update_post_meta($post_id, "cbt_month_posted", $_POST["cbt_month_posted"]);
		update_post_meta($post_id, "cbt_sub_headline", $_POST["cbt_sub_headline"]);
		update_post_meta($post_id, "cbt_headline", $_POST["cbt_headline"]);
		update_post_meta($post_id, "cbt_pick", $_POST["cbt_pick"]);
		update_post_meta($post_id, "cbt_featured", $_POST["cbt_featured"]);
		update_post_meta($post_id, "cbt_top", $_POST["cbt_top"]);
		
		if(isset($_POST["cbt_category"])) {
			$term = get_term_by('id', $_POST["cbt_category"], 'news_article_category');
			wp_set_post_terms( $post_id, array($_POST["cbt_category"]), 'news_article_category', false);
		}
	}
}


add_action("manage_posts_custom_column",  "cb_news_article_custom_columns");
add_filter("manage_edit-cb_news_article_columns", "cb_news_article_edit_columns");
 
function cb_news_article_edit_columns($columns){
  $columns = array(
    "cb" => "<input type=\"checkbox\" />",
    "title" => "Portfolio Title",
    "cbt_sub_headline" => "Sub headline",
    "cbt_month_posted" => "Month",
	 "cbt_category" => "Category",
	 "cbt_featured" => "Fetured",
	 "cbt_pick" => "Pick",
	 "cbt_headline" => "Headline",
  );
 
  return $columns;
}

function cb_news_article_custom_columns($column){
	
	global $post;
	
	switch ($column) {
		case "cbt_sub_headline":
			$custom = get_post_custom();
			echo $custom["cbt_sub_headline"][0];
			break;
		case "cbt_month_posted":
			$custom = get_post_custom();
			if(isset($custom["cbt_month_posted"][0])){
				$dateObj = DateTime::createFromFormat('!Y-n', $custom["cbt_month_posted"][0]);
				echo $dateObj->format('F Y');
			}
			break;
		case "cbt_category":
		
			$terms = get_the_terms($post_id, 'news_article_category');
			$id = 0;
			if(count($terms) > 0) {
				$term = get_term_by('id', $terms[0]->term_id, 'news_article_category');
				echo $term->name;
			}
			break;
		case "cbt_featured":
			$custom = get_post_custom();
			echo $custom["cbt_featured"][0] == 'checked' ? 'Y' : 'N';
			break;
		case "cbt_pick":
			$custom = get_post_custom();
			echo $custom["cbt_pick"][0] == 'checked' ? 'Y' : 'N';
			break;
		case "cbt_headline":
			$custom = get_post_custom();
			echo $custom["cbt_headline"][0] == 'checked' ? 'Y' : 'N';
			break;
	}
}

add_filter('wpseo_title', 'filter_product_wpseo_title');
function filter_product_wpseo_title($title) {
    if( is_post_type_archive('cb_news_article') ) {
        $title = 'Camana Bay Times ' . date('') . ' | Camana Bay';
    }
    return $title;
}

add_filter( 'template_include', 'cb_template_function', 1 );

function cb_template_function( $template_path ) {

	global $currentEdition;

    if ( get_post_type() == 'cb_news_article' && !isset($_GET['s'])) {
		/*
		if(!current_user_can('administrator')) {
			global $wp_query;
			$wp_query->set_404();
    		status_header(404);
			nocache_headers();
			include( get_query_template('404') );
			return;
		}
		*/
		wp_enqueue_style( 'timesCSS', plugins_url( '/times.css?v=1.7', __FILE__ ) );
		
		
		$template = 'pages/home.php';
		
		 if (is_single()) {
			 $template = 'pages/single-article.php';
			 $currentEdition = new DateTime();
			 $currentEdition->setTimestamp(strtotime(get_post_meta( get_the_ID(), 'cbt_month_posted', true )));
		}else if(is_tax()) {
			$template = 'pages/categories.php';
		}
		
		if ( $theme_file = locate_template( array ( $template ) ) ) {
			$template_path = $theme_file;
		} else {
			$template_path = plugin_dir_path( __FILE__ ) . $template;
		}
    }

    return $template_path;
}


add_action( 'pre_get_posts', 'cb_modify_query' );

function cb_modify_query($query) {

	global $currentEdition;
	global $monthStartTimestamp;
	
	$monthStartTimestamp = strtotime( 'first wednesday of ' . date("Y-n")) + 32400;
	
	if( !is_admin() && isset($query->query['news_article_category'])) {
		global $timesCategory;
		$timesCategory = $query->get('news_article_category');
		$timesCategory = get_term_by('slug', $timesCategory, 'news_article_category');
		$query->set('posts_per_page', -1);

	} else if (!is_admin() && $query->get('post_type') == "cb_news_article" && is_archive() && $query->is_main_query() ) {
		
		$month = date('n');
		$year = date('Y');
		
		
		if(time() < $monthStartTimestamp){
			// still last month as current month
			$month--;
		}
		
		if( $currentEdition  == 'next-month' && is_user_logged_in()) {

			$month++;
			if($month==13) {
				$month = 1;
				$year++;
			}
			
			$currentEdition = "{$year}-{$month}";
			
		} else {
			
			$currentEdition = isset($_GET['edition']) ? $_GET['edition'] : "{$year}-{$month}";
		
			$t = explode('-', $currentEdition);
			if(($t[1] > $month && $t[0] == $year) || $t[0] > $year) {
				$currentEdition = date('Y-n');
			}
		}

		$query->set('posts_per_page', -1);
		$query->set('meta_query', array(
			'relation' => 'AND',
			array(
				'key' => 'cbt_month_posted',
				'value' => $currentEdition,
				'compare' => '='
			)
		));
		
		if(isset($_GET['drafts']) && is_user_logged_in()) {
			$query->set('post_status', 'any');
		}
		
		$currentEdition   = DateTime::createFromFormat('Y-n', $currentEdition);
    }
}
