<?php

global $monthStartTimestamp;
$currentMonth = date('n', $monthStartTimestamp);
$currentYear = date('Y', $monthStartTimestamp);
$currentMonth = $currentYear > 2017 ? 12 : $currentMonth-1;
?>

<div class="times-category past">
    <h2>PAST EDITIONS</h2>
    
    <?php for($i = $currentMonth; $i > 0; $i--): ?>
		<?php $dateObj = DateTime::createFromFormat('!m', $i); $monthName = $dateObj->format('F'); ?>
        <a href="<?php echo TIMES_URL . '?edition=2017-' . $i; ?>"><?php echo $monthName; ?>  2017</a>
		
	<?php endfor; ?>
		
    
    
</div>