<div class="times-category all">
    <h2>CATEGORIES</h2>
    <?php
    //$categories = get_terms(array('taxonomy' => 'news_article_category', 'hide_empty' => false));
    ?>
    
    <ul>
    	<li><a href="<?php echo site_url(); ?>/times-categories/camana-bay-news">Camana Bay News</a></li>
        <li><a href="<?php echo site_url(); ?>/times-categories/dart-news">Dart News</a></li>
        <li><a href="<?php echo site_url(); ?>/times-categories/business-environment-sustainability">Business, Environment &amp; Sustainability</a></li>
        <li><a href="<?php echo site_url(); ?>/times-categories/life-leisure">Life &amp; Leisure</a></li>
        <li><a href="<?php echo site_url(); ?>/times-categories/children-families">Children &amp; Families</a></li> 
        <li><a href="<?php echo site_url(); ?>/times-categories/sports-fitness">Sports &amp; Fitness</a></li>
        <li><a href="<?php echo site_url(); ?>/times-categories/magic-moments">Magic Moments</a></li>
        <?php /*foreach($categories as $category) : ?>
        	<li><a href="<?php echo get_site_url() . '/times-categories/' . $category->slug; ?>"><?php echo $category->name; ?></a></li>
        <?php endforeach;*/ ?>
    </ul>
</div>


