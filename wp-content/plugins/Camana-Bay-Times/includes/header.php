<?php global $currentEdition; ?>

<a class="times-header" href="<?php echo TIMES_URL; ?>">
    <img src="<?php echo TIMES_DIR_URL . 'img/logo.png'; ?>" width="1292" height="92" alt="Camana Bay Times" class="desktop tablet" />
    <img src="<?php echo TIMES_DIR_URL . 'img/logo-mobile.png'; ?>" width="306" height="113" alt="Camana Bay Times" class="mobile" />
    <?php if($currentEdition): ?>
    	<div class="times-header-date"><p><strong><?php echo $currentEdition->format('M'); ?></strong>2017</p></div>
    <?php endif; ?>
</a>