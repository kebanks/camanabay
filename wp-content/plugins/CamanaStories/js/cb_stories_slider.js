/* **************************************************************************************
*  cb_stories_slider.js 
*
*
*   JQuery code that manages the story slider for the stories plugin
*
*
*
*******************************************************************************************/


jQuery(document).ready(function ($) {


/*  set up the stories slider initially */
	var options=cb_stories_ajax.options;
	jQuery(".cb_st_right_arrow").attr("href", "1");
	jQuery('.cb_cs_h2').text(options[0]['cb_post_title']);
	jQuery('.cb_cs_p').text(options[0]['cb_excerpt']);


    var link = '<img  src="' +options[0]['cb_featured_image'] + '"/>';
	jQuery('.cb_first_image').append(link);
	
	jQuery ('.cb_st_permalink_a').text(options[0]['cb-stories-link-text'] );
	
	
	jQuery(".cb_st_permalink_a").attr("href", options[0]['cb_permalink'] );
	
	
	/* click function that handles the behavior when the 
	 *  user clicks the right arrow 
	 */ 
	 
	 
	 jQuery('.cb_st_right_arrow').click(function(e) {
		e.preventDefault();
		
		
		var options=cb_stories_ajax.options;
		var last_index = cb_stories_ajax.last_index;
		
	
		
		var a = jQuery(".cb_st_right_arrow").attr("href");
		/* determine what the value of the arrow is - what picture we are using - adjust the value for the next picture */
		var value = a.substring(a.lastIndexOf("/") + 1);
		if (value == last_index)
			value = 1;
		else 
			value++;
		jQuery(".cb_st_right_arrow").attr("href", value);
		var index = value -1;
	
		jQuery('.cb_cs_h2').text(options[index]['cb_post_title']);
		 jQuery('img',jQuery('.cb_first_image')).attr('src',options[index]['cb_featured_image']);
		// jQuery('img',jQuery('.cb_featured_image')).attr('src',options[index]['cb_featured_image']);
		 jQuery('.cb_cs_p').text(options[index]['cb_excerpt']);
		 jQuery(".cb_st_permalink_a").attr("href", options[index]['cb_permalink'] );
		 jQuery ('.cb_st_permalink_a').text(options[index]['cb-stories-link-text'] );
	 });
	
	
});    
