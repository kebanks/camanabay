<?php
/*
Plugin Name: Camana Bay Stories Plugin
Plugin URI: http://www.camanabay.com/
Description: Camana Bay Stories Plugin
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php
/*******************************************************************************************************************
*
*		Camana Bay Stories Plugin 
*
*		Plugin that handles the creation, listing and display of Camana Bay Stories slider
*
*
*
*
*
**********************************************************************************************************************/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access
}
require_once 'includes/storiesDisplay.php';
require_once 'includes/cb_stories_list.php';
if ( ! class_exists( 'CamanaStoriesPlugin' ) ) :


class CamanaStoriesPlugin {

 /**
     * @var string
     */
    public $version = '1.0';
	private $edit_id ;
	public static function init() {

        $camanaStories = new self();

    }
	public function __construct() {

       
        $this->setup_actions();
        

    }
	 /**
     * Hook camanabay stories into WordPress
     */
    private function setup_actions() {
	
	
	/*   we want a separate menu for our plugin 
	 */
	 
	 
		add_action( 'init', array($this, 'cb_stories_init' ));
		add_action( 'admin_menu', array($this,  'register_admin_menu' ));
		add_action( 'admin_post_cb_stories_create_stories', array( $this, 'create_stories' ) );
		add_action( 'admin_post_cb_stories_update_stories', array( $this, 'update_stories' ) );
	
	$s='';
	
	/* determine if we are editing or creating a new story 
	 * 
	 */
	if (isset($_REQUEST['action']))
		$s = $_REQUEST['action'];
	if ($s == 'edit'){
		$this->action = 'cb_stories_update_stories';
	}
	else {
		$this->action = 'cb_stories_create_stories';
	}	
	
	
	}
	public function update_stories()
	{
			$post_id = $_POST['edit_id'];
			$this->update_all($post_id);
			wp_redirect( admin_url( "admin.php?page=cb-stories-listing" ) );
			return;
	
	}
	/*************************************************************
	*
	*   Update_all
	*
	*	Updates any kind of story type page - whether it is new or edited.  
	*
	*
	*
	*
	*
	*****************************************************************/
	public function update_all($post_id)
	{
	
		$title = $_POST['cb-stories-setting-title'];
		$post_data = array();
		for ($i=1;$i<=3;$i++)
		{
			$value = $_POST['cb-stories-setting'][$i]['link'];
		
		$value1 = $_POST['cb-stories-link-text'][$i]['text'];
	
			$post_data[$i][0]=$value;
			$post_data[$i][1]=$value1;
		}
	
		$cb_story_post = array(
			'ID'           => $post_id,
			'post_title'   => $title,
			'post_status' => 'publish'
			
  );
		wp_update_post( $cb_story_post );
		update_post_meta( $post_id, 'cb-story_settings', $post_data );
		
		
	
	
	}
	/******************************************************
	*
	*		Create_Stories
	*
	*		Function to create a new story
	*
	******************************************************/
	public function create_stories()
	{
		
		$post_id =  $this->create_story();
		$this->update_all($post_id);
		wp_redirect( admin_url( "admin.php?page=cb-stories-plugin-menu" ) );
		return;
		
		
		
	}
	
	/********************************************************************************************
	*
	*   create_story
	*
	*	Inserts the initial post for create story
	*
	*
	*
	****************************************************************************************/
	
	
	
	
	
	public function create_story() {

        
        $id = wp_insert_post( array(
                'post_title' => __( "New Story", "cb-stories" ),
                'post_status' => 'draft',
                'post_type' => 'cb-stories-page-type'
            )
        );

		return $id;
	}
	public function register_admin_menu() {

		$this->cb_stories_pagehook = add_menu_page('Camana Bay Stories', 'Camana Bay Stories', 'manage_options', 'cb-stories-plugin-menu',"",plugins_url( 'images/image.png', __FILE__ ));
	
		add_submenu_page( 'cb-stories-plugin-menu', 'Add New Section', 'Add New Section', 'manage_options', 'cb-stories-plugin-menu',array($this,'cb_stories_options_page' ));
		add_submenu_page( 'cb-stories-plugin-menu', 'Camana Bay Stories List', 'Camana Bay Stories List ', 'manage_options', 'cb-stories-listing',array($this,'cb_stories_listing'))	;	
		
		
	}
	public function cb_stories_listing($arg)
	{
	$wp_list_table = new _cb_stories_list_table();
	$wp_list_table->prepare_items();
	echo '<div class="wrap"><h2>' . esc_html( __( 'Camana Bay Stories', 'cb-stories' ) ) ;
	echo ' <a href="' . esc_url( menu_page_url( 'cb-stories-plugin-menu', false ) ) . '" class="add-new-h2">' . esc_html( __( 'Add New', 'cb-stories' ) ) . '</a></h2>';

?> <form id="cb-stories-filter"" method="get"> 
	<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
  
<?php
	
	$wp_list_table->display();
?> </form> <?php
	echo '</div>';
	
	
	
	}
	/********************************************************
	 *
	 *		cb_stories_options_page
	 *
	 *		Options page and form for Stories Plugin
	 *
	 *
	 *
	 **************************************************/
	public function cb_stories_options_page()
	{
	$s='';
	$id='';
	if (isset($_REQUEST['action']))
		$s = $_REQUEST['action'];

	if ($s == 'edit')
	{
		$nonce = $_REQUEST['_wpnonce'];
			if ( ! wp_verify_nonce( $nonce, 'edit_story' ) ){
					die( 'Security check' ); 
			}
			
			$id = $_REQUEST['id'];
			
			
		
	}
		
		
		
		add_meta_box('cb-stories-metabox', "<h1> Camana Bay Stories</h1>", array(&$this, 'cb_stories_metabox_page'), $this->cb_stories_pagehook, 'cb-stories');
			
	   ?>
    <div class="wrap">

        <form accept-charset="UTF-8" action="<?php echo admin_url( 'admin-post.php'); ?>" method="post">
		<input type="hidden" name="action" value="<?php echo $this->action ?>"  >
		<input type="hidden" name="edit_id" value="<?php echo $id ?>"  >

		
		 <div id="poststuff" class="metabox-holder">
          
		 
            <?php 
				
				
			
				do_meta_boxes($this->cb_stories_pagehook, 'cb-stories',$s); 
			
	
	
         submit_button();



			 ?>
		 </div>
        </form>
    </div>

    <?php
	
	}
	/*****************************************************************
	 *
	 *		cb_stories_metabox_page
	 *
	 *		put the fields in a metabox
	 *
	 *
	 ****************************************************************/
	public function cb_stories_metabox_page($action)
	{
	
		$id = '';
		if ($action == 'edit')
		{
			$id =  $_REQUEST['id'];
			$post_meta = get_post_meta($id, 'cb-story_settings');
			$post_title = get_the_title( $id );
		
		}
		
		$args = array(
	
			'post_status'      => 'publish',
			'suppress_filters' => true,
			'post_type' => 'post',
			'numberposts' => -1
		);
		$posts = get_posts($args);

		$post_lists = array();
		
		foreach ($posts as $key => $p) {
	
			$post_lists[$key]['id'] = $p->ID;
			$post_lists[$key]['title'] = $p->post_title;
			
		}
		printf(
            '<label for "cb-stories-setting-title">Title: </label><input required type="text"   name="cb-stories-setting-title" value="%s" />',
            isset( $post_title ) ? esc_attr( $post_title) : ''
        );

		for ($i=1;$i<=3;$i++)
		{
				$matching_id = '';

				if (isset($post_meta[0][$i][0]))
					$matching_id = $post_meta[0][$i][0];
				if (isset($post_meta[0][$i][1]))
					$value = $post_meta[0][$i][1];
?>
				<br /><br />
			<select  style="min-width:150px; min-height:50px; height:auto;" size="3" name="cb-stories-setting[<?php echo $i?>][link]">
<?php 
				foreach($post_lists as $key => $p)
				{ 

					
					if ($p['id'] == $matching_id){
?>
					<option value="<?php echo $p['id'] ?>" selected ><?php echo $p['title'] ?></option>
<?php 	
				}else{
?>
					<option id="<?php echo 'list_' .$i ?>" value="<?php echo $p['id'] ?>"><?php echo $p['title'] ?></option>
<?php 
					}
				}
?>
			</select> 
			
		
<?php		
			printf(
            '<label for "cb-stories-link-text">Link Text: </label><input type="text"   name="cb-stories-link-text[%d][text]" value="%s" />',
            $i, isset( $value ) ? esc_attr( $value) : ''
        );
		
		}
	
	
	}
	
	/*****************************************************************
	 *
	 *		cb_stories_init
	 *
	 *		register the stories page type
	 *
	 *
	 ****************************************************************/
	
	public function cb_stories_init() {
  

		register_post_type( 'cb-stories-page-type',
        array(
            'labels' => array(
                'name' => 'Camana Bay Stories',
                'singular_name' => 'Camana Bay Stories',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Camana Bay Story',
                'edit' => 'Edit',
                'edit_item' => 'Edit Camana Bay Story',
                'new_item' => 'New Camana Bay Story',
                'view' => 'View',
                'view_item' => 'View Camana Bay Story',
				 'all_items' => __( 'All Camana Bay Stories', 'cb-stories' ),	
                'not_found' => 'No Camana Bay Stories found',
                'not_found_in_trash' => 'No Camana Bay Stories found in Trash',
                'parent' => 'Parent Camana Bay Story',
				 'menu_name'=>  'Camana Bay Stories'
				
            ),
 
            
            'menu_position' => 15,
            'supports' => array( 'title', 'editor' ),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => false,
			'exclude_from_search' => true,
                'publicly_queryable' => false,
                'show_in_nav_menus' => false,
			'query_var' => false,
                'rewrite' => false,
				'public' => true,
				'show_ui' => false,
				'show_in_admin_bar' => true,
				'show_in_menu' =>true
              
                
			)
		);
	}

}

endif;

add_action( 'plugins_loaded', array( 'CamanaStoriesPlugin', 'init' ), 10 );


  
  

  
  
  
  
  
  
  
