<?php
/******************************************************************************
*
*		storiesDisplay.php
*
*	    Displays the Camana Bay Stories slider on the page via the short code showCB_StoriesDisplay with an id
*
*****************************************************************************/
class _Stories_Display{

	private $options;
	public function __construct()

    {

		add_shortcode( 'showCB_StoriesDisplay', array( $this, 'cb_show_Stories_Display_func' ) );

		add_action( 'wp_enqueue_scripts', array( $this,'cb_show_Stories_Display_enqueue_styles' ));
	}

	function catch_that_image($post) {
		
		$first_img = '';
		ob_start();
		ob_end_clean();

		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post['post_content'], $matches);
	
		$first_img = $matches[1][0];

		if(empty($first_img)) {
	
		$first_img =   plugins_url( 'CamanaStories/images/blog_default.png' );
		}
		return $first_img;
	}
	/******************************************************************
	*
	*	cb_show_Stories_Display_func
	*
	*	returns the string that shows the slider for display and sets up the localized jQuery script
	*
	******************************************************************/
	function cb_show_Stories_Display_func($atts, $content="" )

	{


		$atts = shortcode_atts(
		array(
			'id' => 0,

		), $atts, 'showCB_StoriesDisplay' );



		$post_id = $atts['id'];
		// this is the cb_stories type post





		$p =get_post( $post_id );

		 $pm = get_post_meta($post_id,'cb-story_settings');



		  $p= get_post($pm[0][1][0]);

		$right_arrow_image =  plugins_url( 'CamanaStories/images/arrow_carrousel_right.png' );


		$string = '<div id="cb_cs_outer_container">
		<div class="cb_cs_title">Camana Bay Stories </div>
		 <div class="cb_cs_subcontainer">
			<div id="cb_cs_image_left">

				<h2 class="cb_cs_h2"> </h2>
				<p class="cb_cs_p">	</p>

			</div>
			<div id="cb_cs_image_right">
					<div class="cb_first_image"  >
					</div>
			</div>
			<div class="cb_st_permalink">
					<a class="cb_st_permalink_a" href="#">The Text Link </a>
				</div>
			</div>
            <a href="#" class="cb_st_right_arrow" >
			         <div class="cb_st_bottom">
			                  <div class="cb_st_next">next story
			                           <img src="' . $right_arrow_image . '"  />
                              </div>
                    </div>
            </a>
		</div>';


		$index=0;

		foreach( $pm[0] as $p1 ){
			if (empty($p1[0]))
			   continue;
			$p = get_post($p1[0],ARRAY_A);

		//	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $p['ID'] ), 'single-post-thumbnail' );

			$image = $this->catch_that_image($p);
		//	$cb_post_info[$index]['cb_image'] = $image[0];
			$cb_post_info[$index]['cb_image'] = $image;


			$excerpt = wp_trim_words( $p['post_content'], 50,'...' );
			$cb_post_info[$index]['cb_excerpt'] = $excerpt;
			$permalink = get_permalink($p['ID']);
			$cb_post_info[$index]['cb_permalink'] = $permalink;
			$cb_post_info[$index]['cb_post_title'] = $p['post_title'];

			//$image = wp_get_attachment_image_src( get_post_thumbnail_id( $p['ID'] ), 'single-post-thumbnail' );
			$cb_post_info[$index]['cb_featured_image'] = $image;
			$cb_post_info[$index]['cb-stories-link-text'] = $p1[1];
			$index++;
		}

			 wp_register_script( 'cb_stories_slider', plugins_url( '/CamanaStories/js/cb_stories_slider.js' ), array('jquery') );
			wp_enqueue_script('cb_stories_slider');

		$params = array(
  'options' => $cb_post_info,
  'last_index' =>$index

);

		// in javascript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
			wp_localize_script( 'cb_stories_slider', 'cb_stories_ajax',$params);




		return $string;
	//

	}

	function cb_show_Stories_Display_enqueue_styles()
	{

		wp_register_style( 'cb_cs_css', plugins_url( 'CamanaStories/css/camanastories.min.css' ) );
		wp_enqueue_style( 'cb_cs_css' );


	}



	public static function instance() {

		 new _Stories_Display;



	}
}



	function cb_init_Stories_Display(){


		return _Stories_Display::instance();

	}

add_action( 'plugins_loaded',  'cb_init_Stories_Display'  );

?>
