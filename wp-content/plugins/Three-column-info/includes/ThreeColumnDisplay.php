<?php
if (!class_exists('_Three_Column_Display')) {
class _Three_Column_Display{

	private $options;
	public function __construct()

    {

		add_shortcode( 'showCB_InfoListDisplay', array( $this, 'cb_show_Three_Column_Display_func' ) );

		add_action( 'wp_enqueue_scripts', array( $this,'cb_show_Three_Column_Display_enqueue_styles' ));

	}


	function cb_show_Three_Column_Display_func($atts, $content="" )

	{



		$post_id = $atts['id'];
		// this is the cb_stories type post


		$p =get_post( $post_id );


		 $pm = get_post_meta($post_id,'cb-three-column-settings');
		$cb_button_color = $pm[0][0];




		$string = '<div id="'. $post_id . '">
			<div id="cb_three_inside_container">
				<div class="cb_three_column_item">
				<span class="cb_three_column_title">'. $pm[0][1]['Title'] . '</span><br/>'.
				nl2br($pm[0][1]['Line_1'],false). '<br />


				<hr class="horizontal-line" />
				</div>

				<div class="cb_vertical-line"> </div>

				<div class="cb_three_column_item">
				<span class="cb_three_column_title">'. $pm[0][2]['Title'] . '</span><br/>'.
				nl2br($pm[0][2]['Line_1'],false). '
				<br />











				<hr class="horizontal-line" />
				</div>
				<div class="cb_vertical-line"> </div>
				<div id="cb_three_3" class="cb_three_column_item">
				<span class="cb_three_column_title">'. $pm[0][3]['Title'] . '</span><br/>'.
				nl2br($pm[0][3]['Line_1'],false). '
				<br />';



			if (strlen($pm[0][3]['facebook']) > 0)
			{
			$string .='
			 <span>
                    <a  href="'.$pm[0][3]['facebook'] . '">
                        <i class="fa fa-facebook-official fa-2x color_the_bg"></i>
                    </a>
                </span>';
			}

            if (strlen($pm[0][3]['twitter']) > 0)
			{
			$string .='
				 <span>
                     <a  href="'.$pm[0][3]['twitter'] . '">
                        <i class="fa fa-twitter-square fa-2x color_the_bg"></i>
                    </a>
                </span>';
			}

			if (strlen($pm[0][3]['instagram']) > 0)
			{
			$string .='
				 <span>
                     <a  href="'.$pm[0][3]['instagram'] . '">
                        <i class="fa fa-instagram fa-2x color_the_bg"></i>
                    </a>
                </span>';
			}


             if (strlen($pm[0][3]['youtube']) > 0)
			{
			$string .='
				 <span>
                    <a  href="'.$pm[0][3]['youtube'] . '">
                        <i class="fa fa-youtube-square fa-2x color_the_bg"></i>
                    </a>
                </span>';
			}

			$string .='

				</div>
			</div>
		</div>';

	?>
<script>

	 jQuery(document).ready(function($) {

		var buttonColor= '<?php echo $cb_button_color;?>';
		var id='#<?php echo $post_id ?>';
		var title = jQuery(id).find('.cb_three_column_title');
		jQuery(title).css('color',buttonColor);
		var social_media = jQuery(id).find('.color_the_bg');
		jQuery(social_media).css('color',buttonColor);




	 });

</script>
<?php


		return $string;


	}


	function cb_show_Three_Column_Display_enqueue_styles()
	{
	$s= plugins_url( 'css/font-awesome.css',__DIR__ );

		wp_register_style( 'cb_tcd_font_css', plugins_url( 'css/font-awesome.min.css',__DIR__ ) );
		wp_enqueue_style( 'cb_tcd_font_css' );
		wp_register_style( 'cb_tcd_css', plugins_url( '../css/three_column_display.min.css',__FILE__ ) );
		wp_enqueue_style( 'cb_tcd_css' );




	}



	public static function instance() {

		 new _Three_Column_Display;



	}
}

}

	function cb_init_Three_Column_Display(){

 new _Three_Column_Display;
		return _Three_Column_Display::instance();

	}

add_action( 'plugins_loaded',  'cb_init_Three_Column_Display'  );

?>
