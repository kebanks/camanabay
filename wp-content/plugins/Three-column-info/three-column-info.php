<?php
/*
Plugin Name: Three Column Info Plugin
Plugin URI: http://www.camanabay.com/
Description: Three Column Info Plugin
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access
}
require_once 'includes/ThreeColumnDisplay.php';
require_once 'includes/cb_three_column_info_list.php';
if ( ! class_exists( 'ThreeColumnInfoPlugin' ) ) :


class ThreeColumnInfoPlugin {

 /**
     * @var string
     */
    public $version = '1.0';
	private $edit_id ;
	public static function init() {

        $camanaStories = new self();

    }
	public function __construct() {

       
        $this->setup_actions();
        

    }
	 /**
     * Hook camanabay stories into WordPress
     */
    private function setup_actions() {
	
		add_action( 'init', array($this, 'admin_post_cb_three_column_info_init' ));
		add_action( 'admin_menu', array($this,  'register_admin_menu' ));
	add_action( 'admin_post_cb_three_column_info_create_three_column', array( $this, 'create_three_column' ) );
	add_action( 'admin_post_cb_three_column_info_update_three_column', array( $this, 'update_three_column' ) );
	 add_action('admin_enqueue_scripts', array($this, 'three_column_admin_styles'));
	
	$s='';
	if (isset($_REQUEST['action']))
		$s = $_REQUEST['action'];
	if ($s == 'edit'){
		$this->action = 'cb_three_column_info_update_three_column';
	}
	else {
		$this->action = 'cb_three_column_info_create_three_column';
	}	
	
	
	}
	public function three_column_admin_styles()
	{
		wp_register_script( 'cb_three_column_tabbed', plugins_url( 'js/tabbed.js', __FILE__ ) );
		wp_enqueue_script( 'cb_three_column_tabbed', plugins_url( 'js/tabbed.js', __FILE__ ) , array(), '1.0.0', true );
		 wp_enqueue_style( 'wp-color-picker' );        
        wp_enqueue_script( 'wp-color-picker' );      
		 wp_register_style( 'cb_three_color_picker_css', plugins_url( '/css/colorpicker.css',__FILE__ ) );
		wp_enqueue_style( 'cb_three_color_picker_css' );
		
	
	}
	public function update_three_column()
	{

			$post_id = $_POST['edit_id'];
			$this->update_all($post_id);
			wp_redirect( admin_url( "admin.php?page=cb_three_column_listing" ) );
			return;
	
	}
	public function update_all($post_id)
	{
		$post_data = array();
		$title = $_POST['cb-three-column-main-title'];
		
		
		
		for ($i=1;$i<4;$i++)
		{
			$x='Title_'.$i;
			$post_data[$i]['Title'] = $_POST[$x];
			$x='Line_1_'.$i;
			$post_data[$i]['Line_1'] = $_POST[$x];
			
			
			
		
			
		}
		
		
			$x='FB_'.$i;
			$post_data[3]['facebook'] = $_POST['FB_3'];
		
			$x='twitter_'.$i;
			$post_data[3]['twitter'] = $_POST['twitter_3'];
			
			$x='instagram_'.$i;
			$post_data[3]['instagram'] = $_POST['instagram_3'];
			
			$x='youtube_'.$i;
			$post_data[3]['youtube'] = $_POST['youtube_3'];
	
		$cb_three_column_post = array(
			'ID'           => $post_id,
			'post_title'   => $title,
			'post_status' => 'publish'
			
		);
		$post_data[0] = $_POST['cb-three-button-color'];

		wp_update_post( $cb_three_column_post );
		update_post_meta( $post_id, 'cb-three-column-settings', $post_data );
		
		
	
	
	}
	public function create_three_column()
	{
		
		$post_id =  $this->create_three_column_item();

		$this->update_all($post_id);
		wp_redirect( admin_url( "admin.php?page=cb-three-column-plugin-menu" ) );
		return;
		
		
		
	}

	
	public function create_three_column_item() {

        // check nonce
      //  check_admin_referer( "cb_three_column_infocreate_three_column_item" );
		// insert the post
        $id = wp_insert_post( array(
                'post_title' => __( "New item", "cb-three-column" ),
                'post_status' => 'draft',
                'post_type' => 'cb-three-page-type'
            )
        );

		return $id;
	}
	public function register_admin_menu() {

		$this->cb_three_column_infopagehook = add_menu_page('Three Column Info', 'Three Column Info', 'manage_options', 'cb-three-column-plugin-menu',"",plugins_url( 'images/image.png', __FILE__ ));
	
		add_submenu_page( 'cb-three-column-plugin-menu', 'Add New Section', 'Add New Section', 'manage_options', 'cb-three-column-plugin-menu',array($this,'my_options_page' ));
		add_submenu_page( 'cb-three-column-plugin-menu', 'Three Column Info List', 'Three Column Info List ', 'manage_options', 'cb_three_column_listing',array($this,'cb_three_column_infolisting'))	;	
		
		
	}
	public function add_color_picker($cb_button_color)
	{
		?>

<script type='text/javascript'>
      jQuery(document).ready(function($) {
         $('.three-color-picker').wpColorPicker();
		jQuery('.cb_three_color_select').click(function(e) {
					e.preventDefault();
					var id = '#'+jQuery (this).attr('id');
					var hexText =  jQuery(id).text();
					var x = '.three-color-picker';
				
				jQuery(x).val(hexText);
					
				jQuery('.wp-color-result').css({backgroundColor: hexText});
				jQuery(x).wpColorPicker('color', hexText);
		 	});
       });
        </script>


	
<?php 
	 $color_array = array( "#a87eb1" ,"#cb003c", "#FFB718 ", "#faa519", "#77bc1f","#64cbc8","#00b6dd","#008aab","#d7d1c4","#c4e76a");
		echo "<h2>Colors</h2>";
		echo "<p>";
		foreach($color_array as $key => $color)
		{	
			$class = "color_" . $key;
		
				echo "<a href='#' id='" . $class . "' class='cb_three_color_select' >". $color ."</a>" ;
				if ($key == 4)
				echo "</p></p>";
	
		}
		echo "</p>";
		
?>

<p><label for="cb-three-button-color"></label><?php _e('Text Color:', 'cb-three-column'); ?></label>
  <p> <input class="three-color-picker" type="text" id="cb-three-button-color" name="cb-three-button-color" value="<?php echo $cb_button_color; ?>" />                            
	</p></p>



<?php
	}
	public function cb_three_column_infolisting($arg)
	{
	$wp_list_table = new cb_three_column_list_table();
	$wp_list_table->prepare_items();
	echo '<div class="wrap"><h2>' . esc_html( __( 'Three Column Info', 'cb-three-column ' ) ) ;
	echo ' <a href="' . esc_url( menu_page_url( 'cb-three-column-plugin-menu', false ) ) . '" class="add-new-h2">' . esc_html( __( 'Add New', 'cb-three-column ' ) ) . '</a></h2>';

?> <form id="cb-three-column-filter"" method="get"> 
	<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
  
<?php
	
	$wp_list_table->display();
?> </form> <?php
	echo '</div>';
	
	
	
	}
	public function my_options_page()
	{
	$s='';
	$id='';
	if (isset($_REQUEST['action']))
		$s = $_REQUEST['action'];
	$cb_button_color = '';
	if ($s == 'edit')
	{
			$nonce = $_REQUEST['_wpnonce'];
			if ( ! wp_verify_nonce( $nonce, 'edit_list_item' ) ){
					die( 'Security check' ); 
			}
			
			$id = $_REQUEST['id'];
			$post_meta = get_post_meta($id, 'cb-three-column-settings');

			$post_title = get_the_title( $id );
			$cb_button_color = $post_meta[0][0];
			
		
	}
		
	 ob_start();	
		
			
	   ?>
   <div class="wrap">

            <?php screen_icon(); ?>

            <h2><?php _e('Three Column Info Plugin','cb-three-column-text-domain' );?> </h2>     

        <form accept-charset="UTF-8" action="<?php echo admin_url( 'admin-post.php'); ?>" method="post">
			<input type="hidden" name="action" value="<?php echo $this->action ?>"  >
		<input type="hidden" name="edit_id" value="<?php echo $id ?>" >	
				<?php
			printf(
            '<br/><label for "cb-three-column-main-title">Plugin Title: </label><input required type="text"   name="cb-three-column-main-title" value="%s" /><br/><br/>',
            isset( $post_title ) ? esc_attr( $post_title) : ''
        );
			
			?>
		
			<div class="tabbedPanels">

			<ul class="tabs">

				<li><a href="#panel1" tabindex="1"><?php _e('First Column Item','cb-three-column-text-domain' );?></a></li>

				<li><a href="#panel2" tabindex="2"><?php _e('Second Column Item','cb-three-column-text-domain' );?></a></li>
				<li><a href="#panel3" tabindex="3"><?php _e('Third Column Item','cb-three-column-text-domain' );?></a></li>
			
			</ul>
			<div class="panelContainer">
				<div id="panel1" class="panel"> 
				  <?php $this->show_fields(1,$post_meta); ?>
				</div>
				<div id="panel2" class="panel"> 
					 <?php $this->show_fields(2,$post_meta); ?>
				</div>
				<div id="panel3" class="panel"> 
					 <?php $this->show_fields(3,$post_meta); ?>
					 
		 
					 <p><label for="FB_3"><?php _e('Facebook:', 'cb_three_line_plugin'); ?></label>
			<input class="widefat" id="FB_3" name="FB_3" type="text" value="<?php echo $post_meta[0][3]['facebook']; ?>" />
	</p>
	<p><label for="twitter_3"><?php _e('Twitter:', 'cb_three_line_plugin'); ?></label>
			<input class="widefat" id="twitter_3" name="twitter_3" type="text" value="<?php echo $post_meta[0][3]['twitter']; ?>" />
	</p>
	<p><label for="instagram_3"><?php _e('Instagram:', 'cb_three_line_plugin'); ?></label>
			<input class="widefat" id="instagram_3" name="instagram_3" type="text" value="<?php echo $post_meta[0][3]['instagram']; ?>" />
	</p>
	<p><label for="youtube_3"><?php _e('Youtube:', 'cb_three_line_plugin'); ?></label>
			<input class="widefat" id="youtube_3" name="youtube_3" type="text" value="<?php echo $post_meta[0][3]['youtube']; ?>" />
	</p>
		
					 
					 
					 
					 
					 
				</div>
			
			</div>

		</div>
		<br/>
		
		
	<?php
		$this->add_color_picker($cb_button_color);
         submit_button();


			 ?>
	
        </form>
    </div>

    <?php
	ob_end_flush();

	
	}
	public function show_fields($i,$post_meta)
	{
	

		$cb_widget_text=$post_meta[0][$i]['Line_1'];
		$cb_three_title=$post_meta[0][$i]['Title'];
	
		?>
		<p><label for="Title_<?php echo $i ?>"><?php _e('Title:', 'cb_three_line_plugin'); ?></label>
			<input class="widefat" id="Title_<?php echo $i ?>" name="Title_<?php echo $i ?>" type="text" value="<?php echo $cb_three_title; ?>" />
		</p>
		
	<p><label for="Line_1_<?php echo $i ?>"><?php _e('Text:', 'cb_three_line_plugin'); ?></label>
	<textarea class="widefat" id="Line_1_<?php echo $i ?>" name="Line_1_<?php echo $i ?>"><?php echo $cb_widget_text; ?></textarea>
	</p>
	
		
		<?php	
	
	
	
	}
	
	
	public function admin_post_cb_three_column_info_init() {
  

		register_post_type( 'cb-three-page-type',
        array(
            'labels' => array(
                'name' => 'Three Column Info',
                'singular_name' => 'Three Column Info',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Three Column Info Item ',
                'edit' => 'Edit',
                'edit_item' => 'Edit Three Column Info Item',
                'new_item' => 'New Three Column Info Item ',
                'view' => 'View',
                'view_item' => 'View Three Column Info ',
				 'all_items' => __( 'All Three Column Info', 'cb-three-column ' ),	
                'not_found' => 'No Three Column Info found',
                'not_found_in_trash' => 'No Three Column Info found in Trash',
                'parent' => 'Parent Three Column Info ',
				 'menu_name'=>  'Three Column Info'
				
            ),
 
            
            'menu_position' => 15,
            'supports' => array( 'title', 'editor' ),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => false,
			'exclude_from_search' => true,
                'publicly_queryable' => false,
                'show_in_nav_menus' => false,
			'query_var' => false,
                'rewrite' => false,
				'public' => true,
				'show_ui' => false,
				'show_in_admin_bar' => true,
				'show_in_menu' =>true
              
                
			)
		);
	}

}

endif;

add_action( 'plugins_loaded', array( 'ThreeColumnInfoPlugin', 'init' ), 10 );


  
  

  
  
  
  
  
  
  
