/* **************************************************************************************
*  cb_directory_detail_js.js 
*
*
*   JQuery code that manages the directory detail display for the directory detail plugin
*
*
*
*******************************************************************************************/


jQuery(document).ready(function ($) {

	var output_string = cb_directory_detail_ajax.cb_content;	
	var type = cb_directory_detail_ajax.cb_type;	


	
	jQuery('#directory_detail_content').html(output_string);


function do_ajax(mydata){

	jQuery.ajax({                
			url: cb_directory_detail_ajax.ajax_url,
			type: "POST",
			data: mydata,     
			cache: false,
			success: function(data, status) {
	
				 var posts = JSON.parse(data);

			 jQuery('#directory_detail_content').html(posts);
      }
		});		


}

	// jQuery('.cb_dd_filter_inner_right_item').click(function(e) {
	jQuery( document ).on( 'click', '.cb_dd_inner_left_item', function(e) {
			e.preventDefault();
			var mydata = {
			action: 'cb-directory-detail',
			field: "",
			type: type
		};
		do_ajax(mydata);
	
		
	});
	
	jQuery(".cb-dir-detail-filter-item-inside").hover(
		function() {
	
			
	
			var x = jQuery( this).find( ".cb-dir-detail-filter-image-item" );
			jQuery(x).hide();
			x = jQuery( this).find( ".cb-dir-detail-filter-hover-image-item" );
			jQuery(x).show();
			
		},
		function() {
		
			var x = jQuery( this).find( ".cb-dir-detail-filter-hover-image-item" );
			jQuery(x).hide();
			x = jQuery( this).find( ".cb-dir-detail-filter-image-item" );
			jQuery(x).show();
		}
	);

	jQuery( document ).on( 'click', '.cb_dd_filter_inner_right_item', function(e) {

		e.preventDefault();
		var a_el = jQuery(this).find('a');
		
		var id = jQuery(a_el).attr('id');
		var mydata = {
			action: 'cb-directory-detail',
			field: id,
			type: type
		};
	 
		
		do_ajax(mydata);
		
	
		
	});
	
	
});    
