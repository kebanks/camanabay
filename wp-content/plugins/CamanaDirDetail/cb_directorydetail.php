<?php
/*
Plugin Name: Camana Directory Detail Plugin
Plugin URI: http://www.camanabay.com/
Description: Camana Directory Detail Plugin
Version: 1.0
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php
/*******************************************************************************************************************
*
*		Camana Directory Detail Plugin 
*
*		Plugin that handles the creation, listing and display of Camana Directory Detail elements
*
*
*
*
*
**********************************************************************************************************************/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // disable direct access
}
require_once 'includes/directoryDetailDisplay.php';

if ( ! class_exists( 'CamanayDirectoryDetailPlugin' ) ) :


class CamanayDirectoryDetailPlugin {

 /**
     * @var string
     */
    public $version = '1.0';
	private $edit_id ;
	public static function init() {

        $CamanaDirectoryDetail = new self();

    }
	public function __construct() {

       
        $this->setup_actions();
        

    }
	 /**
     * Hook camanaDirectory Detail into WordPress
     */
    private function setup_actions() {
	
	
	/*   we want a separate menu for our plugin 
	 */
	 
		 add_action( 'admin_head', array( $this,'cb_show_cb_directory_detail_enqueue_styles' ));
		add_action( 'init', array($this, 'cb_directory_detail_plugin_init' ));
		add_action( 'init', array($this, 'cb_directory_detail_taxonomy_init' ));
		
		add_action( 'admin_menu', array($this,  'register_admin_menu' ));
		add_action( 'save_post_cb-dir_det-page-type', array( $this, 'create_directory_detail' ) );
		
		add_filter( 'manage_edit-cb-dir_det-page-type_columns', array($this, 'edit_directory_detail_columns' ) );
          add_action( 'manage_cb-dir_det-page-type_posts_custom_column', array($this, 'manage_directory_detail_columns'),10,2);
      
		 
	
	
	}
	
	
	
function cb_show_cb_directory_detail_enqueue_styles()
{


	wp_register_style( 'cb_directory_detail_css', plugins_url( 'CamanaDirDetail/css/cb_directorydetail_admin.css' ) );
	wp_enqueue_style( 'cb_directory_detail_css' );

}

function cb_get_taxonomy_parents( $id, $taxonomy = 'category', $link = false, $separator = '/', $nicename = false, $visited = array() ) {

            $chain = '';
            $parent = get_term( $id, $taxonomy );

            if ( is_wp_error( $parent ) )
                    return $parent;

            if ( $nicename )
                    $name = $parent->slug;
            else
                    $name = $parent->name;

            if ( $parent->parent && ( $parent->parent != $parent->term_id ) && !in_array( $parent->parent, $visited ) ) {
                    $visited[] = $parent->parent;
                    $chain .= $this->cb_get_taxonomy_parents( $parent->parent, $taxonomy, FALSE, $separator, $nicename, $visited );
            }

            if ( $link )
                    $chain .= '<a href="' . esc_url( get_term_link( $parent,$taxonomy ) ) . '" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $parent->name ) ) . '">'.$name.'</a>' . $separator;
            else
                    $chain .= $name.$separator;

            return $chain;
    }
function manage_directory_detail_columns( $column, $post_id ) {

	switch( $column ) {
		
		case "directory_detailtype":
		
		
			$s =  wp_get_post_terms( $post_id, 'dirdetailcat' );
		
			
			//echo ($s[0]->name);
			$count = count($s);
					
			foreach ($s as $t)
			{
			   
	
				if ($t->parent != 0){
				    
				    $r = $this->cb_get_taxonomy_parents($t->term_id,$s[0]->taxonomy, FALSE, ' &raquo; ');
					$r .="<br />";
					echo $r;
				}
				if ($count == 1)
					echo $t->name;
			
			}
			
			
			break;
	     case "image":
		    $s = get_post_meta($post_id,'cb-directory_detail_settings');
			$image = $s[0]['cb-directory_detail-pic-url'];
			$output = '<img class="cb_directory_detail_img_display" src = ' . $image . ' /> ';
			echo $output;
		 
		    break;
			case "hover_image":
			$output = '';
		    $s = get_post_meta($post_id,'cb-directory_detail_settings');
			$image = $s[0]['cb-directory_detail-hover-pic-url'];
			if (!empty($image))
				$output = '<img class="cb_directory_detail_img_display" src = ' . $image . ' /> ';
			
			echo $output;
		 
		    break;
		default:
                return print_r($column,true); //Show the whole array for troubleshooting purposes
       
    }
	
	

}

function edit_directory_detail_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
	    'image' => __('Image'),
	    'hover_image' => __('Hover Image'),
		'directory_detailtype' => 'Directory Detail Type',
		'date' => __( 'Date' )
	);

	return $columns;
}
	
	
	function add_directory_detail_metaboxes()
	{
	
		add_meta_box('cb-directory_detail-metabox', "<h1> Camana Directory Detail</h1>", array(&$this,'cb_directory_detail_plugin_metabox_page'),'cb-dir_det-page-type' , 'advanced','high');
		
	
	}
	
	
	/******************************************************
	*
	*		create_directory_detail
	*
	*		Function to create a New Directory Detail  Entry
	*
	******************************************************/
	public function create_directory_detail( $post_id, $post, $update )
	{
		
	
		
		$post_data = array();
		$post_data['cb-directory_detail-pic-url'] = $_POST['cb-directory_detail-pic-url'];
		$post_data['cb-directory_detail-hover-pic-url'] = $_POST['cb-directory_detail-hover-pic-url'];
		$post_data['cb-directory_detail-link-text'] = $_POST['cb-directory_detail-link-text'];
		update_post_meta( $post_id, 'cb-directory_detail_settings', $post_data );
		
	}
	
	/********************************************************************************************
	*
	*   create_directory_detail_custom_post_type
	*
	*	Inserts the initial post for create directory_detail element
	*
	*
	*
	****************************************************************************************/
	
	
	
	
	
	
	public function register_admin_menu() {

		add_submenu_page( 'cb-directory_detail-plugin-menu', 'Camana Directory Detail List', 'Camana Bay Directory Detail List ', 'manage_options', 'cb-directory_detail-listing',array($this,'cb_directory_detail_plugin_listing'))	;	
		add_meta_box('cb-directory_detail-metabox', "<h1> Camana Bay Directory Detail </h1>", array(&$this, 'cb_directory_detail_plugin_metabox_page'),'cb-dir_det-page-type' , 'normal','high');
		
		
	}
	public function cb_directory_detail_plugin_listing($arg)
	{
	$wp_list_table = new _cb_directory_detail_list_table();
	$wp_list_table->prepare_items();
	echo '<div class="wrap"><h2>' . esc_html( __( 'Camana Bay Directory Detail ', 'cb-directory_detail' ) ) ;
	echo ' <a href="' . esc_url( menu_page_url( 'cb-directory_detail-plugin-menu', false ) ) . '" class="add-new-h2">' . esc_html( __( 'Add New', 'cb-directory_detail' ) ) . '</a></h2>';

?> <form id="cb-directory_detail-filter"" method="get"> 
	<input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
  
<?php
	
	$wp_list_table->display();
?> </form> <?php
	echo '</div>';
	
	
	
	}
	/********************************************************
	 *
	 *		cb_directory_detail_plugin_options_page
	 *
	 *		Options page and form for Directory Detail  Plugin
	 *
	 *
	 *
	 **************************************************/
	public function cb_directory_detail_plugin_options_page()
	{

	
		
		
		
	   ?>
    <div class="wrap">

        <form accept-charset="UTF-8" action="<?php echo admin_url( 'admin-post.php'); ?>" method="post">
		<input type="hidden" name="action" value="<?php echo $this->action ?>"  >
		<input type="hidden" name="edit_id" value="<?php echo $id ?>"  >

		
	
          
		 
            <?php 
				
				
			
			
			
	
	
         submit_button();



			 ?>
		 
        </form>
    </div>

    <?php
	
	}
public function cb_directory_detail_taxonomy_init()
{
	
	$labels = array(
        'name'              => 'Directory Detail Types',
        'singular_name'     => 'Directory Detail Type',
        'search_items'      => 'Search Directory Detail  Categories',
        'all_items'         => 'All Directory Detail Categories',
        'parent_item'       => 'Parent Directory Detail Category',
        'parent_item_colon' => 'Parent Directory Detail Category:',
        'edit_item'         => 'Edit Directory Detail Category',
        'update_item'       => 'Update Directory Detail Category',
        'add_new_item'      => 'Add New Directory Detail Category',
        'new_item_name'     => 'New Directory Detail  Category Name',
        'menu_name'         => 'Directory Detail  Category',
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'query_var' => true,
        'rewrite' => true,
        'show_admin_column' => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'sort'              =>  true,
		'rewrite'           =>  'directory_detail-images' ,
    );
 
    register_taxonomy( 'dirdetailcat', 'cb-dir_det-page-type' ,$args);
	
}	
	/*****************************************************************
	 *
	 *		cb_directory_detail_plugin_metabox_page
	 *
	 *		put the fields in a metabox
	 *
	 *
	 ****************************************************************/
	public function cb_directory_detail_plugin_metabox_page($post, $metabox)
	{
	
		
		$id =  $post->ID;
		
		
		if (isset($id))
		{
			
			$post_meta = get_post_meta($id, 'cb-directory_detail_settings');
			$post_title = get_the_title( $id );
			$cb_pic_url = $post_meta[0]['cb-directory_detail-pic-url'];
			$cb_hover_pic_url = $post_meta[0]['cb-directory_detail-hover-pic-url'];
			$cb_directory_detail_link_text = $post_meta[0]['cb-directory_detail-link-text'];
			
		
		}
		
		wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        wp_enqueue_script('upload_media_widget', plugin_dir_url(__FILE__) . '../js/upload-media.js', array('jquery'));
      

        wp_enqueue_style('thickbox');
		
	
			
?>	
		<p>
			<h3>Best size for image is 320px by 320px</h3>
            <label for="cb-directory_detail-pic-url"><?php _e( 'Display Image:' ); ?></label>
            <input name="cb-directory_detail-pic-url" id="cb-directory_detail-pic-url" type="text" size="50"  value="<?php echo esc_url( $cb_pic_url ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
<?php
			printf(
            '<label for "cb-directory_detail-link-text">Link: </label><input type="text" size="50"  name="cb-directory_detail-link-text" value="%s" />',
             isset( $cb_directory_detail_link_text ) ? esc_attr( $cb_directory_detail_link_text) : ''
        );
?>
		<p>
			<h3>Best size for image is 320px by 320px</h3>
            <label for="cb-directory_detail-hover-pic-url"><?php _e( 'Display Hover Image:' ); ?></label>
            <input name="cb-directory_detail-hover-pic-url" id="cb-directory_detail-hover-pic-url" type="text" size="50"  value="<?php echo esc_url( $cb_hover_pic_url ); ?>" />
            <input class="upload_image_button" type="button" value="Upload Image" />
        </p>
<?php
	}
	
	/*****************************************************************
	 *
	 *		cb_directory_detail_plugin_init
	 *
	 *		register the Directory Detail  page type
	 *
	 *
	 ****************************************************************/
	
	public function cb_directory_detail_plugin_init() {
  

		register_post_type( 'cb-dir_det-page-type',
        array(
            'labels' => array(
                'name' => 'Camana Bay Directory Detail ',
                'singular_name' => 'Camana Bay Directory Detail ',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Camana Bay Directory Detail  Entry',
                'edit' => 'Edit',
                'edit_item' => 'Edit Camana Bay Directory Detail  Entry',
                'new_item' => 'New Camana Bay Directory Detail  Entry',
                'view' => 'View',
                'view_item' => 'View Camana Bay Directory Detail  Entry',
				 'all_items' => __( 'All Camana Bay Directory Detail ', 'cb-directory_detail' ),	
                'not_found' => 'No Camana Bay Directory Detail  found',
                'not_found_in_trash' => 'No Camana Bay Directory Detail  found in Trash',
                'parent' => 'Parent Camana Bay Directory Detail  Entry',
				 'menu_name'=>  'Camana Bay Directory Detail '
				
            ),
 
            
            'menu_position' => 15.5,
            'supports' => array( 'title' ),
            'taxonomies' => array( 'dirdetailcat' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => false,
			'exclude_from_search' => true,
                'publicly_queryable' => false,
                'show_in_nav_menus' => false,
			'query_var' => false,
                'rewrite' => false,
				'public' => true,
				'show_ui' => true,
				'show_in_admin_bar' => true,
				'show_in_menu' =>true,
						'show_in_nav_menus' => true,
              
                
			)
		);
		
		
	
		
	}

}

endif;

add_action( 'plugins_loaded', array( 'CamanayDirectoryDetailPlugin', 'init' ), 10 );


  
  

  
  
  
  
  
  
  
