<?php
/******************************************************************************
*
*		directoryDetail.php
*
*	    Displays the Camana Bay Directory Detail via the short code showCB_directoryDetail with an id
*
*****************************************************************************/
class _Directory_Detail_Display{


	private $type;
	public function __construct()

    {

		add_shortcode( 'showCB_directoryDetail', array( $this, 'cb_show_Directory_Detail_Display_func' ) );

		add_action( 'wp_enqueue_scripts', array( $this,'cb_show_Directory_Detail_Display_enqueue_styles' ));
		add_action('wp_ajax_nopriv_cb-directory-detail',  array($this,'cb_directory_detail_action_function'));
		add_action('wp_ajax_cb-directory-detail',  array($this,'cb_directory_detail_action_function'));
	}
	
	function compareOrder($a, $b)
{

  if (strcasecmp ($a["title"], $b["title"]) ==0) 
     return 0;

 return strcasecmp ($a["title"], $b["title"]);


 
  
  
  
  
}
	
	function cb_directory_detail_action_function()
	{
	
		$id = $_POST['field'];
		$type = $_POST['type'];

	    $s =  $this->get_layout($id,$type);
$mystring = json_encode($s,JSON_UNESCAPED_SLASHES);

 echo json_encode( $s ,JSON_UNESCAPED_SLASHES);
		die();
	}
	function get_layout($sort_terms,$type){
	
	$sort_array = array(
	    0 => array(
        'id' => 0
    ),
    1 => array(
        'title'=>''
    ),
);



	$string="";
	
	if (strlen($sort_terms) == 0)
		     $sort_terms = $type;
	if ($type == "shop")
	{
		
		$taxonomy_terms = get_terms( 'dirdetailcat' );

		$t = get_term_by('name',$type, 'dirdetailcat');
		$taxonomy_children = get_term_children($t->term_id,'dirdetailcat');
	
		$string = '<div id="filter_outer"> 
		  <div id="cb_dd_inner_left">
		  <div class="cb_dd_inner_left_item">
		  <a id="" href="#">
				Show All 
		  </a> 
		  </div>
		   </div>
		   <div id="cb_dd_filter_inner_right">';
		         
			/* get the top buttons */
		foreach( $taxonomy_children as $t ) {
		
		
		
	
		$term = get_term( $t, 'dirdetailcat' ) ;
		if ($term->count ==0)
		  continue;
		
			
			$string.= "<div class='cb_dd_filter_inner_right_item'>
			<a id=" . $term->slug . " href='#'>" .
			$term->name . 
			"</a>
			</div>";
				
		
	
		
		}
		$string.= "</div>
		</div>";
		}
		
	

		$string .= "<div class='cb-dir-detail-filter-outside'>";
	$args = array(
        'slug' => $sort_terms
       
    );
		$taxonomy_terms = get_terms('dirdetailcat', $args);	
		
	
		$do_not_duplicate = array();
		foreach( $taxonomy_terms as $term ) {
		
	
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array(
        'post_type' => 'cb-dir_det-page-type',
        'dirdetailcat' => $term->slug,
		'order_by' => 'title',
		'order' => 'ASC',
		'posts_per_page'=>-1,
		'post_status'	 => 'publish',
		'paged'=>$paged
    );
		
		$query = new WP_Query( $args );
		$i = 0;
		while ( $query->have_posts() ) : $query->the_post(); 
				$id= get_the_ID(); 
				
				
		   
				
			// check for duplicates
			if (in_array($id, $do_not_duplicate)) {
			
			
				 continue;
			
			}
			  $title = get_the_title( $id );
			array_push($do_not_duplicate, $id);
			
			
			$sort_array[$i]['id']=$id;
			$sort_array[$i]['title'] = $title;
			$i++;
	/* 
	$post_meta= get_post_meta($id,'cb-directory_detail_settings');

			 
			
			 $image = $post_meta[0]['cb-directory_detail-pic-url'];
			 $link = $post_meta[0]['cb-directory_detail-link-text'];
			 
			$output = '<a href="' . $link . '"><img  src = ' . $image . ' /> </a>';
			
			
		   $string .= '<div class="cb-dir-detail-filter-item">
							<div class="cb-dir-detail-filter-item-inside">
								<div class="cb-dir-detail-filter-image-item"> ' . $output .
								'</div>
								</div>
								</div>';
								

*/
		
		

		
		endwhile;
	
		usort($sort_array,array($this, 'compareOrder'));
		

		
		
	

	
	/*
	ob_start();

	
	
$test = get_next_posts_link('NEXT',$query->max_num_pages );
if (strlen($test) > 0){
			 echo $test;


			$out1 = ob_get_contents();
			}else $out1 = '';
			
	ob_end_clean();
	ob_start();
$prev = get_previous_posts_link('PREVIOUS');
if (strlen($prev) > 0){
	echo $prev;
	$out2 = ob_get_contents();
	} else $out2 = '';
	ob_end_clean();
	
	//var_dump($out1, $out2);
	
	
	
	
	
	
			
			$string .=  "<br / > <span class='cb_dd_prev_next cb_dd_next'> " . $out1 . "</span>";
			$string .= "<span class='cb_dd_prev_next cb_dd_prev'> " . $out2 . "</span>";*/
			
		wp_reset_postdata();
		
		foreach($sort_array as $s )
		{
		$id = $s['id'];
		
		$post_meta= get_post_meta($id,'cb-directory_detail_settings');

	    
			
			 $image = $post_meta[0]['cb-directory_detail-pic-url'];
			 $hover_image = $post_meta[0]['cb-directory_detail-hover-pic-url'];
			 $link = $post_meta[0]['cb-directory_detail-link-text'];
			 
			$output = '<a href="' . $link . '"><img  src = ' . $image . ' /> </a>';
			$output_hover = '<a href="' . $link . '"><img  src = ' . $hover_image . ' /> </a>';
			
			
		   $string .= '<div class="cb-dir-detail-filter-item">
							<div id="' . $id . '" class="cb-dir-detail-filter-item-inside">
								<div class="cb-dir-detail-filter-image-item"> ' . $output .
								'</div>
								<div class="cb-dir-detail-filter-hover-image-item"> ' . $output_hover .
								'</div>
								</div>
								</div>';
		
		
		
		}
		
		
		
		
		
		
		
		
		
		
		
					
		}	
		
		
		
	$string .= "</div>";

		return $string;
	
	
	
	}
	/******************************************************************
	*
	*	cb_show_Directory_Detail_Display_func
	*
	*	returns the string that shows the slider for display and sets up the localized jQuery script
	*
	******************************************************************/
	function cb_show_Directory_Detail_Display_func($atts, $content="" )

	{
	
		
		$atts = shortcode_atts(
		array(
			'type' => "",
			
		), $atts, 'showCB_directoryDetail' );

	
		
		$type = $atts['type'];
		
		
		
		
		$string = '<div id="directory_detail_content">';
	
	
	    $string .= "</div>";

		
					
		 wp_register_script( 'cb_directory_detail_js', plugins_url( '/CamanaDirDetail/js/cb_directory_detail_js.js' ), array('jquery') );
		wp_enqueue_script('cb_directory_detail_js');
		
		$string1 =  $this->get_layout("", $type);
		
		wp_localize_script( 'cb_directory_detail_js', 'cb_directory_detail_ajax',
				array( 'ajax_url' => admin_url( 'admin-ajax.php' ),'cb_content' => $string1, 'cb_type' =>$type));
	

		
		return $string;
	//	

	}
	
	function cb_show_Directory_Detail_Display_enqueue_styles()
	{

		
			wp_register_style( 'cb_directory_detail_display_css', plugins_url( 'CamanaDirDetail/css/cb_directorydetail.min.css' ) );
			wp_enqueue_style( 'cb_directory_detail_display_css' );
	
	}
	
	
	
	public static function instance() {
		
		 new _Directory_Detail_Display;

			

	}
}

	
	
	function cb_init_Directory_Detail_Display(){


		return _Directory_Detail_Display::instance();

	}

add_action( 'plugins_loaded',  'cb_init_Directory_Detail_Display'  );

?>