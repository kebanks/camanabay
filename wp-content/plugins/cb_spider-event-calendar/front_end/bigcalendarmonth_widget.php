<?php
function big_calendar_month_widget() {
  require_once("frontend_functions.php");
  global $wpdb;
  $widget = ((isset($_GET['widget']) && (int) $_GET['widget']) ? (int) $_GET['widget'] : 0);
  $many_sp_calendar = ((isset($_GET['many_sp_calendar']) && is_numeric(esc_html($_GET['many_sp_calendar']))) ? esc_html($_GET['many_sp_calendar']) : 1);
  $calendar_id = (isset($_GET['calendar']) ? (int) $_GET['calendar'] : '');
  $theme_id = (isset($_GET['theme_id']) ? (int) $_GET['theme_id'] : 1);
  $date = ((isset($_GET['date']) && IsDate_inputed(esc_html($_GET['date']))) ? esc_html($_GET['date']) : '');
  $view_select = (isset($_GET['select']) ? esc_html($_GET['select']) : 'month,');
  $path_sp_cal = (isset($_GET['cur_page_url']) ? esc_html($_GET['cur_page_url']) : '');
  $site_url = get_site_url().'/wp-admin/admin-ajax.php';

   ///////////////////////////////////////////////////////////////////////////////////



if(isset($_GET['cat_id']))
  $cat_id = $_GET['cat_id'];
  else $cat_id = "";

  if(isset($_GET['cat_ids']))
  $cat_ids = $_GET['cat_ids'];
  else $cat_ids = "";

if($cat_ids=='')
$cat_ids .= $cat_id.',';
else
$cat_ids .= ','.$cat_id.',';



$cat_ids = substr($cat_ids, 0,-1);

function getelementcountinarray($array , $element)
{
  $t=0;

  for($i=0; $i<count($array); $i++)
  {
    if($element==$array[$i])
	$t++;

  }


  return $t;

}

function getelementindexinarray($array , $element)
{

		$t='';

	for($i=0; $i<count($array); $i++)
		{
			if($element==$array[$i])
			$t.=$i.',';

	    }

	return $t;


}
$cat_ids_array = explode(',',$cat_ids);



if($cat_id!='')
{

if(getelementcountinarray($cat_ids_array,$cat_id )%2==0)
{
$index_in_line = getelementindexinarray($cat_ids_array, $cat_id);
$index_array = explode(',' , $index_in_line);
array_pop ($index_array);
for($j=0; $j<count($index_array); $j++)
unset($cat_ids_array[$index_array[$j]]);
$cat_ids = implode(',',$cat_ids_array);
}
}
else
$cat_ids = substr($cat_ids, 0,-1);


///////////////////////////////////////////////////////////////////////////////////////////////////////

  $theme = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'spidercalendar_widget_theme WHERE id=%d', $theme_id));
  $weekstart = $theme->week_start_day;
  $weekstart = 'su';
  $bg = '#' . str_replace('#','',$theme->header_bgcolor);
//  $bg_color_selected = '#' . str_replace('#','',$theme->bg_color_selected);
  $bg_color_selected = '#ffffff';
  $color_arrow = '#' . str_replace('#','',$theme->arrow_color);
  $evented_color = '#' . str_replace('#','',$theme->text_color_this_month_evented);
  $evented_color_bg = '#' . str_replace('#','',$theme->bg_color_this_month_evented);
  $evented_color_bg = '#ffffff';
  $sun_days = '#' . str_replace('#','',$theme->text_color_sun_days);
  $text_color_other_months = '#' . str_replace('#','',$theme->text_color_other_months);
  $text_color_this_month_unevented = '#' . str_replace('#','',$theme->text_color_this_month_unevented);
  $text_color_month = '#' . str_replace('#','',$theme->text_color_month);
  $color_week_days = '#' . str_replace('#','',$theme->text_color_week_days);
  $text_color_selected = '#' . str_replace('#','',$theme->text_color_selected);
  $border_day = '#' . str_replace('#','',$theme->border_day);
  $calendar_width = $theme->width;
  $calendar_width = "100%";
  $calendar_bg = '#' . str_replace('#','',$theme->footer_bgcolor);
  $calendar_bg = '#ffffff';
  $weekdays_bg_color = '#' . str_replace('#','',$theme->weekdays_bg_color);
  $weekdays_bg_color = '#ffffff';

  $weekday_su_bg_color = '#' . str_replace('#','',$theme->su_bg_color);
  $cell_border_color = '#' . str_replace('#','',$theme->cell_border_color);
  $year_font_size = $theme->year_font_size;
  $year_font_size = "1.3em";
  $year_font_color = '#' . str_replace('#','',$theme->year_font_color);
  $year_tabs_bg_color = '#' . str_replace('#','',$theme->year_tabs_bg_color);
   $year_tabs_bg_color ='#ffffff';
  $font_year = $theme->font_year;
  $font_month = $theme->font_month;
  $font_day = $theme->font_day;
  $font_weekday = $theme->font_weekday;
  $popup_width = $theme->popup_width;
  $popup_height = $theme->popup_height;
  $popup_width=800;
  $popup_height=800;

  __('January', 'sp_calendar');
  __('February', 'sp_calendar');
  __('March', 'sp_calendar');
  __('April', 'sp_calendar');
  __('May', 'sp_calendar');
  __('June', 'sp_calendar');
  __('July', 'sp_calendar');
  __('August', 'sp_calendar');
  __('September', 'sp_calendar');
  __('October', 'sp_calendar');
  __('November', 'sp_calendar');
  __('December', 'sp_calendar');
  if ($date != '') {
    $date_REFERER = $date;
  }
  else {
    $date_REFERER = date("Y-m");
    $date = date("Y") . '-' . php_Month_num(date("F")) . '-' . date("d");
  }

  $year_REFERER = substr($date_REFERER, 0, 4);
  $month_REFERER = Month_name(substr($date_REFERER, 5, 2));
  $day_REFERER = substr($date_REFERER, 8, 2);

  $year = substr($date, 0, 4);
  $month = Month_name(substr($date, 5, 2));
  $day = substr($date, 8, 2);

  $this_month = substr($year . '-' . add_0((Month_num($month))), 5, 2);
  $prev_month = add_0((int) $this_month - 1);
  $next_month = add_0((int) $this_month + 1);

  $cell_width = $calendar_width / 7;
  $cell_width = (int) $cell_width - 2;

  $view = 'bigcalendarmonth_widget';
  $views = explode(',', $view_select);
  $defaultview = 'month';
  array_pop($views);
  $display = '';
  if (count($views) == 0) {
    $display = "display:none";
  }
  if(count($views) == 1 && $views[0] == $defaultview) {
    $display = "display:none";
  }
  ?>
  <style type='text/css'>
    #calendar_<?php echo $many_sp_calendar; ?> table {
      border-collapse: initial;
      border:0px;
    }
    #calendar_<?php echo $many_sp_calendar; ?> table td {
      padding: 0px;
      vertical-align: none;
      border-top:none;
      line-height: none;
      text-align: none;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .cell_body td {
      border:none;
      font-family: <?php echo $font_day; ?>;
    }
    #calendar_<?php echo $many_sp_calendar; ?> p, ol, ul, dl, address {
      margin-bottom: 0;
    }
    #calendar_<?php echo $many_sp_calendar; ?> td,
    #calendar_<?php echo $many_sp_calendar; ?> tr,
    #spiderCalendarTitlesList_<?php echo $many_sp_calendar; ?> td,
    #spiderCalendarTitlesList_<?php echo $many_sp_calendar; ?> tr {
       border:none;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .cala_arrow a:link,
    #calendar_<?php echo $many_sp_calendar; ?> .cala_arrow a:visited {
      color: <?php echo $color_arrow; ?>;
      text-decoration: none;
      background: none;
      font-size: 16px;
	  font-weight: 300;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .cala_arrow a:hover {
      color: <?php echo $color_arrow; ?>;
      text-decoration:none;
      background:none;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .cala_day a:link,
    #calendar_<?php echo $many_sp_calendar; ?> .cala_day a:visited {
      text-decoration:underline;
      background:none;
      font-size:16px;
	  font-weight: 300;
    }
    #calendar_<?php echo $many_sp_calendar; ?> a {
      font-weight: normal;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .cala_day a:hover {
      font-size:16px;
	  font-weight: 300;
      text-decoration:none;
      background:none;
    }


    #calendar_<?php echo $many_sp_calendar; ?> .calyear_table {
      border-spacing:0;
      width:100%;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .calmonth_table {
      border-spacing: 0;
      vertical-align: middle;
      width: 100%;
	  font-size:20px !important;
	  font-weight: 300;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .calbg {
      background-color:<?php echo $bg; ?> !important;
      background-color:#FFB718 !important;
      text-align:center;
      vertical-align: middle;

    }
	#cb_cal_heading{

	}
    #calendar_<?php echo $many_sp_calendar; ?> .caltext_color_other_months {
      color:<?php echo $text_color_other_months; ?>;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .caltext_color_this_month_unevented {
      color:<?php echo $text_color_this_month_unevented; ?>;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .calsun_days {
      color:<?php echo $sun_days; ?>;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .calborder_day {
      border: solid <?php echo $border_day; ?> 1px;
    }
    #TB_window {
      z-index: 10000;
    }
    #calendar_<?php echo $many_sp_calendar; ?> .views {
      float: right;
      background-color: <?php echo $calendar_bg; ?> !important;
      height: 25px;
      width: <?php echo ($calendar_width / 4) - 2; ?>px;
      margin-left: 2px;
      text-align: center;
      cursor: pointer;
      position: relative;
      top: 3px;
      font-family: <?php echo $font_month; ?>;
    }
    #calendar_<?php echo $many_sp_calendar; ?> table tr {
      background: transparent !important;
    }
	#calendar_<?php echo $many_sp_calendar; ?> .views_select ,
	#calendar_<?php echo $many_sp_calendar; ?> #views_select
	{
		width: 120px;
		text-align: center;
		cursor: pointer;
		padding: 6px;
		position: relative;
	}


	#drop_down_views
	{
		list-style-type:none !important;
		position: absolute;
		top: 46px;
		left: -15px;
		display:none;
		z-index: 4545;

	}

	#drop_down_views >li
	{
		border-bottom:1px solid #fff !important;
	}


	#views_tabs_select
	{
		display:none;
	}
    .week_days_header{

			font-family: 'nexabold', sans-serif;
			font-size:24px;
			font-weight: 700;
			text-align:center;
			text-transform:uppercase;
			height:80px !important;

	}
  </style>

  <div id="calendar_<?php echo $many_sp_calendar; ?>" style="width:<?php echo $calendar_width; ?>px;">
    <table  cellpadding="0" cellspacing="0" style="border-spacing:0; width:<?php echo $calendar_width; ?>px; height:500px; margin:0; padding:0;background-color:<?php echo $calendar_bg; ?> !important">
      <tr style="background-color:#FFFFFF;">
        <td style="background-color:#FFFFFF;">
          <div id="views_tabs" style="<?php echo $display; ?>">
            <div class="views" style="<?php if (!in_array('day', $views) AND $defaultview != 'day') echo 'display:none;'; if ($view == 'bigcalendarday_widget') echo 'background-color:' . $bg . ' !important;height:28px;top:0;'; ?>"
              onclick="showbigcalendar('bigcalendar<?php echo $many_sp_calendar; ?>', '<?php echo add_query_arg(array(
                'action' => 'spiderbigcalendar_day_widget',
                'theme_id' => $theme_id,
                'calendar' => $calendar_id,
                'select' => $view_select,
                'date' => $year . '-' . add_0((Month_num($month))) . '-' . date('d'),
                'many_sp_calendar' => $many_sp_calendar,
                'cur_page_url' => $path_sp_cal,
				'cat_id' => '',
				'cat_ids' => $cat_ids,
                'widget' => $widget,
				'rand' => $many_sp_calendar,
                ), $site_url);?>','<?php echo $many_sp_calendar; ?>','<?php echo $widget; ?>')" ><span style="line-height: 2;color:<?php echo $text_color_month; ?>;"><?php echo __('Day', 'sp_calendar'); ?></span>
            </div>
            <div class="views" style="<?php if (!in_array('week', $views) AND $defaultview != 'week') echo 'display:none;'; if ($view == 'bigcalendarweek_widget') echo 'background-color:' . $bg . ' !important;height:28px;top:0;' ?>"
              onclick="showbigcalendar('bigcalendar<?php echo $many_sp_calendar; ?>', '<?php echo add_query_arg(array(
                'action' => 'spiderbigcalendar_week_widget',
                'theme_id' => $theme_id,
                'calendar' => $calendar_id,
                'select' => $view_select,
                'months' => $prev_month . ',' . $this_month . ',' . $next_month,
                'date' => $year . '-' . add_0((Month_num($month))) . '-' . date('d'),
                'many_sp_calendar' => $many_sp_calendar,
                'cur_page_url' => $path_sp_cal,
				'cat_id' => '',
				'cat_ids' => $cat_ids,
                'widget' => $widget,
                ), $site_url);?>','<?php echo $many_sp_calendar; ?>','<?php echo $widget; ?>')" ><span style="line-height: 2;color:<?php echo $text_color_month; ?>;"><?php echo __('Week', 'sp_calendar'); ?></span>
            </div>
            <div class="views" style="<?php if (!in_array('list', $views) AND $defaultview != 'list') echo 'display:none;'; if ($view == 'bigcalendarlist_widget') echo 'background-color:' . $bg . ' !important;height:28px;top:0;'; ?>"
              onclick="showbigcalendar('bigcalendar<?php echo $many_sp_calendar; ?>', '<?php echo add_query_arg(array(
                'action' => 'spiderbigcalendar_list_widget',
                'theme_id' => $theme_id,
                'calendar' => $calendar_id,
                'select' => $view_select,
                'date' => $year . '-' . add_0((Month_num($month))),
                'many_sp_calendar' => $many_sp_calendar,
                'cur_page_url' => $path_sp_cal,
				'cat_id' => '',
				'cat_ids' => $cat_ids,
                'widget' => $widget,
                ), $site_url);?>','<?php echo $many_sp_calendar; ?>','<?php echo $widget; ?>')"><span style="line-height: 2;color:<?php echo $text_color_month; ?>;"><?php echo __('List', 'sp_calendar'); ?></span>
            </div>
            <div class="views" style="margin-left: 0px;margin-right: 2px;<?php if (!in_array('month', $views) AND $defaultview != 'month') echo 'display:none;'; if ($view == 'bigcalendarmonth_widget') echo 'background-color:' . $bg . ' !important;height:28px;top:0;'; ?>"
              onclick="showbigcalendar('bigcalendar<?php echo $many_sp_calendar; ?>', '<?php echo add_query_arg(array(
                'action' => 'spiderbigcalendar_month_widget',
                'theme_id' => $theme_id,
                'calendar' => $calendar_id,
                'select' => $view_select,
                'date' => $year . '-' . add_0((Month_num($month))),
                'many_sp_calendar' => $many_sp_calendar,
                'cur_page_url' => $path_sp_cal,
				'cat_id' => '',
				'cat_ids' => $cat_ids,
                'widget' => $widget,
                ), $site_url);?>','<?php echo $many_sp_calendar; ?>','<?php echo $widget; ?>')" ><span style="position:relative;top:15%;color:<?php echo $text_color_month; ?>;"><?php echo __('Month', 'sp_calendar'); ?></span>
            </div>
          </div>
        </td>
      </tr>
      <tr>
        <td width="100%" style="padding:0; margin:0;">
          <form action="" method="get" style="background:none; margin:0; padding:0;">
            <table id="cb_cal_heading" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0; font-size:16px; margin:0; padding:0;" width="<?php echo $calendar_width; ?>" height="190">
              <tr  height="28px" style="width:<?php echo $calendar_width ?>">
               <!-- <td class="calbg" colspan="7" style="background-image:url('<?php echo plugins_url('/images/Stver.png', __FILE__); ?>');margin:0; padding:0;background-repeat: no-repeat;background-size: 100% 100%;" >
                 -->
<td class="calbg" colspan="7" style="margin:0; padding:0;background-repeat: no-repeat;background-size: 100% 100%;" >


				 <?php //MONTH TABLE ?>
                  <table id="table_test" cellpadding="0" cellspacing="0" border="0" align="center" class="calmonth_table"  style="width:100%; margin:0; padding:0">
                    <tr>
                      <td class="cala_arrow">
                        <a class="cb_cal_arrow" href="javascript:showbigcalendar('bigcalendar<?php echo $many_sp_calendar ?>','<?php
                          if (Month_num($month) == 1) {
                            $needed_date = ($year - 1) . '-12';
                          }
                          else {
                            $needed_date = $year . '-' . add_0((Month_num($month) - 1));
                          }
                          echo add_query_arg(array(
                            'action' => 'spiderbigcalendar_' . $defaultview . '_widget',
                            'theme_id' => $theme_id,
                            'calendar' => $calendar_id,
                            'select' => $view_select,
                            'date' => $needed_date,
                            'many_sp_calendar' => $many_sp_calendar,
                            'cur_page_url' => $path_sp_cal,
							'cat_id' => '',
							'cat_ids' => $cat_ids,
                            'widget' => $widget,
                            ), $site_url);
                            ?>','<?php echo $many_sp_calendar; ?>','<?php echo $widget; ?>')">&#9668; PREV
                        </a>
                      </td>
                      <td width="60%" class="cala_title"   style="text-align:center; margin:0; padding:0; font-family:<?php echo $font_month; ?>">
                        <input type="hidden" name="month" readonly="" value="<?php echo $month; ?>"/>
                        <span class="cb_cal_header_text" style="color:<?php echo $text_color_month; ?>;"><?php echo __($month . " " . $year, 'sp_calendar'); ?></span>
                      </td>
                      <td class="cala_arrow">
                        <a class="cb_cal_arrow" href="javascript:showbigcalendar('bigcalendar<?php echo $many_sp_calendar ?>','<?php
                          if (Month_num($month) == 12) {

                            $needed_date = ($year + 1) . '-01';
                          }
                          else {
                            $needed_date = $year . '-' . add_0((Month_num($month) + 1));
                          }
                          echo add_query_arg(array(
                            'action' => 'spiderbigcalendar_' . $defaultview . '_widget',
                            'theme_id' => $theme_id,
                            'calendar' => $calendar_id,
                            'select' => $view_select,
                            'date' => $needed_date,
                            'many_sp_calendar' => $many_sp_calendar,
                            'cur_page_url' => $path_sp_cal,
							'cat_id' => '',
							'cat_ids' => $cat_ids,
                            'widget' => $widget,
                            ), $site_url);
                            ?>','<?php echo $many_sp_calendar; ?>','<?php echo $widget; ?>')">NEXT &#9658;
                        </a>

                      </td>
                    </tr>

                  </table>

                </td>

              </tr>
          <!--    <tr class="cell_body" align="center"  height="10%" style="background-color:<?php echo $weekdays_bg_color; ?> !important;width:<?php echo $calendar_width; ?>px">
                <?php if ($weekstart == "su") { ?>
                <td style="font-family:<?php echo $font_weekday; ?>;background-color:<?php echo $weekday_su_bg_color; ?> !important;width:<?php echo $cell_width; ?>px; color:<?php echo $color_week_days; ?>; margin:0; padding:0">
                  <div class="calbottom_border" style="text-align:center; width:<?php echo $cell_width; ?>px; margin:0; padding:0;"><b> <?php echo __('Su', 'sp_calendar'); ?> </b></div>
                </td>
                <?php } ?>
                <td style="font-family:<?php echo $font_weekday; ?>;width:<?php echo $cell_width; ?>px; color:<?php echo $color_week_days; ?>; margin:0; padding:0">
                  <div class="calbottom_border" style="text-align:center; width:<?php echo $cell_width; ?>px; margin:0; padding:0;"><b> <?php echo __('Mo', 'sp_calendar'); ?> </b></div>
                </td>
                <td style="font-family:<?php echo $font_weekday; ?>;width:<?php echo $cell_width; ?>px; color:<?php echo $color_week_days; ?>; margin:0; padding:0">
                  <div class="calbottom_border" style="text-align:center; width:<?php echo $cell_width; ?>px; margin:0; padding:0;"><b> <?php echo __('Tu', 'sp_calendar'); ?> </b></div>
                </td>
                <td style="font-family:<?php echo $font_weekday; ?>;width:<?php echo $cell_width; ?>px; color:<?php echo $color_week_days; ?>; margin:0; padding:0">
                  <div class="calbottom_border" style="text-align:center; width:<?php echo $cell_width; ?>px; margin:0; padding:0;"><b> <?php echo __('We', 'sp_calendar'); ?> </b></div>
                </td>
                <td style="font-family:<?php echo $font_weekday; ?>;width:<?php echo $cell_width; ?>px; color:<?php echo $color_week_days; ?>; margin:0; padding:0">
                  <div class="calbottom_border" style="text-align:center; width:<?php echo $cell_width; ?>px; margin:0; padding:0;"><b> <?php echo __('Th', 'sp_calendar'); ?> </b></div>
                </td>
                <td style="font-family:<?php echo $font_weekday; ?>;width:<?php echo $cell_width; ?>px; color:<?php echo $color_week_days; ?>; margin:0; padding:0">
                  <div class="calbottom_border" style="text-align:center; width:<?php echo $cell_width; ?>px; margin:0; padding:0;"><b> <?php echo __('Fr', 'sp_calendar'); ?> </b></div>
                </td>
                <td style="font-family:<?php echo $font_weekday; ?>;width:<?php echo $cell_width; ?>px; color:<?php echo $color_week_days; ?>; margin:0; padding:0">
                  <div class="calbottom_border" style="text-align:center; width:<?php echo $cell_width; ?>px; margin:0; padding:0;"><b> <?php echo __('Sa', 'sp_calendar'); ?> </b></div>
                </td>
                <?php if ($weekstart == "mo") { ?>
                <td style="font-family:<?php echo $font_weekday; ?>;background-color:<?php echo $weekday_su_bg_color; ?> !important;width:<?php echo $cell_width; ?>px; color:<?php echo $color_week_days; ?>; margin:0; padding:0">
                  <div class="calbottom_border" style="text-align:center; width:<?php echo $cell_width; ?>px; margin:0; padding:0;"><b> <?php echo __('Su', 'sp_calendar'); ?> </b></div>
                </td>
                <?php } ?>
              </tr> -->
  <?php

  $month_first_weekday = date("N", mktime(0, 0, 0, Month_num($month), 1, $year));

  if ($weekstart == "su") {
    $month_first_weekday++;
    if ($month_first_weekday == 8) {
      $month_first_weekday = 1;
    }
  }
  $month_days = date("t", mktime(0, 0, 0, Month_num($month), 1, $year));
  $last_month_days = date("t", mktime(0, 0, 0, Month_num($month) - 1, 1, $year));
  $weekday_i = $month_first_weekday;
  $last_month_days = $last_month_days - $weekday_i + 2;
  $percent = 1;
  $sum = $month_days - 8 + $month_first_weekday;
  if ($sum % 7 <> 0) {
    $percent = $percent + 1;
  }
  $sum = $sum - ($sum % 7);
  $percent = $percent + ($sum / 7);
  $percent = 107 / $percent;
  $percent=100;


  $all_calendar_files = php_getdays(1, $calendar_id, $date, $theme_id, $widget);
  $categories=$wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "spidercalendar_event_category WHERE published=1");
  $calendar = (isset($_GET['calendar']) ? $_GET['calendar'] : '');
  $array_days = $all_calendar_files[0]['array_days'];

  $array_days1 = $all_calendar_files[0]['array_days1'];
  $title = $all_calendar_files[0]['title'];
  $ev_ids = $all_calendar_files[0]['ev_ids'];
  echo '<tr class="cell_body">
  <td class="week_days_header">SUN</td>
   <td class="week_days_header">MON</td>
     <td class="week_days_header">TUE</td>
     <td class="week_days_header">WED</td>
     <td class="week_days_header">THU</td>
     <td class="week_days_header">FRI</td>
     <td class="week_days_header">SAT</td>



  </tr>' ;


  echo '      <tr class="cell_body">';


  for ($i = 1; $i < $weekday_i; $i++) {
  //  echo '          <td class="caltext_color_other_months" style="text-align:center;">' . $last_month_days . '</td>';
    echo '          <td class="caltext_color_other_months" style="text-align:center;"></td>';
    $last_month_days = $last_month_days + 1;
  }



  for ($i = 1; $i <= $month_days; $i++) {
    if (isset($title[$i])) {
      $ev_title = explode('</p>', $title[$i]);
      array_pop($ev_title);
      $k = count($ev_title);
      $ev_id = explode('<br>', $ev_ids[$i]);
      array_pop($ev_id);
      $ev_ids_inline = implode(',', $ev_id);
    }
	else
	$k=0;

	if(isset($ev_ids_inline)){
	 if(preg_match("/^[0-9\,]+$/", $ev_ids_inline))
		$query = $wpdb->prepare ("SELECT DISTINCT sec.color FROM " . $wpdb->prefix . "spidercalendar_event AS se JOIN
		" . $wpdb->prefix . "spidercalendar_event_category AS sec ON se.category=sec.id  WHERE  se.published='1' AND sec.published='1' AND se.calendar=%d AND se.id IN (".$ev_ids_inline.") ",$calendar_id);
		$categ_color=$wpdb->get_results($query);
}

    if (($weekday_i % 7 == 0 and $weekstart == "mo") or ($weekday_i % 7 == 1 and $weekstart == "su")) {
      if ($i == $day_REFERER and $month == $month_REFERER and $year == $year_REFERER) {

        echo  ' <td id="1" class="cala_day" style="background-color:' . $bg_color_selected . ' !important;text-align:center;padding:0; margin:0;line-height:inherit;">
                  <div class="calborder_day" style="text-align:center; width:' . $cell_width . '; margin:0; padding:0;">
                    <a class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $text_color_selected . '; text-decoration:underline;"
                      href="' . add_query_arg(array(
                        'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),
                        'theme_id' => $theme_id,
                        'calendar_id' => $calendar_id,
                        'ev_ids' => $ev_ids_inline,
                        'eventID' => $ev_id[0],
                        'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                        'many_sp_calendar' => $many_sp_calendar,
                        'cur_page_url' => $path_sp_cal,
                        'widget' => $widget,
                        'TB_iframe' => 1,
                        'tbWidth' => $popup_width,
                        'tbHeight' => $popup_height,
                        ), $site_url) . '"><b>' . $i . '</b>
                    </a>
                  </div>
				  </td>';
      }
      elseif ($i == date('j') and $month == date('F') and $year == date('Y')) {
        if (in_array($i, $array_days)) {
          if (in_array ($i, $array_days1)) {
	
            echo '
                <td id="2" class="cala_day" style="color:' . $text_color_selected . ';background-color:' . $bg_color_selected . ' !important;text-align:center;padding:0; margin:0;line-height:inherit; background: #FFB718;">
                  <a class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $text_color_selected . ';text-align:center;text-decoration:underline;"
                    href="' . add_query_arg(array(
                      'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),
                      'theme_id' => $theme_id,
                      'calendar_id' => $calendar_id,
                      'ev_ids' => $ev_ids_inline,
                      'eventID' => $ev_id[0],
                      'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                      'many_sp_calendar' => $many_sp_calendar,
                      'cur_page_url' => $path_sp_cal,
                      'widget' => $widget,
                      'TB_iframe' => 1,
                      'tbWidth' => $popup_width,
                      'tbHeight' => $popup_height,
                      ), $site_url) . '"><b>' . $i . '</b>
                  </a>';
				  echo '<table style="width:100%; border:0;margin: 0"><tr>';
				  foreach($categ_color as $color){
				  echo '<td id="cat_width"  style="border:0; border-top:2px solid #'.str_replace('#','',$color->color).'; display:table-cell;"></td>';
					}
					echo '</tr></table>';
					echo '</td>';
          }
          else {

            echo '
                <td id="3" class="cala_day" style="color:' . $text_color_selected . ';background-color:' . $bg_color_selected . ' !important;text-align:center;padding:0; margin:0;line-height:inherit; background: #FFB718;">
                  <a class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $text_color_selected . ';text-align:center;text-decoration:underline;"
                    href="' . add_query_arg(array(
                      'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),
                      'theme_id' => $theme_id,
                      'calendar_id' => $calendar_id,
                      'ev_ids' => $ev_ids_inline,
                      'eventID' => $ev_id[0],
                      'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                      'many_sp_calendar' => $many_sp_calendar,
                      'cur_page_url' => $path_sp_cal,
                      'widget' => $widget,
                      'TB_iframe' => 1,
                      'tbWidth' => $popup_width,
                      'tbHeight' => $popup_height,
                      ), $site_url) . '"><b>' . $i . '</b>
                  </a>';
				 echo '<table style="width:100%; border:0;margin:0;"><tr>';
				  foreach($categ_color as $color){
				  echo '<td id="cat_width"  style="border:0; border-top:2px solid #'.str_replace('#','',$color->color).'; display:table-cell;"></td>';
					}
					echo '</tr></table>';
					echo '</td>';
          }
        }
        else {
          echo '
                <td id="4" class="calsun_days test" style="color:' . $text_color_selected . ';text-align:center;padding:0; margin:0;line-height:inherit; background: #FFB718;">
                  <b>' . $i . '</b>
                </td>';
        }
      }
      else {
        if (in_array ($i, $array_days)) {
          if (in_array ($i, $array_days1)) {

            echo '
                <td id="5" class="cala_day" style="text-align:center;padding:0; margin:0;line-height:inherit;">
                  <a class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $evented_color . ';text-align:center;text-decoration:underline;"
                    href="' . add_query_arg(array(
                      'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),
                      'theme_id' => $theme_id,
                      'calendar_id' => $calendar_id,
                      'ev_ids' => $ev_ids_inline,
                      'eventID' => $ev_id[0],
                      'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                      'many_sp_calendar' => $many_sp_calendar,
                      'cur_page_url' => $path_sp_cal,
                      'widget' => $widget,
                      'TB_iframe' => 1,
                      'tbWidth' => $popup_width,
                      'tbHeight' => $popup_height,
					  'cat_id' => $cat_ids
                      ), $site_url) . '"><b>' . $i . '</b>
                  </a>';
				  echo '<table style="width:100%; border:0;margin:0;"><tr>';
				  foreach($categ_color as $color){
				  echo '<td id="cat_width"  style="border:0; border-top:2px solid #'.str_replace('#','',$color->color).'; display:table-cell;"></td>';
					}
					echo '</tr></table>';
					echo '</td>';
          }
          else {

            echo '
                <td id="6" class="cala_day" style="background-color:' . $evented_color_bg . ' !important;text-align:center;padding:0; margin:0;line-height:inherit;">
                  <a class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $evented_color . ';text-align:center;text-decoration:underline;"
                    href="' . add_query_arg(array(
                      'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),
                      'theme_id' => $theme_id,
                      'calendar_id' => $calendar_id,
                      'ev_ids' => $ev_ids_inline,
                      'eventID' => $ev_id[0],
                      'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                      'many_sp_calendar' => $many_sp_calendar,
                      'cur_page_url' => $path_sp_cal,
                      'widget' => $widget,
                      'TB_iframe' => 1,
                      'tbWidth' => $popup_width,
                      'tbHeight' => $popup_height,
					  'cat_id' => $cat_ids
                      ), $site_url) . '"><b>' . $i . '</b>
                  </a>';
				  echo '<table style="width:100%; border:0;margin:0;"><tr>';
				  foreach($categ_color as $color){
				  echo '<td id="cat_width"  style="border:0; border-top:2px solid #'.str_replace('#','',$color->color).'; display:table-cell;"></td>';
					}
					echo '</tr></table>';
					echo '</td>';
          }
        }
        else {
          echo  '
                <td class="calsun_days test" style="text-align:center;padding:0; margin:0;line-height:inherit;">
                  <b>' . $i . '</b>
                </td>';
        }
      }
    }
    elseif ($i == $day_REFERER and $month == $month_REFERER and $year == $year_REFERER) {

if (in_array ($i,$array_days)) {

      echo '    <td style="background-color:' . $bg_color_selected . ' !important;text-align:center;padding:0; margin:0;line-height:inherit;">
                  <div class="calborder_day" style="text-align:center; width:' . $cell_width . 'px; margin:0; padding:0;">
                    <a class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $text_color_selected . '; text-decoration:underline;"
                      href="' . add_query_arg(array(
                      'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),
                      'theme_id' => $theme_id,
                      'calendar_id' => $calendar_id,
                      'ev_ids' => $ev_ids_inline,
                      'eventID' => $ev_id[0],
                      'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                      'many_sp_calendar' => $many_sp_calendar,
                      'cur_page_url' => $path_sp_cal,
                      'widget' => $widget,
                      'TB_iframe' => 1,
                      'tbWidth' => $popup_width,
                      'tbHeight' => $popup_height,
					  'cat_id' => $cat_ids
                      ), $site_url) . '"><b>' . $i . '</b>
                  </a>';
				 echo '<table style="width:100%; border:0;margin:0;"><tr>';
				  foreach($categ_color as $color){
				  echo '<td id="cat_width"  style="border:0; border-top:2px solid #'.str_replace('#','',$color->color).'; display:table-cell;"></td>';
					}
					echo '</tr></table>';
					echo '</td>';
					}

					else {

          echo '<td  style="text-align:center; color:' . $text_color_selected . ';padding:0; margin:0; line-height:inherit; background: #FFB718;">
                  <b>' . $i . '</b>
                </td>';
        }

    }
    else {
			if ($i == date('j') and $month == date('F') and $year == date('Y')) {


				if (in_array ($i, $array_days)) {


          if (in_array ($i, $array_days1)) {

            echo '
                <td id="7" class="cala_day day_current" style="color:' . $text_color_selected . ' !important;text-align:center;padding:0; margin:0;line-height:inherit;">
                  <a class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $text_color_selected . '; text-align:center;text-decoration:underline;"
                    href="' . add_query_arg(array(
                   /*   'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),*/
                        'action' => 'spiderseemore' ,
                      'theme_id' => $theme_id,
                      'calendar_id' => $calendar_id,
                      'ev_ids' => $ev_ids_inline,
                      'eventID' => $ev_id[0],
                      'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                      'many_sp_calendar' => $many_sp_calendar,
                      'cur_page_url' => $path_sp_cal,
                      'widget' => $widget,
                      'TB_iframe' => 1,
                      'tbWidth' => $popup_width,
                      'tbHeight' => $popup_height,
					  'cat_id' => $cat_ids
                      ), $site_url) . '"><b>' . $i . '</b>
                  </a>';
				  echo '<table style="width:100%; border:0;margin:0;"><tr>';
				  foreach($categ_color as $color){
				  echo '<td id="cat_width"  style="border:0; border-top:2px solid #'.str_replace('#','',$color->color).'; display:table-cell;"></td>';
					}
					echo '</tr></table>';
					echo '</td>';
          }
          else {

            echo '
                <td id="9"  class="cala_day" style="color:' . $text_color_selected . ' !important;background-color:' . $bg_color_selected . ' !important;text-align:center;padding:0; margin:0;line-height:inherit; background: #FFB718;">
                  <a id="cur_day" class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $text_color_selected . '; text-align:center;text-decoration:underline;"
                    href="' . add_query_arg(array(
                      'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),
                      'theme_id' => $theme_id,
                      'calendar_id' => $calendar_id,
                      'ev_ids' => $ev_ids_inline,
                      'eventID' => $ev_id[0],
                      'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                      'many_sp_calendar' => $many_sp_calendar,
                      'cur_page_url' => $path_sp_cal,
                      'widget' => $widget,
                      'TB_iframe' => 1,
                      'tbWidth' => $popup_width,
                      'tbHeight' => $popup_height,
					  'cat_id' => $cat_ids
                      ), $site_url) . '"><b>' . $i . '</b></a>';
				  echo '<table style="width:100%; border:0;margin:0;"><tr>';
				  foreach($categ_color as $color){
				  echo '<td id="cat_width"  style="border:0; border-top:2px solid #'.str_replace('#','',$color->color).'; display:table-cell;"></td>';
					}
					echo '</tr></table>';
					echo '</td>';
          }
        }
        else {

          echo '<td class="test" style="text-align:center; color:' . $text_color_selected . ';padding:0; margin:0; line-height:inherit; background: #FFB718;">
                  <b>' . $i . '</b>
                </td>';
        }
      }
      elseif (in_array($i, $array_days)) {

        if (in_array ($i, $array_days1)) {
		
  //error_log('#3');

          echo '<td id="10" class="cala_day" style="text-align:center;padding:0; margin:0;line-height:inherit;">
                  <a class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $evented_color . '; text-align:center;text-decoration:underline;"
                  href="' . add_query_arg(array(
                    //  'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),
                      'action' => 'spiderseemore' ,
                      'theme_id' => $theme_id,
                      'calendar_id' => $calendar_id,
                      'ev_ids' => $ev_ids_inline,
                      'eventID' => $ev_id[0],
                      'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                      'many_sp_calendar' => $many_sp_calendar,
                      'cur_page_url' => $path_sp_cal,
                      'widget' => $widget,
                      'TB_iframe' => 1,
                      'tbWidth' => $popup_width,
                      'tbHeight' => $popup_height,
					  'cat_id' => $cat_ids
                      ), $site_url) . '"><b>' . $i . '</b>
                  </a>';
				  echo '<table style="width:100%; border:0;margin:0;"><tr>';
				  foreach($categ_color as $color){
				  echo '<td id="cat_width"  style="border:0; border-top:2px solid #'.str_replace('#','',$color->color).'; display:table-cell;"></td>';
					}
					echo '</tr></table>';
					echo '</td>';
        }
        else {

          echo '<td id="11" class="cala_day" style="background-color:' . $evented_color_bg . ' !important;text-align:center;padding:0; margin:0;line-height:inherit;">
                  <a class="thickbox-previewbigcalendar' . $many_sp_calendar . '" style="background:none;color:' . $evented_color . '; text-align:center;text-decoration:underline;"
                    href="' . add_query_arg(array(
                      'action' => ((isset($ev_id[1])) ? 'spiderseemore' : 'spidercalendarbig'),
                      'theme_id' => $theme_id,
                      'calendar_id' => $calendar_id,
                      'ev_ids' => $ev_ids_inline,
                      'eventID' => $ev_id[0],
                      'date' => $year . '-' . add_0(Month_num($month)) . '-' . $i,
                      'many_sp_calendar' => $many_sp_calendar,
                      'cur_page_url' => $path_sp_cal,
                      'widget' => $widget,
                      'TB_iframe' => 1,
                      'tbWidth' => $popup_width,
                      'tbHeight' => $popup_height,
					  'cat_id' => $cat_ids
                      ), $site_url) . '"><b>' . $i . '</b></a>
                ';
				  echo '<table style="width:100%; border:0;margin:0;"><tr>';
				  foreach($categ_color as $color){
				  echo '<td id="cat_width"  style="border:0; border-top:2px solid #'.str_replace('#','',$color->color).'; display:table-cell;"></td>';
					}
					echo '</tr></table>';
					echo '</td>';
        }
			}
      else {
        echo '  <td class="test" style="text-align:center; color:' . $text_color_this_month_unevented . ';padding:0; margin:0; line-height:inherit;">
                  <b>' . $i . '</b>
                </td>';
      }
    }
    if ($weekday_i % 7 == 0 && $i <> $month_days) {
      echo   '</tr>
              <tr  class="cell_body">';

      $weekday_i = 0;
    }
    $weekday_i++;
  }
  $weekday_i;
  $next_i = 1;
  if ($weekday_i != 1) {
    for ($i = $weekday_i; $i <= 7; $i++) {
    /*  echo '    <td class="caltext_color_other_months" style="text-align:center;">' . $next_i . '</td>';*/
      echo '    <td class="caltext_color_other_months" style="text-align:center;"></td>';
      $next_i++;
    }
  }
  echo '      </tr>';
  ?>
            <!--  <tr style="font-family: <?php echo $font_year; ?>;">
                <td colspan="2" onclick="showbigcalendar('bigcalendar<?php echo $many_sp_calendar ?>','<?php
               /*   echo add_query_arg(array(
                    'action' => 'spiderbigcalendar_' . $defaultview . '_widget',
                    'theme_id' => $theme_id,
                    'calendar' => $calendar_id,
                    'select' => $view_select,
                    'date' => ($year - 1) . '-' . add_0((Month_num($month))),
                    'many_sp_calendar' => $many_sp_calendar,
                    'cur_page_url' => $path_sp_cal,
                    'widget' => $widget,
					'cat_id' => '',
					'cat_ids' => $cat_ids,
					'TB_iframe' => 1,
                    ), $site_url);?>','<?php echo $many_sp_calendar; ?>','<?php echo $widget; ?>')" style="cursor:pointer;font-size:<?php echo $year_font_size; ?>px;color:<?php echo $year_font_color; ?>;text-align: center;background-color:<?php echo $year_tabs_bg_color; ?> !important">
                  <?php echo ($year - 1); ?>
                </td>
                <td colspan="3" style="font-size:<?php echo $year_font_size + 2; ?>px;color:<?php echo $year_font_color; ?>;text-align: center;border-right:1px solid <?php echo $cell_border_color; ?>;border-left:1px solid <?php echo $cell_border_color; ?>">
                  <?php echo $year; ?>
                </td>
                <td colspan="2" onclick="showbigcalendar('bigcalendar<?php echo $many_sp_calendar ?>','<?php
                  echo add_query_arg(array(
                    'action' => 'spiderbigcalendar_' . $defaultview . '_widget',
                    'theme_id' => $theme_id,
                    'calendar' => $calendar_id,
                    'select' => $view_select,
                    'date' => ($year + 1) . '-' . add_0((Month_num($month))),
                    'many_sp_calendar' => $many_sp_calendar,
                    'cur_page_url' => $path_sp_cal,
                    'widget' => $widget,
					'cat_id' => '',
					'cat_ids' => $cat_ids,
					'TB_iframe' => 1,
                    ), $site_url);?>','<?php echo $many_sp_calendar; ?>','<?php echo $widget; ?>')" style="cursor:pointer;font-size:<?php echo $year_font_size; ?>px;text-align: center;background-color:<?php echo $year_tabs_bg_color; ?> !important;color:<?php echo $year_font_color; ?> !important">
                  <?php echo ($year + 1);*/ ?>
                </td>
              </tr> -->
            </table>
            <input type="text" value="1" name="day" style="display:none" />
          </form>
        </td>
      </tr>
    </table>
  </div>
   <style>

   
   #calendar_<?php echo $many_sp_calendar; ?> table{
	width: 100%;
   }

  .categories1 , .categories2
		{
			display:inline-block;
		}

		.categories2
		{
			position:relative;
			left: -9px;
			cursor:pointer;
		}
		.categories2:first-letter
		{
			color:#fff;

		}
  </style>
  <?php

		//reindex cat_ids_array

$re_cat_ids_array = array_values($cat_ids_array);

for($i=0; $i<count($re_cat_ids_array); $i++)
{
echo'
<style>
#cats_widget_'.$many_sp_calendar.' #category'.$re_cat_ids_array[$i].'
{
	text-decoration:underline;
	cursor:pointer;

}

</style>';

}



	if($cat_ids=='')
		$cat_ids='';
	
?>	

<script language="javascript"> 
 function OnChangeSpiderCalendar(dropDown)
 {
	 var selectedValue = dropDown.value;
	 var bg = 'bigcalendar'+<?php echo $many_sp_calendar; ?>;
	 var many_sp_calendar = <?php echo $many_sp_calendar; ?>;
	 var widget = <?php echo $widget; ?>;
	 var view_select = "<?php echo $view_select ?>";
	 var cal_id = <?php echo $calendar_id; ?>;
     var  t_id = <?php echo $theme_id ?>;
	 
	 var x = "<?php echo esc_html($path_sp_cal); ?>";
	
	
	
	
	var site_url = "<?php echo $site_url ?>";
	
	var my_data=site_url+"?action=spiderbigcalendar_month_widget&theme_id="+"<?php echo $theme_id; ?>&calendar="+cal_id+"&select=month&date=<?php echo  $year . '-' . add_0((Month_num($month))) ; ?>&many_sp_calendar="+many_sp_calendar+"&cur_page_url="+x+"&cat_ids&cat_id="+selectedValue+"&widget="+widget;
	
	
	
	 showbigcalendar(bg,my_data,many_sp_calendar,widget);
 
 
 }
 
 </script>

<?php
		
$cats_in_db = $wpdb->get_results( "SELECT category, count(*) as count FROM " .$wpdb->prefix . "spidercalendar_event GROUP By category" ,ARRAY_A );


?>
<select id="CategoryDropDown" name="CategoryDropDown" onChange="OnChangeSpiderCalendar(this)">			
 <option value="ALL" > <?php echo  "All" ?></option>
	<?php
		foreach($categories as $category)
		{	
			$key = array_search($category->id, array_column($cats_in_db, 'category'));
		
			
			if (empty($key))
			   continue;

			if ($category->id != $cat_id)
			{
?>	
				<?php /*	<option value="<?php echo $category->id?>"  style="background-color:#<?php echo str_replace('#','',$category->color);?> !important;  style='color:#FFFFFF'"  id="category<?php echo $category->id?> "  > <?php echo  $category->title ?></option> */ ?>
					<option value="<?php echo $category->id?>"    id="category<?php echo $category->id?> "  > <?php echo  $category->title ?></option>
<?php  		 
			}else
			{?>
					<?php /*<option value="<?php echo $category->id?>" selected="selected" style="background-color:#<?php echo str_replace('#','',$category->color);?> !important;  style='color:#FFFFFF'"  id="category<?php echo $category->id?> "  > <?php echo  $category->title ?></option> */ ?>
					<option value="<?php echo $category->id?>" selected="selected"   id="category<?php echo $category->id?> "  > <?php echo  $category->title ?></option>
			<?php
			}
		}
?>
 </select> <br/><br/>	
		
<?php	


	
/*		
echo '<ul id="cats_widget_'.$many_sp_calendar.'" style="list-style-type:none;">';

foreach($categories as $category)
{
?>

<li style="height:30px"><p class="categories1" style="background-color:#<?php echo str_replace('#','',$category->color);?> !important">&nbsp;&nbsp;&nbsp;&nbsp;</p><p class="categories2" id="category<?php echo $category->id ?>" style="color:#<?php echo str_replace('#','',$category->color); ?> !important" onclick="showbigcalendar('bigcalendar<?php echo $many_sp_calendar; ?>', '<?php echo add_query_arg(array(
                'action' => 'spiderbigcalendar_month_widget',
                'theme_id' => $theme_id,
                'calendar' => $calendar_id,
                'select' => $view_select,
                'date' => $year . '-' . add_0((Month_num($month))),
                'many_sp_calendar' => $many_sp_calendar,
                'cur_page_url' => $path_sp_cal,
				'cat_id' => $category->id,
				'cat_ids' => $cat_ids,
                'widget' => $widget,
                ), $site_url);?>','<?php echo $many_sp_calendar ?>','<?php echo $widget; ?>')"> <?php echo  $category->title ?></p></li>
<?php
}

if (!empty($categories)) {
?>
<li style="height:30px"><p class="categories1" style="background-color:#<?php echo str_replace('#','',$bg);?> !important">&nbsp;&nbsp;&nbsp;&nbsp;</p><p class="categories2" id="category0" style="color:#<?php echo str_replace('#','',$bg); ?> !important" onclick="showbigcalendar('bigcalendar<?php echo $many_sp_calendar; ?>', '<?php echo add_query_arg(array(
                'action' => 'spiderbigcalendar_month_widget',
                'theme_id' => $theme_id,
                'calendar' => $calendar_id,
                'select' => $view_select,
                'date' => $year . '-' . add_0((Month_num($month))),
                'many_sp_calendar' => $many_sp_calendar,
                'cur_page_url' => $path_sp_cal,
				'cat_id' => '',
				'cat_ids' => '',
                'widget' => $widget,
                ), $site_url);?>','<?php echo $many_sp_calendar ?>','<?php echo $widget; ?>')"><?php echo __('All categories', 'sp_calendar'); ?></p></li>
<?php echo '</ul>';

}*/
?>
<script>

jQuery(document).ready(function($) {


	jQuery('.cala_day').click(function(e){

	  // console.log('background clicked ');

		var first =  $(this).find('a')
		//console.log('first is '  + first.attr('href'));

		jQuery(first).trigger('click');

	});
	jQuery(".cala_day").hover(function() {

		jQuery(this).css('cursor','pointer');
	}, function() {
		jQuery(this).css('cursor','auto');
	});

});

</script>

<script>
    function calDayMatchHeight() {

        var date = document.querySelectorAll(".cala_day"),
              dateAlt = document.querySelectorAll("td.test"),
              dateOther = document.querySelectorAll(".caltext_color_other_months");

              date.height = date.width();
              dateAlt.height = dateAlt.width;
              dateOther.height = dateOther.width;
    }
</script>
<?php

  die();
}

?>
