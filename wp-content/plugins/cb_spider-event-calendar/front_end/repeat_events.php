<?php

add_action( 'wp_print_scripts', 'repeated_feature_scripts' );

function  repeated_feature_scripts(){
  
  wp_enqueue_style("Css", plugins_url("elements/calendar-jos.css", __FILE__));
 
}


if (!class_exists('WP_Widget')) {
  return;
}
class display_repeated_events extends WP_Widget {

	 function __construct($id = 'cb_widget_display_repeated_events', $description = 'Camana Bay Display Repeated Events', $opts = array()) {
	
		

		$opts['description'] = 'Display Repeated Events from the calendar';
		parent::__construct($id,$description,$opts);
		  
      
		
		
	}
	

	// widget update
	function update($new_instance, $old_instance) {
		 $instance = $new_instance;
      // Fields
		
	  //$instance['cb-calendar'] = $new_instance['cb-calendar'];
	
     return $instance;
	}
	
	function form($instance)
	{
		 global $wpdb;
		 
	
		 $all_calendars = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'spidercalendar_calendar');
		
?>
			<select  style="min-width:150px;  "  name="<?php echo $this->get_field_name( 'cb-calendar' ); ?>" >
			 <option value="0">Select Calendar</option>
<?php				foreach($all_calendars as $i => $cal)
{


 ?>
         <option value="<?php echo $all_calendars[$i]->id; ?>" <?php if ($instance['cb-calendar'] == $all_calendars[$i]->id) echo  'selected="selected"'; ?>><?php echo $all_calendars[$i]->title ?></option>
          
			
  <?php
	}   
?>	
			</select>
		
			
			
			
<?php
}
function widget($args, $instance) {
	global $wpdb;
	$today = date("Y-m-d H:i:s");


	$repeat_days = array (  "Sun" => "Sundays",
							"Mon" => "Mondays",	
							 "Tue" => "Tuesdays",
							 "Wed" => "Wednesdays",
							 "Thu" => "Thursdays",
							 "Fri" => "Fridays",
								 "Sat" => "Saturdays",
							);
						
                                       
	
	
?><div class="cb_repeated_events_header"><span class="featured_repeat_title">Weekly Events </span></div>
<?php

//SELECT * FROM wp_spidercalendar_event WHERE  published=1 and calendar=1 AND featured_repeat_event=1 AND repeat_method='weekly' AND '2016-03-15 15:08:46' < 'date_end'  ORDER BY week DESC 
 $s = $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "spidercalendar_event WHERE  published=%d AND featured_repeat_event=1 and calendar=%d AND repeat_method=%s AND %s < %s  ORDER BY week DESC ",  1, $instance['cb-calendar'],'weekly',$today, 'date_end');

	
	$row1 = $wpdb->get_results($s) ;
	
	
	$ordered_week_array = array(
	"Sun" => array(),
	"Mon" => array(),
	"Tue" => array(),
	"Wed" => array(),
	"Thu" => array(),
	"Fri" => array(),
	"Sat" =>array(),

	
	
	
	);
	foreach($row1 as $row)
	{

	$index_array = explode(',' , $row->week);

	$i = 0;
		foreach($index_array as $key => $index)
		{
		
			if (!empty($index))
				array_push($ordered_week_array["$index"], $row);
		
		}
	
	}
//	error_log('ordered week array ' . print_r($ordered_week_array, TRUE));
	foreach($ordered_week_array as $key=>$day)
	{
	  if (empty($day))
			   continue;
		echo '<div class="repeat_display_outside"><h1 class="repeat_event_header">' .$repeat_days[$key] . '</h1><hr class="cb_repeat_hr">  <div class="repeat_display_outside1">';
			

		foreach ($day as $d )
		
		{
		
			
			//$d = $day[0];
			
			if (strlen($d->image_file) > 0){
		    if (empty($d->event_link)){
				$first_image = '<img src = "' . $d->image_file . '" />';
			}else 
			{
			
				$first_image = '<a href="' . $d->event_link . '" ><img src = "' . $d->image_file . '" /> </a>';
			
			
			}
					echo  $first_image ;
			}
?>			
			<div class="repeat_event_title">
		
					<?php
		 if (empty($d->event_link)){			
				echo $d->title;
			}
			else 
			
			{
			
			$output =  '<a href="' . $d->event_link . '" >' . $d->title . '</a>';
			echo $output;
			
			
			}

				?>
				</div>
			
				<div class="featured_right_bottom">
					<span class="featured_time_bottom">
						<?php echo $d->time . ', ' ?>
					</span>
					<span class="featured_place_bottom">
						<?php echo $d->location; ?>
					</span>
				<div>
<?php   if (!empty($d->event_link)){ ?>
					<a class="repeat_event_link" href="<?php echo $d->event_link; ?>">MORE
<?php } ?>
				</a>
				</div>
			</div>
			
			
<?php
		}

?> </div></div>
<?php
	    
	
	}
		
	
	
}


}
add_action('widgets_init', create_function('', 'return register_widget("display_repeated_events");'));
?>