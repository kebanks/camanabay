<?php

add_action( 'wp_print_scripts', 'featured_scripts' );

function  featured_scripts(){
  
  wp_enqueue_style("Css", plugins_url("elements/calendar-jos.css", __FILE__));
 
}


if (!class_exists('WP_Widget')) {
  return;
}
class display_featured_events extends WP_Widget {

	 function __construct($id = 'cb_widget_display_featured_events', $description = 'Camana Bay Display Featured Events', $opts = array()) {
	
		

		$opts['description'] = 'Display Featured Events from the calendar';
		parent::__construct($id,$description,$opts);
		  
      
		
		
	}
	// widget update
	function update($new_instance, $old_instance) {
		 $instance = $old_instance;
      // Fields
		
	  $instance['cb-calendar'] = $new_instance['cb-calendar'];
	
     return $instance;
	}
	
	function form($instance)
	{
		 global $wpdb;
		 $all_calendars = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'spidercalendar_calendar');

?>
			<select  style="min-width:150px;  "  name="<?php echo $this->get_field_name( 'cb-calendar' ); ?>" >
			 <option value="0">Select Calendar</option>
<?php				foreach($all_calendars as $i => $cal)
{


 ?>
         <option value="<?php echo $all_calendars[$i]->id; ?>" <?php if ($instance['cb-calendar'] == $all_calendars[$i]->id) echo  'selected="selected"'; ?>><?php echo $all_calendars[$i]->title ?></option>
          
			
  <?php
	}   
?>	
			</select>
<?php
}
	 function widget($args, $instance) {
		global $wpdb;
	

	   $s = $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "spidercalendar_event WHERE featured_event=%d AND published=%d and calendar=%d  and date_end > CURDATE() ORDER BY date ASC", 1, 1, $instance['cb-calendar']);
  

	 $rows = $wpdb->get_results($s) ;

$date = new DateTime();
$date->modify('+1 month');


?>
	<div class="cb_featured_events_header"><span class="featured_title">Featured Events </span></div>

<?php

	  foreach ($rows as $row) 
	  {
	
	  $y = $date->format("Y-m-d");
	   
	  //event starts more than 30 days ahead
		
		
		if ($row->date > $y )
			{		
				continue;				
			}
	         

		//error_log('row is ' . print_r($row, TRUE));
		$content = preg_replace("/<img[^>]+\>/i", "", $row->text_for_date);   
?>    <div id="featured_display_outside">
			<div class="featured_display_left">
			
<?php    if ($row->repeat_method == "no_repeat")
		{
?>
				<span class="featured_left_date">
				
						<?php echo date("D", strtotime($row->date));?>
		
						
					<hr class="my_hr">
				</span>
				
				<span class="featured_left_month_day">
			
						<?php echo date("d/m",strtotime($row->date));?>					
				<span>
<?php 
		 }else {
 
				$index_array = explode(',' , $row->week);
				
				$lastElement = end($index_array);
				$lastElementKey = key($index_array);
				$lastElementKey --;
			
				foreach($index_array as $i)
				{
				   if (!empty($i)){
?>		 
				<span class="featured_left_date">
				
<?php 
				      if ($i != $index_array[$lastElementKey])
							echo $i . "<br/>";
						else 
							echo $i ;
?>
			</span>
<?php
					}
				}

?>				
				<span class="featured_left_date">
				<hr class="my_hr">
				</span>
				
		 
		 
<?php		 
		 
		 }
		 
		 

		 
?>
			</div>
			
		

		<div class="featured_display_right">
			<div class="featured_event_title">
				<?php

                if (empty($row->event_link)){
					echo $row->title; 
				}
				else{
				
				echo '<a href="' . $row->event_link . '"> ' . $row->title . '</a>';
				
				
				}
					
					
					
					?>
			</div>
			<div class="featured_event_text">
					<?php echo $content; ?>					
			</div>
			<div class="featured_right_bottom">
				<span class="featured_time_bottom">
					<?php echo $row->time . ', ' ?>
				</span>
				<span class="featured_place_bottom">
					<?php echo $row->location; ?>
				</span>
<?php if (!empty($row->event_link)){ ?>
					<a class="featured_event_link" href="<?php echo $row->event_link; ?>">MORE
				</a>
<?php } ?>			
			</div>
		</div>

	</div>
		
<?php
		
	}
	    
		
		
		
		
		
	}	
		
		
		
		
		
		
		
		
		
	 


}
add_action('widgets_init', create_function('', 'return register_widget("display_featured_events");'));
?>