
jQuery(document).ready(function() {

  function calendarStyles() {

    var rowWidth = jQuery(".cell_body").width(),
        rowNodes = jQuery(".cell_body").children();

    var calData = jQuery(".cell_body td"),
        calDataWidth = jQuery(".cell_body td").width();

    calData.css("height", calDataWidth);
  }

  setTimeout(calendarStyles, 300);

  jQuery(window).resize(function() {

    calendarStyles();
  });

  jQuery('body').on("click", ".cb_cal_arrow", function() {

      setTimeout(calendarStyles, 300);
  });
});
