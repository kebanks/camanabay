<?php
global $photonic_setup_options, $photonic_generic_options, $photonic_flickr_options, $photonic_picasa_options, $photonic_smugmug_options, $photonic_500px_options, $photonic_instagram_options, $photonic_zenfolio_options;

$photonic_setup_options = array();

require_once(plugin_dir_path(__FILE__) . "/generic-options.php");
foreach ($photonic_generic_options as $option) {
	$photonic_setup_options[] = $option;
}

require_once(plugin_dir_path(__FILE__) . "/flickr-options.php");
foreach ($photonic_flickr_options as $option) {
	$photonic_setup_options[] = $option;
}

require_once(plugin_dir_path(__FILE__) . "/picasa-options.php");
foreach ($photonic_picasa_options as $option) {
	$photonic_setup_options[] = $option;
}

require_once(plugin_dir_path(__FILE__) . "/500px-options.php");
foreach ($photonic_500px_options as $option) {
	$photonic_setup_options[] = $option;
}

require_once(plugin_dir_path(__FILE__) . "/smugmug-options.php");
foreach ($photonic_smugmug_options as $option) {
	$photonic_setup_options[] = $option;
}

require_once(plugin_dir_path(__FILE__) . "/instagram-options.php");
foreach ($photonic_instagram_options as $option) {
	$photonic_setup_options[] = $option;
}

require_once(plugin_dir_path(__FILE__) . "/zenfolio-options.php");
foreach ($photonic_zenfolio_options as $option) {
	$photonic_setup_options[] = $option;
}

function photonic_title_styles() {
	$ret = array(
		"regular" => "<img src='".trailingslashit(PHOTONIC_URL).'include/images/title-regular.png'."'>Normal title display using the HTML \"title\" attribute</img>",
		"below" => "<img src='".trailingslashit(PHOTONIC_URL).'include/images/title-below.png'."'>Below the thumbnail</img>",
		"tooltip" => "<img src='".trailingslashit(PHOTONIC_URL).'include/images/title-jq-tooltip.png'."'>Using the JQuery Tooltip plugin</img>",
		"hover-slideup-show" => "<img src='".trailingslashit(PHOTONIC_URL).'include/images/title-slideup.png'."'>Slide up from bottom upon hover</img>",
//		"hover-slidedown-show" => "<img src='".trailingslashit(PHOTONIC_URL).'include/images/title-slideup.png'."'>Slide down from top upon hover</img>",
	);
	return $ret;
}
