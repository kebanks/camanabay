<?php
/**
 * Gallery processor class to be extended by individual processors. This class has an abstract method called <code>get_gallery_images</code>
 * that has to be defined by each inheriting processor.
 *
 * This is also where the OAuth support is implemented. The URLs are defined using abstract functions, while a handful of utility functions are defined.
 * Most utility functions have been adapted from the OAuth PHP package distributed here: http://code.google.com/p/oauth-php/.
 *
 * @package Photonic
 * @subpackage Extensions
 */

abstract class Photonic_Processor {
	public $library, $thumb_size, $full_size, $api_key, $api_secret, $provider, $nonce, $oauth_timestamp, $signature_parameters, $link_lightbox_title,
		$oauth_version, $oauth_done, $show_more_link, $is_server_down, $is_more_required, $login_shown, $login_box_counter, $gallery_index, $bypass_popup;

	function __construct() {
		global $photonic_slideshow_library, $photonic_custom_lightbox, $photonic_enable_popup;
		if ($photonic_slideshow_library != 'custom') {
			$this->library = $photonic_slideshow_library;
		}
		else {
			$this->library = $photonic_custom_lightbox;
		}
		$this->nonce = Photonic_Processor::nonce();
		$this->oauth_timestamp = time();
		$this->oauth_version = '1.0';
		$this->show_more_link = false;
		$this->is_server_down = false;
		$this->is_more_required = true;
		$this->login_shown = false;
		$this->login_box_counter = 0;
		$this->gallery_index = 0;
		$this->bypass_popup = /*($this->library == 'swipebox' || $this->library == 'prettyphoto' || $this->library == 'fancybox2') && */
			(!isset($photonic_enable_popup) || $photonic_enable_popup === false || $photonic_enable_popup == '' || $photonic_enable_popup == 'off');
	}

	/**
	 * Main function that fetches the images associated with the shortcode.
	 *
	 * @abstract
	 * @param array $attr
	 */
	abstract protected function get_gallery_images($attr = array());

	public function oauth_signature_method() {
		return 'HMAC-SHA1';
	}

	/**
	 * Takes a token response from a request token call, then puts it in an appropriate array.
	 *
	 * @param $response
	 */
	public abstract function parse_token($response);

	/**
	 * Generates a nonce for use in signing calls.
	 *
	 * @static
	 * @return string
	 */
	public static function nonce() {
		$mt = microtime();
		$rand = mt_rand();
		return md5($mt . $rand);
	}

	/**
	 * Encodes the URL as per RFC3986 specs. This replaces some strings in addition to the ones done by a rawurlencode.
	 * This has been adapted from the OAuth for PHP project.
	 *
	 * @static
	 * @param $input
	 * @return array|mixed|string
	 */
	public static function urlencode_rfc3986($input) {
		if (is_array($input)) {
			return array_map(array('Photonic_Processor', 'urlencode_rfc3986'), $input);
		}
		else if (is_scalar($input)) {
			return str_replace(
				'+',
				' ',
				str_replace('%7E', '~', rawurlencode($input))
			);
		}
		else {
			return '';
		}
	}

	/**
	 * Takes an array of parameters, then parses it and generates a query string. Prior to generating the query string the parameters are sorted in their natural order.
	 * Without sorting the signatures between this application and the provider might differ.
	 *
	 * @static
	 * @param $params
	 * @return string
	 */
	public static function build_query($params) {
		if (!$params) {
			return '';
		}
		$keys = array_map(array('Photonic_Processor', 'urlencode_rfc3986'), array_keys($params));
		$values = array_map(array('Photonic_Processor', 'urlencode_rfc3986'), array_values($params));
		$params = array_combine($keys, $values);

		// Sort by keys (natsort)
		uksort($params, 'strnatcmp');
		$pairs = array();
		foreach ($params as $key => $value) {
			if (is_array($value)) {
				natsort($value);
				foreach ($value as $v2) {
					$pairs[] = ($v2 == '') ? "$key=0" : "$key=$v2";
				}
			}
			else {
				$pairs[] = ($value == '') ? "$key=0" : "$key=$value";
			}
		}

		$string = implode('&', $pairs);
		return $string;
	}

	/**
	 * Takes a string of parameters in an HTML encoded string, then returns an array of name-value pairs, with the parameter
	 * name and the associated value.
	 *
	 * @static
	 * @param $input
	 * @return array
	 */
	public static function parse_parameters($input) {
		if (!isset($input) || !$input) return array();

		$pairs = explode('&', $input);

		$parsed_parameters = array();
		foreach ($pairs as $pair) {
			$split = explode('=', $pair, 2);
			$parameter = urldecode($split[0]);
			$value = isset($split[1]) ? urldecode($split[1]) : '';

			if (isset($parsed_parameters[$parameter])) {
				// We have already recieved parameter(s) with this name, so add to the list
				// of parameters with this name
				if (is_scalar($parsed_parameters[$parameter])) {
					// This is the first duplicate, so transform scalar (string) into an array
					// so we can add the duplicates
					$parsed_parameters[$parameter] = array($parsed_parameters[$parameter]);
				}

				$parsed_parameters[$parameter][] = $value;
			}
			else {
				$parsed_parameters[$parameter] = $value;
			}
		}
		return $parsed_parameters;
	}

	/**
	 * If authentication is enabled for this processor and the user has not authenticated this site to access his profile,
	 * this shows a login box.
	 *
	 * @param $post_id
	 * @return string
	 */
	public function get_login_box($post_id = '') {
		$login_box_option = 'photonic_'.$this->provider.'_login_box';
		$login_button_option = 'photonic_'.$this->provider.'_login_button';
		global $$login_box_option, $$login_button_option;
		$login_box = $$login_box_option;
		$login_button = $$login_button_option;
		$this->login_box_counter++;
		$ret = '<div id="photonic-login-box-'.$this->provider.'-'.$this->login_box_counter.'" class="photonic-login-box photonic-login-box-'.$this->provider.'">';
		if ($this->is_server_down) {
			$ret .= __("The authentication server is down. Please try after some time.", 'photonic');
		}
		else {
			$ret .= wp_specialchars_decode($login_box, ENT_QUOTES);
			if (trim($login_button) == '') {
				$login_button = 'Login';
			}
			else {
				$login_button = wp_specialchars_decode($login_button, ENT_QUOTES);
			}
			$url = '#';
			$target = '';
			if ($this->provider == 'picasa' || $this->provider == 'instagram') {
				$url = $this->get_authorization_url();
				$target = 'target="_blank"';
			}

			if (!empty($post_id)) {
				$rel = "rel='auth-button-single-$post_id'";
			}
			else {
				$rel = '';
			}
			$ret .= "<p class='photonic-auth-button'><a href='$url' $target class='auth-button auth-button-{$this->provider}' $rel>".$login_button."</a></p>";
		}
		$ret .= '</div>';
		return $ret;
	}

	function more_link_button($link_to = '') {
		global $photonic_archive_link_more;
		if (empty($photonic_archive_link_more) && $this->is_more_required) {
			return "<div class='photonic-more-link-container'><a href='$link_to' class='photonic-more-button more-button-{$this->provider}'>See the rest</a></div>";
		}
		$this->is_more_required = true;
		return '';
	}

	/**
	 * Prints the header for a section. Typically used for albums / photosets / groups, where some generic information about the album / photoset / group is available.
	 * The <code>$options</code> accept the following prarameters:
	 * 	- string type Indicates what type of object is being displayed like gallery / photoset / album etc. This is added to the CSS class.
	 * 	- array $hidden Contains the elements that should be hidden from the header display.
	 * 	- array $counters Contains counts of the object that the header represents. In most cases this has just one value. Zenfolio objects have multiple values.
	 * 	- string $link Should clicking on the thumbnail / title take you anywhere?
	 * 	- string $display Indicates if this is on the page or in a popup
	 * 	- bool $iterate_level_3 If this is a level 3 header, this field indicates whether an expansion icon should be shown. This is to improve performance for Flickr collections.
	 * 	- string $provider What is the source of the data?
	 *
	 * @param array $header The header object, which contains the title, thumbnail source URL and the link where clicking on the thumb will take you
	 * @param array $options The options to display this header. Options contain the listed internal fields fields
	 * @return string
	 */
	function process_object_header($header, $options = array()) {
		$type = empty($options['type']) ? 'group' : $options['type'];
		$hidden = isset($options['hidden']) && is_array($options['hidden']) ? $options['hidden'] : array();
		$counters = isset($options['counters']) && is_array($options['counters']) ? $options['counters'] : array();
		$link = !isset($options['link']) ? true : $options['link'];
		$display = empty($options['display']) ? 'in-page' : $options['display'];
		$iterate_level_3 = !isset($options['iterate_level_3']) ? true : $options['iterate_level_3'];

		if ($this->bypass_popup && $display != 'in-page') {
			return '';
		}
		$ret = '';
		if (!empty($header['title'])) {
			global $photonic_external_links_in_new_tab;
			$title = esc_attr($header['title']);
			if (!empty($photonic_external_links_in_new_tab)) {
				$target = ' target="_blank" ';
			}
			else {
				$target = '';
			}

			$anchor = '';
			if (!empty($header['thumb_url'])) {
				$image = '<img src="'.$header['thumb_url'].'" alt="'.$title.'" />';

				if ($link) {
					$anchor = "<a href='".$header['link_url']."' class='photonic-header-thumb photonic-{$this->provider}-$type-solo-thumb' title='".$title."' $target>".$image."</a>";
				}
				else {
					$anchor = "<div class='photonic-header-thumb photonic-{$this->provider}-$type-solo-thumb'>$image</div>";
				}
			}

			if (empty($hidden['thumbnail']) || empty($hidden['title']) || empty($hidden['counter']) || empty($iterate_level_3)) {
				$popup_header_class = '';
				if ($display == 'popup') {
					$popup_header_class = 'photonic-panel-header';
				}
				$ret .= "<div class='photonic-{$this->provider}-$type $popup_header_class'>";

				if (empty($hidden['thumbnail'])) {
					$ret .= $anchor;
				}
				if (empty($hidden['title']) || empty($hidden['counter']) || empty($iterate_level_3)) {
					$ret .= "<div class='photonic-header-details photonic-$type-details'>";
					if (empty($hidden['title']) || empty($iterate_level_3)) {
						$provider = $options['provider'];
						$expand = empty($iterate_level_3) ? '<a href="#" title="'.esc_attr__('Show', 'photonic').'" class="photonic-level-3-expand photonic-level-3-expand-plus" data-photonic-level-3="'.$provider.'-'.$type.'-'.$header['id'].'" data-photonic-layout="'.$options['layout'].'">&nbsp;</a>' : '';

						if ($link) {
							$ret .= "<div class='photonic-header-title photonic-$type-title'><a href='".$header['link_url']."' $target>".$title.'</a>'.$expand.'</div>';
						}
						else {
							$ret .= "<div class='photonic-header-title photonic-$type-title'>".$title.$expand.'</div>';
						}
					}
					if (empty($hidden['counter'])) {
						$counter_texts = array();
						if (!empty($counters['groups'])) {
							$counter_texts[] = sprintf(_n('%s group', '%s groups', $counters['groups'], 'photonic'), $counters['groups']);
						}
						if (!empty($counters['sets'])) {
							$counter_texts[] = sprintf(_n('%s set', '%s sets', $counters['sets'], 'photonic'), $counters['sets']);
						}
						if (!empty($counters['photos'])) {
							$counter_texts[] = sprintf(_n('%s photo', '%s photos', $counters['photos'], 'photonic'), $counters['photos']);
						}

						apply_filters('photonic_modify_counter_texts', $counter_texts, $counters);

						if (!empty($counter_texts)) {
							$ret .= "<span class='photonic-header-info photonic-$type-photos'>".implode(', ', $counter_texts).'</span>';
						}
					}

					$ret .= "</div><!-- .photonic-$type-details -->";
				}
				$ret .= "</div>";
			}
		}

		return $ret;
	}

	/**
	 * Generates the HTML for the lowest level gallery, i.e. the photos. This is used for both, in-page and popup displays.
	 * The code for the random layouts is handled in JS, but just the HTML markers for it are provided here.
	 *
	 * @param $photos
	 * @param array $options
	 * @return string
	 */
	function generate_level_1_gallery($photos, $options, $shortcode_attr = array()) {
		$title_position = $options['title_position'];
		$row_constraints = isset($options['row_constraints']) && is_array($options['row_constraints']) ? $options['row_constraints'] : array();
		$columns = !empty($options['columns']) ? $options['columns'] : 'auto';
		$panel = !empty($options['panel']) ? $options['panel'] : '';
		$layout = !empty($options['layout']) ? $options['layout'] : 'square';
		$display = !empty($options['display']) ? $options['display'] : 'in-page';
		$sizes = isset($options['sizes']) && is_array($options['sizes']) ? $options['sizes'] : array();
		$show_lightbox = !isset($options['show_lightbox']) ? true: $options['show_lightbox'];
		$type = !empty($options['type']) ? $options['type'] : 'photo';
		$parent = !empty($options['parent']) ? $options['parent'] : 'stream';
		$level_2_meta = isset($options['level_2_meta']) && is_array($options['level_2_meta']) ? $options['level_2_meta'] : array();
		$more = !empty($shortcode_attr['more']) ? esc_attr($shortcode_attr['more']) : '';

		$randomized = $layout == 'random';

		$col_class = '';
		if (Photonic::check_integer($columns)) {
			$col_class = 'photonic-gallery-'.$columns.'c';
		}

		if ($col_class == '' && $row_constraints['constraint-type'] == 'padding') {
			$col_class = 'photonic-pad-photos';
		}
		else if ($col_class == '') {
			$col_class = 'photonic-gallery-'.$row_constraints['count'].'c';
		}
		$col_class .= ' photonic-level-1';

		$link_attributes = $this->get_lightbox_attributes($display, $col_class, $show_lightbox, $panel);

		$ul_class = "class='title-display-$title_position ".($randomized ? 'photonic-random-layout' : 'photonic-standard-layout')."'";
		if ($display == 'popup') {
			$ul_class = "class='slideshow-grid-panel lib-{$this->library} title-display-$title_position'";
		}

		$ret = '';
		if (!$randomized && $display != 'popup') {
			$container_tag = 'ul';
			$element_tag = 'li';
		}
		else {
			$container_tag = 'div';
			$element_tag = 'div';
		}

		$level_2_data = '';
		if (!empty($level_2_meta)) {
			$level_2_data = array();
			// Should have total, start, end, per-page
			foreach ($level_2_meta as $meta => $value) {
				$level_2_data[] = 'data-photonic-stream-'.$meta.'="'.$value.'"';
			}
			$level_2_data = implode(' ', $level_2_data);
		}

		$to_be_glued = '';
		if (!empty($shortcode_attr)) {
			$to_be_glued = array();
			foreach ($shortcode_attr as $name => $value) {
				if (is_scalar($value)) {
					$to_be_glued[] = $name.'='.$value;
				}
			}
			$to_be_glued = implode('&',$to_be_glued);
			$to_be_glued = esc_attr($to_be_glued);
		}

		$level_2_data .= ' data-photonic-stream-query="'.$to_be_glued.'"';

		$ret .= "<$container_tag $ul_class $level_2_data>";

		global $photonic_external_links_in_new_tab;
		if (!empty($photonic_external_links_in_new_tab)) {
			$target = " target='_blank' ";
		}
		else {
			$target = '';
		}

		$counter = 0;
		$thumbnail_class = " class='$layout' ";

		foreach ($photos as $photo) {
			$counter++;

			$thumb = ($randomized && $display == 'in-page') ? (isset($photo['tile_image']) ? $photo['tile_image'] : $photo['main_image']) : $photo['thumbnail'];
			$orig = $photo['main_image'];
			$url = $photo['main_page'];
			$title = esc_attr($photo['title']);
			$description = esc_attr($photo['description']);
			$alt = esc_attr($photo['alt_title']);
			$orig = ($this->library == 'none' || !$show_lightbox) ? $url : $orig;

			$title = empty($title) ? ((empty($alt) && $this->link_lightbox_title && $this->library != 'thickbox') ? apply_filters('photonic_default_lightbox_text', __('View', 'photonic')) : $alt) : $title;
			$shown_title = '';
			if (in_array($title_position, array('below', 'hover-slideup-show', 'hover-slidedown-show')) && $alt != '') {
				$shown_title = '<div class="photonic-title-info"><div class="photonic-photo-title photonic-title">'.wp_specialchars_decode($alt, ENT_QUOTES).'</div></div>';
			}
			$ret .= "\n\t<".$element_tag.' class="photonic-'.$this->provider.'-image photonic-'.$this->provider.'-'.$type.' '.$col_class.'">';

			$deep_value = 'gallery[photonic-'.$this->provider.'-'.$parent.'-'.(empty($panel) ? $this->gallery_index : $panel).']/'.(empty($photo['id']) ? $counter : $photo['id']).'/';
			$deep_link = ' data-photonic-deep="'.$deep_value.'" ';

			$style = array();
			if (!empty($sizes['thumb-width'])) $style[] = 'width:'.$sizes['thumb-width'].'px';
			if (!empty($sizes['thumb-height'])) $style[] = 'height:'.$sizes['thumb-height'].'px';
			if (!empty($style)) $style = 'style="'.implode(';', $style).'"'; else $style = '';
			if ($this->link_lightbox_title && $this->library != 'thickbox') {
				$title_link_start = esc_attr("<a href='$url' $target>");
				$title_link_end = esc_attr("</a>");
			}
			else {
				$title_link_start = '';
				$title_link_end = '';
			}

			$title_markup = $title_link_start.$title.$title_link_end;
			$ret .= '<a '.$link_attributes.' href="'.$orig.'" title="'.$title_markup.'" data-title="'.$title_markup.'" '.$target.$deep_link.'><img alt="'.$alt.'" src="'.$thumb.'" '.$style.$thumbnail_class.'/>'.$shown_title.'</a>';
			$ret .= "</$element_tag>";
		}

		if ($ret != "<$container_tag $ul_class $level_2_data>") {
			$trailing = strlen($element_tag) + 3;
			if (substr($ret, -$trailing) != "</$element_tag>" && !$this->bypass_popup && !$randomized) {
				$ret .= "</$element_tag><!-- last $element_tag.photonic-pad-photos -->";
			}

			$ret .= "\n</$container_tag> <!-- container -->\n";
			if (!empty($level_2_meta) && isset($level_2_meta['end']) && isset($level_2_meta['total']) && $level_2_meta['total'] > $level_2_meta['end']) {
				$ret .= !empty($more) ? "<a href='#' class='photonic-more-button photonic-more-dynamic'>$more</a>\n" : '';
			}
		}
		else {
			$ret = '';
		}

		if (is_archive()) {
			global $photonic_archive_thumbs;
			if (!empty($photonic_archive_thumbs) && $counter < $photonic_archive_thumbs) {
				$this->is_more_required = false;
			}
		}

		return $ret;
	}

	function generate_level_2_gallery($objects, $options = array()) {
		$row_constraints = isset($options['row_constraints']) && is_array($options['row_constraints']) ? $options['row_constraints'] : array();
		$columns = $options['columns'];
		$type = $options['type'];
		$singular_type = $options['singular_type'];
		$title_position = $options['title_position'];
		$level_1_count_display = $options['level_1_count_display'];
		$layout = !isset($options['layout']) ? 'square' : $options['layout'];
		$indent = !isset($options['indent']) ? '' : $options['indent'];
		$provider = !isset($options['provider']) ? '' : $options['provider'];

		$randomized = $layout == 'random';
		$ul_class = "class='title-display-$title_position ".($randomized ? 'photonic-random-layout' : 'photonic-standard-layout')."'";

		$ret = "\n$indent<ul $ul_class>";
		if ($randomized) {
			$ret = "\n$indent<div $ul_class>";
		}

		if ($columns != 'auto') {
			$col_class = 'photonic-gallery-'.$columns.'c';
		}
		else if ($row_constraints['constraint-type'] == 'padding') {
			$col_class = 'photonic-pad-'.$type;
		}
		else {
			$col_class = 'photonic-gallery-'.$row_constraints['count'].'c';
		}

		$col_class .= ' photonic-level-2';

		$counter = 0;
		foreach ($objects as $object) {
			$data_attributes = isset($object['data_attributes']) && is_array($object['data_attributes']) ? $object['data_attributes'] : array();
			$data_array = array();
			foreach ($data_attributes as $attr => $value) {
				$data_array[] = 'data-photonic-'.$attr.'="'.$value.'"';
			}

			$data_array = implode(' ', $data_array);


			$id = empty($object['id_1']) ? '' : $object['id_1'].'-';
			$id = $id.$this->gallery_index;
			$id = empty($object['id_2']) ? $id : ($id.'-'.$object['id_2']);
			$title = esc_attr($object['title']);
			$image = "<img src='".(($randomized && isset($object['tile_image'])) ? $object['tile_image'] : $object['thumbnail'])."' alt='".$title."' class='$layout'/>";
			$additional_classes = !empty($object['classes']) ? implode(' ', $object['classes']) : '';
			$realm_class = '';
			if (!empty($object['classes'])) {
				foreach ($object['classes'] as $class) {
					if (stripos($class, 'photonic-'.$this->provider.'-realm') !== FALSE) {
						$realm_class = $class;
					}
				}
			}
			$anchor = "\n{$indent}\t\t<a href='{$object['main_page']}' class='photonic-{$this->provider}-$singular_type-thumb $additional_classes' id='photonic-{$this->provider}-$singular_type-thumb-$id' title='".$title."' data-title='".$title."' $data_array>\n$indent\t\t\t".$image;
			$text = '';
			if (in_array($title_position, array('below', 'hover-slideup-show', 'hover-slidedown-show'))) {
				$text = "\n{$indent}\t\t\t<div class='photonic-title-info'>\n{$indent}\t\t\t\t<div class='photonic-$singular_type-title photonic-title'>".$title."";
				if (!$level_1_count_display && !empty($object['counter'])) {
					$text .= '<span class="photonic-'.$singular_type.'-photo-count">'.sprintf(__('%s photos', 'photonic'), $object['counter']).'</span>';
				}
			}
			if ($text != '') {
				$text .= "</div>\n{$indent}\t\t\t</div>";
			}

			$anchor .= $text."\n{$indent}\t\t</a>";
			$password_prompt = '';
			if (!empty($object['passworded'])) {
				$prompt_title = esc_attr__('Protected Content', 'photonic');
				$prompt_submit = esc_attr__('Access', 'photonic');
				$password_type = " type='password' ";
				$prompt_type = 'password';
				$prompt_text = esc_attr__('This album is password-protected. Please provide a valid password.', 'photonic');
				if (in_array("photonic-$provider-passworded-authkey", $object['classes'])) {
					$prompt_text = esc_attr__('This album is protected. Please provide a valid authorization key.', 'photonic');
				}
				else if (in_array("photonic-$provider-passworded-link", $object['classes'])) {
					$prompt_text = esc_attr__('This album is protected. Please provide the short-link for it.', 'photonic');
					$password_type = '';
					$prompt_type = 'link';
				}
				$password_prompt = "
							<div class='photonic-password-prompter $realm_class' id='photonic-{$this->provider}-$singular_type-prompter-$id' title='$prompt_title' data-photonic-prompt='$prompt_type'>
								<p>$prompt_text</p>
								<input $password_type name='photonic-{$this->provider}-password' />
								<span class='photonic-{$this->provider}-submit photonic-password-submit'><a href='#'>$prompt_submit</a></span>
							</div>";
			}

			if ($randomized) {
				$ret .= "\n$indent\t<div class='photonic-{$this->provider}-image photonic-{$this->provider}-$singular_type-thumb $col_class' id='photonic-{$this->provider}-$singular_type-$id'>{$anchor}{$password_prompt}\n$indent\t</div>";
			}
			else {
				$ret .= "\n$indent\t<li class='photonic-{$this->provider}-image photonic-{$this->provider}-$singular_type-thumb $col_class' id='photonic-{$this->provider}-$singular_type-$id'>{$anchor}{$password_prompt}\n$indent\t</li>";
			}
			$counter++;
		}

		if ($ret != "\n$indent<ul $ul_class>" && !$randomized) {
			$ret .= "\n$indent</ul>\n";
		}
		else if ($randomized) {
			$ret .= "\n$indent</div>\n";
		}
		else {
			$ret = '';
		}

		if (is_archive()) {
			global $photonic_archive_thumbs;
			if (!empty($photonic_archive_thumbs) && $counter < $photonic_archive_thumbs) {
				$this->is_more_required = false;
			}
		}
		return $ret;
	}

	/**
	 * Depending on the lightbox library, this function provides the CSS class and the rel tag for the thumbnail. This method borrows heavily from
	 * Justin Tadlock's Cleaner Gallery Plugin.
	 *
	 * @param $display
	 * @param $col_class
	 * @param $show_lightbox
	 * @param $rel_id
	 * @return string
	 */
	function get_lightbox_attributes($display, $col_class, $show_lightbox, $rel_id) {
		global $photonic_slideshow_mode;
		$class = '';
		$rel = '';
		$data_rel = '';
		if ($this->library != 'none' && $show_lightbox) {
			$class = 'photonic-launch-gallery launch-gallery-'.$this->library." ".$this->library;
			$rel = 'lightbox-photonic-'.$this->provider.'-stream-'.(empty($rel_id) ? $this->gallery_index : $rel_id);
			switch ($this->library) {
				case 'lightbox':
				case 'slimbox':
				case 'jquery_lightbox_plugin':
				case 'jquery_lightbox_balupton':
					$class = 'launch-gallery-lightbox lightbox';
					$rel = "lightbox[{$rel}]";
					break;

				case 'fancybox2':
					$class = 'photonic-launch-gallery launch-gallery-fancybox fancybox';
					break;

				case 'prettyphoto':
					$rel = 'photonic-prettyPhoto['.$rel.']';
					break;

				case 'lightcase':
					$data_rel = 'lightcase:lightbox-photonic-'.$this->provider.'-stream-'.(empty($rel_id) ? $this->gallery_index : $rel_id).((isset($photonic_slideshow_mode) && $photonic_slideshow_mode == 'on') ? ':slideshow' : '');
					break;

				default:
					$class = 'photonic-launch-gallery launch-gallery-'.$this->library." ".$this->library;
					$rel = 'lightbox-photonic-'.$this->provider.'-stream-'.(empty($rel_id) ? $this->gallery_index : $rel_id);
					break;
			}

			if ($display == 'popup') {
				$class .= ' '.$col_class;
			}
			$class = " class='$class' ";
			$rel = " rel='$rel' ";
			$data_rel = " data-rel='$data_rel' ";
		}

		return $class.$rel.$data_rel;
	}

	function get_header_display($args) {
		if (!isset($args['headers'])) {
			return array(
				'thumbnail' => 'inherit',
				'title' => 'inherit',
				'counter' => 'inherit',
			);
		}
		else if (empty($args['headers'])) {
			return array (
				'thumbnail' => 'none',
				'title' => 'none',
				'counter' => 'none',
			);
		}
		else {
			$header_array = explode(',', $args['headers']);
			return array(
				'thumbnail' => in_array('thumbnail', $header_array) ? 'show' : 'none',
				'title' => in_array('title', $header_array) ? 'show' : 'none',
				'counter' => in_array('counter', $header_array) ? 'show' : 'none',
			);
		}
	}

	function get_hidden_headers($arg_headers, $setting_headers) {
		return array(
			'thumbnail' => $arg_headers['thumbnail'] === 'inherit' ? $setting_headers['thumbnail'] : ($arg_headers['thumbnail'] === 'none' ? true : false),
			'title' => $arg_headers['title'] === 'inherit' ? $setting_headers['title'] : ($arg_headers['title'] === 'none' ? true : false),
			'counter' => $arg_headers['counter'] === 'inherit' ? $setting_headers['counter'] : ($arg_headers['counter'] === 'none' ? true : false),
		);
	}
}
