// <---- Custom Additions end here.
// Photonic additions begin ----->

//$ = jQuery.noConflict();
jQuery(document).ready(function($) {
	var tabs = $("#photonic-options").tabs({
		fx: {
			opacity: "toggle",
			duration: "fast"
		}
	});

	$.fn.tabIndex = function () {
		return $(this).parent().find(this).index() - 2;
	};

	//tabs.tabs('option', 'active', '#' + Photonic_Admin_JS.category);
	$(tabs).tabs('option', 'active', $('#' + Photonic_Admin_JS.category).tabIndex());

	$('.photonic-border-options input[type="text"], .photonic-border-options select').change(function(event) {
		var thisId = event.currentTarget.id;
		thisId = thisId.substring(0, thisId.indexOf('-'));
		var edges = new Array('top', 'right', 'bottom', 'left');
		var border = '';
		for (var x in edges) {
			var edge = edges[x];
			var thisName = thisId + '-' + edge;
			border += edge + '::';
			border += 'color=' + $("#" + thisName + "-color").val() + ';' +
					'colortype=' + $("input[name=" + thisName + "-colortype]:checked").val() + ';' +
					'style=' + $("#" + thisName + "-style").val() + ';' +
					'border-width=' + $("#" + thisName + "-border-width").val() + ';' +
					'border-width-type=' + $("#" + thisName + "-border-width-type").val() + ';';
			border += '||';
		}
		$('#' + thisId).val(border);
	});

	$('.photonic-border-options input[type="radio"]').change(function(event) {
		var thisId = event.currentTarget.name;
		thisId = thisId.substring(0, thisId.indexOf('-'));
		var edges = new Array('top', 'right', 'bottom', 'left');
		var border = '';
		for (var x in edges) {
			var edge = edges[x];
			var thisName = thisId + '-' + edge;
			border += edge + '::';
			border += 'color=' + $("#" + thisName + "-color").val() + ';' +
					'colortype=' + $("input[name=" + thisName + "-colortype]:checked").val() + ';' +
					'style=' + $("#" + thisName + "-style").val() + ';' +
					'border-width=' + $("#" + thisName + "-border-width").val() + ';' +
					'border-width-type=' + $("#" + thisName + "-border-width-type").val() + ';';
			border += '||';
		}
		$('#' + thisId).val(border);
	});

	$('.photonic-background-options input[type="text"], .photonic-background-options select').change(function(event) {
		var thisName = event.currentTarget.id;
		thisName = thisName.substring(0, thisName.indexOf('-'));
		$("#" + thisName).val('color=' + $("#" + thisName + "-bgcolor").val() + ';' +
			'colortype=' + $("input[name=" + thisName + "-colortype]:checked").val() + ';' +
			'image=' + $("#" + thisName + "-bgimg").val() + ';' +
			'position=' + $("#" + thisName + "-position").val() + ';' +
			'repeat=' + $("#" + thisName + "-repeat").val() + ';' +
			'trans=' + $("#" + thisName + "-trans").val() + ';'
		);
	});

	$('.photonic-background-options input[type="radio"]').change(function(event) {
		var thisName = event.currentTarget.name;
		thisName = thisName.substring(0, thisName.indexOf('-'));
		$("#" + thisName).val('color=' + $("#" + thisName + "-bgcolor").val() + ';' +
			'colortype=' + $("input[name=" + thisName + "-colortype]:checked").val() + ';' +
			'image=' + $("#" + thisName + "-bgimg").val() + ';' +
			'position=' + $("#" + thisName + "-position").val() + ';' +
			'repeat=' + $("#" + thisName + "-repeat").val() + ';' +
			'trans=' + $("#" + thisName + "-trans").val() + ';'
		);
	});

	$('.photonic-padding-options input[type="text"], .photonic-padding-options select').change(function(event) {
		var thisId = event.currentTarget.id;
		thisId = thisId.substring(0, thisId.indexOf('-'));
		var edges = new Array('top', 'right', 'bottom', 'left');
		var padding = '';
		for (var x in edges) {
			var edge = edges[x];
			var thisName = thisId + '-' + edge;
			padding += edge + '::';
			padding += 'padding=' + $("#" + thisName + "-padding").val() + ';' +
					'padding-type=' + $("#" + thisName + "-padding-type").val() + ';';
			padding += '||';
		}
		$('#' + thisId).val(padding);
	});

	$('.photonic-button-bar').draggable();

	$('.photonic-button-toggler a').on('click', function() {
		var thisClass = this.className;
		thisClass = thisClass.substr(24);
		var dialogClass = '.photonic-button-bar-' + thisClass;
		$(dialogClass).slideToggle();
		return false;
	});

	$('.photonic-button-bar a').click(function() {
		var thisParent = $(this).parent().parent();
		thisParent.slideToggle();
		return false;
	});

	$("#photonic-options h3").each(function() {
		var text = $(this).text();
		if (text == '') {
			$(this).remove();
		}
	});

	$(".photonic-options-form :input[type='submit']").click(function() {
		//This is needed, otherwise the event handler cannot figure out which button was clicked.
		photonic_submit_button = $(this);
	});

	$('.photonic-options-form').submit(function(event) {
		var field = photonic_submit_button;
		var value = field.val();

		if (value.substring(0, 5) == 'Reset') {
			if (!confirm("This will reset your configurations to the original values!!! Are you sure you want to continue? This is not reversible!")) {
				return false;
			}
		}
		else if (value.substring(0, 6) == 'Delete') {
			if (!confirm("This will delete all your Photonic configuration options!!! Are you sure you want to continue? This is not reversible!")) {
				return false;
			}
		}
	});
});