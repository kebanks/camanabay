=== WWM Social Share On Image Hover ===
Contributors: jobinjose01
Donate link: http://www.walkswithme.net/social-share-icons-on-image-hover
Tags:  facebook , image hover, sharing, social hover, social media, social share on hover, twitter, wordpress image hover, hover, image
Requires at least: 2.8
Tested up to: 4.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily add social sharing icons on every wordpress image hover! 

== Description ==

<strong>WWM Social Share On Image Hover</strong> is WordPess Plugin that helps to add social meadia icons on your Post images . It will show social media icons on hover the post images on your site. This plugin will support 6 social media icons, check the features of <strong>WWM Social Share On Image Hover</strong> below.


<h4>Features:</h4>
* <strong>Alignment Option</strong> - Top Left, Top Right, Bottom Left and Bottom Right!
* <strong>Socia Medias</strong> - Supports facebook, twitter,linkedin, pinterest, google+ and pinterest.
* <strong>No Need of wrapping your images </strong> - No need of wrapping your images within a tag or class. It will do the plugin itself!
* <strong>Browser Support</strong> - All modern browsers, with IE8.
* <strong>Admin Options</strong> - Simply Enable or disable social share from admin panel.
* <strong>Width restriction</strong> - can avoid showing share icons on small images(width should specified).
* <strong>Exact Image Sharing</strong> - This plugin allows you to share exactly same image on Pinterest,Facebook,Tumblr, and Linked In.


For more details about <a href="http://www.walkswithme.net/social-share-icons-on-image-hover" target="_blank"><strong>WWM Social Share On Image Hover</strong></a> plugin integration.


== Installation ==

How to install <strong>WWM Social Share On Image Hover</strong> ?

Via FTP

1. Upload the full directory into your wp-content/plugins directory
2. Activate the plugin at the plugin administration page
3. Just set the Options from Settings Menu -> WWM Social Share

Via Admin Panel

1. Login to admin dash board.
2. Plugins -> Add New -> Upload -> Choose the Zip file of <strong>WWM Social Share On Image Hover</strong> and Install Now
3. Activate the Plugin.
4. Set the Options from Settings Menu -> WWM Social Share.

== Frequently Asked Questions ==

For more information,refer to <a href="http://www.walkswithme.net/social-share-icons-on-image-hover">WWM Social Share On Image Hover</a>

== Screenshots ==

1. screenshot-1
2. screenshot-2


== Changelog ==

= 1.0 =
* Initial Plugin release

== Upgrade Notice ==

= 1.0 =
* Initial Plugin release