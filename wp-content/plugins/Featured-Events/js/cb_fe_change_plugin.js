/************************************************************************************
*
*   Jquery file for Featured events dropdown
*
*	
*
*
*
*
************************************************************************************/


jQuery(document).ready(function($) {
		
		jQuery("#ALL").css("color","#000000");
		jQuery("#ALL").css("background-color","#f1f1f1");
		jQuery("#ALL").addClass("selected");

			jQuery('.fe_dropdown').hover(function(){
			var id = jQuery(this).attr('id');
				 var y = "#"+id
			jQuery(y).css("color","#000000");
			jQuery(y).css("background-color","#f1f1f1");
		

		
		
		},
			 function(){
			 var id = jQuery(this).attr('id');
				 var y = "#"+id
				 
			if (jQuery(this).hasClass("selected"))
			{
			
				 
				jQuery(y).css("color","#000000");
				jQuery(y).css("background-color","#f1f1f1");
			
			}else{
				jQuery(y).css("background-color", "#f9f9f9");
			}
});
		
		
		
		
	
	   jQuery('.fe_dropdown').click(function (e) {
	   
		
		var options=myAjaxfe.options;
		var ajax_url = myAjaxfe.ajax_url;
		
		
    var cal = options['Events_Calendar'];
	
		var mydata = {
                
                action: 'my_special_action1',
				val:  jQuery(this).val(),
				cal : options['Events_Calendar']
       
		
		
        };
		
    var value = jQuery(this).val();
	var id_val = "#"+value;
	
	
	jQuery('.fe_dropdown').each(function(){
		console.log('this id ' + this.id);
		var s="#"+this.id;
		if (this.id != value){
		
			jQuery(s).css("color","#00000");
			jQuery(s).css("background-color","#f9f9f9");
			jQuery(s).removeClass('selected');
		} else 
		{
			jQuery(s).css("color","#000000");
			jQuery(s).css("background-color","#f1f1f1");
			jQuery(s).addClass('selected');
		}
	});
	jQuery.ajax({                
			url: ajax_url,
			type: "POST",
			data: mydata,     
			cache: false,
       success:function(data) {
            // This outputs the result of the ajax request
            
			jQuery("#all_featured_text").replaceWith(data);
			
			
        }
     
	

		});		
	

	});
	
});

