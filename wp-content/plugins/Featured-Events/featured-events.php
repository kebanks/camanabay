<?php
/*
Plugin Name: Calendar Featured Events Plugin
Plugin URI: http://www.camanabay.com/
Description: Calendar Featured Events Plugin
Version: 1.1
Author: Ilene Johnson
Author URI: http://www.camanabay.com/
License: GPL2
*/
?>
<?php
/******************************************************************************
*
*	featured-events.php 
*
*	Main file for the Calendar Featured Events Plugin. 
*
*	Featured Events from the Calendar
*
*
*
*
*********************************************************************************/


require_once 'includes/display_featured_events.php';


class __CamanaBay_Cal_featured_events_plugin{
	private $options = array();
public function __construct()
    {
		
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		//add_action( 'init', array( $this, 'page_init' ) );
		add_action ('admin_init', array($this,'admin_page_setup'));
		
		
		$this->counter=0;
	}
	
	public function add_plugin_page()
	{
        // This page will be under "Settings"
        
		add_options_page( 'featured_events_util',
							'Calendar Featured Events',
							'manage_options',
							'featured_events_utility',
							array( $this, 'create_admin_page' ) );
		
	
				
	}	
	
	public function create_admin_page()
    {
	
		$this->options = get_option('featured_event_settings');
		
		
	
	?>
		
		<div class="wrap">

            <?php screen_icon(); ?>

            <h2><?php _e('Select a Menu','cb-featured-events-text-domain' );?> </h2>         

            <form id="cb_featured_events_admin" name="submit_button(); " method="post" action="options.php">
<?php
			settings_fields( 'manage_featured_events' ); 
			do_settings_sections( 'featured-events-settings-admin' );
?>
			
		</div>
	<?php	submit_button(); ?>
		</form>
	
	<?php
	
	}
	public function pf_ev_section_info()
	{
	
		error_log('pf_ev_section_info');
		
		add_settings_field(

            'menu_list', // ID

            __('Select a Menu:','cb-featured-events-text-domain' ) , // Title 

            array( $this, 'select_a_menu' ), // Callback

            'featured-events-settings-admin' , // Page
			'featured-events-settings-menu-string'  //section
           
        );
		
		
	}
	
	
	
	public function select_a_menu($arg)
	{

	global $wpdb;
	
		 
		 $all_calendars = $wpdb->get_results('SELECT * FROM ' . $wpdb->prefix . 'spidercalendar_calendar');
error_log('this options events cal ' . $this->options['Events_Calendar']);
?>
			<select  style="min-width:150px;  "  name="Events_Calendar" >
			 <option value="0">Select Calendar</option>
<?php				foreach($all_calendars as  $cal)
{   error_log('cal id is ' . $cal->id);

	
 ?>
         <option value="<?php echo $cal->id; ?>" selected=" <?php echo  $this->options['Events_Calendar']== $cal->id?'selected': ''; ?>"> <?php echo $cal->title ?></option>
          
			
  <?php
	}   
?>	
			</select>
<?php
	
	}
	public function sanitize_featured( $input )

    {

	 $new_input = $input;
	 
	 $new_input['Events_Calendar'] = $_POST['Events_Calendar'] ;
	 error_log('saved input is ' . $new_input['Events_Calendar']);
	 return $new_input;
	
	}
	
	public function admin_page_setup()
	{
		error_log('admin page setup');
		register_setting(

            'manage_featured_events', // Option group

            'featured_event_settings', // Option name

            array( $this, 'sanitize_featured' ) // sanitize_featured

        );
			
	
			
			add_settings_section(
	'featured-events-settings-menu-string',
	 __('<h2>Featured events Settings </h2>','cb-featured-events-text-domain' ) , // Title
			 array( $this, 'pf_ev_section_info' ), // Callback
				'featured-events-settings-admin'
);
			
			
        

      
	}
	public function page_init()
    { 

		add_action( 'wp_ajax_menu_item_func', array($this,'cb_menu_item_func') );
		
		
		
	
	}
	
}
 new __CamanaBay_Cal_featured_events_plugin();
?>