<?php
/********************************************************************************************************
*
*
*	display_featured_events.php
*
*	Displays the featured events
*
***************************************************************************************************/

class _featured_events_display{

	private $options;
	private $cat_id;
		
	public function __construct()

    {

		$this->options = get_option('featured_event_settings');
		$this->cat_id = "ALL";
		

		add_shortcode( 'show_CB_featured', array( $this, 'cb_show_featured_settings' ) );

		add_action( 'wp_enqueue_scripts', array( $this,'cb_show_fe_enqueue_styles' ));
		add_action('wp_ajax_my_special_action1', array($this,'events_action_callback'));
		add_action('wp_ajax_nopriv_my_special_action1', array($this,'events_action_callback'));
	
	}
	function events_action_callback() {
      	global $wpdb;
       
        $val = $_POST['val'];
        $cal = $_POST['cal'];
		$this->cat_id = $val;
		
		if ($val != "ALL")
			$y = $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "spidercalendar_event WHERE featured_event=%d AND published=%d and calendar=%d and category=%d and date_end > CURDATE() ORDER BY date ASC", 1, 1, $cal,$val);
		else 
		
				$y = $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "spidercalendar_event WHERE featured_event=%d AND published=%d and calendar=%d  and date_end > CURDATE() ORDER BY date ASC", 1, 1, $cal);
	
		
   $rows = $wpdb->get_results($y) ;
   
   error_log('rows are ' . print_r($rows, TRUE));
   $content = $this->get_the_content($rows);
 

	echo $content;

      wp_die();
  
  
	}
	function get_the_content($rows)
	{
	$content = '<div id="featured_text_outside"><div id="all_featured_text">';
	 
	 $date = new DateTime();
	$date->modify('+1 month');	
	  foreach ($rows as $row) 
	  {
	
	  $y = $date->format("Y-m-d");
	   
	  //event starts more than 30 days ahead
		
		
		if ($row->date > $y )
			{		
				continue;				
			}
	         

		
		$text_for_date = preg_replace("/<img[^>]+\>/i", "", $row->text_for_date);   
    $content .= '<div id="featured_display_outside"><div class="featured_display_left">';
			
							if ($row->repeat_method == "no_repeat")
						{

								$content .='<span class="featured_left_date">' . date("D", strtotime($row->date)) . '
		
						
								<hr class="my_hr">
										</span>
				
								<span class="featured_left_month_day">' .	date("d/m",strtotime($row->date)) . '					
								<span> ';

		 }else {
 
				$index_array = explode(',' , $row->week);
				
				$lastElement = end($index_array);
				$lastElementKey = key($index_array);
				$lastElementKey --;
			
				foreach($index_array as $i)
				{
				   if (!empty($i)){
	 
	

				      if ($i != $index_array[$lastElementKey])
			$content .= '<span class="featured_left_date">' .$i . "<br/>";
						else 
						$content .= '<span class="featured_left_date">' . $i ;

			$content .= '</span>';

					}
				}

			
			$content .='<span class="featured_left_date">
				<hr class="my_hr">
				</span>';
				
		 
		 
	 
		 
		 }
		 
		 

		 

		$content .='</div>
			
		

		<div class="featured_display_right">
			<div class="featured_event_title"> ';
				

                if (empty($row->event_link)){
					 $content .= $row->title; 
				}
				else{
				
				$content .= '<a href="' . $row->event_link . '"> ' . $row->title . '</a>';
				
				
				}
					
					
					
		$content .='</div>
			<div class="featured_event_text">'.
					$text_for_date . '				
			</div>
			<div class="featured_right_bottom">
				<span class="featured_time_bottom"> '.
					$row->time . ', 
				</span>
				<span class="featured_place_bottom"> '.
					 $row->location  .'
				</span> ';
				if (!empty($row->event_link)){ 
					$content .='<a class="featured_event_link" href=" ' .$row->event_link .'" >MORE
					</a> ';
			    }
				$content .= '</div>
			</div>

		</div>';
		

		
		}	
	   $content .= "</div></div>";
	   return $content;
	  }
	  
	function cb_show_featured_settings($atts, $content="" )

	{
	error_log('cb_show_featured_settings');
			global $wpdb;


	   $s = $wpdb->prepare( "SELECT * FROM " . $wpdb->prefix . "spidercalendar_event WHERE featured_event=%d AND published=%d and calendar=%d  and date_end > CURDATE() ORDER BY date ASC", 1, 1, $this->options['Events_Calendar']);
	 error_log('s is ' . $s);

	 $rows = $wpdb->get_results($s) ;

error_log('rows are ' . print_r($rows, TRUE));
	
	$string1=' <div class="dropdown ">
	<button class="dropbtn cb_featured_events_header featured_title">Featured Events</button>';
	
	

	$cats_in_db = $wpdb->get_results( "SELECT category, count(*) as count FROM " .$wpdb->prefix . "spidercalendar_event GROUP By category" ,ARRAY_A );

	$s = "SELECT category, count(*) as count FROM " .$wpdb->prefix . "spidercalendar_event WHERE featured_event=1 GROUP By category";

     $categories=$wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "spidercalendar_event_category WHERE published=1");

	$string1 .= '<div class="dropdown-content">
	<option id="ALL" class="fe_dropdown" value="ALL" >All </option>';
 
  
 
  foreach($categories as $category)
		{	
			$key = array_search($category->id, array_column($cats_in_db, 'category'));
		
			
			if (empty($key))
			   continue; 
			  
			   
			/*if ($category->id != $this->cat_id)
			{*/
	
			$string1 .='<option  id="' . $category->id . '" class="fe_dropdown" value="' . $category->id .  '"     > ' .$category->title . '</option>';

			
	
		/*	}else{
					$string1 .='<option id="' . $category->id . '"  class="fe_dropdown " value="' . $category->id .  '"  style="background-color:' . $category->color . ' !important; color:#FFFFFF"  > '.	$category->title . '</option>';

				
	
			}*/
		}
	$string1 .= '
    
  </div>
</div> <br/><br/>'; 
  
		$content = $this->get_the_content($rows);
	
		return ($string1.$content);
	}
	function cb_show_fe_enqueue_styles()
	{

		
	
		wp_register_style( 'fe-plugin', plugins_url( 'Featured-Events/css/fe.css' ) );
		wp_enqueue_style( 'fe-plugin' );
			
		 wp_register_script( 'cb_show_fe_script', plugins_url( '/Featured-Events/js/cb_fe_change_plugin.js' ), array('jquery') );
		wp_enqueue_script('cb_show_fe_script');
		$params = array(
  'options' => $this->options,
 'ajax_url' => admin_url( 'admin-ajax.php' )
  

);
		// in javascript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
			wp_localize_script( 'cb_show_fe_script', 'myAjaxfe',$params);
				
			
	
	}
	
	
	
	public static function instance() {

		 new _featured_events_display;

			

	}
}

	
	
	function cb_init_show_featured_events(){

     error_log('cb_init_show_featured_events');
		return _featured_events_display::instance();

	}

add_action( 'plugins_loaded',  'cb_init_show_featured_events'  );

?>